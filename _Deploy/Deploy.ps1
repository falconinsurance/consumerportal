#CD ..
param($Branch,$BuildNumber)
$Sln ='ConsumerApp.sln'
function SetNugets {
	$Nugets = ''
	$obj = Get-Content -Path '_Deploy/nuspec.json' |  ConvertFrom-Json
	if ($obj.PackageAll -EQ 1) {
		$Nugets = $obj.All.Path
	}
	else {
		$NugetsArray = [array] $obj.Packages | Where-Object Enable -EQ 1 | Select-Object Name, Path
		if ( $NugetsArray.Count -EQ 0 ) {
			$Nugets = $obj.Default.Path
		}
		elseif ( $NugetsArray -isnot [system.array] ) {
	  $Nugets = $NugetsArray.Path
		}
		else {
	  foreach ($Item in $NugetsArray ) {
				$Nugets += $Item.Path + "|n"
	  }
		}
	}
	Write-Host "Nugets:"
	Write-Host $Nugets
	Write-Host "##teamcity[setParameter name='NuspecFiles' value='$Nugets']"
}


function SetNugetVersion {
	# Set Variables
	# This gets the name of the current Git branch.

	Write-Host "Branch  $Branch   BuildNumber $BuildNumber"
	# Sometimes the branch will be a full path, e.g., 'refs/heads/master'.
	# If so we'll base our logic just on the last part.
	if ($Branch.Contains("/")) {
		$Branch = $Branch.substring($Branch.lastIndexOf("/")).trim("/")
	}
	$NugetVersion = "$BuildNumber"
	if ($Branch -ne "master") {
		$NugetVersion = "$BuildNumber-$Branch"
	}
	Write-Host "NugetVersion:"
	Write-Host $NugetVersion
	Write-Host "##teamcity[setParameter name='NugetVersion' value='$NugetVersion']"
	return $NugetVersion
}

# Restore and Build
dotnet --version
$Version = SetNugetVersion
dotnet build $Sln -p:Version=$Version -Restore -c Release -clp:ErrorsOnly
SetNugets
