using Diamond.Common.Objects.Policy;
using Diamond.Common.Services.Messages.CreditCardService.SaveCreditCardToken;
using DiamondServices.Service;
using CreditCardService = Diamond.Common.Services.Messages.CreditCardService;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface ICreditCardApi
{
  #region Public Methods

  CreditCardService.DeletePaymentProfile.Response DeletePaymentProfile(ISession session,int policyId,int clientId,string paymentProfileId,bool isDefaultPaymentProfile);

  Task Initialize();

  Response SaveCCToken(ISession session,Image image,int clientId,DCO.Billing.CreditCard creditCard);

  CreditCardService.SaveCustomerAndPaymentProfiles.Response SaveCustomerAndPaymentProfiles(ISession session,Image image,int clientId,string defaultPaymentProfileId);

  #endregion Public Methods
}
