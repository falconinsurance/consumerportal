namespace DiamondServices.Apis;

public class UtilityApi:BaseApi, IUtilityApi
{
  #region Private Fields
  private readonly IRelax<DateTime> _SystemDateRelaxed;
  #endregion Private Fields

  #region Public Constructors

  public UtilityApi(IDiamondApi diamondApi) : base(diamondApi)
  {
    _SystemDateRelaxed = RelaxFactory.Create(GetSystemDate,() => TimeSpan.FromMinutes(2));
  }

  #endregion Public Constructors

  #region Public Properties
  public DateTime SystemDate => _SystemDateRelaxed.Value;
  #endregion Public Properties

  #region Public Methods

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods

  #region Private Methods

  private DateTime GetSystemDate() => FalconHandler.SqlFirstOrDefault("SELECT TOP(1) sysdate FROM Diamond..SystemDate(NOLOCK)",null,c => c.To<DateTime>("sysdate"));

  #endregion Private Methods
}
