using DiamondServices.Service;
using Falcon.DependencyInjection;
using Falcon.FalconDB;

namespace DiamondServices.Apis;

internal class DiamondApiRelaxed
{
  #region Public Constructors

  public DiamondApiRelaxed(IDiamondApi consumerApi)
  {
    var logHandler = AppResolver.Get<ILogHandler>();
    Administration = Create<IAdministrationApi>(new AdministrationApi(consumerApi),nameof(AdministrationApi));
    Attachment = Create<IAttachmentApi>(new AttachmentApi(consumerApi),nameof(AttachmentApi));
    Billing = Create<IBillingApi>(new BillingApi(consumerApi),nameof(BillingApi));
    Workflow = Create<IWorkflowApi>(new WorkflowApi(consumerApi),nameof(WorkflowApi));
    CreditCard = Create<ICreditCardApi>(new CreditCardApi(consumerApi),nameof(CreditCardApi));
    Security = Create<ISecurityApi>(new SecurityApi(consumerApi),nameof(SecurityApi));
    DbFalcon = RelaxFactory.Create(AppResolver.Get<IFalconHandler>);
    DiamondService = RelaxFactory.Create(AppResolver.Get<IDiamondService>);
    Login = Create<ILoginApi>(new LoginApi(consumerApi),nameof(LoginApi));
    Policy = Create<IPolicyApi>(new PolicyApi(consumerApi),nameof(PolicyApi));
    //Resolver = RelaxFactory.Create(AppResolver.Get<IResolver>);
    Static = Create<IStatic>(new Static(consumerApi),nameof(Static));
    Vehicle = Create<IVehicleApi>(new VehicleApi(consumerApi),nameof(VehicleApi));
    Utility = Create<IUtilityApi>(new UtilityApi(consumerApi),nameof(UtilityApi));
    Notes = Create<INotesApi>(new NotesApi(consumerApi),nameof(NotesApi));
    PolicyForm = Create<IPolicyFormApi>(new PolicyFormApi(consumerApi),nameof(PolicyFormApi));
    Printing = Create<IPrintingApi>(new PrintingApi(consumerApi),nameof(PrintingApi));

    IRelax<T> Create<T>(T concreat,string vlsName) => RelaxFactory.Create(() => AopLog.Create<T>(concreat,logHandler,vlsName));
  }

  #endregion Public Constructors

  #region Internal Properties
  internal IRelax<IAdministrationApi> Administration { get; }
  internal IRelax<IAttachmentApi> Attachment { get; }

  internal IRelax<IBillingApi> Billing { get; }

  internal IRelax<ICreditCardApi> CreditCard { get; }

  internal IRelax<IFalconHandler> DbFalcon { get; }

  internal IRelax<IDiamondService> DiamondService { get; }

  internal IRelax<ILoginApi> Login { get; }

  internal IRelax<INotesApi> Notes { get; }

  internal IRelax<IPolicyApi> Policy { get; }

  internal IRelax<IPolicyFormApi> PolicyForm { get; }

  internal IRelax<IPrintingApi> Printing { get; }

  internal IRelax<ISecurityApi> Security { get; }

  internal IRelax<IStatic> Static { get; }

  internal IRelax<IUtilityApi> Utility { get; }

  internal IRelax<IVehicleApi> Vehicle { get; }

  internal IRelax<IWorkflowApi> Workflow { get; }
  #endregion Internal Properties
}
