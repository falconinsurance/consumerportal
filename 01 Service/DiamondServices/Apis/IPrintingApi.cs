using DiamondServices.Service;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface IPrintingApi:IBaseApi
{
  #region Public Methods

  byte[] GetDocument(ISession session,int policyId,int imageNumber,DCO.Printing.PrintForm printForm);

  ICollection<DCO.Printing.PrintForm> LoadPrintForms(ISession session,int policyId,int imageNumber);

  #endregion Public Methods
}
