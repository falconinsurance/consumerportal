using DiamondServices.Service;

namespace DiamondServices.Apis;

public interface IBaseApi
{
  #region Public Properties
  IDiamondApi DiamondApi { get; }
  IDiamondService DiamondService { get; }
  #endregion Public Properties

  #region Public Methods

  Task Initialize();

  #endregion Public Methods
}
