using DiamondServices.Service;
using AttachmentService = Diamond.Common.Services.Messages.AttachmentService;
using DCE = Diamond.Common.Enums;

namespace DiamondServices.Apis;

public class AttachmentApi:BaseApi, IAttachmentApi
{
  #region Public Constructors

  public AttachmentApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public int AttachFileAndGetId(ISession session,byte[] document,int policyId,int imageNum,string fileName,string desc)
  {
    var request = new AttachmentService.AttachFile.Request()
    {
      RequestData = new()
      {
        AttachData = document,
        FileName = fileName,
        Keys = new()
        {
          AddedDate = DateTime.Now,
          PolicyId = policyId,
          PolicyImageNum = imageNum,
          AttachmentLevel = (int)DCE.Attachments.AttachmentType.Policy_Attachment,
        },
        Dscr = desc
      }
    }.ApplySession(session);

    var response = DiamondService.Attachment.AttachFile(request);
    return response.ResponseData?.Attachment?.AttachmentId ?? 0;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods
}
