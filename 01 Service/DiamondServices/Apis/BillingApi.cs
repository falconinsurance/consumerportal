using DiamondServices.Service;
using BillingService = Diamond.Common.Services.Messages.BillingService;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public class BillingApi:BaseApi, IBillingApi
{
  #region Private Fields

  private static readonly ClassCache _Cache = new();

  #endregion Private Fields

  #region Public Constructors

  public BillingApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public DCO.InsCollection<DCO.Billing.PayPlanPreview> LoadAllPayPlans(ISession session,DCO.Policy.Image image)
  {
    var request = new BillingService.CreateMultipleShortPreviewInvoices.Request()
    {
      RequestData = new()
      {
        PolicyImage = image,
        //IsFromPayplanPreview = true,
      }
    }.ApplySession(session);
    var response = DiamondService.Billing.CreateMultipleShortPreviewInvoices(request);
    return response.ResponseData.PayPlanPreviews;
  }

  public DCO.Billing.Data LoadBillingData(ISession session,int policyId)
  {
    var request = new BillingService.Load.Request()
    {
      RequestData = new()
      {
        PolicyId = policyId,
        LoadAccountDetail = true
      }
    }.ApplySession(session);
    var response = DiamondService.Billing.Load(request);
    return response.ResponseData.BillingData;
  }

  public DCO.Billing.Data LoadBillingDataPreview(ISession session,int policyId,int policyImageNum,int currentPayplanId)
  {
    var request = new BillingService.LoadPreview.Request()
    {
      RequestData = new()
      {
        PolicyId = policyId,
        PolicyImageNum = policyImageNum,
        BillingPayPlanId = currentPayplanId,
        BillingTransactionTypeId = (int)DCE.Billing.BillingTransactionType.None,
        PayPlanChangeTransaction = false
      }
    }.ApplySession(session);
    var response = DiamondService.Billing.LoadPreview(request);
    return response.ResponseData.BillingData;
  }

  public DCO.Billing.PayPlanDetailData LoadPayPlan(int payplanId)
  {
    return _Cache.SetGet(Request,new(payplanId));

    DCO.Billing.PayPlanDetailData Request()
    {
      var request = new BillingService.GetSingleBillingPayPlan.Request()
      {
        RequestData = new()
        {
          BillingPayPlanId = payplanId
        }
      };
      var response = DiamondService.Billing.GetSingleBillingPayPlan(request);
      return response.ResponseData.PayPlanDetailData;
    }
  }

  public BillingService.SavePolicyCreditCardInfo.Response SaveCreditCard(ISession session,DCO.Billing.CreditCard creditCard)
  {
    var ccResponse = new BillingService.SavePolicyCreditCardInfo.Response
    {
      ResponseData = new()
      {
        Success = true
      }
    };
    try
    {
      if(creditCard.CreditCardDataId > 0 || creditCard.CreditCardType > 0)
      {
        ccResponse = DiamondService.Billing.SavePolicyCreditCardInfo(new BillingService.SavePolicyCreditCardInfo.Request
        {
          RequestData = new()
          {
            //PolicyId = data.PolicyId,
            CreditCard = creditCard
            //RequireCreditCard = data.RequireCreditCard,
          }
        }.ApplySession(session));
      }
    }
    catch(Exception ex)
    {
      throw new DiamondException($"Failed to save Credit Card information. PolicyId={creditCard.PolicyId}",ex);
    }
    return ccResponse;
  }

  public BillingService.SavePolicyCreditCardInfo.Response SaveCreditCardInfo(ISession session,int policyId,DCO.Billing.CreditCard creditCard)
  {
    var ccResponse = DiamondService.Billing.SavePolicyCreditCardInfo(new BillingService.SavePolicyCreditCardInfo.Request
    {
      RequestData = new()
      {
        PolicyId = policyId,
        CreditCard = creditCard,
        RequireCreditCard = true,
      }
    }.ApplySession(session));
    return ccResponse;
  }

  public BillingService.SavePolicyEftInfo.Response SaveEft(ISession session,DCO.EFT.Eft eft)
  {
    var eftResponse = new BillingService.SavePolicyEftInfo.Response
    {
      ResponseData = new()
      {
        Success = true
      }
    };
    try
    {
      if(eft.EftAccountId > 0 || eft.BankAccountTypeId > 0)
      {
        eftResponse = DiamondService.Billing.SavePolicyEftInfo(new BillingService.SavePolicyEftInfo.Request
        {
          RequestData = new()
          {
            Eft = eft
          }
        }.ApplySession(session));
      }
    }
    catch(Exception ex)
    {
      throw new DiamondException($"Failed to save EFT information. PolicyId={eft.PolicyId}",ex);
    }
    return eftResponse;
  }

  #endregion Public Methods
}
