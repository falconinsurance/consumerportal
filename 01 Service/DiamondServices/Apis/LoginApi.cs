using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCS = Diamond.Common.Services;

namespace DiamondServices.Apis;

public class LoginApi:BaseApi, ILoginApi
{
  #region Private Fields
  private readonly IRelax<BaseSession> _Session_ConsumerServiceRelaxed;
  #endregion Private Fields

  #region Public Constructors

  public LoginApi(IDiamondApi diamondApi) : base(diamondApi)
  {
    _Session_ConsumerServiceRelaxed = GetUserSession("ConsumerService");
  }

  #endregion Public Constructors

  #region Public Properties
  public ISession Session_ConsumerService => _Session_ConsumerServiceRelaxed.Value;
  #endregion Public Properties

  #region Public Methods

  public T ForDomainUsername<T>(string loginName,string loginDomain = "CORP") where T : BaseSession, new()
  {
    if(string.IsNullOrEmpty(loginName))
    {
      throw new ArgumentException($"'{nameof(loginName)}' cannot be null or empty.",nameof(loginName));
    }
    DCS.Messages.LoginService.GetDiamTokenForDomainUsername.Request req = new()
    {
      RequestData = new()
      {
        BusinessInterfaceSourceId = DCE.Security.BusinessInterfaceSourceType.DiamondAgencyPortal,
        LoginName = loginName.Trim(),
        LoginDomain = loginDomain
      }
    };
    var response = DiamondService.Login.GetDiamTokenForDomainUsername(req);
    var token = response.ResponseData.diamondSecurityToken;
    return new T().With(c => c.SetSession(token));
  }

  public T ForUsernamePassword<T>(string loginName,string password) where T : BaseSession, new()
  {
    if(string.IsNullOrEmpty(loginName))
    {
      throw new ArgumentException($"'{nameof(loginName)}' cannot be null or empty.",nameof(loginName));
    }
    if(string.IsNullOrEmpty(password))
    {
      throw new ArgumentException($"'{nameof(password)}' cannot be null or empty.",nameof(password));
    }
    var response = DiamondService.Login.GetDiamTokenForUsernamePassword(new DCS.Messages.LoginService.GetDiamTokenForUsernamePassword.Request
    {
      RequestData =
        {
            LoginName = loginName.Trim(),
            Password = password.Trim()
          }
    });
    return new T().With(c => c.SetSession(response.ResponseData.DiamondSecurityToken,response.ResponseData.DiamondTrustedSecurityToken));
  }

  public T ForUsersId<T>(int userId) where T : BaseSession, new()
  {
    DCS.Messages.LoginService.GetDiamTokenForUsersId.Request req = new() { RequestData = new() { UsersId = userId } };

    var response = DiamondService.Login.GetDiamTokenForUsersId(req);
    var token = response.ResponseData.DiamondSecurityToken;
    return new T().With(c => c.SetSession(token));
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods

  #region Private Methods

  private IRelax<BaseSession> GetUserSession(string loginName,string loginDomain = "CORP") => RelaxFactory.Create(() => ForDomainUsername<BaseSession>(loginName,loginDomain),() => TimeSpan.FromHours(12));

  #endregion Private Methods
}
