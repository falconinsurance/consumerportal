using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;
using PolicyService = Diamond.Common.Services.Messages.PolicyService;

namespace DiamondServices.Apis;

public class PolicyApi:BaseApi, IPolicyApi
{
  #region Public Constructors

  public PolicyApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public DCO.Policy.Image Demote(ISession session,int policyId,int imageNum)
  {
    var request = new PolicyService.DemotePendingToQuote.Request()
    {
      RequestData = {
        PolicyId  = policyId,
        PolicyImageNum = imageNum,
         IgnoreAuthority = true,
         LoadDemotedImage = true,
      },
    }.ApplySession(session);

    var response = DiamondService.Policy.DemotePendingToQuote(request);
    return response.ResponseData.PolicyImage;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public DCO.Policy.Image LoadImage(ISession session,int policyId,int imageNum)
  {
    var request = new PolicyService.LoadImage.Request()
    {
      RequestData = {
        ImageNumber = imageNum,
        PolicyId  = policyId,
      },
    }.ApplySession(session);

    var response = DiamondService.Policy.LoadImage(request);
    return response.ResponseData.Image;
  }

  public DCO.Policy.Image Promote(ISession session,int policyId,int imageNum)
  {
    var request = new PolicyService.PromoteQuoteToPending.Request()
    {
      RequestData = {
        PolicyId  = policyId,
        PolicyImageNum = imageNum
      },
    }.ApplySession(session);
    var response = DiamondService.Policy.PromoteQuoteToPending(request);
    return response.ResponseData.PolicyImage;
  }

  public DCO.Policy.Image Rate(ISession session,DCO.Policy.Image policyImage)
  {
    var request = new PolicyService.Rate.Request()
    {
      RequestData = new()
      {
        PolicyImage = policyImage.With(c => c.TransactionUsersId = session.DiamondSecurityToken.DiamUserId),
      }
    }.ApplySession(session);

    var response = DiamondService.Policy.Rate(request);
    return response.ResponseData.PolicyImage;
  }

  public (DCO.Policy.Image Image, IList<DCO.ValidationItem> ValidationItems) SaveRate(ISession session,DCO.Policy.Image policyImage)
  {
    var request = new PolicyService.SaveRate.Request()
    {
      RequestData = new()
      {
        Image = policyImage,
      }
    }.ApplySession(session);

    var response = DiamondService.Policy.SaveRate(request);

    return (response.ResponseData.SaveSuccessful ? response.ResponseData.Image : null, response.DiamondValidation.ValidationItems);
  }

  public PolicyService.SaveRateIssue.Response SaveRateIssue(ISession session,DCO.Policy.Image image,DCO.Billing.ApplyCash applyCash)
  {
    var request = new PolicyService.SaveRateIssue.Request()
    {
      RequestData = new()
      {
        Image = image,
        ReturnIssuedImage = true,
        TransactionIsOutofSequence = image.PolicyStatusCodeId == (int)DCE.PolicyStatusCode.Pending_Oos,
        PaymentInformation = applyCash,
      }
    }.ApplySession(session);
    var result = DiamondService.Policy.SaveRateIssue(request);
    return result;
  }

  public (DCO.Policy.Image Image, IList<DCO.ValidationItem> ValidationItems) Submit(ISession session,DCO.Policy.Image policyImage,En_State state) => policyImage.PolicyId == 0 ? SubmitApplication(session,policyImage,state) : SaveRate(session,policyImage);

  public (DCO.Policy.Image Image, IList<DCO.ValidationItem> ValidationItems) SubmitApplication(ISession session,DCO.Policy.Image policyImage,En_State state)
  {
    var request = new PolicyService.SubmitApplication.Request()
    {
      RequestData = new()
      {
        Rate = true,
        ReturnImage = true,
        PolicyImage = policyImage.With(c => c.TransactionUsersId = session.DiamondSecurityToken.DiamUserId),
        SubmitVersion = new()
        {
          StateId = (int)state,
          LOBId = 1,
          CompanyId = 1,
          TransEffDate = policyImage.TransactionEffectiveDate,
          TransTypeId = (DCE.TransType)policyImage.TransactionTypeId,
          GuaranteedRatePeriodEffDate = policyImage.TransactionEffectiveDate
        },
        IsQuote = true,
      }
    }.ApplySession(session);

    var response = DiamondService.Policy.SubmitApplication(request);
    return (response.ResponseData.Success ? response.ResponseData.Image : null, response.DiamondValidation.ValidationItems);
  }

  #endregion Public Methods

  //private void AnswerOldUWQuestions(int policyId,int ratingVersionId,int versionId)
  //{
  //  var stateId = DiamondApi.Static.GetState(versionId);
  //  DiamondApi.DbFalcon.Sql("EXEC [dbo].[spAnswerOldUWQuestions] @PolicyId=@1, @RateVersionId=@2, @StateId=@3",c => c.AddValues(policyId,ratingVersionId,stateId));
  //}
}
