using DiamondServices.Service;
using Falcon.Tools.DBHandler;
using DCE = Diamond.Common.Enums;
using SecurityService = Diamond.Common.Services.Messages.SecurityService;

namespace DiamondServices.Apis;

public class SecurityApi:BaseApi, ISecurityApi
{
  #region Public Constructors

  public SecurityApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public string DecrypAccountNumber(ISession session,string encryptedValue,int agencyId)
  {
    var request = new SecurityService.DecryptBankAccountNumber.Request
    {
      RequestData = new()
      {
        EncryptionId = agencyId,
        EncryptionKeyType = DCE.Security.EncryptionKeyType.Agency,
        EncryptedText = encryptedValue,
      }
    }.ApplySession(session);
    var response = DiamondService.Security.DecryptBankAccountNumber(request);

    return MaskAccountNumber(response.ResponseData.PlainText);
  }

  /// <summary> in the data each Record should contain EncryptedText =
  /// item.To<string>("EncryptedText"), EncryptionId = item.To<int>("AgencyId"), and the result each
  /// Record should have AgencyId =item.To<int>("AgencyId") PlainText =item.To<string>("PlainText")
  /// HasError =item.To<bool>("HasError") </summary> <param name="session"></param> <param
  /// name="data"></param> <returns></returns>
  public RecordSet DecrypAccountNumbers(ISession session,RecordSet data)
  {
    try
    {
      var req = new SecurityService.DecryptMultipleItems.Request
      {
        RequestData = { }
      }.ApplySession(session);
      var items = req.RequestData.Items;

      foreach(var item in data)
      {
        items.Add(new()
        {
          EncryptedText = item.To<string>("EncryptedText"),
          EncryptionId = item.To<int>("AgencyId"),
          EncryptionKeyType = DCE.Security.EncryptionKeyType.Agency,
          DataType = DCE.Security.DataElementType.BankAccountNumber,
          CheckAuthority = false,
        });
      }
      var ret = DiamondService.Security.DecryptMultipleItems(req);

      var result = new RecordSet();
      foreach(var item in ret.ResponseData.Items)
      {
        result.Add(new Record()
          .With(c =>
          {
            c["AgencyId"] = item.EncryptionId;
            c["PlainText"] = item.PlainText;
            c["HasError"] = item.EncryptedText.Length == item.PlainText.Length;
          }));
      }

      return result;
    }
    catch
    {
      throw;
    }
  }

  public string DecryptCreditCard(ISession session,string encryptedValue,int policyId)
  {
    var request = new SecurityService.DecryptCreditCardNumber.Request
    {
      RequestData = new()
      {
        EncryptionId = policyId,
        EncryptionKeyType = DCE.Security.EncryptionKeyType.Policy,
        EncryptedText = encryptedValue
      }
    }.ApplySession(session);
    var response = DiamondService.Security.DecryptCreditCardNumber(request);

    return MaskAccountNumber(response.ResponseData.PlainText);
  }

  public string DecryptEft(ISession session,string encryptedValue,int policyId)
  {
    var request = new SecurityService.DecryptEftAccountNumber.Request
    {
      RequestData = new()
      {
        EncryptionId = policyId,
        EncryptionKeyType = DCE.Security.EncryptionKeyType.Policy,
        EncryptedText = encryptedValue
      }
    }.ApplySession(session);
    var response = DiamondService.Security.DecryptEftAccountNumber(request);

    return MaskAccountNumber(response.ResponseData.PlainText);
  }

  public string DecryptLicenseNumber(ISession session,string encryptedValue,int policyId)
  {
    var request = new SecurityService.DecryptLicenseNumber.Request
    {
      RequestData = new()
      {
        EncryptionId = policyId,
        EncryptionKeyType = DCE.Security.EncryptionKeyType.Policy,
        EncryptedText = encryptedValue
      }
    }.ApplySession(session);
    var response = DiamondService.Security.DecryptLicenseNumber(request);

    return response.ResponseData.PlainText;
  }

  public string Encrypt(ISession session,string plainTextValue,int policyId)
  {
    var request = new SecurityService.Encrypt.Request
    {
      RequestData = new()
      {
        EncryptionId = policyId,
        EncryptionKeyType = DCE.Security.EncryptionKeyType.Policy,
        PlainText = plainTextValue
      }
    }.ApplySession(session);
    var response = DiamondService.Security.Encrypt(request);

    return response.ResponseData.EncryptedText;
  }

  public int GetRegistrationClientId(string policyNumber,string zipCode)
  {
    var request = new SecurityService.GetUserRegistrationClientId.Request
    {
      RequestData = new()
      {
        CompanyId = 0,
        ZipCode = zipCode,
        PolicyNumber = policyNumber
      }
    };
    var result = DiamondService.Security.GetUserRegistrationClientId(request);
    return result?.ResponseData?.ClientId ?? 0;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public string MaskAccountNumber(string source)
  {
    if(source.IsNullOrWhiteSpace())
      throw new ArgumentNullException(nameof(source));
    if(source.Length > 4)
      return new string('X',source.Length - 4) + source.Substring(source.Length - 4);
    else if(source.Length > 1)
      return "X" + source.Substring(1);
    else
      return source;
  }

  #endregion Public Methods
}
