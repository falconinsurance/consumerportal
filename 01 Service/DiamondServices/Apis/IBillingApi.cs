using DiamondServices.Service;
using BillingService = Diamond.Common.Services.Messages.BillingService;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface IBillingApi:IBaseApi
{
  #region Public Methods

  DCO.InsCollection<DCO.Billing.PayPlanPreview> LoadAllPayPlans(ISession session,DCO.Policy.Image image);

  DCO.Billing.Data LoadBillingData(ISession session,int policyId);

  DCO.Billing.Data LoadBillingDataPreview(ISession session,int policyId,int policyImageNum,int currentPayplanId);

  DCO.Billing.PayPlanDetailData LoadPayPlan(int payplanId);

  BillingService.SavePolicyCreditCardInfo.Response SaveCreditCard(ISession session,DCO.Billing.CreditCard creditCard);

  BillingService.SavePolicyCreditCardInfo.Response SaveCreditCardInfo(ISession session,int policyId,DCO.Billing.CreditCard creditCard);

  BillingService.SavePolicyEftInfo.Response SaveEft(ISession session,DCO.EFT.Eft eft);

  #endregion Public Methods
}
