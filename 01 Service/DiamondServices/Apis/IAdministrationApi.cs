using Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface IAdministrationApi:IBaseApi
{
  #region Public Methods

  IList<ValidationItem> SaveUser(string emailAddress,string password,int clientId);

  #endregion Public Methods
}
