using Falcon.DependencyInjection;
using Falcon.Tools.VehicleInfoCache;

namespace DiamondServices.Apis;

internal class VehicleApi:BaseApi, IVehicleApi
{
  #region Private Fields

  private static IRelax<IVehicleInfoCache> _VehicleInfoCacheRelaxed;

  #endregion Private Fields

  #region Public Constructors

  public VehicleApi(IDiamondApi diamondApi) : base(diamondApi)
  {
    _VehicleInfoCacheRelaxed ??= RelaxFactory.Create(AppResolver.Get<IVehicleInfoCache>);
  }

  #endregion Public Constructors

  #region Private Properties

  private static IVehicleInfoCache VehicleInfoCache => _VehicleInfoCacheRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  public IdDescEntity[] GetBodyTypes(int year,IdDescEntity make,IdDescEntity model) => VehicleInfoCache.GetBodyTypesForYearAndMakeAndModel(year,make,model);

  public IdDescEntity[] GetMakes(int year) => VehicleInfoCache.GetMakesForYear(year);

  public IdDescEntity[] GetModels(int year,IdDescEntity make) => VehicleInfoCache.GetModelsForYearAndMake(year,make);

  public IdDescEntity[] GetSymbols(int year,IdDescEntity make,IdDescEntity model,IdDescEntity bodyType) => VehicleInfoCache.GetSymbolsForYearAndMakeAndModelAndBodyType(year,make,model,bodyType);

  public VehicleVinMatch[] GetVehicles(string vin) => VehicleInfoCache.GetVehiclesForVin(vin).ToArray();

  public IdDescEntity GetBodyType(int id) => VehicleInfoCache.FindBodyType(id);

  public int[] GetYears() => VehicleInfoCache.GetYears();

  public override Task Initialize()
  {
    var tasks = new List<Task>() {
      Task.Run(()=> _VehicleInfoCacheRelaxed.Value),
    };
    return Task.WhenAll(tasks);
  }

  public bool TryGetBodyTypeForVin(string vin,out int bodyType) => VehicleInfoCache.TryGetBodyTypeForVin(vin,out bodyType);

  public bool TryGetSymbolsForSymbol(string symbol,out (string Comprehensive, string Collision, string Liability) symbols) => VehicleInfoCache.TryGetSymbolsForSymbol(symbol,out symbols);

  public bool TryGetSymbolsForVin(string vin,out (string Comprehensive, string Collision, string Liability) symbols) => VehicleInfoCache.TryGetSymbolsForVin(vin,out symbols);

  #endregion Public Methods
}
