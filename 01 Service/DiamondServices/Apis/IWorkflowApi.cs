using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface IWorkflowApi:IBaseApi
{
  #region Public Methods

  bool Create(ISession session,DCO.Policy.Image image,string remarks,int dueDateDays,DCE.Workflow.WorkflowType workflowTypeId,EnWorkflowQueue workflowQueue = EnWorkflowQueue.RequestedInformation);

  bool Create(ISession session,int policyId,int imageNum,string remarks,DCO.InsDateTime dueDate,DCE.Workflow.WorkflowType workflowTypeId,EnWorkflowQueue workflowQueue);
  bool Delete(ISession session,int deleteWorkflowId);

  #endregion Public Methods
}
