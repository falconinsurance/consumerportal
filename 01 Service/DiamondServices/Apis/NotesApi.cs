using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;
using NotesService = Diamond.Common.Services.Messages.NotesService;

namespace DiamondServices.Apis;

public class NotesApi:BaseApi, INotesApi
{
  #region Public Constructors

  public NotesApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public bool Create(ISession session,int policyId,string note,string title,int notesTypeId)
  {
    var request = new NotesService.CreateNote.Request()
    {
      RequestData = new()
      {
        NoteStruct = new()
        {
          Key01 = policyId,
          Note = new()
          {
            PCAddedDate = DateTime.Now,
            AttachLevelId = (int)DCE.Notes.Level.Policy,
            Note = note,
            Title = title,
            NotesTypeIds = notesTypeId > 0 ? new() { new DCO.Notes.NotesTypeLink { NotesTypeId = notesTypeId } } : default
          }
        }
      }
    }.ApplySession(session);
    var response = DiamondService.Notes.CreateNote(request);
    return response?.ResponseData?.Success == true;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods
}
