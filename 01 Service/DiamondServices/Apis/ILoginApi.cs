using DiamondServices.Service;

namespace DiamondServices.Apis;

public interface ILoginApi:IBaseApi
{
  #region Public Properties
  ISession Session_ConsumerService { get; }
  #endregion Public Properties

  #region Public Methods

  T ForDomainUsername<T>(string loginName,string loginDomain = "CORP") where T : BaseSession, new();

  T ForUsernamePassword<T>(string loginName,string password) where T : BaseSession, new();

  T ForUsersId<T>(int userId) where T : BaseSession, new();

  #endregion Public Methods
}
