using Diamond.Common.Objects.Policy;
using DiamondServices.Service;
using CreditCardService = Diamond.Common.Services.Messages.CreditCardService;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public class CreditCardApi:BaseApi, ICreditCardApi
{
  #region Public Constructors

  public CreditCardApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public CreditCardService.DeletePaymentProfile.Response DeletePaymentProfile(ISession session,int policyId,int clientId,string paymentProfileId,bool isDefaultPaymentProfile)
  {
    var req = new CreditCardService.DeletePaymentProfile.Request
    {
      RequestData =
          {
            ClientId = clientId,
            CustomerProfileId = clientId.ToString(),
            PolicyId = policyId,
            PaymentProfileId = paymentProfileId,
            IsDefaultPaymentProfile = isDefaultPaymentProfile,
          }
    }.ApplySession(session);

    var ret = DiamondService.CreditCard.DeletePaymentProfile(req);
    return ret;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public CreditCardService.SaveCreditCardToken.Response SaveCCToken(ISession session,Image image,int clientId,DCO.Billing.CreditCard creditCard)
  {
    var req = new CreditCardService.SaveCreditCardToken.Request
    {
      RequestData =
          {
            PolicyId=image?.PolicyId ?? 0,
            PolicyImageNum=image?.PolicyImageNum?? 0,
            ClientId = clientId,
            CustomerProfileId = clientId.ToString(),
            NewPaymentProfileId = creditCard.PaymentProfileId,
            CardNumber = creditCard.AccountNumber,
            ExpirationMonth = Convert.ToInt32(creditCard.ExpirationDate?.Substring(0,2) ?? "0"),
            ExpirationYear = Convert.ToInt32(creditCard.ExpirationDate?.Substring(3) ?? "0"),
            CreditCardTypeId = creditCard.CreditCardType,
          }
    }.ApplySession(session);
    var ret = DiamondService.CreditCard.SaveCreditCardToken(req);
    return ret;
  }

  public CreditCardService.SaveCustomerAndPaymentProfiles.Response SaveCustomerAndPaymentProfiles(ISession session,Image image,int clientId,string defaultPaymentProfileId)
  {
    var req = new CreditCardService.SaveCustomerAndPaymentProfiles.Request
    {
      RequestData =
          {
            ClientId = clientId,
            CustomerProfileId = clientId.ToString(),
            PolicyId = image.PolicyId,
            DefaultPaymentProfileId = defaultPaymentProfileId,
          }
    }.ApplySession(session);

    var ret = DiamondService.CreditCard.SaveCustomerAndPaymentProfiles(req);
    return ret;
  }

  #endregion Public Methods
}
