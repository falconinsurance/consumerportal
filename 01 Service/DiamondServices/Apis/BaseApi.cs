using DiamondServices.Service;
using Falcon.DependencyInjection;
using Falcon.FalconDB;

namespace DiamondServices.Apis;

public abstract class BaseApi:IBaseApi
{
  #region Private Fields
  private static readonly IRelax<IDiamondService> _DiamondServiceRelaxed = RelaxFactory.Create(AppResolver.Get<IDiamondService>);
  private static readonly IRelax<IFalconHandler> _FalconHandlerRelaxed = RelaxFactory.Create(AppResolver.Get<IFalconHandler>);
  #endregion Private Fields

  #region Protected Constructors

  protected BaseApi(IDiamondApi diamondApi)
  {
    DiamondApi = diamondApi;
  }

  #endregion Protected Constructors

  #region Public Properties
  public IDiamondApi DiamondApi { get; }

  public IDiamondService DiamondService => _DiamondServiceRelaxed.Value;

  public IFalconHandler FalconHandler => _FalconHandlerRelaxed.Value;
  #endregion Public Properties
  #region Public Methods

  public abstract Task Initialize();

  #endregion Public Methods
}
