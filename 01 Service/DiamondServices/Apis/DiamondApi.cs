using DiamondServices.Service;
using Falcon.FalconDB;

namespace DiamondServices.Apis;

public class DiamondApi:IDiamondApi
{
  #region Private Fields

  private static DiamondApiRelaxed _Relaxed;

  #endregion Private Fields

  #region Public Properties

  public IAdministrationApi Administration => _Relaxed.Administration.Value;

  public IAttachmentApi Attachment => _Relaxed.Attachment.Value;

  public IBillingApi Billing => _Relaxed.Billing.Value;

  public ICreditCardApi CreditCard => _Relaxed.CreditCard.Value;

  public IFalconHandler DbFalcon => _Relaxed.DbFalcon.Value;

  public IDiamondService DiamondService => _Relaxed.DiamondService.Value;

  public ILoginApi Login => _Relaxed.Login.Value;

  public INotesApi Notes => _Relaxed.Notes.Value;

  public IPolicyApi Policy => _Relaxed.Policy.Value;

  public IPolicyFormApi PolicyForm => _Relaxed.PolicyForm.Value;

  public IPrintingApi Printing => _Relaxed.Printing.Value;

  public ISecurityApi Security => _Relaxed.Security.Value;

  public IStatic Static => _Relaxed.Static.Value;

  public DateTime SystemDate => Utility.SystemDate;

  public DateTime SystemTime => DateTime.Now;

  public IUtilityApi Utility => _Relaxed.Utility.Value;

  public IVehicleApi Vehicle => _Relaxed.Vehicle.Value;

  public IWorkflowApi Workflow => _Relaxed.Workflow.Value;

  #endregion Public Properties

  #region Public Constructors

  public DiamondApi()
  {
    if(_Relaxed is not null)
      throw new Exception($"You can not Create {nameof(DiamondApi)} more than once.");
    _Relaxed ??= new DiamondApiRelaxed(this);
  }

  #endregion Public Constructors

  #region Public Methods

  public Task Initialize()
  {
    var tasks = new List<Task>()
    {
      Administration.Initialize(),
      Attachment.Initialize(),
      Billing.Initialize(),
      Workflow.Initialize(),
      CreditCard.Initialize(),
      Login.Initialize(),
      Policy.Initialize(),
      Vehicle.Initialize(),
      Static.Initialize(),
      Utility.Initialize(),
      Security.Initialize(),
      PolicyForm.Initialize(),
      Notes.Initialize(),
      Printing.Initialize()
    };
    return Task.WhenAll(tasks);
  }

  #endregion Public Methods
}
