using Diamond.Common.Objects.Policy;
using Diamond.Common.Services.Messages.PolicyService.SaveRateIssue;
using DiamondServices.Service;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public interface IPolicyApi:IBaseApi
{
  #region Public Methods

  Image Demote(ISession session,int policyId,int imageNum);

  Image LoadImage(ISession session,int policyId,int imageNum);

  Image Promote(ISession session,int policyId,int imageNum);

  Image Rate(ISession session,Image policyImage);

  (Image Image, IList<DCO.ValidationItem> ValidationItems) SaveRate(ISession session,Image policyImage);

  Response SaveRateIssue(ISession session,Image image,Diamond.Common.Objects.Billing.ApplyCash applyCash);

  (Image Image, IList<DCO.ValidationItem> ValidationItems) Submit(ISession session,Image policyImage,En_State state);

  (Image Image, IList<DCO.ValidationItem> ValidationItems) SubmitApplication(ISession session,Image policyImage,En_State state);

  #endregion Public Methods
}
