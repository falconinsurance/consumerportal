using Diamond.Common.Objects.Policy;
using DiamondServices.Service;

namespace DiamondServices.Apis;

public interface IPolicyFormApi:IBaseApi
{
  #region Public Methods

  bool AddAndPrintByFormVersion(ISession session,Image image,string remarks,string additionalInfo,int formVersionId,int dueDateDays,int policyFormNum,En_State state);

  #endregion Public Methods
}
