using Diamond.Common.Objects.Policy;
using DiamondServices.Service.Models;

namespace DiamondServices.Apis;

public interface IStatic:IBaseApi
{
  #region Public Methods

  Modifier CreateModifier(int modifierId,int versionId);

  List<string> GetCounties(En_State state,string zip);

  (string Token, string Last4, string ExpirationDate, int CreditCardTypeId) GetDefaultCreditCardTokenData(int policyId);

  List<int> GetPayPlans(En_State stateId,int policyTermId);
  int GetPayplanTypeId(int payplanId);
  En_State GetState(int versionId);

  int GetTerritory(En_State state,string zip);

  int GetVersionId(En_State state,DateTime effectiveDate);

  VersionResult GetVersionResult(En_State state,DateTime effectiveDate);

  #endregion Public Methods
}
