using DiamondServices.Service;

namespace DiamondServices.Apis;

public interface IAttachmentApi:IBaseApi
{
  int AttachFileAndGetId(ISession session,byte[] document,int policyId,int imageNum,string fileName,string desc);
}
