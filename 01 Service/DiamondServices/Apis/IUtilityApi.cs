namespace DiamondServices.Apis;

public interface IUtilityApi:IBaseApi
{
  #region Public Properties
  DateTime SystemDate { get; }
  #endregion Public Properties
}
