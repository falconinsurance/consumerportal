using DiamondServices.Service;

namespace DiamondServices.Apis;

public interface INotesApi:IBaseApi
{
  #region Public Methods

  bool Create(ISession session,int policyId,string note,string title,int notesTypeId);

  #endregion Public Methods
}
