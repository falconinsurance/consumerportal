using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;
using WorkflowService = Diamond.Common.Services.Messages.WorkflowService;

namespace DiamondServices.Apis;

public class WorkflowApi:BaseApi, IWorkflowApi
{
  #region Public Constructors

  public WorkflowApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public bool Create(ISession session,DCO.Policy.Image image,string remarks,int dueDateDays,DCE.Workflow.WorkflowType workflowTypeId,EnWorkflowQueue workflowQueue = EnWorkflowQueue.RequestedInformation)
  {
    var dateTime = DiamondApi.SystemDate.ToInsDateTime();
    return Create(session,image.PolicyId,image.PolicyImageNum,remarks,dateTime.AddDays(dueDateDays),workflowTypeId,workflowQueue);
  }

  public bool Create(ISession session,int policyId,int imageNum,string remarks,DCO.InsDateTime dueDate,DCE.Workflow.WorkflowType workflowTypeId,EnWorkflowQueue workflowQueue)
  {
    var dateTime = DiamondApi.SystemDate.ToInsDateTime();
    var request = new WorkflowService.CreateWorkflowForPolicy.Request()
    {
      RequestData = new()
      {
        PolicyId = policyId,
        PolicyImageNum = imageNum,
        Workflow = new()
        {
          WorkflowQueueId = (int)workflowQueue,
          AddDate = dateTime,
          DueDate = dueDate,
          ReviewDate = dateTime,
          ReviewDays = 0,
          Urgent = false,
          Diary = true,
          Remarks = remarks,
          WorkflowTypeId = (int)workflowTypeId,
          WorkflowStatusCodeId = (int)DCE.Workflow.WorkflowStatus.Open,
          Mandatory = false
        }
      }
    }.ApplySession(session);
    var response = DiamondService.Workflow.CreateWorkflowForPolicy(request);
    return response?.ResponseData?.Success == true;
  }

  public bool Delete(ISession session,int deleteWorkflowId)
  {
    var request = new WorkflowService.DeleteTask.Request()
    {
      RequestData = new()
      {
        WorkflowId = deleteWorkflowId
      }
    }.ApplySession(session);
    var response = DiamondService.Workflow.DeleteTask(request);
    return response?.ResponseData?.Success == true;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods
}
