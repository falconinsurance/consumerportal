using DiamondServices.Service;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;
using FCE = Diamond.C0057.Common.Library.Enumerations;
using PolicyFormService = Diamond.Common.Services.Messages.PolicyFormService;

namespace DiamondServices.Apis;

public class PolicyFormApi:BaseApi, IPolicyFormApi
{
  #region Public Constructors

  public PolicyFormApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public bool AddAndPrintByFormVersion(ISession session,DCO.Policy.Image image,string remarks,string additionalInfo,int formVersionId,int dueDateDays,int policyFormNum,En_State state)
  {
    var request = new PolicyFormService.AddFormsAndPrintByFormVersion.Request()
    {
      RequestData = new()
      {
        PolicyId = image.PolicyId,
        policyImage = image,
        PolicyImageNum = image.PolicyImageNum,
        level = DCE.Forms.FormLevel.Policy,
        WorkflowQueueId = (int)EnWorkflowQueue.RequestedInformation,
        // Actual UW Memo
        // CDAP-2124 In AZ, the cancellation process is different so we use a different UW Memo type here
        PrintItems =
        [
          new()
          {
            FormVersionId = formVersionId,
            ParentNum = -1,
            TransactionInfo = new()
            {
              RespondByDate = DiamondApi.SystemDate.ToInsDateTime().AddDays(dueDateDays),
              TransactionReasonId = (state == En_State.AZ) ? 10132 : (int)FCE.TransReason.Cancellation_RequestedInformationNotReceived,
              TransactionRemarks = remarks,
              TransactionTypeId = (state == En_State.AZ) ? (int)DCE.TransType.AutoManualCancel : (int)DCE.TransType.Cancel
            },
          }
        ],
        PolicyFormAdditionalInfo =
        [
          new()
          {
            PolicyId = image.PolicyId,
            PolicyImageNum = image.PolicyImageNum,
            PolicyFormNum = policyFormNum,
            PolicyFormAdditionalInfoNum = 1,
            DetailStatusCode = 1,
            Description = "Additional Info",
            AddInfo = additionalInfo,
          }
        ]
      }
    }.ApplySession(session);
    var response = DiamondService.PolicyForm.AddFormsAndPrintByFormVersion(request);
    return response?.ResponseData?.Successful == true;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  #endregion Public Methods
}
