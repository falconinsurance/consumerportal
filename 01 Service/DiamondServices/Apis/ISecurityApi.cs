using DiamondServices.Service;
using Falcon.Tools.DBHandler;

namespace DiamondServices.Apis;

public interface ISecurityApi:IBaseApi
{
  #region Public Methods

  string DecrypAccountNumber(ISession session,string encryptedValue,int agencyId);

  RecordSet DecrypAccountNumbers(ISession session,RecordSet data);

  string DecryptCreditCard(ISession session,string encryptedValue,int policyId);

  string DecryptEft(ISession session,string encryptedValue,int policyId);

  string DecryptLicenseNumber(ISession session,string encryptedValue,int policyId);

  string Encrypt(ISession session,string plainTextValue,int policyId);

  int GetRegistrationClientId(string policyNumber,string zipCode);

  #endregion Public Methods
}
