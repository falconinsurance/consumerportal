using AdministrationService = Diamond.Common.Services.Messages.AdministrationService;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;

namespace DiamondServices.Apis;

public class AdministrationApi:BaseApi, IAdministrationApi
{
  #region Public Constructors

  public AdministrationApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public IList<DCO.ValidationItem> SaveUser(string emailAddress,string password,int clientId)
  {
    var request = new AdministrationService.SaveUser.Request
    {
      RequestData = new AdministrationService.SaveUser.RequestData()
      {
        UsersRecord = new()
        {
          Password = password,
          LoginName = emailAddress,
          LoginDomain = "",
          UserEmailAddr = emailAddress,
          DisplayName = emailAddress,
          UsercategoryId = (int)DCE.UserCategory.UserCategory_Client,
          PasswordHashTypeId = DCE.Security.PasswordHashType.MD5,
          Active = true,
        },
        EncryptPassword = true,
        UserLinkRecords = [new DCO.Administration.UserClientLink { ClientId = clientId }]
      }
    };
    var response = DiamondService.Administration.SaveUser(request);
    return response?.ResponseData?.Success == false? response.DiamondValidation.ValidationItems:null;
  }

  #endregion Public Methods
}
