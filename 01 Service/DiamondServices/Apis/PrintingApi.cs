using DiamondServices.Service;
using DCO = Diamond.Common.Objects;

using PrintingService = Diamond.Common.Services.Messages.PrintingService;

namespace DiamondServices.Apis;

public class PrintingApi:BaseApi, IPrintingApi
{
  #region Public Constructors

  public PrintingApi(IDiamondApi diamondApi) : base(diamondApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public byte[] GetDocument(ISession session,int policyId,int imageNumber,DCO.Printing.PrintForm printForm)
  {
    var request = new PrintingService.ReprintJob.Request()
    {
      RequestData = new()
      {
        PolicyId = policyId,
        PolicyImageNum = imageNumber,
        PrintForms = [printForm],
        ReprintByFormCategory = false,
      }
    }.ApplySession(session);
    var response = DiamondService.Printing.ReprintJob(request);
    return response.ResponseData.Data;
  }

  public override Task Initialize()
  {
    var task = new List<Task>();
    return Task.WhenAll(task);
  }

  public ICollection<DCO.Printing.PrintForm> LoadPrintForms(ISession session,int policyId,int imageNumber)
  {
    try
    {
      var request = new PrintingService.LoadPrintHistory.Request()
      {
        RequestData = new()
        {
          PolicyId = policyId,
          PolicyImageNum = imageNumber
        }
      }.ApplySession(session);
      var response = DiamondService.Printing.LoadPrintHistory(request);
      return response.ResponseData.PrintHistory;
    }
    catch
    {
      // Don't need to throw an exception here, just fail gracefully
      //_Log.Warn(msg: $"Encountered error loading documents for PolicyId: {policyId}, ImageNum: {imageNum} - {documentsResponse.DiamondValidation}");
      return new List<DCO.Printing.PrintForm>();
    }
  }

  #endregion Public Methods
}
