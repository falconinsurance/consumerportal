using Diamond.Common.Objects.Policy;
using DiamondServices.Service.Models;
using Falcon.Tools.DBHandler;

using DB = Falcon.Tools.DBHandler;

namespace DiamondServices.Apis;

public class Static:BaseApi, IStatic
{
  #region Private Fields

  private static readonly ClassCache _Cache = new();
  private static IRelax<Dictionary<En_State,Dictionary<string,List<CountyTerritory>>>> _DicGetCountiesRelaxed = null;
  private static IRelax<Dictionary<En_State,Dictionary<int,List<int>>>> _DicGetCurrentPayplanIdsRelaxed = null;

  #endregion Private Fields

  #region Public Constructors

  public Static(IDiamondApi diamondApi) : base(diamondApi)
  {
    _DicGetCurrentPayplanIdsRelaxed ??= RelaxFactory.Create(GetCurrentPayplanIds);
    _DicGetCountiesRelaxed ??= RelaxFactory.Create(GetCounties);
  }

  #endregion Public Constructors

  #region Public Methods

  public Modifier CreateModifier(int modifierId,int versionId)
  {
    if(GetModifiers().Find(c => c.TypeId == modifierId && c.VersionId == versionId) is ModifierResult modifierResult)
    {
      return new()
      {
        DetailStatusCode = 1,
        ModifierTypeId = modifierResult.TypeId,
        ParentModifierTypeId = modifierResult.ParentTypeId,
        ModifierGroupId = modifierResult.GroupId,
        ModifierLevelId = modifierResult.LevelId
      };
    }
    return null;
  }

  public List<string> GetCounties(En_State state,string zip) => GetCountiesTerritory(state,zip).ConvertAll(c => c.County);

  public (string Token, string Last4, string ExpirationDate, int CreditCardTypeId) GetDefaultCreditCardTokenData(int policyId)
  {
    if(policyId < 1)
    {
      return default;
    }
    const string sqlQuery = """
        SELECT TOP(1)
            V1 = payment_profile_identifier
          , V2 = last_four_digits
          , V3 = CONVERT(INT,expiration_month)
          , V4 = CONVERT(INT,expiration_year)
          , V5 = creditcardtype_id
        FROM Diamond..Policy                          P(NOLOCK)
        LEFT JOIN Diamond..CreditCardTokenData        CCTD(NOLOCK)  ON P.client_id = CCTD.client_id
        LEFT JOIN Diamond..CreditCardTokenClientInfo  CCTCI(NOLOCK) ON CCTD.payment_profile_identifier = CCTCI.default_payment_profile_identifier
        WHERE P.policy_id = @1
          AND CCTCI.client_id IS NOT NULL;
        """;
    var rec = FalconHandler.SqlFirstOrDefault(sqlQuery,c => c.AddValues(policyId));
    return rec is not null ? (rec.To<string>("V1"), rec.To<string>("V2"), $"{rec.To<int>("V3"):D2}/{rec.To<int>("V4"):D4}", rec.To<int>("V3")) : default;
  }

  public List<int> GetPayPlans(En_State stateId,int policyTermId)
  {
    var dic = _DicGetCurrentPayplanIdsRelaxed.Value;
    if(dic.TryGetValue(stateId,out var dic2))
    {
      return dic2.TryGetValue(policyTermId,out var lst) ? lst : [];
    }
    return [];
  }

  //public En_State GetState(int versionId) => _DicTempRelaxed.Value.TryGetValue(versionId,out var stateId) ? stateId : En_State.NA;
  public En_State GetState(int versionId)
  {
    return _Cache.SetGet(Request,new()).TryGetValue(versionId,out var stateId) ? stateId : En_State.NA;
    Dictionary<int,En_State> Request()
    {
      const string sqlQuery = """
        SELECT DISTINCT StateId=V.state_id
          ,  VersionId = V.version_id
        FROM Diamond..Version V(NOLOCK)
        WHERE V.company_id = 1
        ORDER BY VersionId
        ,        StateId;
        """;
      return FalconHandler.Sql(sqlQuery).ToDictionary(c => c.To<int>("VersionId"),c => c.To<En_State>("StateId"));
    }
  }

  public int GetTerritory(En_State state,string zip) => GetCountiesTerritory(state,zip).Select(c => c.Territory).FirstOrDefault();

  public int GetVersionId(En_State state,DateTime effectiveDate) => GetVersionResult(state,effectiveDate)?.VersionId ?? -1;

  public VersionResult GetVersionResult(En_State state,DateTime effectiveDate)
  {
    return _Cache.SetGet(Request,new(state,effectiveDate.Date));
    VersionResult Request() => FalconHandler.SqlFirstOrDefault<VersionResult>("EXEC spGetVersions @StateId=@1, @EffDate=@2;",c => c.AddValues(state.ToInt(),effectiveDate));
  }

  public int GetPayplanTypeId(int payplanId)
  {
    return _Cache.SetGet(Request,new(payplanId));
    int Request()
    {
      const string sqlQuery = """
        SELECT [paymenttype_id]
        FROM [Diamond].[dbo].[BillingPayPlan]
        where [billingpayplan_id] = @1
        """;
      return FalconHandler.SqlFirstOrDefault<int>(sqlQuery,c => c.AddValues(payplanId));
    }
  }

  public override Task Initialize()
  {
    var tasks = new List<Task>() {
      Task.Run(()=> GetState(0)),
      Task.Run(()=> _DicGetCurrentPayplanIdsRelaxed.Value),
      Task.Run(()=> _DicGetCountiesRelaxed.Value),
    };
    return Task.WhenAll(tasks);
  }

  #endregion Public Methods

  #region Private Methods

  private Dictionary<En_State,Dictionary<string,List<CountyTerritory>>> GetCounties()
  {
    const string sqlQuery = "Exec [spGetTerritories]";
    return FalconHandler.Sql(sqlQuery,null,County.Cast)
  .GroupBy(g => g.StateId)
    .ToDictionary(
      d => d.Key,
      d => d.GroupBy(s => s.Zip)
      .ToDictionary(
      d => d.Key,
      d => d.Select(s => new CountyTerritory { County = s.County,Territory = s.Territory }).ToList()));
  }

  private static List<CountyTerritory> GetCountiesTerritory(En_State state,string zip)
  {
    var dic = _DicGetCountiesRelaxed.Value;
    if(dic.TryGetValue(state,out var dic2)
      && dic2.TryGetValue(zip,out var lst))
    {
      return lst;
    }
    return [];
  }

  private Dictionary<En_State,Dictionary<int,List<int>>> GetCurrentPayplanIds()
  {
    const string sqlQuery = """
        SELECT StateId=CS.state_id,TermId=V.policyterm_id, PayPlanId=V.billingpayplan_id
        FROM Diamond..vpayplan V(NOLOCK)
        Left Join Diamond..CompanyState CS(NOLOCK) ON CS.companystate_id=V.companystatelob_id
        Where billmethod_id=2
        order By CS.state_id,PayPlanId
        """;
    return FalconHandler.Sql(sqlQuery,null,PayPlan.Cast)
      .GroupBy(g => g.StateId)
      .ToDictionary(c => c.Key,c => c.GroupBy(g1 => g1.TermId).ToDictionary(d => d.Key,d => d.Select(s => s.PayPlanId).ToList()));
  }

  private List<ModifierResult> GetModifiers()
  {
    return _Cache.SetGet(Request);
    List<ModifierResult> Request()
    {
      const string sqlQuery = """
          SELECT modifiertype_id AS TypeId
          ,      VersionId    =   version_id
          ,      GroupId      =   modifiergroup_id
          ,      LevelId      =   modifierlevel_id
          ,      ParentTypeId =   parent_modifiertype_id
          FROM Diamond..ModifierTypeVersion
          """;
      return FalconHandler.Sql(sqlQuery,null,ModifierResult.Cast).ToList();
    }
  }

  #endregion Private Methods

  #region Private Classes

  private class County:CountyTerritory
  {
    #region Public Properties

    public En_State StateId { get; set; }
    public string Zip { get; set; }

    #endregion Public Properties

    #region Public Methods

    public static County Cast(DB.Record c) => new()
    {
      County = c.To<string>("County") ?? "",
      StateId = c.To<En_State>("State"),
      Territory = c.To<int>("Territory"),
      Zip = c.To<string>("ZipCode"),
    };

    #endregion Public Methods
  }

  private class CountyTerritory
  {
    #region Public Properties

    public string County { get; set; }
    public int Territory { get; set; }

    #endregion Public Properties
  }

  private class ModifierResult
  {
    #region Public Properties

    public int GroupId { get; set; }
    public int LevelId { get; set; }
    public int ParentTypeId { get; set; }
    public int TypeId { get; set; }
    public int VersionId { get; set; }

    #endregion Public Properties

    #region Internal Methods

    internal static ModifierResult Cast(Record record) => new()
    {
      VersionId = record.To<int>("VersionId"),
      TypeId = record.To<int>("TypeId"),
      ParentTypeId = record.To<int>("ParentTypeId"),
      GroupId = record.To<int>("GroupId"),
      LevelId = record.To<int>("LevelId"),
    };

    #endregion Internal Methods
  }

  private class PayPlan
  {
    #region Public Properties

    public int PayPlanId { get; set; }
    public En_State StateId { get; set; }
    public int TermId { get; set; }

    #endregion Public Properties

    #region Public Methods

    public static PayPlan Cast(DB.Record c) => new()
    {
      StateId = c.To<En_State>("StateId"),
      PayPlanId = c.To<int>("PayPlanId"),
      TermId = c.To<int>("TermId"),
    };

    #endregion Public Methods
  }

  #endregion Private Classes
}
