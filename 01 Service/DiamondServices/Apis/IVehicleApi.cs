using Falcon.Tools.VehicleInfoCache;

namespace DiamondServices.Apis;

public interface IVehicleApi:IBaseApi
{
  #region Public Methods

  IdDescEntity GetBodyType(int id);

  IdDescEntity[] GetBodyTypes(int year,IdDescEntity make,IdDescEntity model);

  IdDescEntity[] GetMakes(int year);

  IdDescEntity[] GetModels(int year,IdDescEntity make);

  IdDescEntity[] GetSymbols(int year,IdDescEntity make,IdDescEntity model,IdDescEntity bodyType);

  VehicleVinMatch[] GetVehicles(string vin);

  int[] GetYears();

  bool TryGetBodyTypeForVin(string vin,out int bodyType);

  bool TryGetSymbolsForSymbol(string symbol,out (string Comprehensive, string Collision, string Liability) Symbols);

  bool TryGetSymbolsForVin(string vin,out (string Comprehensive, string Collision, string Liability) Symbols);

  #endregion Public Methods
}
