using DiamondServices.Service;
using Falcon.FalconDB;

namespace DiamondServices.Apis;

public interface IDiamondApi
{
  #region Public Properties

  IAdministrationApi Administration { get; }
  IAttachmentApi Attachment { get; }
  IBillingApi Billing { get; }
  ICreditCardApi CreditCard { get; }
  IFalconHandler DbFalcon { get; }
  IDiamondService DiamondService { get; }
  ILoginApi Login { get; }
  INotesApi Notes { get; }
  IPolicyApi Policy { get; }
  IPolicyFormApi PolicyForm { get; }
  IPrintingApi Printing { get; }
  ISecurityApi Security { get; }
  IStatic Static { get; }
  DateTime SystemDate { get; }
  DateTime SystemTime { get; }
  IUtilityApi Utility { get; }
  IVehicleApi Vehicle { get; }
  IWorkflowApi Workflow { get; }

  #endregion Public Properties

  #region Public Methods

  Task Initialize();

  #endregion Public Methods
}
