using DiamondServices.Service;
using Falcon.DependencyInjection;
using Falcon.FalconDB;

namespace DiamondServices.Apis;

public class DiamondDependencyInjection:IServiceModule
{
  #region Private Fields
  private IServiceBuilder _Builder;
  #endregion Private Fields

  #region Public Constructors

  public DiamondDependencyInjection(string diamondConection,string databaseServer)
  {
    DiamondConection = diamondConection;
    DatabaseServer = databaseServer;
  }

  #endregion Public Constructors

  #region Public Properties
  public string DatabaseServer { get; }
  public string DiamondConection { get; }
  #endregion Public Properties

  #region Public Methods

  public void Load(IServiceBuilder register)
  {
    _Builder = register;
    RegisterDiamondService();
    RegisterDiamondApi();
    RegisterFalconHandler();
  }

  #endregion Public Methods

  #region Private Methods

  private string GetConnectionString(string Database) => $"Server={DatabaseServer};Database={Database};Trusted_Connection=True;Connection Timeout=60";

  private void RegisterDiamondApi() => _Builder.AopSetSingle<IDiamondApi>(_ => new DiamondApi());

  private void RegisterDiamondService()
  {
    if(!string.IsNullOrEmpty(DiamondConection))
    {
      _Builder.AopSetSingle<IDiamondService>(_ => new DiamondService(DiamondConection));
    }
  }

  private void RegisterFalconHandler()
  {
    if(!string.IsNullOrEmpty(DatabaseServer))
    {
      _Builder.AopSetSingle<IFalconHandler>(_ => new FalconHandler(o => o.NameOrConnection = GetConnectionString("Falcon")));
    }
  }

  #endregion Private Methods
}
