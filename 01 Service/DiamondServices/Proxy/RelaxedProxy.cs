using Diamond.Common.Services.Interfaces;

namespace DiamondServices.Proxy;

internal class RelaxedProxy
{
  #region Private Fields
  private readonly string _DiamondServicesHostName;
  #endregion Private Fields

  #region Public Constructors

  public RelaxedProxy(string diamondServicesHostName)
  {
    _DiamondServicesHostName = diamondServicesHostName ?? throw new ArgumentNullException(nameof(diamondServicesHostName));
    Accounting = Create<IAccountingService>();
    AdditionalInterestService = Create<IAdditionalInterestService>();
    AdministrationService = Create<IAdministrationService>();
    AgencyAdministrationService = Create<IAgencyAdministrationService>();
    AttachmentService = Create<IAttachmentService>();
    AutoService = Create<IAutoService>();
    BillingService = Create<IBillingService>();
    CheckService = Create<ICheckService>();
    ClaimsService = Create<IClaimsService>();
    CLMClassService = Create<ICLMClassService>();
    ConfigurableBookService = Create<IConfigurableBookService>();
    ContactManagement = Create<IContactManagementService>();
    CreditCard = Create<ICreditCardService>();
    DiamondComposer = Create<IDiamondComposerService>();
    DMV = Create<IDMVService>();
    Document = Create<IDocumentService>();
    EFT = Create<IEFTService>();
    EmployInfo = Create<IEmployInfoService>();
    EOPProcess = Create<IEOPProcessService>();
    ExperienceModificationImport = Create<IExperienceModificationImportService>();
    FAQ = Create<IFAQService>();
    GlobalAdditionalInterestListManagement = Create<IGlobalAdditionalInterestListManagementService>();
    IIXS = Create<IIIXService>();
    Login = Create<ILoginService>();
    Lookup = Create<ILookupService>();
    Merchandise = Create<IMerchandiseService>();
    Modifier = Create<IModifierService>();
    News = Create<INewsService>();
    Notes = Create<INotesService>();
    Policy = Create<IPolicyService>();
    PolicyControl = Create<IPolicyControlService>();
    PolicyForm = Create<IPolicyFormService>();
    Portal = Create<IPortalService>();
    Printing = Create<IPrintingService>();
    Process = Create<IProcessService>();
    Renewal = Create<IRenewalService>();
    ReplacementCost = Create<IReplacementCostService>();
    Report = Create<IReportService>();
    ResidenceInfo = Create<IResidenceInfoService>();
    RuleEngine = Create<IRuleEngineService>();
    Sample = Create<ISampleService>();
    ScheduleRunner = Create<IScheduleRunnerService>();
    Security = Create<ISecurityService>();
    StaticDataManager = Create<IStaticDataManagerService>();
    StatsExport = Create<IStatsExportService>();
    Supplies = Create<ISuppliesService>();
    Support = Create<ISupportService>();
    SystemDate = RelaxFactory.Create(() => Utility.Value.GetSystemDate(new())?.ResponseData?.SystemDate ?? DateTime.Today,() => TimeSpan.FromMinutes(2));
    SystemEmail = Create<ISystemEmailService>();
    SystemSettings = Create<ISystemSettingsService>();
    ThirdParty = Create<IThirdPartyService>();
    Transaction = Create<ITransactionService>();
    Utility = Create<IUtilityService>();
    Version = Create<IVersionService>();
    Vin = Create<IVinService>();
    VisualTree = Create<IVisualTreeService>();
    WebSite = Create<IWebSiteService>();
    Workflow = Create<IWorkflowService>();
  }

  #endregion Public Constructors

  #region Internal Properties
  internal IRelax<IAccountingService> Accounting { get; }
  internal IRelax<IAdditionalInterestService> AdditionalInterestService { get; }
  internal IRelax<IAdministrationService> AdministrationService { get; }
  internal IRelax<IAgencyAdministrationService> AgencyAdministrationService { get; }
  internal IRelax<IAttachmentService> AttachmentService { get; }
  internal IRelax<IAutoService> AutoService { get; }
  internal IRelax<IBillingService> BillingService { get; }
  internal IRelax<ICheckService> CheckService { get; }
  internal IRelax<IClaimsService> ClaimsService { get; }
  internal IRelax<ICLMClassService> CLMClassService { get; }
  internal IRelax<IConfigurableBookService> ConfigurableBookService { get; }
  internal IRelax<IContactManagementService> ContactManagement { get; }
  internal IRelax<ICreditCardService> CreditCard { get; }
  internal IRelax<IDiamondComposerService> DiamondComposer { get; }
  internal IRelax<IDMVService> DMV { get; }
  internal IRelax<IDocumentService> Document { get; }
  internal IRelax<IEFTService> EFT { get; }
  internal IRelax<IEmployInfoService> EmployInfo { get; }
  internal IRelax<IEOPProcessService> EOPProcess { get; }
  internal IRelax<IExperienceModificationImportService> ExperienceModificationImport { get; }
  internal IRelax<IFAQService> FAQ { get; }
  internal IRelax<IGlobalAdditionalInterestListManagementService> GlobalAdditionalInterestListManagement { get; }
  internal IRelax<IIIXService> IIXS { get; }
  internal IRelax<ILoginService> Login { get; }
  internal IRelax<ILookupService> Lookup { get; }
  internal IRelax<IMerchandiseService> Merchandise { get; }
  internal IRelax<IModifierService> Modifier { get; }
  internal IRelax<INewsService> News { get; }
  internal IRelax<INotesService> Notes { get; }
  internal IRelax<IPolicyService> Policy { get; }
  internal IRelax<IPolicyControlService> PolicyControl { get; }
  internal IRelax<IPolicyFormService> PolicyForm { get; }
  internal IRelax<IPortalService> Portal { get; }
  internal IRelax<IPrintingService> Printing { get; }
  internal IRelax<IProcessService> Process { get; }
  internal IRelax<IRenewalService> Renewal { get; }
  internal IRelax<IReplacementCostService> ReplacementCost { get; }
  internal IRelax<IReportService> Report { get; }
  internal IRelax<IResidenceInfoService> ResidenceInfo { get; }
  internal IRelax<IRuleEngineService> RuleEngine { get; }
  internal IRelax<ISampleService> Sample { get; }
  internal IRelax<IScheduleRunnerService> ScheduleRunner { get; }
  internal IRelax<ISecurityService> Security { get; }
  internal IRelax<IStaticDataManagerService> StaticDataManager { get; }
  internal IRelax<IStatsExportService> StatsExport { get; }
  internal IRelax<ISuppliesService> Supplies { get; }
  internal IRelax<ISupportService> Support { get; }
  internal IRelax<DateTime> SystemDate { get; }
  internal IRelax<ISystemEmailService> SystemEmail { get; }
  internal IRelax<ISystemSettingsService> SystemSettings { get; }
  internal IRelax<IThirdPartyService> ThirdParty { get; }
  internal IRelax<ITransactionService> Transaction { get; }
  internal IRelax<IUtilityService> Utility { get; }
  internal IRelax<IVersionService> Version { get; }
  internal IRelax<IVinService> Vin { get; }
  internal IRelax<IVisualTreeService> VisualTree { get; }
  internal IRelax<IWebSiteService> WebSite { get; }
  internal IRelax<IWorkflowService> Workflow { get; }
  #endregion Internal Properties

  #region Private Methods

  private IRelax<T> Create<T>() where T : IDisposable => RelaxFactory.Create(() => DiamondProxy<T>.Create(_DiamondServicesHostName));

  #endregion Private Methods
}
