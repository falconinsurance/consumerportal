using System.IO.Compression;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml;

namespace DiamondServices.Proxy;

public static class Bindings
{
  #region Private Fields
  private static Binding _Basic;
  private static Binding _Binarry;
  private static Binding _Compressed;
  private static Binding _Integration;
  private static Binding _NetHttp;
  private static Binding _NetHttpStreamed;
  private static Binding _WSHttpBinding;
  #endregion Private Fields

  #region Internal Properties
  internal static Binding Basic => _Basic ??= new BasicHttp();
  internal static Binding Binarry => _Binarry ??= new BinaryHttp();
  internal static Binding Compressed => _Compressed ??= new CompressedHttp();
  internal static Binding Integration => _Integration ??= new IntegrationService();
  internal static Binding NetHttp => _NetHttp ??= new NetTcp();
  internal static Binding NetHttpStreamed => _NetHttpStreamed ??= new NetTcpStreamed();
  internal static Binding WSHttpBinding => _WSHttpBinding ??= new WSHttp();
  #endregion Internal Properties

  #region Public Classes

  public sealed class GZipMessageEncodingBindingElement:MessageEncodingBindingElement, IPolicyExportExtension
  {
    #region Public Constructors

    public GZipMessageEncodingBindingElement() : this(new TextMessageEncodingBindingElement())
    {
    }

    public GZipMessageEncodingBindingElement(MessageEncodingBindingElement messageEncoderBindingElement)
    {
      InnerMessageEncodingBindingElement = messageEncoderBindingElement;
    }

    #endregion Public Constructors

    #region Public Properties
    public MessageEncodingBindingElement InnerMessageEncodingBindingElement { get; set; }

    public override MessageVersion MessageVersion
    {
      get => InnerMessageEncodingBindingElement.MessageVersion;
      set => InnerMessageEncodingBindingElement.MessageVersion = value;
    }

    public XmlDictionaryReaderQuotas ReaderQuotas
    {
      get
      {
        if(InnerMessageEncodingBindingElement is BinaryMessageEncodingBindingElement binaryMessageEncodingBindingElement)
        {
          return binaryMessageEncodingBindingElement.ReaderQuotas;
        }

        return (InnerMessageEncodingBindingElement as TextMessageEncodingBindingElement)?.ReaderQuotas;
      }
    }

    #endregion Public Properties

    #region Public Methods

    public override IChannelFactory<TChannel> BuildChannelFactory<TChannel>(BindingContext context)
    {
      if(context == null)
      {
        throw new ArgumentNullException(nameof(context));
      }

      context.BindingParameters.Add(this);
      return context.BuildInnerChannelFactory<TChannel>();
    }

    public override IChannelListener<TChannel> BuildChannelListener<TChannel>(BindingContext context)
    {
      if(context == null)
      {
        throw new ArgumentNullException(nameof(context));
      }

      context.BindingParameters.Add(this);
      return context.BuildInnerChannelListener<TChannel>();
    }

    public override bool CanBuildChannelListener<TChannel>(BindingContext context)
    {
      if(context == null)
      {
        throw new ArgumentNullException(nameof(context));
      }

      context.BindingParameters.Add(this);
      return context.CanBuildInnerChannelListener<TChannel>();
    }

    public override BindingElement Clone() => new GZipMessageEncodingBindingElement(InnerMessageEncodingBindingElement);

    public override MessageEncoderFactory CreateMessageEncoderFactory() => new GZipMessageEncoderFactory(InnerMessageEncodingBindingElement.CreateMessageEncoderFactory());

    public void ExportPolicy(MetadataExporter _,PolicyConversionContext policyContext)
    {
      if(policyContext == null)
      {
        throw new ArgumentNullException(nameof(policyContext));
      }

      var xmlDocument = new XmlDocument();
      policyContext.GetBindingAssertions().Add(xmlDocument.CreateElement("gzip","GZipEncoding","http://schemas.microsoft.com/ws/06/2004/mspolicy/netgzip1"));
    }

    public override T GetProperty<T>(BindingContext context)
    {
      if(typeof(T).Equals(typeof(XmlDictionaryReaderQuotas)))
      {
        return InnerMessageEncodingBindingElement.GetProperty<T>(context);
      }

      return base.GetProperty<T>(context);
    }

    #endregion Public Methods
  }
  #endregion Public Classes

  #region Internal Classes

  internal class BasicHttp:BasicHttpBinding
  {
    #region Public Constructors

    public BasicHttp()
    {
      MaxReceivedMessageSize = 102400000;
      MaxBufferSize = 102400000;
      MaxBufferSize = 102400000;
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      ReaderQuotas = new()
      {
        MaxArrayLength = 16384000,
        MaxBytesPerRead = 40960000,
        MaxDepth = 32,
        MaxNameTableCharCount = 16384000,
        MaxStringContentLength = 81920000
      };
    }

    #endregion Public Constructors
  }

  internal class BinaryHttp:CustomBinding
  {
    #region Public Constructors

    public BinaryHttp()
    {
      BinaryMessageEncodingBindingElement binaryMessageEncodingBindingElement = new()
      {
        ReaderQuotas = new()
        {
          MaxArrayLength = 2147483647,
          MaxBytesPerRead = 40960000,
          MaxDepth = 32,
          MaxNameTableCharCount = 16384000,
          MaxStringContentLength = 81920000
        }
      };

      HttpTransportBindingElement httpTransport = new()
      {
        HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
        ManualAddressing = false,
        MaxReceivedMessageSize = 2100000000,
        AuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
        ProxyAuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
        BypassProxyOnLocal = false,
        Realm = "",
        UseDefaultWebProxy = true,
      };
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      Elements.Add(binaryMessageEncodingBindingElement);
      Elements.Add(httpTransport);
    }

    #endregion Public Constructors
  }

  internal class CompressedHttp:CustomBinding
  {
    #region Public Constructors

    public CompressedHttp()
    {
      BinaryMessageEncodingBindingElement binaryMessageEncodingBindingElement = new()
      {
        ReaderQuotas = new()
        {
          MaxArrayLength = 16384000,
          MaxBytesPerRead = 40960000,
          MaxDepth = 32,
          MaxNameTableCharCount = 16384000,
          MaxStringContentLength = 81920000
        }
      };

      var gzipMessageEncoding = new GZipMessageEncodingBindingElement(binaryMessageEncodingBindingElement);
      HttpTransportBindingElement httpTransport = new()
      {
        HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
        ManualAddressing = false,
        MaxReceivedMessageSize = 2100000000,
        AuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
        ProxyAuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
        BypassProxyOnLocal = false,
        Realm = "",
        UseDefaultWebProxy = true,
      };
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      Elements.Add(gzipMessageEncoding);
      Elements.Add(httpTransport);
    }

    #endregion Public Constructors
  }

  internal class GZipMessageEncoderFactory:MessageEncoderFactory
  {
    #region Private Fields
    private readonly MessageEncoder _MessageEncoder;
    #endregion Private Fields

    #region Public Constructors

    public GZipMessageEncoderFactory(MessageEncoderFactory messageEncoderFactory)
    {
      if(messageEncoderFactory == null)
      {
        throw new ArgumentNullException(nameof(messageEncoderFactory),"A valid message encoder factory must be passed to the GZipEncoder");
      }

      _MessageEncoder = new GZipMessageEncoder(messageEncoderFactory.Encoder);
    }

    #endregion Public Constructors

    #region Public Properties
    public override MessageEncoder Encoder => _MessageEncoder;

    public override MessageVersion MessageVersion => _MessageEncoder.MessageVersion;
    #endregion Public Properties

    #region Protected Properties
    protected string ClassName => "GZipMessageEncoderFactory";
    #endregion Protected Properties

    #region Private Classes

    private class GZipMessageEncoder:MessageEncoder
    {
      #region Private Fields
      private const string _GZipContentType = "application/x-gzip";

      private readonly MessageEncoder _InnerEncoder;
      #endregion Private Fields

      #region Internal Constructors

      internal GZipMessageEncoder(MessageEncoder messageEncoder)
      {
        _InnerEncoder = messageEncoder ?? throw new ArgumentNullException(nameof(messageEncoder),"A valid message encoder must be passed to the GZipEncoder");
      }

      #endregion Internal Constructors

      #region Public Properties
      public override string ContentType => _GZipContentType;

      public override string MediaType => _GZipContentType;

      public override MessageVersion MessageVersion => _InnerEncoder.MessageVersion;
      #endregion Public Properties

      #region Public Methods

      public override Message ReadMessage(ArraySegment<byte> buffer,BufferManager bufferManager,string contentType)
      {
        ArraySegment<byte> buffer2 = DecompressBuffer(buffer,bufferManager);
        Message message = _InnerEncoder.ReadMessage(buffer2,bufferManager);
        message.Properties.Encoder = this;
        return message;
      }

      public override Message ReadMessage(Stream stream,int maxSizeOfHeaders,string contentType)
      {
        var stream2 = new GZipStream(stream,CompressionMode.Decompress,leaveOpen: true);
        return _InnerEncoder.ReadMessage(stream2,maxSizeOfHeaders);
      }

      public override ArraySegment<byte> WriteMessage(Message message,int maxMessageSize,BufferManager bufferManager,int messageOffset)
      {
        ArraySegment<byte> buffer = _InnerEncoder.WriteMessage(message,maxMessageSize,bufferManager,messageOffset);
        return CompressBuffer(buffer,bufferManager,messageOffset);
      }

      public override void WriteMessage(Message message,Stream stream)
      {
        using(var stream2 = new GZipStream(stream,CompressionMode.Compress,leaveOpen: true))
        {
          _InnerEncoder.WriteMessage(message,stream2);
        }

        stream.Flush();
      }

      #endregion Public Methods

      #region Private Methods

      private static ArraySegment<byte> CompressBuffer(ArraySegment<byte> buffer,BufferManager bufferManager,int messageOffset)
      {
        var memoryStream = new MemoryStream();
        memoryStream.Write(buffer.Array,0,messageOffset);
        using(var gZipStream = new GZipStream(memoryStream,CompressionMode.Compress,leaveOpen: true))
        {
          gZipStream.Write(buffer.Array,messageOffset,buffer.Count);
        }

        var array = memoryStream.ToArray();
        var array2 = bufferManager.TakeBuffer(array.Length);
        Array.Copy(array,0,array2,0,array.Length);
        bufferManager.ReturnBuffer(buffer.Array);
        return new ArraySegment<byte>(array2,messageOffset,checked(array2.Length - messageOffset));
      }

      private static ArraySegment<byte> DecompressBuffer(ArraySegment<byte> buffer,BufferManager bufferManager)
      {
        checked
        {
          var stream = new MemoryStream(buffer.Array,buffer.Offset,buffer.Count - buffer.Offset);
          var memoryStream = new MemoryStream();
          var num = 0;
          const int num2 = 1024;
          var array = bufferManager.TakeBuffer(num2);
          using(var gZipStream = new GZipStream(stream,CompressionMode.Decompress))
          {
            while(true)
            {
              var num3 = gZipStream.Read(array,0,num2);
              if(num3 == 0)
              {
                break;
              }

              memoryStream.Write(array,0,num3);
              num += num3;
            }
          }

          bufferManager.ReturnBuffer(array);
          var array2 = memoryStream.ToArray();
          var array3 = bufferManager.TakeBuffer(array2.Length + buffer.Offset);
          Array.Copy(buffer.Array,0,array3,0,buffer.Offset);
          Array.Copy(array2,0,array3,buffer.Offset,array2.Length);
          var result = new ArraySegment<byte>(array3,buffer.Offset,array2.Length);
          bufferManager.ReturnBuffer(buffer.Array);
          return result;
        }
      }

      #endregion Private Methods
    }
    #endregion Private Classes
  }

  internal class IntegrationService:WSHttpBinding
  {
    #region Public Constructors

    public IntegrationService()
    {
      MaxBufferPoolSize = 1048576;
      ReaderQuotas = new()
      {
        MaxDepth = 1048576,
        MaxArrayLength = 1048576,
        MaxStringContentLength = 1048576,
        MaxBytesPerRead = 1048576,
        MaxNameTableCharCount = 1048576,
      };
      Security = new()
      {
        Mode = SecurityMode.None
      };
    }

    #endregion Public Constructors
  }

  internal class NetTcp:NetTcpBinding
  {
    #region Public Constructors

    public NetTcp()
    {
      MaxReceivedMessageSize = 10485760;
      MaxBufferSize = 10485760;
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      ReaderQuotas = new()
      {
        MaxArrayLength = 2147483647,
        MaxBytesPerRead = 2147483647,
        MaxDepth = 32,
        MaxNameTableCharCount = 2147483647,
        MaxStringContentLength = 2147483647
      };
      Security = new()
      {
        Mode = SecurityMode.None
      };
    }

    #endregion Public Constructors
  }

  internal class NetTcpStreamed:NetTcpBinding
  {
    #region Public Constructors

    public NetTcpStreamed()
    {
      MaxReceivedMessageSize = 10485760;
      MaxBufferSize = 10485760;
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      ReaderQuotas = new()
      {
        MaxArrayLength = 2147483647,
        MaxBytesPerRead = 2147483647,
        MaxDepth = 32,
        MaxNameTableCharCount = 2147483647,
        MaxStringContentLength = 2147483647
      };
      TransferMode = TransferMode.Streamed;
      Security = new()
      {
        Mode = SecurityMode.None
      };
    }

    #endregion Public Constructors
  }

  internal class WSHttp:WSHttpBinding
  {
    #region Public Constructors

    public WSHttp()
    {
      MaxReceivedMessageSize = 102400000;
      SendTimeout = TimeSpan.FromMinutes(30);
      ReceiveTimeout = TimeSpan.MaxValue;
      ReaderQuotas = new()
      {
        MaxArrayLength = 16384000,
        MaxBytesPerRead = 40960000,
        MaxDepth = 32,
        MaxNameTableCharCount = 16384000,
        MaxStringContentLength = 81920000
      };
    }

    #endregion Public Constructors
  }
  #endregion Internal Classes
}
