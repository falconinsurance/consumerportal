using System.ServiceModel;

namespace DiamondServices.Proxy;

internal class DiamondEndPointSetting
{
  #region Public Fields
  public static readonly Dictionary<Type,object> _Settings = [];
  #endregion Public Fields

  #region Internal Properties
  internal string Address { get; set; }

  internal string FullAddress { get; set; }
  #endregion Internal Properties

  #region Internal Methods

  internal static ChannelFactory<TInterface> GetChannel<TInterface>(string _DiamondServicesHostName)
  {
    var type = typeof(TInterface);
    if(_Settings.TryGetValue(type,out var channelFactory))
    {
      return (ChannelFactory<TInterface>)channelFactory;
    }
    var service = type.Name;
    if(type.IsInterface && service.StartsWith("I"))
    {
      service = service.Substring(1);
    }

    var fullAddress = $"{_DiamondServicesHostName}/Diamond/{service}.svc";
    channelFactory = _Settings[type] = new ChannelFactory<TInterface>(Bindings.Basic,fullAddress);
    return (ChannelFactory<TInterface>)channelFactory;
  }

  #endregion Internal Methods
}
