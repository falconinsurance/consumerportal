namespace DiamondServices.Proxy
{
  public class DiaProxyException:Exception
  {
    #region Public Constructors

    public DiaProxyException()
    {
    }

    public DiaProxyException(string message) : base(message)
    {
    }

    public DiaProxyException(string message,Exception innerException) : base(message,innerException)
    {
    }

    #endregion Public Constructors
  }
}
