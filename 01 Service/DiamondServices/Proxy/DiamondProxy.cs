using System.Reflection;
using System.ServiceModel;
using Falcon.DependencyInjection;

namespace DiamondServices.Proxy;

public class DiamondProxy<T>:DispatchProxy
  where T : IDisposable
{
  #region Internal Fields
  internal ChannelFactory<T> _Channel;
  #endregion Internal Fields

  #region Private Fields
  private const int _Trials = 2;

  private ILog _Log = null;
  #endregion Private Fields

  #region Public Methods

  public static T Create(string _DiamondServicesHostName)
  {
    object obj = Create<T,DiamondProxy<T>>();
    var aopp = (DiamondProxy<T>)obj;
    aopp._Channel = DiamondEndPointSetting.GetChannel<T>(_DiamondServicesHostName);

    aopp._Log = AppResolver.GetLogger(typeof(T).Name);

    return (T)obj;
  }

  #endregion Public Methods

  #region Protected Methods

  protected override object Invoke(MethodInfo targetMethod,object[] args)
  {
    var methodName = targetMethod.Name;
    object res = default;
    Exception exception = null;
    var count = 0;
    using var _Decorated = _Channel.CreateChannel();
    do
    {
      try
      {
        //var task = Task.Run(() => targetMethod.Invoke(_Decorated,args));
        //task.Wait();
        //res = task.Result;
        res = targetMethod.Invoke(_Decorated,args);
      }
      catch(Exception ex)
      {
        exception = ex;
      }
      count++;
    } while(exception != null && count < _Trials);
    try
    {
      if(exception != null)
      {
        throw new DiaProxyException("Diamond Service Errors",exception);
      }
      if(res is null)
      {
        throw new DiaProxyException($"Diamond Service Response Is null for [{methodName}], [{targetMethod.ReturnType.FullName}]");
      }
      if(res is ResponseBase responseBase && responseBase.DiamondValidation.HasErrors())
      {
        var errors = responseBase.DiamondValidation.ValidationItems.Select(c => c.ToString()).ToList();
        errors.Insert(0,$"Diamond Service Response Has Errors for [{methodName}], [{targetMethod.ReturnType.FullName}]");
        //throw new DiaProxyException(string.Join("\r\n",errors));
        return res;
      }
    }
    catch(Exception ex)
    {
      if(args.Length > 0 && args[0] is RequestBase requestBase)
      {
        requestBase.DiamondSecurityToken = null;
        requestBase.DiamondTrustedSecurityToken = null;
      }
      _Log.Error(methodName: targetMethod.Name,ex: ex,obj: args);
      throw;
    }
    return res;
  }

  #endregion Protected Methods
}
