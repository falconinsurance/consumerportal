namespace DiamondServices.Service.Models;

public class VersionResult
{
  #region Public Properties
  public int AddFormVersionId { get; set; }

  public int RatingVersionId { get; set; }

  public int UnderwritingVersionId { get; set; }

  public int VersionId { get; set; }
  #endregion Public Properties
}
