using DCS = Diamond.Common.Services;

namespace DiamondServices.Service;

public class BaseSession:ISession
{
  #region Public Properties
  public DiamondSecurityToken DiamondSecurityToken { get; private set; }

  public string DiamondTrustedSecurityToken { get; private set; }

  public int UserCategoryId { get; private set; }
  public int UserId { get; private set; }
  public int UserTypeId { get; private set; }
  #endregion Public Properties
  #region Public Methods

  public void SetSession(DiamondSecurityToken token,string trustedSecurityToken = null)
  {
    DiamondSecurityToken = token;
    DiamondTrustedSecurityToken = trustedSecurityToken ?? DCS.Proxies.ProxyUtility.CreateTokenWithX509SigningCredentials(token);
    UserId = token.DiamUserId;
    UserTypeId = token.UserTypeId;
    UserCategoryId = token.UserCategoryId;
  }

  #endregion Public Methods
}
