namespace DiamondServices.Service;

public interface ISession
{
  #region Public Properties
  DiamondSecurityToken DiamondSecurityToken { get; }
  string DiamondTrustedSecurityToken { get; }
  int UserCategoryId { get; }
  int UserId { get; }
  int UserTypeId { get; }
  #endregion Public Properties

  #region Public Methods

  void SetSession(DiamondSecurityToken token,string trustedSecurityToken = null);

  #endregion Public Methods
}
