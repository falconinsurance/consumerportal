namespace DiamondServices.Service;

public class DiamondException:Exception
{
  #region Public Constructors

  public DiamondException(string message) : base(message)
  {
    Data["Logged"] = true;
  }

  public DiamondException(string message,Exception innerException) : base(message,innerException)
  {
    Data["Logged"] = true;
  }

  #endregion Public Constructors
}
