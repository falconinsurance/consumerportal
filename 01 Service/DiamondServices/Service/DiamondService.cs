using Diamond.Common.Services.Interfaces;
using DiamondServices.Proxy;

namespace DiamondServices.Service;

public class DiamondService:IDiamondService
{
  #region Private Fields
  private readonly RelaxedProxy _Relaxed;
  #endregion Private Fields

  #region Public Constructors

  public DiamondService(string diamondServicesHostNam)
  {
    _Relaxed = new RelaxedProxy(diamondServicesHostNam);
  }

  #endregion Public Constructors

  #region Public Properties
  public IAccountingService Accounting => _Relaxed.Accounting.Value;
  public IAdditionalInterestService AdditionalInterest => _Relaxed.AdditionalInterestService.Value;
  public IAdministrationService Administration => _Relaxed.AdministrationService.Value;
  public IAgencyAdministrationService AgencyAdministration => _Relaxed.AgencyAdministrationService.Value;
  public IAttachmentService Attachment => _Relaxed.AttachmentService.Value;
  public IAutoService Auto => _Relaxed.AutoService.Value;
  public IBillingService Billing => _Relaxed.BillingService.Value;
  public ICheckService Check => _Relaxed.CheckService.Value;
  public IClaimsService Claims => _Relaxed.ClaimsService.Value;
  public ICLMClassService CLMClass => _Relaxed.CLMClassService.Value;
  public IConfigurableBookService ConfigurableBook => _Relaxed.ConfigurableBookService.Value;
  public IContactManagementService ContactManagement => _Relaxed.ContactManagement.Value;
  public ICreditCardService CreditCard => _Relaxed.CreditCard.Value;
  public IDiamondComposerService DiamondComposer => _Relaxed.DiamondComposer.Value;
  public IDMVService DMV => _Relaxed.DMV.Value;
  public IDocumentService Document => _Relaxed.Document.Value;
  public IEFTService EFT => _Relaxed.EFT.Value;
  public IEmployInfoService EmployInfo => _Relaxed.EmployInfo.Value;
  public IEOPProcessService EOPProcess => _Relaxed.EOPProcess.Value;
  public IExperienceModificationImportService ExperienceModificationImport => _Relaxed.ExperienceModificationImport.Value;
  public IFAQService FAQ => _Relaxed.FAQ.Value;
  public IGlobalAdditionalInterestListManagementService GlobalAdditionalInterestListManagement => _Relaxed.GlobalAdditionalInterestListManagement.Value;
  public IIIXService IIXS => _Relaxed.IIXS.Value;
  public ILoginService Login => _Relaxed.Login.Value;
  public ILookupService Lookup => _Relaxed.Lookup.Value;
  public IMerchandiseService Merchandise => _Relaxed.Merchandise.Value;
  public IModifierService Modifier => _Relaxed.Modifier.Value;
  public INewsService News => _Relaxed.News.Value;
  public INotesService Notes => _Relaxed.Notes.Value;
  public IPolicyService Policy => _Relaxed.Policy.Value;
  public IPolicyControlService PolicyControl => _Relaxed.PolicyControl.Value;
  public IPolicyFormService PolicyForm => _Relaxed.PolicyForm.Value;
  public IPortalService Portal => _Relaxed.Portal.Value;
  public IPrintingService Printing => _Relaxed.Printing.Value;
  public IProcessService Process => _Relaxed.Process.Value;
  public IRenewalService Renewal => _Relaxed.Renewal.Value;
  public IReplacementCostService ReplacementCost => _Relaxed.ReplacementCost.Value;
  public IReportService Report => _Relaxed.Report.Value;
  public IResidenceInfoService ResidenceInfo => _Relaxed.ResidenceInfo.Value;
  public IRuleEngineService RuleEngine => _Relaxed.RuleEngine.Value;
  public ISampleService Sample => _Relaxed.Sample.Value;
  public IScheduleRunnerService ScheduleRunner => _Relaxed.ScheduleRunner.Value;
  public ISecurityService Security => _Relaxed.Security.Value;
  public IStaticDataManagerService StaticDataManager => _Relaxed.StaticDataManager.Value;
  public IStatsExportService StatsExport => _Relaxed.StatsExport.Value;
  public ISuppliesService Supplies => _Relaxed.Supplies.Value;
  public ISupportService Support => _Relaxed.Support.Value;
  public DateTime SystemDate => _Relaxed.SystemDate.Value;
  public ISystemEmailService SystemEmail => _Relaxed.SystemEmail.Value;
  public ISystemSettingsService SystemSettings => _Relaxed.SystemSettings.Value;
  public IThirdPartyService ThirdParty => _Relaxed.ThirdParty.Value;
  public ITransactionService Transaction => _Relaxed.Transaction.Value;
  public IUtilityService Utility => _Relaxed.Utility.Value;
  public IVersionService Version => _Relaxed.Version.Value;
  public IVinService Vin => _Relaxed.Vin.Value;
  public IVisualTreeService VisualTree => _Relaxed.VisualTree.Value;
  public IWebSiteService WebSite => _Relaxed.WebSite.Value;
  public IWorkflowService Workflow => _Relaxed.Workflow.Value;
  #endregion Public Properties
}
