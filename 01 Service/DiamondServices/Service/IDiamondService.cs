using Diamond.Common.Services.Interfaces;

namespace DiamondServices.Service
{
  public interface IDiamondService
  {
    #region Public Properties
    IAccountingService Accounting { get; }
    IAdditionalInterestService AdditionalInterest { get; }
    IAdministrationService Administration { get; }
    IAgencyAdministrationService AgencyAdministration { get; }
    IAttachmentService Attachment { get; }
    IAutoService Auto { get; }
    IBillingService Billing { get; }
    ICheckService Check { get; }
    IClaimsService Claims { get; }
    ICLMClassService CLMClass { get; }
    IConfigurableBookService ConfigurableBook { get; }
    IContactManagementService ContactManagement { get; }
    ICreditCardService CreditCard { get; }
    IDiamondComposerService DiamondComposer { get; }
    IDMVService DMV { get; }
    IDocumentService Document { get; }
    IEFTService EFT { get; }
    IEmployInfoService EmployInfo { get; }
    IEOPProcessService EOPProcess { get; }
    IExperienceModificationImportService ExperienceModificationImport { get; }
    IFAQService FAQ { get; }
    IGlobalAdditionalInterestListManagementService GlobalAdditionalInterestListManagement { get; }
    IIIXService IIXS { get; }
    ILoginService Login { get; }
    ILookupService Lookup { get; }
    IMerchandiseService Merchandise { get; }
    IModifierService Modifier { get; }
    INewsService News { get; }
    INotesService Notes { get; }
    IPolicyService Policy { get; }
    IPolicyControlService PolicyControl { get; }
    IPolicyFormService PolicyForm { get; }
    IPortalService Portal { get; }
    IPrintingService Printing { get; }
    IProcessService Process { get; }
    IRenewalService Renewal { get; }
    IReplacementCostService ReplacementCost { get; }
    IReportService Report { get; }
    IResidenceInfoService ResidenceInfo { get; }
    IRuleEngineService RuleEngine { get; }
    ISampleService Sample { get; }
    IScheduleRunnerService ScheduleRunner { get; }
    ISecurityService Security { get; }
    IStaticDataManagerService StaticDataManager { get; }
    IStatsExportService StatsExport { get; }
    ISuppliesService Supplies { get; }
    ISupportService Support { get; }
    DateTime SystemDate { get; }
    ISystemEmailService SystemEmail { get; }
    ISystemSettingsService SystemSettings { get; }
    IThirdPartyService ThirdParty { get; }
    ITransactionService Transaction { get; }
    IUtilityService Utility { get; }
    IVersionService Version { get; }
    IVinService Vin { get; }
    IVisualTreeService VisualTree { get; }
    IWebSiteService WebSite { get; }
    IWorkflowService Workflow { get; }
    #endregion Public Properties
  }
}
