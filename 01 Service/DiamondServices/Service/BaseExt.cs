using System.Globalization;
using Diamond.Common.Objects;

namespace DiamondServices.Service;

public static class BaseExt
{
  #region Public Methods

  public static T ApplySession<T>(this T request,ISession session) where T : RequestBase
  {
    if(request is null)
      throw new ArgumentNullException(nameof(request));
    request.DiamondSecurityToken = session.DiamondSecurityToken;
    request.DiamondTrustedSecurityToken = session.DiamondTrustedSecurityToken ?? Diamond.Common.Services.Proxies.ProxyUtility.CreateTokenWithX509SigningCredentials(session.DiamondSecurityToken);
    return request;
  }

  public static string AsString(this DiamondValidation valdiation)
          => string.Join("\n",valdiation.ValidationItems.Select(x => $"ItemType={x.ItemType}, Message={x.Message}").ToArray());

  public static InsDateTime ToInsDateTime(this DateTime falconDate)
        => new(falconDate);

  public static InsDateTime ToInsDateTime(this DateTime? @this)
       => @this.HasValue ? @this.Value.ToInsDateTime() : new InsDateTime();

  public static bool TryGetDate(this string dateString,out DateTime date) => DateTime.TryParse(dateString,CultureInfo.InvariantCulture,DateTimeStyles.None,out date);

  public static T WriteToTempAsJson<T>(this T obj,string fileName)
  {
    File.WriteAllText(@$"C:\Temp\{fileName}-{DateTime.Now:yyMMdd HHmmssff}.json",obj.SerializeJson());
    return obj;
  }

  #endregion Public Methods
}
