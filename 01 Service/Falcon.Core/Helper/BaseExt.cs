using System.Globalization;

namespace Falcon.Core.Helper;

public static class BaseExt
{
  #region Public Methods

  public static bool TryGetDate(this string dateString,out DateTime date) => DateTime.TryParse(dateString,CultureInfo.InvariantCulture,DateTimeStyles.None,out date);

  #endregion Public Methods
}
