using System.IO;
using Newtonsoft.Json;

namespace Falcon.Core.Settings;

public class ConfigSettings
{
  #region Private Fields

  private static ConfigSettings _Default;
  private string _DbServer;
  private string _DiamondHost;
  private bool? _IsProduction;
  private string _Version;
  private string _WebApiHost;

  #endregion Private Fields

  #region Public Properties

  public static ConfigSettings Default => _Default ??= JsonConvert.DeserializeObject<ConfigSettings>(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\Settings\\appsettings.json"));
  public string DbServer => _DbServer ??= GetDbServer();
  public string DiamondHost => _DiamondHost ??= GetDiamondHost();
  public bool IsLocal { set; get; }
  public bool IsProduction => _IsProduction ??= Server.In(EnLocalServer.Production_01,EnLocalServer.Production_02,EnLocalServer.Production_03);

  public EnLocalServer Server { set; get; }
  public string Version => _Version ??= typeof(ConfigSettings).Assembly.GetName().Version.ToString();
  public string WebApiHost => _WebApiHost ??= GetWebApiHost();

  private const int _PortNumber = 23246;

  #endregion Public Properties

  #region Public Methods

  public string GetDBConnection(EnLocalDatabase enDatabase) => $"Server={DbServer};Database={enDatabase};Trusted_Connection=True;Connection Timeout=300;";

  public string GetDBConnection(string enDatabase) => $"Server={DbServer};Database={enDatabase};Trusted_Connection=True;Connection Timeout=300;";

  public string GetEsignReturnUrl()
  {
    if(IsLocal)
    {
      return "https://localhost:44300/CP/Quoting/DocuSignResp/";
    }
    var url = Server switch
    {
      EnLocalServer.Demo => "https://demo.falconinsgroup.com",
      EnLocalServer.Local => "https://localhost:44300",
      EnLocalServer.Production or EnLocalServer.Production_01 or EnLocalServer.Production_02 or EnLocalServer.Production_03 => "https://consumer.falconinsgroup.com",
      EnLocalServer.SandBox => "https://consumer-sandbox.falconinsgroup.com",
      EnLocalServer.Selenium => "https://consumer-selenium.falconinsgroup.com",
      EnLocalServer.Test01 or EnLocalServer.Test01_01 or EnLocalServer.Test01_02 => "https://consumer-test.falconinsgroup.com/",
      EnLocalServer.Test02 => "https://consumer-test-0002.falconinsgroup.com",
      EnLocalServer.Test03 => "https://consumer-test-0003.falconinsgroup.com",
      _ => "https://localhost:44300",
    };
    return url + "/CP/Quoting/DocuSignResp/";
  }

  public string GetNotificationServiceUrl()
  {
    if(IsLocal)
    {
      return Server == EnLocalServer.Test01 ? "http://192.168.61.87:1086" : "http://localhost:51799";
      //return Server == EnLocalServer.Test02? "http://192.168.61.123:1086" : "http://localhost:51799";
    }
    return Server switch
    {
      EnLocalServer.Demo => "http://192.168.71.103:1086",
      EnLocalServer.Local => "http://localhost:51799",
      EnLocalServer.Production or EnLocalServer.Production_01 or EnLocalServer.Production_02 or EnLocalServer.Production_03 => "http://192.168.41.87:1086",
      EnLocalServer.SandBox => "http://192.168.50.171:1086",
      EnLocalServer.Selenium => "http://192.168.61.87:1086",
      EnLocalServer.Test01 or EnLocalServer.Test01_01 or EnLocalServer.Test01_02 => "http://192.168.61.87:1086",
      EnLocalServer.Test02 => "http://192.168.61.123:1086",
      EnLocalServer.Test03 => "http://192.168.61.133:1086",
      _ => "http://localhost:51799",
    };
  }

  public void LogOption(LogSettings obj)
  {
    obj.Application = "ConsumerService";
    obj.Version = Version;
    if(Environment.UserInteractive)
    {
      obj.LogConsoleSettings = new()
      {
        Active = true,
        LogError = true,
        LogInfo = true,
        LogOther = true,
      };
    }
    if(IsProduction)
    {
      obj.LogEmailSettings = new()
      {
        Active = true,
        LogError = true,
        EnableSsl = true,
        FromAddress = "no-reply@falconinsgroup.com",
        MailAddresses = "TechOperations@falconinsgroup.com",
        Host = "smtp.sendgrid.net",
        Port = 587,
        Password = "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",
        UserName = "apikey",
        UseDefaultCredentials = true
      };
    }
    obj.LogSqlSettings = new()
    {
      Active = true,
      ConnectionStr = GetDBConnection(EnLocalDatabase.AppLog),
      LogError = true,
      LogInfo = true,
      LogOther = true,
    };
    obj.LogFileSettings = new()
    {
      Active = true,
      LogError = true,
      LogInfo = true,
      LogOther = true,
      Folder = @"C:\Logs",
    };
  }

  #endregion Public Methods

  #region Private Methods

  private string GetDbServer() => Server switch
  {
    EnLocalServer.Demo => "D02-MDB-0001",
    EnLocalServer.Local => "S01-MLT-0001",
    EnLocalServer.Production or EnLocalServer.Production_01 or EnLocalServer.Production_02 or EnLocalServer.Production_03 => "P01-LSR-0001",
    EnLocalServer.SandBox => "S01-MLT-0001",
    EnLocalServer.Selenium => "T01-SEL-0001",
    EnLocalServer.Test01 or EnLocalServer.Test01_01 or EnLocalServer.Test01_02 => "T01-LSR-0001",
    EnLocalServer.Test02 => "T02-MDB-0001",
    EnLocalServer.Test03 => "T03-LSR-0001",
    _ => "S01-MLT-0001",
  };

  private string GetDiamondHost()
  {
    var diamondHost = Server switch
    {
      EnLocalServer.Demo => "192.168.71.101",
      EnLocalServer.Local => "192.168.50.154",
      EnLocalServer.Production => "192.168.41.61",
      EnLocalServer.Production_01 => "192.168.41.61",
      EnLocalServer.Production_02 => "192.168.41.71",
      EnLocalServer.Production_03 => "192.168.41.81",
      EnLocalServer.SandBox => "192.168.50.154",
      EnLocalServer.Selenium => "192.168.61.22",
      EnLocalServer.Test01 => "192.168.61.61",
      EnLocalServer.Test01_01 => "192.168.61.61",
      EnLocalServer.Test01_02 => "192.168.61.71",
      EnLocalServer.Test02 => "192.168.61.120",
      EnLocalServer.Test03 => "192.168.61.130",
      _ => "192.168.50.154",
    };
    return $"http://{diamondHost}";
  }

  private string GetWebApiHost()
  {
    if(IsLocal)
      return $"http://localhost:{_PortNumber}";
    var server = Server switch
    {
      EnLocalServer.Demo => "192.168.71.105",
      EnLocalServer.Local => "localhost",
      EnLocalServer.Production => "192.168.41.63",
      EnLocalServer.Production_01 => "192.168.41.63",
      EnLocalServer.Production_02 => "192.168.41.73",
      EnLocalServer.Production_03 => "192.168.41.83",
      EnLocalServer.SandBox => "192.168.50.170",
      EnLocalServer.Selenium => "192.168.61.26",
      EnLocalServer.Test01 => "192.168.61.63",
      EnLocalServer.Test01_01 => "192.168.61.63",
      EnLocalServer.Test01_02 => "192.168.61.73",
      EnLocalServer.Test02 => "192.168.61.125",
      EnLocalServer.Test03 => "192.168.61.135",
      _ => "localhost",
    };
    return $"http://{server}:{_PortNumber}";
  }

  #endregion Private Methods
}
