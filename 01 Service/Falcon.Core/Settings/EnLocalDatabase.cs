namespace Falcon.Core.Settings;

public enum EnLocalDatabase
{
  AppConfig,
  AppLog,
  Consumer,
  DecCarFax,
  DecCommon,
  DecEquifax,
  DecHistory,
  DecHugo,
  DecMVR,
  DecRedMountain,
  Falcon,
  Services,
  Verisk
}
