namespace Falcon.Core.Settings;

public enum EnLocalServer
{
  Default = 0,
  Local,
  Demo,
  Production,
  Production_01,
  Production_02,
  Production_03,
  SandBox,
  Selenium,
  Test01,
  Test01_01,
  Test01_02,
  Test02,
  Test03,
}
