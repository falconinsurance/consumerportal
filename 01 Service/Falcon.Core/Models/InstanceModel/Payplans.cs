namespace Falcon.Core.Models;

public class Payplans
{
  #region Public Properties

  public List<PayplanItem> Collection { get; set; } = [];
  public PayplanItem Current { get; set; } = new();

  #endregion Public Properties
}
