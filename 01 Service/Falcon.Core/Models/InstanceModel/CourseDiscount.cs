namespace Falcon.Core.Models;

public class CourseDiscount:IValidate
{
  #region Public Properties

  public Course AccidentPrevention { get; set; }
  public Course DefensiveDriver { get; set; }
  public Course MatureDriver { get; set; }

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(CourseDiscount),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return new() { Name = nameof(AccidentPrevention),IsValid = AccidentPrevention.Validate(failedItems,nameof(AccidentPrevention)).IsValid };
    yield return new() { Name = nameof(DefensiveDriver),IsValid = DefensiveDriver.Validate(failedItems,nameof(DefensiveDriver)).IsValid };
    yield return new() { Name = nameof(MatureDriver),IsValid = MatureDriver.Validate(failedItems,nameof(MatureDriver)).IsValid };
  }

  #endregion Private Methods
}

public class Course:IValidate
{
  #region Public Properties

  public string Date { get; set; }
  public En_YesNo Value { get; set; }

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => VE.New(nameof(Date)).For(Date,_ => true);

  #endregion Public Methods
}
