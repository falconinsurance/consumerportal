using Diamond.Common.Objects.ESignInterface;
using Falcon.Tools;

namespace Falcon.Core.Models;

public class Documents:IValidate
{
  #region Public Properties

  public List<Document> ESign { get; set; } = [];
  public List<Document> Regular { get; set; } = [];
  public List<Document> WetSign { get; set; } = [];

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Documents),failedItems);

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return new() { Name = nameof(ESign),IsValid = ESign.Validate(nameof(ESign),failedItems).IsValid };
    yield return new() { Name = nameof(Regular),IsValid = Regular.Validate(nameof(Regular),failedItems).IsValid };
    yield return new() { Name = nameof(WetSign),IsValid = WetSign.Validate(nameof(WetSign),failedItems).IsValid };
  }

  #endregion Public Properties
}
