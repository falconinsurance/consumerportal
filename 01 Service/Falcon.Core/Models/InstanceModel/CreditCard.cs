namespace Falcon.Core.Models;

public class CreditCard
{
  #region Public Properties

  public Expiration Expiration { get; set; } = new();
  public string HolderName { get; set; }
  public string Last4 { get; set; }
  public string Number { get; set; }
  public string Token { get; set; }
  public En_PaymentCard Type { get; set; }

  #endregion Public Properties
}
