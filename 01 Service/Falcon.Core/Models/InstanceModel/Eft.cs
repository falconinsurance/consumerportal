namespace Falcon.Core.Models;

public class Eft
{
  #region Public Properties
  public string AccountNumber { get; set; }
  public string FullName { get; set; }
  public string RoutingNumber { get; set; }
  public En_BankAccount Type { get; set; }
  #endregion Public Properties
}
