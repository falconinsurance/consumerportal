namespace Falcon.Core.Models;

public class Insurance:IValidate
{
  #region Public Properties

  public string Company { get; set; }
  public string Number { get; set; }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation().Validate(nameof(Policy),failedItems);

  private IEnumerable<ValidateObj> GetValidation()
  {
    yield return VE.New(nameof(Company)).For(Company,_ => true);
    //yield return new() { Name = nameof(Company),IsValid = ValidateExt.IsValidOrNull(Company) };
    //yield return new() { Name = nameof(Number),IsValid = ValidateExt.IsValidOrNull(Number) };
  }

  #endregion Public Properties
}
