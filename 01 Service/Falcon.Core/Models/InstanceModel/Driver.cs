namespace Falcon.Core.Models;

public class Driver:IValidate
{
  #region Public Properties

  public CourseDiscount CourseDiscount { get; set; } = new();

  //public En_DriverExclusionReason DriverExclusionReason { get; set; }
  public Employment Employment { get; set; } = new();

  public Filing Filing { get; set; } = new();
  public int Id { get; set; }
  public bool IsUDR { get; set; }
  public License License { get; set; }
  public Name Name { get; set; }
  public En_DriverRelationship Relationship { get; set; }
  public En_Driver Type { get; set; }
  public List<Violation> Violations { get; set; } = [];

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return new() { Name = nameof(CourseDiscount),IsValid = CourseDiscount.Validate(failedItems,nameof(CourseDiscount)).IsValid };
    yield return new() { Name = nameof(Employment),IsValid = Employment.Validate(failedItems,nameof(Employment)).IsValid };
    yield return new() { Name = nameof(License),IsValid = License.Validate(failedItems,nameof(License)).IsValid };
    yield return new() { Name = nameof(Name),IsValid = Name.Validate(failedItems,nameof(Name)).IsValid };
    yield return new() { Name = nameof(Violations),IsValid = Violations.Validate(nameof(Violations),failedItems).IsValid };
  }

  #endregion Public Properties
}

public class Filing
{
  #region Public Properties

  public bool IsSR22 { get; set; }
  public bool IsSR50 { get; set; }

  #endregion Public Properties
}

public class Employment:IValidate
{
  #region Public Properties

  public string Employer { get; set; }
  public string Occupation { get; set; }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation().Validate(nameof(Employment),failedItems);

  #endregion Public Properties

  private IEnumerable<ValidateObj> GetValidation()
  {
    yield return VE.New(nameof(Employer)).For(Employer,_ => true);
    //yield return new() { Name = nameof(Employer),IsValid = ValidateExt.IsValidOrNull(Employer) };
    //yield return new() { Name = nameof(Occupation),IsValid = ValidateExt.IsValidOrNull(Occupation) };
  }
}
