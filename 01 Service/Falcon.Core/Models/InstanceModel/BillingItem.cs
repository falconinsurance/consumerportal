using System.Linq;
using DCO = Diamond.Common.Objects;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

namespace Falcon.Core.Models;

public class BillingItem
{
  #region Public Properties
  public decimal Down { get; set; }
  public decimal Installment { get; set; }
  public decimal Total { get; set; }

  public void Build(DCO.Billing.PayPlanPreview diaPayplan,En_State state,decimal sR22Fee)
  {
    Down = diaPayplan.DownPaymentAmount;
    switch(state)
    {
      case En_State.AZ:
      case En_State.CO:
        Down = diaPayplan.DownPaymentAmount - sR22Fee;
        break;
      case En_State.IL:
      case En_State.IN:
      case En_State.OK:
      case En_State.TX:
      case En_State.UT:
        break;
      default:
        break;
    }
    Installment = diaPayplan.TotalInstallmentAmount;
    Total = diaPayplan.TotalPremiumFeeServiceChargeAmount;
  }
  #endregion Public Properties
}
