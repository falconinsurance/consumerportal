namespace Falcon.Core.Models;

public class Address:IValidate
{
  #region Public Properties

  public string City { get; set; }
  public string County { get; set; }
  public En_State State { get; set; }
  public string Street { get; set; }
  public string Zip { get; set; }

  public string GetFull() => $"{Street}, {City}, {State} {Zip}";

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation().Validate(nameof(Address),failedItems);

  private IEnumerable<ValidateObj> GetValidation()
  {
    yield return VE.New(nameof(City)).For(City,_ => true);
    //yield return new() { Name = nameof(City),IsValid = ValidateExt.IsValidOrNull(City) };
    //yield return new() { Name = nameof(County),IsValid = ValidateExt.IsValidOrNull(County) };
    //yield return new() { Name = nameof(Street),IsValid = ValidateExt.IsValidOrNull(Street) };
    //yield return new() { Name = nameof(Zip),IsValid = ValidateExt.IsValidOrNull(Zip) };
  }

  #endregion Public Properties
}
