namespace Falcon.Core.Models;

public class Policy:IValidate
{
  #region Public Properties

  public PolicyCoverages Coverages { get; set; }
  public Documents Documents { get; set; } = new();
  public string EffectiveDate { get; set; }
  public string ExpirationDate { get; set; }
  public int Id { get; set; }
  public int ImageNum { get; set; }
  public Modifiers Modifiers { get; set; }
  public string Number { get; set; }
  //public PaymentInfo PaymentInfo { get; set; }
  public PaymentInfo PaymentInfo { get; set; } = new();
  public int RatingVersionId { get; set; }
  public En_TermLength Term { get; set; } = En_TermLength.SixMonths;
  public En_Policy Type { get; set; }
  public List<UnderwritingResponse> UnderwritingResponses { get; set; } = [];
  public int VersionId { get; set; }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Policy),failedItems);

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return VE.New(nameof(Documents)).For(Documents,_ => true);
    //yield return new() { Name = nameof(Documents),IsValid = Documents.Validate(failedItems,nameof(Documents)).IsValid };
    //yield return new() { Name = nameof(EffectiveDate),IsValid = EffectiveDate.TryGetDate(out var effectiveDate) && ValidateExt.IsValid(effectiveDate) };
    //yield return new() { Name = nameof(ExpirationDate),IsValid = ExpirationDate.TryGetDate(out var expirationDate) && ValidateExt.IsValid(expirationDate) };
    //yield return new() { Name = nameof(Modifiers),IsValid = Modifiers.Validate(failedItems,nameof(Modifiers)).IsValid };
    //yield return new() { Name = nameof(Number),IsValid = ValidateExt.IsValidOrNull(Number) };
    //yield return new() { Name = nameof(UnderwritingResponses),IsValid = UnderwritingResponses.Validate(nameof(UnderwritingResponses),failedItems).IsValid };
  }

  #endregion Public Properties
}
