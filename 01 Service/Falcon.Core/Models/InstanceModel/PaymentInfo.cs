namespace Falcon.Core.Models;

public class PaymentInfo
{
  #region Public Properties

  public Payplans Payplans { get; set; } = new();
  public Payment RecurringPayment { get; set; }

  #endregion Public Properties
}
