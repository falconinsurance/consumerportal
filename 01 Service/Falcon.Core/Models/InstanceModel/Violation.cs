namespace Falcon.Core.Models;

public class Violation:IValidate
{
  #region Public Properties
  public string Date { get; set; }

  public int Id { get; set; } = -1;

  public En_Violation Type { get; set; }
  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => VE.New(nameof(Date)).For(Date,_ => true);

  #endregion Public Methods
}
