namespace Falcon.Core.Models;

public class PolicyCoverages
{
  #region Private Fields

  private static readonly Dictionary<En_State,PolicyCoverages> _DicPolicyCoverages = new()
  {
    [En_State.AZ] = new()
    {
      BI = En_BILimit.TwentyFiveFifty,
      MP = En_MPLimit.None,
      PD = En_PDLimit.Fifteen,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.Rejected,
      UIMBI = En_UIMBILimit.Rejected,
    },
    [En_State.CO] = new()
    {
      BI = En_BILimit.TwentyFiveFifty,
      MP = En_MPLimit.Rejected,
      PD = En_PDLimit.Fifteen,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.Rejected,
      UIMBI = En_UIMBILimit.None,
    },
    [En_State.IL] = new()
    {
      BI = En_BILimit.TwentyFiveFifty,
      MP = En_MPLimit.None,
      PD = En_PDLimit.Twenty,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.TwentyFiveFifty,
      UIMBI = En_UIMBILimit.None,
    },
    [En_State.IN] = new()
    {
      BI = En_BILimit.TwentyFiveFifty,
      MP = En_MPLimit.None,
      PD = En_PDLimit.TwentyFive,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.None,
      UIMBI = En_UIMBILimit.None,
    },
    [En_State.OK] = new()
    {
      BI = En_BILimit.TwentyFiveFifty,
      MP = En_MPLimit.None,
      PD = En_PDLimit.TwentyFive,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.None,
      UIMBI = En_UIMBILimit.None,
    },
    [En_State.TX] = new()
    {
      BI = En_BILimit.ThirtySixty,
      MP = En_MPLimit.None,
      PD = En_PDLimit.TwentyFive,
      PIP = En_PIPLimit.None,
      UMBI = En_UMBILimit.None,
      UIMBI = En_UIMBILimit.None,
    },
    [En_State.UT] = new()
    {
      BI = En_BILimit.TwentyFiveSixtyFive,
      MP = En_MPLimit.None,
      PD = En_PDLimit.Fifteen,
      PIP = En_PIPLimit.ThreeThousand,
      UMBI = En_UMBILimit.Rejected,
      UIMBI = En_UIMBILimit.Rejected,
      WorkLossBenefitsWaiver = En_YesNo.No
    },
  };

  #endregion Private Fields

  #region Public Properties

  public En_BILimit BI { set; get; }
  public En_MPLimit MP { set; get; }
  public En_PDLimit PD { set; get; }
  public En_PIPLimit PIP { set; get; }
  public En_SR22Payment SR22 { get; set; }
  public bool SR50 { get; set; }
  public En_UIMBILimit UIMBI { set; get; }
  public En_UMBILimit UMBI { set; get; }
  public En_YesNo WorkLossBenefitsWaiver { get; set; }

  public PolicyCoverages SetGetDefaults(En_State state)
  {
    var p = GetDefaultCoverage(state);
    BI = p.BI;
    MP = p.MP;
    PD = p.PD;
    PIP = p.PIP;
    SR22 = p.SR22;
    SR50 = p.SR50;
    UIMBI = p.UIMBI;
    UMBI = p.UMBI;
    WorkLossBenefitsWaiver = p.WorkLossBenefitsWaiver;
    return this;
  }

  #endregion Public Properties

  #region Public Methods

  public static PolicyCoverages GetDefaultCoverage(En_State state) => _DicPolicyCoverages.TryGetValue(state,out PolicyCoverages coverage) ? coverage : new();

  #endregion Public Methods
}
