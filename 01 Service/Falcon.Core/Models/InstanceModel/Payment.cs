namespace Falcon.Core.Models;

public class Payment
{
  #region Public Properties

  public decimal Amount { get; set; }
  public CreditCard CreditCard { get; set; }
  public Eft Eft { get; set; }
  public string Source { get; set; }
  public En_Payment Type { get; set; } = En_Payment.NoVal;

  #endregion Public Properties
}
