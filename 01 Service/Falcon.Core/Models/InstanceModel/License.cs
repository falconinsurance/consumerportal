namespace Falcon.Core.Models;

public class License:IValidate
{
  #region Public Properties
  public En_YesNo Months36 { get; set; }
  public string Number { get; set; }
  public En_State State { get; set; }
  public En_License Type { get; set; }
  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => VE.New(nameof(Number)).For(Number,_ => true);

  #endregion Public Methods
}
