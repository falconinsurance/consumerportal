namespace Falcon.Core.Models;

public enum En_AnswerValue
{
  Default = 0,
  Yes = 1,
  No = -1,
}

public class UnderwritingResponse:IValidate
{
  #region Public Properties
  public string Answer { get; set; }
  public int DiamondId { get; set; }
  public En_AnswerValue Value { get; set; }
  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => VE.New(nameof(Answer)).For(Answer,_ => true);

  #endregion Public Methods
}
