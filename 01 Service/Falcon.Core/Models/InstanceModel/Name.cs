namespace Falcon.Core.Models;

public class Name:IValidate
{
  #region Public Properties

  public string DateOfBirth { get; set; }
  public string First { get; set; }
  public string Full => !(string.IsNullOrEmpty(Middle) && string.IsNullOrWhiteSpace(Middle)) ? $"{First} {Middle} {Last}" : $"{First} {Last}";
  public En_Gender Gender { get; set; }
  public string Last { get; set; }
  public En_MaritalStatus MaritalStatus { get; set; }
  public string Middle { get; set; }

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation().Validate(nameof(Name),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation()
  {
    yield return VE.New(nameof(DateOfBirth)).For(DateOfBirth,_ => true);
    //yield return new() { Name = nameof(DateOfBirth),IsValid = DateOfBirth.TryGetDate(out var date) && ValidateExt.IsValid(date) };
    //yield return new() { Name = nameof(First),IsValid = ValidateExt.IsValidOrNull(First) };
    //yield return new() { Name = nameof(Last),IsValid = ValidateExt.IsValidOrNull(Last) };
    //yield return new() { Name = nameof(Middle),IsValid = ValidateExt.IsValidOrNull(Middle) };
  }

  #endregion Private Methods
}
