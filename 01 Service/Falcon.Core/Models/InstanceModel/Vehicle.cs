using System.Text.RegularExpressions;
using Falcon.Tools.VehicleInfoCache;

namespace Falcon.Core.Models;

public class Vehicle:IValidate
{
  #region Public Properties

  public IdDescEntity BodyType { get; set; }
  public VehicleCoverages Coverages { get; set; }
  public int Id { get; set; }
  public IdDescEntity Make { get; set; }
  public IdDescEntity Model { get; set; }
  public int OdometerReading { get; set; }
  public bool PhotoDiscount { get; set; }
  public En_VehicleUse PrimaryUse { get; set; }
  public IdDescEntity Symbol { get; set; }
  public string Vin { get; set; }
  public int Year { get; set; }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => new()
  {
    Name = nameof(Vin),
    IsValid = string.IsNullOrEmpty(Vin) || Regex.IsMatch(Vin,"(?i)^([A-Z0-9&]{10,11}|[A-Z0-9&]{17})$")
  };

  #endregion Public Properties
}
