namespace Falcon.Core.Models;

public class Modifiers:IValidate
{
  #region Public Properties

  public En_YesNo DirectBillDiscount { get; set; } = En_YesNo.No;
  public En_YesNo HomeownersDiscount { get; set; } = En_YesNo.No;
  public En_YesNo ReceiveElectronicDocuments { get; set; } = En_YesNo.No;
  public TransferDiscount TransferDiscount { get; set; } = new();

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => new()
  {
    Name = nameof(TransferDiscount),
    IsValid = TransferDiscount.Validate(failedItems,nameof(TransferDiscount)).IsValid
  };

  #endregion Public Methods
}

public class TransferDiscount:IValidate
{
  #region Public Properties

  public string CompanyName { get; set; }
  public string ExpirationDate { get; set; }
  public En_YesNo Flag { get; set; } = En_YesNo.No;
  public string PolicyNumber { get; set; }

  #endregion Public Properties

  #region Public Methods

  public void SetObj(string policyNumber,string companyName,DateTime dateTime)
  {
    Flag = En_YesNo.Yes;
    PolicyNumber = policyNumber;
    CompanyName = companyName;
    if(dateTime != default)
    {
      ExpirationDate = dateTime.ToString("MM/dd/yyyy");
    }
  }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return VE.New(nameof(CompanyName)).For(CompanyName,_ => true);
    //yield return new() { Name = nameof(CompanyName),IsValid = ValidateExt.IsValidOrNull(CompanyName) };
    //yield return new() { Name = nameof(ExpirationDate),IsValid = ExpirationDate.TryGetDate(out var date) && ValidateExt.IsValid(date) };
    //yield return new() { Name = nameof(PolicyNumber),IsValid = ValidateExt.IsValidOrNull(PolicyNumber) };
  }

  #endregion Private Methods
}
