using Diamond.Common.Objects.Modifiers;

namespace Falcon.Core.Models;

public class VehicleCoverages
{
  #region Private Fields

  private static readonly Dictionary<En_State,VehicleCoverages> _DicVehicleCoverages = new()
  {
    [En_State.AZ] = new()
    {
      SafetyEquipment = En_YesNo.No
    },
    [En_State.CO] = new()
    {
      UMPD = En_UMPDLimit.Rejected,
    },
    [En_State.IL] = new()
    {
      UMPD = En_UMPDLimit.None,
    },
    [En_State.IN] = new()
    {
      UMPD = En_UMPDLimit.None,
      UMPDDeductible = En_UMPDDeductible.None,
    },
    [En_State.OK] = new()
    {
    },
    [En_State.TX] = new()
    {
      UMPD = En_UMPDLimit.None,
    },
    [En_State.UT] = new()
    {
      UMPD = En_UMPDLimit.None,
    },
  };

  #endregion Private Fields

  #region Public Properties

  public En_ComColDeductible ComColDeductible { get; set; } = En_ComColDeductible.NoInsurance;
  public En_Rental Rental { get; set; } = En_Rental.None;
  public En_YesNo SafetyEquipment { get; set; } = En_YesNo.No;
  public En_YesNo Towing { get; set; } = En_YesNo.No;
  public En_UMPDLimit UMPD { get; set; } = En_UMPDLimit.None;
  public En_UMPDDeductible UMPDDeductible { get; set; } = En_UMPDDeductible.None;

  #endregion Public Properties

  #region Public Methods
  public VehicleCoverages SetGetDefaults(En_State state)
  {
    var p = GetDefaultCoverage(state);
    ComColDeductible = p.ComColDeductible;
    Rental = p.Rental;
    SafetyEquipment = p.SafetyEquipment;
    Towing = p.Towing;
    UMPD = p.UMPD;
    UMPDDeductible = p.UMPDDeductible;
    return this;
  }
  public static VehicleCoverages GetDefaultCoverage(En_State state) => _DicVehicleCoverages.TryGetValue(state,out VehicleCoverages coverage) ? coverage : new();

  #endregion Public Methods
}
