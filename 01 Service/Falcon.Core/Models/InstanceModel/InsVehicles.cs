namespace Falcon.Core.Models;

public class InsVehicles:IValidate
{
  #region Public Properties

  public Address Address { get; set; }
  public List<Vehicle> Vehicles { get; set; } = [];

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return new() { Name = nameof(Address),IsValid = Address is null || Address.Validate(failedItems,nameof(Address)).IsValid };
    yield return new() { Name = nameof(Vehicles),IsValid = Vehicles.Validate(nameof(Vehicles),failedItems).IsValid };
  }

  #endregion Private Methods
}
