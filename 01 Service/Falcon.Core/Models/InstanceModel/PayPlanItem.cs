using Diamond.Common.Objects.Billing;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

namespace Falcon.Core.Models;

public class PayplanItem
{
  #region Public Properties

  public BillingItem Amount { get; set; } = new();
  public string Description { get; set; }
  public int Id { get; set; }
  public int Installments { get; set; }
  public En_Payment PaymentType { get; set; }
  public string Tag { get; set; }
  public En_PayPlan Type { get; set; }

  #endregion Public Properties

  #region Private Properties

  private BillingItem AgencyFee { get; } = new();
  private BillingItem COTPAFee { get; } = new();
  private BillingItem MVCPAFee { get; } = new();
  private BillingItem PolicyFee { get; } = new();
  private BillingItem SR22Fee { get; } = new();
  private BillingItem TheftProtectionFee { get; } = new();

  #endregion Private Properties

  #region Public Methods

  public void Build(PayPlanPreview diaPayplan,En_PayPlan type,int payplanTypeId,Data diaBillingData,En_State state)
  {
    SetFees(diaBillingData,state.GetDiaId());
    //TODO: Add amount logic based on states
    Amount.Build(diaPayplan,state,SR22Fee.Down);
    Id = diaPayplan.PayPlanId;
    Installments = diaPayplan.NumInstalls - 1;
    PaymentType = GetPaymentType(payplanTypeId);
    Type = type;
    Tag = PaymentType == En_Payment.CreditCard || PaymentType == En_Payment.ACH ? "Recurring" : Type == En_PayPlan.PayInFull ? nameof(En_PayPlan.PayInFull) : "Installments";
  }

  #endregion Public Methods

  #region Private Methods

  private static En_Payment GetPaymentType(int id) => id switch
  {
    1 => En_Payment.CreditCard,
    2 => En_Payment.ACH,
    _ => En_Payment.AgencySweep,
  };

  private void SetFees(Data billingData,int state)
  {
    var isEftReoccuringPayPlan = billingData.Futures.Any(f => string.Equals(f.Description,"installment 1",StringComparison.InvariantCultureIgnoreCase));

    SetFee(MVCPAFee,(int)FDEnums.BillingChargesCreditsType.ABTPAFee,(int)FDEnums.BillingChargesCreditsType.MVCPAFee,state,isEftReoccuringPayPlan);
    SetFee(PolicyFee,(int)FDEnums.BillingChargesCreditsType.PolicyFee,(int)FDEnums.BillingChargesCreditsType.ServiceCharge,state,isEftReoccuringPayPlan); // Policy Fees or Installment Fees
    SetFee(SR22Fee,(int)FDEnums.BillingChargesCreditsType.SR22Fee,-1,state,isEftReoccuringPayPlan);
    SetFee(AgencyFee,(int)FDEnums.BillingChargesCreditsType.AgencyFee,-1,state,isEftReoccuringPayPlan);
    SetFee(TheftProtectionFee,(int)FDEnums.BillingChargesCreditsType.TheftProtectionFee,-1,state,isEftReoccuringPayPlan);
    SetFee(COTPAFee,(int)FDEnums.BillingChargesCreditsType.COTPAFee,-1,state,isEftReoccuringPayPlan);

    void SetFee(BillingItem fee,int fee1,int fee2,int stateId,bool isEftReoccuringPayPlan)
    {
      var indexOfInstallment2 = billingData.Futures.FindIndex(x => x.Description == "Installment 2");
      var installmentsList = new List<Future>(billingData.Futures.ToList().Skip(indexOfInstallment2));
      var feeInstallment = installmentsList.Where(i => i.BillingChargesCreditsTypeId == fee1 || i.BillingChargesCreditsTypeId == fee2)
          .Take(1)
          .Select(x => x.Amount)
          .Sum();

      decimal feeDown;
      if(isEftReoccuringPayPlan || (stateId == 15) /*IL*/)
      {
        feeDown = billingData.Futures.OrderBy(x => x.BillingChargesCreditsTypeId)
        .Where(i => i.BillingChargesCreditsTypeId == fee1)
        .Take(1)
        .Select(x => x.Amount)
        .Sum();
      }
      else /*TX or IN and not EFT*/
      {
        feeDown = billingData.Statements
            .Where(i => (i.BillingChargesCreditsTypeId == fee1 || i.BillingChargesCreditsTypeId == fee2) && !i.BilledAmount.IsNullOrWhiteSpace())
            .Select(x => decimal.Parse(x.BilledAmount))
            .Sum();
      }

      var feeTotal = billingData.Statements
          .Where(i => (i.BillingChargesCreditsTypeId == fee1 || i.BillingChargesCreditsTypeId == fee2) && !i.BilledAmount.IsNullOrWhiteSpace())
          .Select(x => decimal.Parse(x.BilledAmount))
          .Concat(billingData.Futures.Where(i => i.BillingChargesCreditsTypeId == fee1 || i.BillingChargesCreditsTypeId == fee2).Select(i => i.Amount))
          .Sum();

      fee.Installment = feeInstallment;
      fee.Down = feeDown;
      fee.Total = feeTotal;
    }

    MVCPAFee.Installment = 0;
    AgencyFee.Installment = 0;
  }

  #endregion Private Methods
}

//using Diamond.Common.Objects.Billing;
//using Diamond.Common.Objects.Policy;
//using DCE = Diamond.Common.Enums;
//using FDEnums = Diamond.C0057.Common.Library.Enumerations;

//namespace Falcon.Core.Models;

//public class PayPlanItem
//{
//  #region Public Properties
//  public decimal DownpaymentPercent { get; set; }
//  public int NumberOfPayments { get; set; }

//  //public En_Payment RecurringPayment { get; set; } = En_Payment.NoVal;
//  public En_PayPlan PayPlan { get; set; } = En_PayPlan.NoVal;

//  public BillingItem MVCPAFee { get; set; } = new();
//  public BillingItem COTPAFee { get; set; } = new();
//  public BillingItem AgencyFee { get; set; } = new();
//  public BillingItem PolicyFee { get; set; } = new();
//  public BillingItem SR22Fee { get; set; } = new();
//  public BillingItem Premium { get; set; } = new();
//  public BillingItem TheftProtectionFee { get; set; } = new();
//  public BillingItem TotalFees { get; set; } = new();
//  #endregion Public Properties

//  #region Internal Methods

//  public void setObj(Image image,Data billingData,En_State state,int numInstalls)
//  {
//    SetFees(image,billingData,state.GetDiaId());
//    SetPremium(image,billingData,state);
//    NumberOfPayments = numInstalls;
//    PayPlan = PayPlanType.FromDiaId(image.CurrentPayplanId);
//    //RecurringPayment = PaymentType.FromDiaId(image.CurrentPayplanId);
//  }

//  #endregion Internal Methods

//  #region Private Methods

//  private void SetPremium(Image image,Data billingData,En_State state)
//  {
//    Premium.Total = image?.WrittenPremium ?? 0;

//    // If EFT Reoccuring and IL
//    if(billingData.Futures.Any(f => string.Equals(f.Description,"installment 1",StringComparison.InvariantCultureIgnoreCase)) && image != null && state == En_State.IL)
//    {
//      Premium.Down = billingData.Futures.DefaultIfEmpty(new Future()).First(i => string.Equals(i.Description,"installment 1",StringComparison.OrdinalIgnoreCase)).Amount;
//    }
//    else
//    {
//      var premiumDown = billingData.Statements.FirstOrDefault(i => string.Equals(i.Description,"installment 1",StringComparison.OrdinalIgnoreCase) && !i.BilledAmount.IsNullOrWhiteSpace());
//      Premium.Down = premiumDown != null ? decimal.Parse(premiumDown.BilledAmount) : 0;
//    }

//    var premiumInstallment = billingData.Futures.FirstOrDefault(i => i.BillingChargeTypeCategoryId == (int)DCE.Billing.BillingChargeTypeCategory.Premium);
//    Premium.Installment = premiumInstallment?.Amount ?? 0;
//  }

//  #endregion Private Methods
//}
