namespace Falcon.Core.Models;

public class Phone:IValidate
{
  #region Public Properties

  public string Number { get; set; }
  public En_PhoneNumber Type { get; set; }

  #endregion Public Properties

  #region Public Methods

  public string GetConvertedNumber()
  {
    if(!string.IsNullOrEmpty(Number))
    {
      var p = Number.Replace("(","").Replace(")","").Replace("-","").Replace(" ","");
      return p;
    }
    return Number;
  }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => VE.New(nameof(Number)).For(Number,_ => true);

  #endregion Public Methods
}
