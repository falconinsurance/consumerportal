namespace Falcon.Core.Models;

public class Expiration
{
  #region Public Properties
  public string Date => $"{Month:D2}/{Year}";
  public int Month { get; set; }
  public int Year { get; set; }
  #endregion Public Properties

  #region Public Methods

  public void SetObj(string expirationDate)
  {
    if(expirationDate.Contains("/"))
    {
      var expirationParts = expirationDate.Split(new[] { '/' });
      Month = int.Parse(expirationParts[0]);
      Year = int.Parse(expirationParts[1]);
    }
  }

  #endregion Public Methods
}
