namespace Falcon.Core.Models;

public class PolicyHolder:IValidate
{
  #region Public Properties

  public Address Address { get; set; }
  public string Email { get; set; }

  //TODO: Check TransferDiscount and add it to image
  public Insurance Insurance { get; set; }

  public Name Name { get; set; }
  public List<Phone> Phones { get; set; } = [];

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return VE.New(nameof(Address)).For(Address,_ => true);
    //yield return new() { Name = nameof(Address),IsValid = Address.Validate(failedItems,nameof(Address)).IsValid };
    //yield return new() { Name = nameof(Email),IsValid = ValidateExt.IsValidEmailOrNull(Email) };
    //yield return new() { Name = nameof(Insurance),IsValid = Insurance is null || Insurance.Validate(failedItems,nameof(Insurance)).IsValid };
    //yield return new() { Name = nameof(Name),IsValid = Name.Validate(failedItems,nameof(Name)).IsValid };
    //yield return new() { Name = nameof(Phones),IsValid = Phones.Validate(nameof(Phones),failedItems).IsValid };
  }

  #endregion Public Properties
}
