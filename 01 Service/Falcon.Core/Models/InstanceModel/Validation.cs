namespace Falcon.Core.Models;

public class Validation
{
  #region Public Properties
  public HashSet<string> Errors { get; set; } = [];
  public bool HasErrors => Errors.Count > 0;
  public bool HasWarnings => Warnings.Count > 0;
  public HashSet<string> Warnings { get; set; } = [];
  #endregion Public Properties
}
