namespace Falcon.Core.Models;

public class DrvsViolations
{
  #region Public Properties

  public bool AnyDriver
  {
    get => Drivers.Any(c => c.AnyViolation);
    private set => _ = value;
  }

  public List<DrvViolationItem> Drivers { get; set; } = [];
  public string Key { get; set; }
  public string Status { get; set; }

  #endregion Public Properties
}

public class DrvViolation
{
  #region Public Properties

  public decimal Amount { get; set; }
  public string CarrierName { get; set; }
  public string CarrierNameShort { get; set; }
  public string ClaimNumber { get; set; }
  public string ClaimNumberShort { get; set; }
  public string Date { get; set; }
  public string Status { get; set; }
  public string Type { get; set; }

  #endregion Public Properties
}

public class DrvViolationItem
{
  #region Public Properties

  public bool AnyViolation
  {
    get => Violations.Count > 0;
    private set => _ = value;
  }

  public string DOB { get; set; }
  public string First { get; set; }
  public string Last { get; set; }
  public List<DrvViolation> Violations { get; set; } = [];

  #endregion Public Properties
}
