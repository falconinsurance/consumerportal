namespace Falcon.Core.Models;

public class Document:IValidate
{
  #region Public Properties

  public string Description { get; set; }
  public int FormCategoryTypeId { get; set; }
  public int FormId { get; set; }
  public string FormNumber { get; set; }
  public string KeyValue { get; set; }
  public int PolicyId { get; set; }
  public int PolicyImageNumber { get; set; }
  public string Recipient { get; set; }
  public string Version { get; set; }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation().Validate(nameof(Document),failedItems);

  private IEnumerable<ValidateObj> GetValidation()
  {
    yield return VE.New(nameof(Description)).For(Description,_ => true);

    //yield return new() { Name = nameof(Description),IsValid = ValidateExt.IsValidOrNull(Description) };
    //yield return new() { Name = nameof(FormNumber),IsValid = ValidateExt.IsValidOrNull(FormNumber) };
    //yield return new() { Name = nameof(KeyValue),IsValid = ValidateExt.IsValidOrNull(KeyValue) };
    //yield return new() { Name = nameof(Recipient),IsValid = ValidateExt.IsValidOrNull(Recipient) };
    //yield return new() { Name = nameof(Version),IsValid = ValidateExt.IsValidOrNull(Version) };
  }

  #endregion Public Properties
}
