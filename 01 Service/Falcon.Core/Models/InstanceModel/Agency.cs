namespace Falcon.Core.Models;

public class Agency
{
  #region Public Properties
  public int Id { get; set; }
  public int ProducerId { get; set; }
  public En_State State { get; set; }
  #endregion Public Properties
}
