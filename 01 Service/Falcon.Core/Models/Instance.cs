namespace Falcon.Core.Models;

public class Instance:IValidate
{
  #region Public Properties

  public Agency Agency { get; set; } = new();

  //external types common between Instance and Decisioning
  public AttachedTypes AttachedTypes { get; set; } = new();

  public List<Driver> Drivers { get; set; } = [];
  public InsVehicles InsVehicles { get; set; } = new();
  public Policy Policy { get; set; } = new();
  public PolicyHolder PolicyHolder { get; set; } = new();
  public string RiskCheck { get; set; }
  public Validation Validation { get; set; } = new();

  #endregion Public Properties

  #region Public Methods

  public void SetSR22()
  {
    Policy.Coverages ??= PolicyCoverages.GetDefaultCoverage(Agency.State);
    Policy.Coverages.SR22 = En_SR22Payment.SR22Yes;
    Drivers[0].Filing ??= new();
    Drivers[0].Filing.IsSR22 = true;
  }

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {

    yield return VE.New(nameof(Drivers)).For(Drivers,_ => true);
    //yield return new() { Name = nameof(Drivers),IsValid = Drivers.Validate(nameof(Drivers),failedItems).IsValid };
    //yield return new() { Name = nameof(Policy),IsValid = Policy.Validate(failedItems,nameof(Policy)).IsValid };
    //yield return new() { Name = nameof(PolicyHolder),IsValid = PolicyHolder.Validate(failedItems,nameof(PolicyHolder)).IsValid };
    //yield return new() { Name = nameof(RiskCheck),IsValid = ValidateExt.IsValidOrNull(RiskCheck) };
    //yield return new() { Name = nameof(InsVehicles),IsValid = InsVehicles.Validate(failedItems,nameof(InsVehicles)).IsValid };
  }

  #endregion Private Methods
}
