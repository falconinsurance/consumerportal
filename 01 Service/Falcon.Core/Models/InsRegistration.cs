namespace Falcon.Core.Models;

public class InsRegistration:IValidate
{
  #region Public Properties
  public string Email { get; set; }
  public Name Name { get; set; }
  public string Password { get; set; }
  public string PolicyNumber { get; set; }
  public string ZipCode { get; set; }

  #endregion Public Properties

  #region Public Methods

  public ValidateObj Validate(List<string> failedItems,string parent = "") => GetValidation(failedItems).Validate(nameof(Instance),failedItems);

  #endregion Public Methods

  #region Private Methods

  private IEnumerable<ValidateObj> GetValidation(List<string> failedItems)
  {
    yield return VE.New(nameof(Email)).For(Email,c => VE.IsRequired(c) && VE.IsEmail(c));
    yield return VE.New(nameof(Password)).For(Password,VE.IsRequired);
    yield return VE.New(nameof(PolicyNumber)).For(PolicyNumber,VE.IsRequired);
    yield return VE.New(nameof(ZipCode)).For(ZipCode,VE.IsZipCode);
    yield return VE.New(nameof(Name)).For(Name,VE.Validate(failedItems,nameof(InsRegistration)));
  }

  #endregion Private Methods
}
