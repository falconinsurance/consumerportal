namespace ConsumerServiceHost.Models;

public class PaymentProfile
{
  public decimal Amount { get; internal set; }
  public int ImageNum { get; internal set; }
  public En_Payment PaymentType { get; internal set; }
  public int PolicyId { get; internal set; }
  public string Source { get; internal set; }
  public bool Success { get; internal set; }
  public DateTime DateAdded { get; internal set; }
  public string Method { get; internal set; }
}
