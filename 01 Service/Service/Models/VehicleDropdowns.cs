using Falcon.Core.Models;
using Falcon.Tools.VehicleInfoCache;

namespace ConsumerServiceHost.Models;

public class VehicleDropdowns
{
  #region Public Properties
  public IdDescEntity[] BodyTypes { get; set; }
  public IdDescEntity[] Makes { get; set; }
  public IdDescEntity[] Models { get; set; }
  public IdDescEntity[] Symbols { get; set; }
  public Vehicle Vehicle { get; set; }
  #endregion Public Properties
}
