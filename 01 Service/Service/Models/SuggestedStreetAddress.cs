using ConsumerServiceHost.Apis;
using Falcon.Core.Models;
using Falcon.Service.SmartyStreets;

namespace ConsumerServiceHost.Models;

public class SuggestedAddress
{
  public SuggestedAddress()
  {
  }

  public SuggestedAddress(CorrectedStreetAddress address)
  {
    Address = address.CorrectedAddress.ToConsumerAddress();
    IsResidentialAddress = address.IsResidentialAddress;
    MissingOrWrongSecondaryInfo = address.MissingOrWrongSecondaryInfo;
    ValidAddressWithUnknownApt = address.ValidAddressWithUnknownApt;
    WasSuccess = address.WasSuccess;
  }

  public Address Address { get; set; }
  public bool IsResidentialAddress { get; set; }
  public bool MissingOrWrongSecondaryInfo { get; set; }
  public bool ValidAddressWithUnknownApt { get; set; }
  public bool WasSuccess { get; set; }
}
