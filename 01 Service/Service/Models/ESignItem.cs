using DoucuSignCommon.Model;

namespace ConsumerServiceHost.Models;

public class ESignItem
{
  #region Public Properties

  public Guid ClientUserId { get; set; }

  public string Description { get; set; }

  public string Name { get; set; }

  public string Signer { get; set; }

  public string Status { get; set; }

  public string Url { get; set; }

  #endregion Public Properties

  #region Public Constructors

  public ESignItem(SignerInfo signerInfo,string description,string url)
  {
    ClientUserId = signerInfo.ClientUserId;
    Name = signerInfo.Name;
    Signer = signerInfo.Signer.ToString();
    Status = signerInfo.Status.ToString();
    Description = description;
    Url = url;
  }

  #endregion Public Constructors
}
