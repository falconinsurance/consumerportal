using DiamondServices.Service;
using Falcon.Core.Models;
using DCO = Diamond.Common.Objects;

namespace ConsumerServiceHost.Models;

public static class InstanceConverter
{
  #region Public Methods

  public static (DCO.Policy.Image New, DCO.Policy.Image Old) ToImage(this Instance instance,ISession consumerSession,DateTime systemDate, bool checkQuoteSource = false) => ConvertToImage.Convert(consumerSession,instance,systemDate,checkQuoteSource);

  public static Instance ToInstance(this DCO.Policy.Image image,ISession consumerSession,bool boundPolicy = false) => ConvertToInstance.Convert(consumerSession,image,boundPolicy);

  #endregion Public Methods
}
