namespace ConsumerServiceHost.Models;

public class NotificationResponse<T>
{
  #region Public Properties

  public string Message { get; set; }
  public T Request { get; set; }

  public bool Success { get; set; }

  #endregion Public Properties
}
