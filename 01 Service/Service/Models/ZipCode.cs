namespace ConsumerServiceHost.Models;

public class ZipCode
{
  #region Public Constructors

  public ZipCode(string zipcode)
  {
    Zip = "";
    PlusFour = "";
    if(!string.IsNullOrEmpty(zipcode))
    {
      if((zipcode.Contains("-") && zipcode.Length == 10) || zipcode.Length == 9)
      {
        Zip = zipcode.Substring(0,5);
      }
      else
      {
        Zip = zipcode;
      }
    }
  }

  #endregion Public Constructors

  #region Public Properties

  public string PlusFour { get; }
  public string Zip { get; }

  #endregion Public Properties
}
