using System.Net.Http;
using System.Net.Http.Headers;

namespace ConsumerServiceHost.Models;

public class HttpFile:HttpResponseMessage
{
  #region Private Fields

  private const string _ERROR_TXT = "Error.txt";

  private static readonly CookieHeaderValue[] _SuccessCookies = { new CookieHeaderValue("file_download","Success").With(c => {
    c.Path = "/";
    c.HttpOnly = false; }) };

  #endregion Private Fields

  #region Public Constructors

  public HttpFile(byte[] bytes = null,string fileName = _ERROR_TXT,string errorMessage = "Document doesn't exist.")
  {
    if(bytes is null)
    {
      fileName = _ERROR_TXT;
    }
    StatusCode = System.Net.HttpStatusCode.OK;
    Content = GetContent(errorMessage,bytes);
    Headers.AddCookies(_SuccessCookies);
    Content.Headers.ContentType = GetContentType(fileName);
    Content.Headers.ContentDisposition = new("attachment") { FileName = fileName };
  }

  #endregion Public Constructors

  #region Private Methods

  private static HttpContent GetContent(string message,byte[] bytes = null) => bytes == null ? new StringContent(message) : new ByteArrayContent(bytes);

  private static MediaTypeHeaderValue GetContentType(string fileName) => (Path.GetExtension(fileName)?.ToLower()) switch
  {
    ".csv" => Media._Csv,
    ".jpg" => Media._Jpg,
    ".pdf" => Media._Pdf,
    ".png" => Media._Png,
    ".zip" => Media._Zip,
    ".txt" => Media._Txt,
    _ => Media._Other,
  };

  #endregion Private Methods

  #region Private Classes

  private static class Media
  {
    #region Internal Fields

    internal static MediaTypeHeaderValue _Csv = new("text/csv");
    internal static MediaTypeHeaderValue _Jpg = new("image/jpeg");
    internal static MediaTypeHeaderValue _Other = new("application/octet-stream");
    internal static MediaTypeHeaderValue _Pdf = new("application/pdf");
    internal static MediaTypeHeaderValue _Png = new("image/png");
    internal static MediaTypeHeaderValue _Txt = new("text/txt");
    internal static MediaTypeHeaderValue _Zip = new("application/zip");

    #endregion Internal Fields
  }

  #endregion Private Classes
}
