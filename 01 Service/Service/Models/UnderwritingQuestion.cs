using Db = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Models;

public enum En_Description
{
  Default = 0,
  Yes = 1,
  No = 2,
}

public class UnderwritingQuestion
{
  #region Public Properties

  public int DiamondId { get; set; }
  public En_Description HasDescription { get; set; }
  public string Question { get; set; }

  #endregion Public Properties

  #region Public Methods

  public static UnderwritingQuestion Cast(Db.Record record) => new()
  {
    DiamondId = record.To<int>("DiamondId"),
    HasDescription = record.To<En_Description>("HasDescription"),
    Question = record.To<string>("Question")
  };

  #endregion Public Methods
}
