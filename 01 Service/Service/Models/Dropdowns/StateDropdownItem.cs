namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownItem
{
  #region Public Properties
  public string Label { get; init; }
  public string Name { get; init; }
  public int Value { get; init; }
  #endregion Public Properties
}
