namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdownsPolicyOptions
{
  #region Public Properties

  public SqlStateDropdown<En_BILimit> BILimit { get; init; } = new();
  public SqlStateDropdown<En_YesNo> EFTAutopay { get; init; } = new();
  public SqlStateDropdown<En_YesNo> HomeownersDiscount { get; init; } = new();
  public SqlStateDropdown<En_YesNo> IsSR50 { get; init; } = new();
  public SqlStateDropdown<En_SR22Payment> IsSR22 { get; init; } = new();
  public SqlStateDropdown<En_MPLimit> MPLimit { get; init; } = new();
  public SqlStateDropdown<En_PayPlan> PayPlanType { get; init; } = new();
  public SqlStateDropdown<En_PDLimit> PDLimit { get; init; } = new();
  public SqlStateDropdown<En_PIPLimit> PIPLimit { get; init; } = new();
  public SqlStateDropdown<En_Policy> PolicyType { get; init; } = new();
  public SqlStateDropdown<En_PhoneNumber> PrimaryPhone { get; init; } = new();
  public SqlStateDropdown<En_YesNo> PriorCoverage { get; init; } = new();
  public SqlStateDropdown<En_PhoneNumber> SecondaryPhone { get; init; } = new();
  public SqlStateDropdown<En_TermLength> TermLength { get; init; } = new();
  public SqlStateDropdown<En_UIMBILimit> UIMBILimit { get; init; } = new();
  public SqlStateDropdown<En_UMBILimit> UMBILimit { get; init; } = new();
  public SqlStateDropdown<En_YesNo> WorkLossBenefitsWaiver { get; init; } = new();

  #endregion Public Properties
}
