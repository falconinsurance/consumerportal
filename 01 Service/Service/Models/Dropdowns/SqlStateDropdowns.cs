namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdowns
{
  #region Public Properties
  public SqlStateDropdownsDriverOptions DriverOptions { get; init; } = new();
  public SqlStateDropdownsPolicyOptions PolicyOptions { get; init; } = new();
  public SqlStateDropdownsVehicleOptions VehicleOptions { get; init; } = new();
  public SqlStateDropdownsPaymentOptions PaymentOptions { get; init; } = new();
  #endregion Public Properties
}
