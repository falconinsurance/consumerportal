using Falcon.Enums;

namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownsDriverOptions:StatedropdownOptions
{
  #region Public Properties
  public StateDropdown DriverExclusionReason { get; init; } = new();
  public StateDropdown DriverType { get; init; } = new();
  public StateDropdown Gender { get; init; } = new();
  public StateDropdown MatureDriver { get; init; } = new();
  public StateDropdown DefensiveDriver { get; init; } = new();
  public StateDropdown AccidentPrevention { get; init; } = new();
  public StateDropdown License36Months { get; init; } = new();
  public StateDropdown LicenseState { get; init; } = new();
  public StateDropdown LicenseType { get; init; } = new();
  public StateDropdown MaritalStatus { get; init; } = new();
  public StateDropdown Relationship { get; init; } = new();
  public StateDropdown UDRResolution { get; init; } = new();
  public StateDropdown Violations { get; init; } = new();
  #endregion Public Properties

  #region Internal Methods

  internal static StateDropdownsDriverOptions Convert(SqlStateDropdownsDriverOptions driverOptions,En_Language lang) => new()
  {
    LicenseState = Convert(driverOptions.LicenseState,lang),
    LicenseType = Convert(driverOptions.LicenseType,lang),
    DriverType = Convert(driverOptions.DriverType,lang),
    DriverExclusionReason = Convert(driverOptions.DriverExclusionReason,lang),
    Gender = Convert(driverOptions.Gender,lang),
    MatureDriver = Convert(driverOptions.MatureDriver,lang),
    DefensiveDriver = Convert(driverOptions.DefensiveDriver,lang),
    AccidentPrevention = Convert(driverOptions.AccidentPrevention,lang),
    License36Months = Convert(driverOptions.License36Months,lang),
    MaritalStatus = Convert(driverOptions.MaritalStatus,lang),
    Relationship = Convert(driverOptions.Relationship,lang),
    UDRResolution = Convert(driverOptions.UDRResolution,lang),
    Violations = Convert(driverOptions.Violations,lang),
  };

  #endregion Internal Methods
}
