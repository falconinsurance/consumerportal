namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownsPaymentOptions:StatedropdownOptions
{
  #region Public Properties

  public StateDropdown AccountType { get; init; } = new();
  public StateDropdown PaymentType { get; init; } = new();

  #endregion Public Properties

  #region Internal Methods

  internal static StateDropdownsPaymentOptions Convert(SqlStateDropdownsPaymentOptions policyOptions,En_Language lang) => new()
  {
    PaymentType = Convert(policyOptions.PaymentType,lang),
    AccountType = Convert(policyOptions.AccountType,lang),
  };

  #endregion Internal Methods
}
