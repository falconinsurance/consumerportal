namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdown<T> where T : Enum
{
  #region Public Properties
  public T Default { get; init; }
  public List<T> Items { get; init; } = [];
  public StateDropdownLabel Label { get; set; }
  public bool Locked { get; init; }

  internal void Remove()
  {
    Items.Clear();
    Label = null;
  }
  #endregion Public Properties
}
