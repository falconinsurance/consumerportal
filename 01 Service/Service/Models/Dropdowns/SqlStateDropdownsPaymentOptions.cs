﻿namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdownsPaymentOptions
{
  #region Public Properties


  public SqlStateDropdown<En_Payment> PaymentType { get; init; } = new();
  public SqlStateDropdown<En_BankAccount> AccountType { get; init; } = new();

  #endregion Public Properties
}
