using Falcon.Enums;

namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdownsDriverOptions
{
  #region Public Properties
  public SqlStateDropdown<En_DriverExclusionReason> DriverExclusionReason { get; init; } = new();
  public SqlStateDropdown<En_Driver> DriverType { get; init; } = new();
  public SqlStateDropdown<En_Gender> Gender { get; init; } = new();
  public SqlStateDropdown<En_YesNo> MatureDriver { get; init; } = new();
  public SqlStateDropdown<En_YesNo> DefensiveDriver { get; init; } = new();
  public SqlStateDropdown<En_YesNo> AccidentPrevention { get; init; } = new();
  public SqlStateDropdown<En_YesNo> License36Months { get; init; } = new();
  public SqlStateDropdown<En_State> LicenseState { get; init; } = new();
  public SqlStateDropdown<En_License> LicenseType { get; init; } = new();
  public SqlStateDropdown<En_MaritalStatus> MaritalStatus { get; init; } = new();
  public SqlStateDropdown<En_DriverRelationship> Relationship { get; init; } = new();
  public SqlStateDropdown<En_UDRResolution> UDRResolution { get; init; } = new();
  public SqlStateDropdown<En_Violation> Violations { get; init; } = new();
  #endregion Public Properties
}
