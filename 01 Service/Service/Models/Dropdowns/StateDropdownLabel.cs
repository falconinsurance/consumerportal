using Falcon.Enums;

namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownLabel
{
  #region Public Properties
  public string Default { get; init; }

  public Dictionary<En_Language,string> LookUp { get; init; } = [];
  #endregion Public Properties
}
