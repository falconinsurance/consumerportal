namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdowns
{
  #region Public Properties

  public StateDropdownsDriverOptions DriverOptions { get; init; } = new();
  public StateDropdownsPaymentOptions PaymentOptions { get; init; } = new();
  public StateDropdownsPolicyOptions PolicyOptions { get; init; } = new();
  public StateDropdownsVehicleOptions VehicleOptions { get; init; } = new();

  #endregion Public Properties

  #region Public Methods

  public static StateDropdowns Convert(SqlStateDropdowns obj,En_State state,En_Language lang = En_Language.EN) => new()
  {
    DriverOptions = StateDropdownsDriverOptions.Convert(obj.DriverOptions,lang),
    PolicyOptions = StateDropdownsPolicyOptions.Convert(obj.PolicyOptions,lang,state),
    VehicleOptions = StateDropdownsVehicleOptions.Convert(obj.VehicleOptions,lang),
    PaymentOptions = StateDropdownsPaymentOptions.Convert(obj.PaymentOptions,lang),
  };

  #endregion Public Methods
}
