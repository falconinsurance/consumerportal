using Falcon.Enums;

namespace ConsumerServiceHost.Models.Dropdowns;

public class StatedropdownOptions
{
  #region Protected Methods

  protected static StateDropdown Convert<T>(SqlStateDropdown<T> input,En_Language lang) where T : struct, Enum => new()
  {
    DefaultValue = System.Convert.ToInt32(input.Default),
    Items = ConvertItems(input.Items,lang),
    Label = input.Label != null ? GetLabel(input.Label,lang) : null,
  };

  #endregion Protected Methods

  #region Private Methods

  private static List<StateDropdownItem> ConvertItems<T>(List<T> items,En_Language lang) where T : struct, Enum
  {
    var ret = new List<StateDropdownItem>();
    foreach(var type in EnType<T>.EnTypes)
    {
      var list = items.Where(c => System.Convert.ToInt32(c) == type.Id);
      if(!list.Any())
        continue;
      var item = items[0];
      ret.Add(new()
      {
        Label = type.Labels.Get(lang),
        Name = type.Enum,
        Value = type.Id,
      });
    }
    return ret;
  }

  private static string GetLabel(StateDropdownLabel label,En_Language lang)
    => label.LookUp is not null
       ? (label.LookUp.TryGetValue(lang,out var outLabel) ? outLabel : label?.Default)
       : label?.Default;

  #endregion Private Methods
}
