using Db = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlDropdowns
{
  #region Public Properties

  public En_State State { get; set; }
  public SqlStateDropdowns Value { get; set; }

  #endregion Public Properties

  #region Public Methods

  //public static SqlStateDropdowns Cast(Db.Record record) => record.To<SqlStateDropdowns>("Value");
  public static SqlStateDropdowns Cast(Db.Record record)
  {
    var p = record.To<SqlStateDropdowns>("Value");
    return p;
  }
  #endregion Public Methods
}
