namespace ConsumerServiceHost.Models.Dropdowns;

public class SqlStateDropdownsVehicleOptions
{
  #region Public Properties

  public SqlStateDropdown<En_ComColDeductible> ComColDeductible { get; init; } = new();
  public SqlStateDropdown<En_YesNo> Lienholder { get; init; } = new();
  public SqlStateDropdown<En_YesNo> SafetyEquipment { get; init; } = new();
  public SqlStateDropdown<En_VehicleOwnership> Ownership { get; init; } = new();
  public SqlStateDropdown<En_Rental> Rental { get; init; } = new();
  public SqlStateDropdown<En_YesNo> Towing { get; init; } = new();
  public SqlStateDropdown<En_UMPDDeductible> UMPDDeductible { get; init; } = new();
  public SqlStateDropdown<En_UMPDLimit> UMPDLimit { get; init; } = new();
  public SqlStateDropdown<En_VehicleUse> VehicleUse { get; init; } = new();

  #endregion Public Properties
}
