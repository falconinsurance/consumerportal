namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdown
{
  #region Public Properties
  public int DefaultValue { get; init; }
  public List<StateDropdownItem> Items { get; init; } = [];
  public string Label { get; init; }
  public bool Locked { get; init; }
  #endregion Public Properties
}
