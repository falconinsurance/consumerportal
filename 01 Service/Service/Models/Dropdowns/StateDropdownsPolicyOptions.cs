namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownsPolicyOptions:StatedropdownOptions
{
  #region Public Properties

  public StateDropdown BILimit { get; init; } = new();
  public string EffectiveDate { get; set; } = DateTime.Now.ToString("MM/dd/yyyy");
  public StateDropdown EFTAutopay { get; init; } = new();
  public StateDropdown HomeownersDiscount { get; init; } = new();
  public StateDropdown IsSR22 { get; init; } = new();
  public StateDropdown MPLimit { get; init; } = new();
  public StateDropdown PayPlanType { get; init; } = new();
  public StateDropdown PDLimit { get; init; } = new();
  public StateDropdown PIPLimit { get; init; } = new();
  public StateDropdown PolicyType { get; init; } = new();
  public StateDropdown PrimaryPhone { get; init; } = new();
  public StateDropdown PriorCoverage { get; init; } = new();
  public StateDropdown SecondaryPhone { get; init; } = new();
  public StateDropdown State { get; init; } = new();
  public StateDropdown TermLength { get; init; } = new();
  public StateDropdown UIMBILimit { get; init; } = new();
  public StateDropdown UMBILimit { get; init; } = new();
  public StateDropdown WorkLossBenefitsWaiver { get; init; } = new();

  #endregion Public Properties

  #region Internal Methods

  internal static StateDropdownsPolicyOptions Convert(SqlStateDropdownsPolicyOptions policyOptions,En_Language lang,En_State state) => new()
  {
    State = new()
    {
      Items =
      [
        new()
        {
          Label = state.GetLabel(lang),
          Name = state.ToString(),
          Value = state.ToInt()
        }
      ],
      DefaultValue = state.ToInt()
    },
    BILimit = Convert(policyOptions.BILimit,lang),
    EFTAutopay = Convert(policyOptions.EFTAutopay,lang),
    IsSR22 = Convert(policyOptions.IsSR22,lang),
    HomeownersDiscount = Convert(policyOptions.HomeownersDiscount,lang),
    MPLimit = Convert(policyOptions.MPLimit,lang),
    PayPlanType = Convert(policyOptions.PayPlanType,lang),
    PDLimit = Convert(policyOptions.PDLimit,lang),
    PIPLimit = Convert(policyOptions.PIPLimit,lang),
    PolicyType = Convert(policyOptions.PolicyType,lang),
    PrimaryPhone = Convert(policyOptions.PrimaryPhone,lang),
    PriorCoverage = Convert(policyOptions.PriorCoverage,lang),
    SecondaryPhone = Convert(policyOptions.SecondaryPhone,lang),
    TermLength = Convert(policyOptions.TermLength,lang),
    UIMBILimit = Convert(policyOptions.UIMBILimit,lang),
    UMBILimit = Convert(policyOptions.UMBILimit,lang),
    WorkLossBenefitsWaiver = Convert(policyOptions.WorkLossBenefitsWaiver,lang),
  };

  #endregion Internal Methods
}
