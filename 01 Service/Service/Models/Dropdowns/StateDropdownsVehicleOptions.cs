namespace ConsumerServiceHost.Models.Dropdowns;

public class StateDropdownsVehicleOptions:StatedropdownOptions
{
  #region Public Properties

  public StateDropdown ComColDeductible { get; init; } = new();
  public StateDropdown Lienholder { get; init; } = new();
  public StateDropdown SafetyEquipment { get; init; } = new();
  public StateDropdown Ownership { get; init; } = new();
  public StateDropdown Rental { get; init; } = new();
  public StateDropdown Towing { get; init; } = new();
  public StateDropdown UMPDDeductible { get; init; } = new();
  public StateDropdown UMPDLimit { get; init; } = new();
  public StateDropdown VehicleUse { get; init; } = new();

  #endregion Public Properties

  #region Internal Methods

  internal static StateDropdownsVehicleOptions Convert(SqlStateDropdownsVehicleOptions vehicleOptions,En_Language lang) => new()
  {
    ComColDeductible = Convert(vehicleOptions.ComColDeductible,lang),
    Ownership = Convert(vehicleOptions.Ownership,lang),
    Rental = Convert(vehicleOptions.Rental,lang),
    Towing = Convert(vehicleOptions.Towing,lang),
    Lienholder = Convert(vehicleOptions.Lienholder,lang),
    SafetyEquipment = Convert(vehicleOptions.SafetyEquipment,lang),
    UMPDLimit = Convert(vehicleOptions.UMPDLimit,lang),
    UMPDDeductible = Convert(vehicleOptions.UMPDDeductible,lang),
    VehicleUse = Convert(vehicleOptions.VehicleUse,lang),
  };

  #endregion Internal Methods
}
