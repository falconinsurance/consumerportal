using System.Net.Http;

namespace ConsumerServiceHost.Models;

public class HtmlContent:HttpResponseMessage
{
  #region Public Constructors

  public HtmlContent(string html)
  {
    if(html is null)
    {
      throw new ArgumentNullException(nameof(html));
    }

    StatusCode = System.Net.HttpStatusCode.OK;
    Content = GetContent(html);
  }

  #endregion Public Constructors

  #region Private Methods

  private static HttpContent GetContent(string html) => new StringContent(html,encoding: Encoding.Default,mediaType: "text/html");

  #endregion Private Methods
}
