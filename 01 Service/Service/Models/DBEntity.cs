//namespace ConsumerServiceHost.Models;

///// <summary>
///// Marker interface hiding generic type
///// </summary>
//public interface IDBEntity
//{
//  #region Public Properties

//  /// <summary>
//  /// The description
//  /// </summary>
//  string Description { get; }

//  /// <summary>
//  /// The Id, as an object
//  /// </summary>
//  string Id { get; }

//  #endregion Public Properties

//  #region Public Methods

//  int ToInt();

//  #endregion Public Methods
//}

///// <summary>
/////<para>
///// A particular value for a db entity type. For example, if the db entity is "BodyType", then an instance
///// </para>
///// <para>The type parameter "V" indicates the raw database type being represented, usually an integer for "Id".</para>
///// <para>This class is abstract; it cannot be instantiated. Instead create subclasses that provide the type parameters.</para>
///// </summary>

//public class DBEntity:IDBEntity, IEquatable<DBEntity>
//{
//  #region Public Constructors

//  /// <summary>
//  /// Used in serialization
//  /// </summary>
//  public DBEntity()
//  { }

//  /// <summary>
//  /// Creates a db entity value. Description can be null.
//  /// </summary>
//  public DBEntity(string id,string description)
//  {
//    Id = id;
//    Description = description;
//  }

//  public DBEntity(int id,string description)
//  {
//    Id = id.ToString();
//    Description = description;
//  }

//  #endregion Public Constructors

//  #region Public Properties
//  public string Description { get; set; }

//  public string Id { get; set; }
//  #endregion Public Properties

//  #region Public Methods

//  /// <summary>
//  /// The description. This field may be null if the database enttity being represented
//  /// doesn't have the concept of a description.
//  /// </summary>
//  /// <summary>
//  /// The identifying primary key, as understood by the source database.
//  /// </summary>
//  public static bool operator !=(DBEntity l,DBEntity r) => !(l == r);

//  public static bool operator ==(DBEntity l,DBEntity r) => ReferenceEquals(l,r) || l.Equals(r);

//  public override bool Equals(object obj) => ReferenceEquals(this,obj) || (obj is DBEntity cobj && Equals(cobj));

//  public bool Equals(DBEntity other) => !(other is null) && string.Equals(Id,other.Id,System.StringComparison.OrdinalIgnoreCase);

//  /// <summary>
//  /// Subclasses must override gethashcode
//  /// </summary>
//  public override int GetHashCode() => (Id).GetHashCode();

//  public int ToInt() => int.TryParse(Id,out var _Id) ? _Id : 0;

//  #endregion Public Methods
//}
