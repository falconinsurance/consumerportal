using System.Globalization;
using System.Text.RegularExpressions;
using ConsumerServiceHost.Apis;
using DiamondServices.Apis;
using DiamondServices.Service;
using Falcon.Core.Models;
using Newtonsoft.Json;
using DCE = Diamond.Common.Enums;

using DCO = Diamond.Common.Objects;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

using M = Falcon.Core.Models;

namespace ConsumerServiceHost.Models;

internal static class ConvertToImage
{
  #region Private Fields

  private static readonly IConsumerApi _ConsumerApi = AppResolver.Get<IConsumerApi>();
  private static readonly IDiamondApi _DiamondApi = AppResolver.Get<IDiamondApi>();
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(ConvertToImage));
  private static readonly Regex _PORegex = new(@"^\s*((?:P(?:OST)?.?\s*(?:O(?:FF(?:ICE)?)?)?.?\s*(?:B(?:IN|OX)?)?)+|(?:B(?:IN|OX)+\s+)+)\s*(\d+)",RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase);
  private static readonly Regex _StreetRegex = new(@"\s*(\d+)\s*(.+)",RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline);
  private static readonly TextInfo _TextInfo = new CultureInfo("en-US",false).TextInfo;

  #endregion Private Fields

  #region Public Methods

  public static bool IsEndorsement(this DCO.Policy.Image diamondImage)
    => (diamondImage.PolicyStatusCodeId == (int)DCE.PolicyStatusCode.Quote) && (diamondImage.TransactionTypeId == (int)DCE.TransType.Endt);

  #endregion Public Methods

  #region Internal Methods

  internal static (DCO.Policy.Image New, DCO.Policy.Image Old) Convert(ISession consumerSession,Instance instance,DateTime systemDate,bool checkQuoteSource)
  {
    //instance.WriteToTempAsJson("Instance2FromCP");
    if(instance is null)
    {
      return (null, null);
    }

    DCO.Policy.Image image;
    DCO.Policy.Image loadedImage = null;

    //Load image if it already exists or else create a new image
    if(instance.Policy?.Id > 0)
    {
      image = _DiamondApi.Policy.LoadImage(consumerSession,instance.Policy.Id,instance.Policy.ImageNum);
      loadedImage = JsonConvert.DeserializeObject<DCO.Policy.Image>(image.SerializeJson());
      if(checkQuoteSource && loadedImage.Policy.QuoteSourceId != En_QuoteSource.FalconWeb.GetDiaId())
      {
        return (null, null);
      }
    }
    else
    {
      image = CreateNewImage(instance,systemDate);
    }

    UpdateVersions(image,instance.Agency.State,image.EffectiveDate);

    return (UpdateAllImageFields(), loadedImage);

    DCO.Policy.Image UpdateAllImageFields()
    {
      image.Policy.QuoteSourceId = En_QuoteSource.FalconWeb.GetDiaId(); //En_QuoteSource.QuickQuote.ToInt();
      image.Policy.Account ??= new();
      image.Policy.Account.BillMethodId = En_Billing.DirectBill.GetDiaId();
      //image.CurrentPayplanId = Controllers.Quoting.Tools.GetPayPlanId(_DiamondApi,instance.Agency.State,image.PolicyTermId,instance.Policy.PaymentInfo,image.Policy.Account.BillMethodId);
      image.CurrentPayplanId = Controllers.Quoting.Tools.GetPayPlanId1(_DiamondApi,instance.Agency.State,image.PolicyTermId,instance.Policy.PaymentInfo.Payplans.Current,instance.Policy.Coverages.SR22,image.Policy.Account.BillMethodId);
      /*******************************************************/
      //TODO: Create/Modify/Delete the Agency Fee Coverage
      /*******************************************************/
      var lob = image.LOB ??= new();

      var policyLevel = lob.PolicyLevel ??= new();
      var modifiers = policyLevel.Modifiers ??= [];

      AddOrUpdateDiaModifiers(image,modifiers,instance);

      var riskLevel = lob.RiskLevel ??= new();
      var diaDrivers = riskLevel.Drivers ??= [];

      //Need to send effective date for setting some driver fields like def driver, mature driver
      AddOrUpdateDiaDrivers(diaDrivers,instance,image.EffectiveDate,consumerSession);

      AddOrUpdateDiaPolicyHolderAndClient(image,diaDrivers[0],instance);

      var diaVehicles = riskLevel.Vehicles ??= [];

      AddOrUpdateDiaVehicles(diaVehicles,instance);

      AddOrUpdateDiaUnderwritingQs(image.LOB.PolicyLevel,instance.Policy.UnderwritingResponses);

      UpdateDiaCcAndEftInfo(image);

      return image;
    }
  }

  #endregion Internal Methods

  #region Private Methods

  private static void AddNewDiaVehicle(DCO.InsCollection<DCO.Policy.Vehicle> diaVehicles,Vehicle vehicle,Address garageAddress,PolicyCoverages policyCoverages,En_State state,string effectiveDate)
  {
    if(string.IsNullOrEmpty(vehicle.Vin))
    {
      vehicle.Vin = vehicle.Symbol.Description.Substring(0,10);
    }
    var diaVehicle = new DCO.Policy.Vehicle()
    {
      Year = vehicle.Year,
      Make = vehicle.Make.Id,
      Model = vehicle.Model.Id,
      BodyTypeId = vehicle.BodyType.ToInt(),
      Vin = vehicle.Vin,
    };
    UpdateOtherDiaVehicleFields(vehicle,garageAddress,diaVehicle,policyCoverages,state,effectiveDate);
    diaVehicles.Add(diaVehicle);
  }

  private static void AddOrUpdateDiaDrivers(DCO.InsCollection<DCO.Policy.Driver> diaDrivers,Instance instance,DCO.InsDateTime effectiveDate,ISession consumerSession)
  {
    var state = instance.Agency.State;
    //Mark delete for drivers that dont exist in the instance
    foreach(var diaDriver in diaDrivers)
    {
      var driver = instance.Drivers.Find(d => d.Id == diaDriver.DriverNum);

      if(driver is null)
      {
        MarkForDelete(diaDriver);
      }
    }

    //Now loop through instance drivers and add/update diamond drivers
    foreach(var driver in instance.Drivers)
    {
      var diaDriver = diaDrivers.Find(d => d.DriverNum == driver.Id);

      //if diamond driver exists, update it or else just add new diamond driver.
      if(diaDriver is not null)
      {
        if(diaDriver.Name is null)
        {
          diaDriver.Name = GetNameFromDriver(driver,instance.Agency.State);
        }
        else
        {
          diaDriver.Name.FirstName = driver.Name.First;
          diaDriver.Name.MiddleName = driver.Name.Middle;
          diaDriver.Name.LastName = driver.Name.Last;
          diaDriver.Name.BirthDate = new(driver.Name.DateOfBirth);
          diaDriver.Name.SexId = (int)driver.Name.Gender;
          diaDriver.Name.MaritalStatusId = (int)driver.Name.MaritalStatus;
          diaDriver.Name.DLStateId = (DCE.State)(driver.License?.State ?? instance.Agency.State);
          diaDriver.Name.TypeId = 1;
          diaDriver.Name.NameAddressSourceId = DCE.NameAddressSource.Driver;
        }
        UpdateOtherDiaDriverFields(diaDriver,driver);
      }
      else
      {
        AddNewDriverToDiamond(diaDrivers,driver);
      }
    }

    void AddNewDriverToDiamond(DCO.InsCollection<DCO.Policy.Driver> diaDrivers,Driver driver)
    {
      var diaDriver = CreateActiveDiamondInsPolicyEntity<DCO.Policy.Driver>().With(d => d.Name = GetNameFromDriver(driver,state));
      UpdateOtherDiaDriverFields(diaDriver,driver);
      diaDrivers.Add(diaDriver);
    }

    void UpdateOtherDiaDriverFields(DCO.Policy.Driver diaDriver,Driver driver)
    {
      diaDriver.DetailStatusCode = (int)DCE.StatusCode.Active;
      diaDriver.DriverExcludeTypeId = driver.Type == En_Driver.NoVal ? En_Driver.Insured.GetDiaId() : driver.Type.GetDiaId();
      //diaDriver.DriverExcludeReasonId = En_DriverExclusionReason.NA.GetDiaId();
      if(driver.IsUDR)
      {
        diaDriver.ReasonExcludedDescription = "UDR";
      }
      diaDriver.RelationshipTypeId = driver.Relationship == En_DriverRelationship.NoVal ? En_DriverRelationship.Insured.GetDiaId() : driver.Relationship.GetDiaId();
      UpdateOccupationAndEmployment(driver.Employment);
      UpdateCourseDiscount(driver.CourseDiscount);
      UpdateDriverLicenseFields(driver.License,instance.Policy.Id,consumerSession);
      AddOrUpdateViolations(diaDriver.AccidentViolations,driver.Violations);
      UpdateSRFillingInfo(driver.Filing);

      void UpdateOccupationAndEmployment(Employment employment)
      {
        if(!string.IsNullOrEmpty(employment.Occupation))
        {
          diaDriver.Name.PositionTitle = _TextInfo.ToTitleCase(employment.Occupation.ToLower());
        }
        if(!string.IsNullOrEmpty(employment.Employer))
        {
          // Create it if it doesn't exist
          diaDriver.Employment ??= CreateActiveDiamondInsPolicyEntity<DCO.Policy.Employment>();

          diaDriver.Employment.Occupation = _TextInfo.ToTitleCase(employment.Employer.ToLower());
        }
        else
        {
          // Mark it for deletion if it does exist
          if(diaDriver.Employment != null)
          {
            MarkForDelete(diaDriver.Employment);
          }
        }
      }
      void UpdateCourseDiscount(CourseDiscount courseDiscount)
      {
        UpdateAccidentPrevention(courseDiscount.AccidentPrevention);
        UpdateDefensiveDriver(courseDiscount.DefensiveDriver,effectiveDate);
        UpdateSeniorDriver(courseDiscount.MatureDriver,effectiveDate);

        void UpdateAccidentPrevention(Course accidentPrevention)
        {
          if(accidentPrevention?.Value == En_YesNo.Yes)
          {
            diaDriver.AccPrevCourse = true;
            var date = accidentPrevention.Date;
            if(date.IsValidDate())
            {
              diaDriver.AccPrevCourseDate = new(date);
              diaDriver.AccPreventionCourse = new(date);
            }
          }
          else
          {
            // Making sure Accident Prevention Course gets removed
            diaDriver.AccPrevCourse = false;
            diaDriver.AccPreventionCourse = diaDriver.AccPrevCourseDate = new DCO.InsDateTime();
          }
        }
        void UpdateDefensiveDriver(Course defensiveDriver,DCO.InsDateTime effectiveDate)
        {
          if(defensiveDriver?.Value == En_YesNo.Yes)
          {
            diaDriver.DefDriver = true;
            //setting with effectiveDate if it is not a valid date
            diaDriver.DefDriverDate = defensiveDriver.Date.IsValidDate() ? new(defensiveDriver.Date) : effectiveDate;
          }
          else
          {
            diaDriver.DefDriver = false;
            diaDriver.DefDriverDate = new DCO.InsDateTime();
          }
        }
        void UpdateSeniorDriver(Course matureDriver,DCO.InsDateTime effectiveDate)
        {
          if(matureDriver?.Value == En_YesNo.Yes)
          {
            diaDriver.SeniorDiscount = true;
            //setting with effectiveDate if it is not a valid date
            //diaDriver.AccPreventionCourse = matureDriver.IsValidDate()? matureDriver.ToInsDateTime() : effectiveDate;
            diaDriver.SeniorDriverDate = matureDriver.Date.IsValidDate() ? new(matureDriver.Date) : effectiveDate;
          }
          else
          {
            diaDriver.SeniorDiscount = false;
            diaDriver.SeniorDriverDate = new DCO.InsDateTime();
          }
        }
      }
      void AddOrUpdateViolations(DCO.InsCollection<DCO.Policy.AccidentViolation> diaViolations,List<M.Violation> violations)
      {
        foreach(var diaViolation in diaViolations)
        {
          var violation = violations.Find(v => diaViolation.ViolationNum == v.Id);
          //Mark delete for violations that dont exist in the instance driver
          if(violation == null)
          {
            MarkForDelete(diaViolation);
          }
        }

        foreach(var violation in violations)
        {
          var diaViolation = diaViolations.Find(v => v.ViolationNum == violation.Id); // && falconViolation.Id > 0

          //if diamond violation exists, update it or else just add new diamond driver.
          if(diaViolation is not null)
          {
            UpdateViolationFields(diaViolation,violation);
          }
          else
          {
            AddNewViolationToDiamond(diaViolations,violation);
          }
        }

        void AddNewViolationToDiamond(DCO.InsCollection<DCO.Policy.AccidentViolation> diaViolations,M.Violation violation)
        {
          //Make a new accident violation and add it to diamond
          var diaViolation = CreateActiveDiamondInsPolicyDetailTableObject<DCO.Policy.AccidentViolation>()
            .With(v =>
            {
              UpdateViolationFields(v,violation);
              v.ViolationSourceId = (int)FDEnums.ViolationSourceId.Manual;
            });
          diaViolations.Add(diaViolation);
        }
        void UpdateViolationFields(DCO.Policy.AccidentViolation diaViolation,M.Violation violation)
        {
          diaViolation.ConvictionDate = new(violation.Date);
          diaViolation.AvDate = new(violation.Date);
          //Added because we have to different sets of violations mapped like a Venn Diagram between states
          diaViolation.AccidentsViolationsTypeId = violation.Type.GetMultiKeyIntersectionId(StateViolation.FromStr(state.ToString()));
        }
      }
      void UpdateSRFillingInfo(Filing filing)
      {
        if(filing.IsSR22)
        {
          //update if exists or create new
          diaDriver.FilingInformation ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Policy.FilingInfo>();

          diaDriver.FilingInformation.SR22 = true;
          diaDriver.FilingInformation.FilingState = state.ToString();
          if(diaDriver.FilingInformation.FilingRequiredTypeId == (int)DCE.FilingInfo.FilingRequiredType.Not_Required)
          {
            diaDriver.FilingInformation.FilingRequiredTypeId = (int)DCE.FilingInfo.FilingRequiredType.SR22_Required;
          }
        }
        else if(filing.IsSR50)
        {
          // Check to see if the filing Info exists

          // Create the filing info
          diaDriver.FilingInformation ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Policy.FilingInfo>();

          // Fill in the filing info data
          diaDriver.FilingInformation.SR50 = true;
          diaDriver.FilingInformation.FilingState = state.ToString();
          if(diaDriver.FilingInformation.SR50FilingRequiredTypeId == (int)DCE.FilingInfo.SR50FilingRequiredType.Not_Required)
          {
            diaDriver.FilingInformation.SR50FilingRequiredTypeId = (int)DCE.FilingInfo.SR50FilingRequiredType.SR50_Required;
          }
        }
        else
        {
          // Check to see if the filing Info exists
          if(diaDriver.FilingInformation is not null)
          {
            // Set some info and mark it for deletion
            diaDriver.FilingInformation.SR22 = false;
            diaDriver.FilingInformation.SR50 = false;
            diaDriver.FilingInformation.FilingRequiredTypeId = (int)DCE.FilingInfo.FilingRequiredType.Not_Required;
            diaDriver.FilingInformation.SR50FilingRequiredTypeId = (int)DCE.FilingInfo.SR50FilingRequiredType.Not_Required;
            MarkForDelete(diaDriver.FilingInformation);
          }
        }
      }
      void UpdateDriverLicenseFields(License license,int policyId,ISession consumerSession)
      {
        diaDriver.LicenseTypeId = license.Months36 == En_YesNo.Yes ? (int)FDEnums.LicenseTypeId.GreaterThanEqualTo36Months : (int)FDEnums.LicenseTypeId.LessThan36Months;

        if(!string.IsNullOrEmpty(license.Number))
        {
          diaDriver.Name.DLN = license.Number.ToUpper();
        }
        else if(!diaDriver.Name.DLN.IsNullOrEmpty())
        {
          var temp = _DiamondApi.Security.DecryptLicenseNumber(consumerSession,diaDriver.Name.DLN,policyId);
          if(!string.IsNullOrEmpty(temp))
          {
            diaDriver.Name.DLN = temp;
          }
        }
        diaDriver.Name.DLStateId = (DCE.State)state;
        diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Valid;
        switch(license.Type)
        {
          case En_License.StateID:
            diaDriver.Name.DLStateId = (DCE.State)state;
            if(state == En_State.AZ || state == En_State.OK)
            {
              diaDriver.LicenseStatusId = 11;
            }
            else
            {
              // Don't change the Diamond value if it's set to one of the similar types
              if((diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.NotLicensed)
                  && (diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Unknown)
                  && (diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Expired)) // Can probably take this part out now that Expired is exposed
              {
                diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.NotLicensed;
              }
            }
            break;

          case En_License.NotLicensed:
            diaDriver.Name.DLStateId = (DCE.State)state;
            // Don't change the Diamond value if it's set to one of the similar types
            if((diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.NotLicensed)
                && (diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Unknown)
                && (diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Expired)) // Can probably take this part out now that Expired is exposed
            {
              diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.NotLicensed;
            }
            break;

          case En_License.Expired:
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Expired;
            diaDriver.Name.DLStateId = GetValidDLStateId(license.State,diaDriver.Name,state);
            break;

          case En_License.Permit:
            diaDriver.Name.DLStateId = license.State.IsValidUSState() ? (DCE.State)license.State.GetDiaId() : (DCE.State)state;
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.TemporaryLearnerPermit;
            break;

          case En_License.TVDL:
            diaDriver.Name.DLStateId = license.State.IsValidUSState() ? (DCE.State)license.State.GetDiaId() : (DCE.State)state;
            diaDriver.LicenseStatusId = 10; // TVDL
            break;

          case En_License.OutOfState:
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Valid;
            diaDriver.Name.DLStateId = GetValidDLStateId(license.State,diaDriver.Name,state);
            break;

          case En_License.Matricula:
          case En_License.ForeignPassport:
          case En_License.International:
            diaDriver.Name.DLStateId = DCE.State.NA_Other;
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Valid;
            break;

          case En_License.Suspended:
            diaDriver.Name.DLStateId = license.State.IsValidUSState() ? (DCE.State)license.State.GetDiaId() : (DCE.State)state;
            if((diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Suspended)
                && (diaDriver.LicenseStatusId != (int)FDEnums.LicenseStatus.Revoked))
            {
              diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Suspended;
            }
            break;

          case En_License.IL:
          case En_License.TX:
          case En_License.IN:
            diaDriver.Name.DLStateId = (DCE.State)(license.Type.GetDiaId());
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Valid;
            break;

          default:
            diaDriver.Name.DLStateId = (DCE.State)state;
            diaDriver.LicenseStatusId = (int)FDEnums.LicenseStatus.Valid;
            break;
        }
      }
    }
  }

  private static void AddOrUpdateDiaModifiers(DCO.Policy.Image image,DCO.InsCollection<DCO.Policy.Modifier> diaModifiers,Instance instance)
  {
    var state = instance.Agency.State;
    var versionId = _DiamondApi.Static.GetVersionId(state,image.EffectiveDate);

    AddOrUpdateTransferDiscount(image.LOB.PolicyLevel,diaModifiers,instance.Policy.Modifiers.TransferDiscount);
    AddOrdUpdateHomeOwnersDiscount(instance.Policy.Modifiers.HomeownersDiscount,instance.Policy.Term);
    if(state.In(En_State.TX))
    {
      // don't do this for IL/IN policies since IL/IN version data cannot handle it
      AddPolicyTypeModifier();
    }
    AddOrUpdateElectronicDocsModifier();
    if(image.TransactionTypeId == (int)DCE.TransType.NewPolicy)
    {
      AddPaidInFullModifier();
    }

    void AddOrUpdateTransferDiscount(DCO.Policy.PolicyLevel diaPolicyLevel,DCO.InsCollection<DCO.Policy.Modifier> diaModifiers,TransferDiscount transferDiscount)
    {
      const int modifier = (int)FDEnums.ModifierTypeId.TransferDiscount;
      // Try and find the transfer discount modifier
      var modTD = diaModifiers.FirstOrDefault(m => m.ModifierTypeId == modifier);

      if(transferDiscount.Flag == En_YesNo.Yes)
      {
        // If we didn't find the transfer discount modifier in the image then create one and add it.
        if(modTD == null)
        {
          modTD = _DiamondApi.Static.CreateModifier(modifier,versionId);
          diaModifiers.Add(modTD);
        }

        //update fields
        diaPolicyLevel.PriorCarrier ??= CreateActiveDiamondInsPolicyEntity<DCO.Policy.PriorCarrier>();
        diaPolicyLevel.PriorCarrier.PriorExpirationDate = transferDiscount.ExpirationDate == default ? image.EffectiveDate : new(transferDiscount.ExpirationDate);

        diaPolicyLevel.PriorCarrier.Name ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Name>();
        diaPolicyLevel.PriorCarrier.Name.CommercialName1 = transferDiscount.CompanyName;

        diaPolicyLevel.PriorCarrier.PriorPolicy = transferDiscount.PolicyNumber;

        // 20 Days in IL, 30 Days Everywhere Else
        if(!image.IsEndorsement())
          modTD.CheckboxSelected = DateTime.Compare(diaPolicyLevel.PriorCarrier.PriorExpirationDate.AddDays(state.In(En_State.IL) ? 20 : 30),image.EffectiveDate) >= 0;
      }
      else
      {
        // If we found a transfer discount modifier then remove it
        if(modTD != null)
        {
          MarkForDelete(modTD);
          modTD.CheckboxSelected = false;
        }

        // Check to see if the Prior Carrier exists
        if(diaPolicyLevel.PriorCarrier != null)
        {
          MarkForDelete(diaPolicyLevel.PriorCarrier);
        }
      }
    }
    void AddOrdUpdateHomeOwnersDiscount(En_YesNo homeownersDiscount,En_TermLength term)
    {
      var modifier = (int)FDEnums.ModifierTypeId.HomeownersDiscount;
      // Try and find the home owner discount modifier
      var modHO = image.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == modifier);

      if(homeownersDiscount == En_YesNo.Yes && term >= En_TermLength.SixMonths)
      {
        // If we didn't find the homeowners discount modifier in the image then create one and add it.
        if(modHO == null)
        {
          modHO = _DiamondApi.Static.CreateModifier(modifier,versionId);
          modHO.CheckboxSelected = true;
          image.LOB.PolicyLevel.Modifiers.Add(modHO);
        }
      }
      else
      {
        // If we found a home owner discount modifier then remove it
        if(modHO != null)
        {
          MarkForDelete(modHO);
          modHO.CheckboxSelected = false;
        }
      }
    }
    void AddPolicyTypeModifier()
    {
      var modifierId = (int)FDEnums.ModifierTypeId.PolicyType;
      var policyTypeModifier = image.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == modifierId);
      if(policyTypeModifier == null)
      {
        policyTypeModifier = _DiamondApi.Static.CreateModifier(modifierId,versionId);
        image.LOB.PolicyLevel.Modifiers.Add(policyTypeModifier);
      }
      policyTypeModifier.ModifierOptionId = instance.Policy.Type.GetDiaId();
    }
    void AddOrUpdateElectronicDocsModifier()
    {
      var modifierId = (int)FDEnums.ModifierTypeId.ConsentToReceiveDocumentsElectronically;
      var electronicDocumentsModifier = image.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == modifierId);
      if(instance.Policy.Modifiers.ReceiveElectronicDocuments == En_YesNo.Yes)
      {
        // Add a modifier for the Consent To Receive Documents Electronically
        if(electronicDocumentsModifier == null)
        {
          electronicDocumentsModifier = _DiamondApi.Static.CreateModifier(modifierId,versionId);
          image.LOB.PolicyLevel.Modifiers.Add(electronicDocumentsModifier);
        }
        // Sets the Consent To Receive Documents Electronically
        electronicDocumentsModifier.CheckboxSelected = true;
      }
      else
      {
        // If we found a home owner discount modifier then remove it
        if(electronicDocumentsModifier != null)
        {
          MarkForDelete(electronicDocumentsModifier);
          electronicDocumentsModifier.CheckboxSelected = false;
        }
      }
    }
    void AddPaidInFullModifier()
    {
      const int paidInFullDiscountId = (int)FDEnums.ModifierTypeId.PaidInFullDiscount;
      var paidInFullModifier = image.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == paidInFullDiscountId);
      if(paidInFullModifier == null)
      {
        paidInFullModifier = _DiamondApi.Static.CreateModifier(paidInFullDiscountId,versionId);
        image.LOB.PolicyLevel.Modifiers.Add(paidInFullModifier);
      }

      paidInFullModifier.CheckboxSelected = PayPlanType.FromDiaId(image.CurrentPayplanId) == En_PayPlan.PayInFull
        && instance.Policy.Term.In(En_TermLength.SixMonths,En_TermLength.TwelveMonths)
        && image.Policy.Account.BillMethodId == (int)DCE.Billing.BillMethod.Direct;
    }
  }

  private static void AddOrUpdateDiaPolicyHolderAndClient(DCO.Policy.Image image,DCO.Policy.Driver diaFirstDriver,Instance instance)
  {
    diaFirstDriver.Address ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Address>();
    SetDiaAddress(diaFirstDriver.Address,instance.PolicyHolder.Address);

    //PolicyHolder stuff
    image.PolicyHolder ??= CreateDiamondPolicyEntityObj<DCO.Policy.PolicyHolder>();
    //Need to use Fill(), this ensures proper updates for policy holder's name.
    image.PolicyHolder.Name.Fill(diaFirstDriver.Name);
    SetDiaAddressFromDiaAddress(image.PolicyHolder.Address,diaFirstDriver.Address);

    image.PolicyHolder.Phones ??= [];
    SetDiaPhones(image.PolicyHolder.Phones,instance.PolicyHolder.Phones);

    image.PolicyHolder.Emails ??= [];
    SetDiaEmail(image.PolicyHolder.Emails,instance.PolicyHolder.Email);

    //Client stuff
    image.Policy.Client ??= new();
    SetDiaNameAndAddress(image.Policy.Client,diaFirstDriver);

    image.Policy.Client.Phones ??= [];
    SetDiaPhones(image.Policy.Client.Phones,instance.PolicyHolder.Phones);

    image.Policy.Client.Emails ??= [];
    SetDiaEmail(image.Policy.Client.Emails,instance.PolicyHolder.Email);
  }

  private static void AddOrUpdateDiaUnderwritingQs(DCO.Policy.PolicyLevel policyLevel,List<UnderwritingResponse> underwritingResponses)
  {
    policyLevel.PolicyUnderwritings ??= [];
    underwritingResponses.ForEach(response =>
    {
      // -1 is the default, do not add it to the list. It confuses Diamond and then bad things happen.
      if(response.DiamondId == -1)
        return;
      var pu = policyLevel.PolicyUnderwritings.FirstOrDefault(p => p.PolicyUnderwritingCodeId == response.DiamondId);
      if(pu is null)
      {
        pu = new DCO.Policy.PolicyUnderwriting()
        {
          DetailStatusCode = (int)DCE.StatusCode.Active,
          PolicyUnderwritingLevelId = (int)DCE.Underwriting.PolicyUnderwritingLevel.PolicyImage,
          PolicyUnderwritingTabId = 1,
          PolicyUnderwritingCodeId = response.DiamondId
        };
        policyLevel.PolicyUnderwritings.Add(pu);
      }

      pu.PolicyUnderwritingAnswer = (int)response.Value;
      pu.PolicyUnderwritingExtraAnswer = response.Answer;
    });
  }

  private static void AddOrUpdateDiaVehicles(DCO.InsCollection<DCO.Policy.Vehicle> diaVehicles,Instance instance)
  {
    //Mark delete for vehicles that dont exist in the instance
    foreach(var diaVehicle in diaVehicles)
    {
      var vehicle = instance.InsVehicles.Vehicles.Find(d => d.Id == diaVehicle.VehicleNum);

      if(vehicle is null)
      {
        MarkForDelete(diaVehicle);
      }
    }

    //Now loop through instance vehicles and add/update diamond vehicles
    foreach(var vehicle in instance.InsVehicles.Vehicles)
    {
      var diaVehicle = diaVehicles.Find(v => v.VehicleNum == vehicle.Id);

      //if diamond vehicle exists, update it or else just add new diamond vehicle.
      if(diaVehicle is not null)
      {
        diaVehicle.Year = vehicle.Year > 1000 ? vehicle.Year : diaVehicle.Year;
        diaVehicle.Make = vehicle.Make != null && !string.IsNullOrEmpty(vehicle.Make.Description) ? vehicle.Make.Description : diaVehicle.Make;
        diaVehicle.Model = vehicle.Model != null && !string.IsNullOrEmpty(vehicle.Model.Description) ? vehicle.Model.Description : diaVehicle.Model;
        diaVehicle.BodyTypeId = vehicle.BodyType != null ? vehicle.BodyType.ToInt() : diaVehicle.BodyTypeId;
        vehicle.Vin = string.IsNullOrEmpty(vehicle.Vin) ? vehicle.Symbol.Description.Substring(0,10) : vehicle.Vin;
        diaVehicle.Vin = vehicle.Vin != null ? vehicle.Vin.ToUpper() : diaVehicle.Vin;

        UpdateOtherDiaVehicleFields(vehicle,instance.InsVehicles.Address,diaVehicle,instance.Policy.Coverages,instance.Agency.State,instance.Policy.EffectiveDate);
      }
      else
      {
        AddNewDiaVehicle(diaVehicles,vehicle,instance.InsVehicles.Address,instance.Policy.Coverages,instance.Agency.State,instance.Policy.EffectiveDate);
      }
    }
  }

  private static T CreateActiveDiamondInsPolicyDetailTableObject<T>() where T : DCO.InsPolicyDetailTableObject, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static T CreateActiveDiamondInsPolicyEntity<T>() where T : DCO.InsPolicyEntity, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static T CreateActiveDiamondPremiumAndLimitBearingObject<T>() where T : DCO.PremiumAndLimitBearingObject, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static T CreateDiamondObj<T>() where T : DCO.InsPolicyDetailTableObject, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static T CreateDiamondPolicyEntityObj<T>() where T : DCO.InsPolicyEntity, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static T CreateDiamondPremiumAndLimitBearingObject<T>() where T : DCO.PremiumAndLimitBearingObject, new() => new() { DetailStatusCode = (int)DCE.StatusCode.Active };

  private static DCO.Policy.Image CreateNewImage(Instance instance,DateTime systemDate)
  {
    var effectiveDate = systemDate;
    instance.Policy ??= new();
    if(instance.Policy.EffectiveDate != default)
    {
      effectiveDate = instance.Policy.EffectiveDate.ToDate();
    }
    var effDate = effectiveDate.ToInsDateTime();
    var expDate = effectiveDate.AddMonths(6).ToInsDateTime();
    instance.Policy.Coverages ??= PolicyCoverages.GetDefaultCoverage(instance.Agency.State);
    var image = new DCO.Policy.Image
    {
      TransactionTypeId = (int)DCE.TransType.NewPolicy,
      QuoteTypeId = (int)DCE.QuoteType.QuickQuote,
      ReceivedDate = new(systemDate),
      TransactionDate = new(systemDate),
      EffectiveDate = effDate,
      GuaranteedRatePeriodEffectiveDate = effDate,
      TransactionEffectiveDate = effDate,
      PolicyStatusCodeId = (int)DCE.PolicyStatusCode.Quote,
      PolicyTermId = instance.Policy.Term.GetDiaId(),//1, //DiaId for 6 months; should be state default
      ExpirationDate = expDate,
      TransactionExpirationDate = expDate,
      GuaranteedRatePeriodExpirationDate = expDate,
      AgencyId = instance.Agency.Id,
      AgencyProducerId = instance.Agency.ProducerId,
      BillToId = (int)DCE.Billing.BillTo.Insured,
      RatingVersionId = 0,
      PolicyId = instance.Policy.Id,
      PolicyNumber = instance.Policy.Number,
      PolicyImageNum = instance.Policy.ImageNum,
    }.With(i => i.Agency ??= GetAgency(instance));
    //image.WriteToTempAsJson("ImageFromInstance");
    return image;
  }

  private static void CreateVehicleCoverage(FDEnums.CoverageCode coverageCode,bool isCovered,DCO.Policy.Vehicle diamondVehicle)
  {
    var coverage = diamondVehicle.Coverages.FirstOrDefault(c => c.CoverageCodeId == (int)coverageCode);

    if(coverage == null)
    {
      coverage = CreateActiveDiamondPremiumAndLimitBearingObject<DCO.Coverage>();
      diamondVehicle.Coverages.Add(coverage);
    }

    // Set the rest of the coverage data
    coverage.CoverageCodeId = (int)coverageCode;
    coverage.Checkbox = isCovered;
  }

  private static void CreateVehicleCoverage<T>(FDEnums.CoverageCode coverageCode,T enumFalcon,DCO.Policy.Vehicle diamondVehicle)
  where T : struct, Enum
  {
    var coverageLimitId = enumFalcon.GetDiaId();
    var coverage = diamondVehicle.Coverages.FirstOrDefault(c => c.CoverageCodeId == (int)coverageCode);

    // Only make or convert the coverage if the Diamond Coverage Limit Id <> 0 (None)
    if(coverageLimitId > 0)
    {
      // If we didn't find the coverage then make it
      if(coverage == null)
      {
        coverage = CreateActiveDiamondPremiumAndLimitBearingObject<DCO.Coverage>();
        diamondVehicle.Coverages.Add(coverage);
      }

      // Set the rest of the coverage data
      coverage.CoverageCodeId = (int)coverageCode;
      coverage.CoverageLimitId = coverageLimitId;
    }
    else // If the Diamond Coverage Limit Id = 0 (None) then delete the coverage
    {
      // If we didn't find the coverage then make it
      if(coverage != null)
      {
        MarkForDelete(coverage);
      }
    }
  }

  private static void CreateVehicleSymbol(FDEnums.VehicleSymbolCoverageTypeId symbolTypeId,string symbolId,DCO.Policy.Vehicle diamondVehicle)
  {
    var symbol = diamondVehicle.VehicleSymbols.FirstOrDefault(s => s.VehicleSymbolCoverageTypeId == (int)symbolTypeId);
    if(symbol == null)
    {
      symbol = CreateActiveDiamondInsPolicyDetailTableObject<DCO.Policy.VehicleSymbol>();
      diamondVehicle.VehicleSymbols.Add(symbol);
      symbol.VehicleSymbolCoverageTypeId = (int)symbolTypeId;
    }

    // Set the rest of the symbol data
    symbol.UserOverrideSymbol = symbolId;
    symbol.SystemGeneratedSymbol = symbolId;
  }

  private static DCO.Policy.Agency.Agency GetAgency(Instance instance)
  {
    var agency = _ConsumerApi.Agency.GetAgency(instance.Agency.Id,instance.Agency.ProducerId);
    return new()
    {
      Code = agency.Code,
      AgencyId = agency.Id
    };
  }

  private static DCO.Name GetNameFromDriver(Driver driver,En_State state) => CreateActiveDiamondInsPolicyDetailTableObject<DCO.Name>().With(n =>
                                        {
                                          n.FirstName = _TextInfo.ToTitleCase(driver.Name.First.ToLower());
                                          n.MiddleName = !string.IsNullOrEmpty(driver.Name.Middle) ? _TextInfo.ToTitleCase(driver.Name.Middle.ToLower()) : driver.Name.Middle;
                                          n.LastName = _TextInfo.ToTitleCase(driver.Name.Last.ToLower());
                                          n.BirthDate = new(driver.Name.DateOfBirth);
                                          n.SexId = (int)driver.Name.Gender;
                                          n.MaritalStatusId = (int)driver.Name.MaritalStatus;
                                          n.DLStateId = (DCE.State)(driver.License?.State ?? state);
                                          n.TypeId = (int)DCE.NameType.Personal;
                                          n.NameAddressSourceId = DCE.NameAddressSource.Driver;
                                        });

  private static DCE.State GetValidDLStateId(En_State licenseState,DCO.Name diaName,En_State state)
  {
    if(licenseState.IsValidUSState())
    {
      return (DCE.State)licenseState.GetDiaId();
    }
    else if(diaName.DLStateId <= 0 || diaName.DLStateId == (DCE.State)state || diaName.DLStateId == DCE.State.NA_Other)
    {
      return DCE.State.FM;
    }
    return DCE.State.NA_Other;
  }

  //  int GetStateDefaultPayPlanId()
  //  {
  //    var payplan = state.WeWrite() ? En_PayPlan.Pay_05_16P : En_PayPlan.NoVal;
  //    if(payplan > En_PayPlan.NoVal)
  //    {
  //      return payplan.GetMultiKeyIntersectionId(En_Payment.AgencySweep,En_SR22Payment.SR22No,statePayPlan);
  //    }
  //    var payPlans = _DiamondApi.Static.GetPayPlans(state,termId);
  //    if(payPlans.Count > 0)
  //    {
  //      var temp = payPlans.Select(c => PayPlanType.FromDiaId(c).GetEnType()).FirstOrDefault(c => c.Id > 3);
  //      return temp?.Id ?? 1;
  //    }
  //    return 1;
  //  }
  //}
  private static bool IsValidDate(this DateTime date) => date.Year >= 1800 && date.Year <= 2100 && (date.Month >= 1 && date.Month <= 12) && (date.Day >= 1 && date.Day <= 31);

  private static bool IsValidDate(this string date) => IsValidDate(date.ToDate());

  //  if(billingMethodId == 1)
  //  {
  //    return 1;
  //  }
  //  if(paymentInfo?.PayPlanItem is not null && paymentInfo.PayPlanItem.PayPlan != En_PayPlan.NoVal && paymentInfo.RecurringPayment is not null)
  //  {
  //    return paymentInfo.RecurringPayment.Type != En_Payment.NoVal
  //      ? paymentInfo.PayPlanItem.PayPlan.GetMultiKeyIntersectionId(paymentInfo.RecurringPayment.Type,paymentInfo.Sr22,statePayPlan)
  //      : paymentInfo.PayPlanItem.PayPlan.GetMultiKeyIntersectionId(En_Payment.AgencySweep,paymentInfo.Sr22,statePayPlan);
  //  }
  //  return GetStateDefaultPayPlanId();
  private static void MarkForDelete<T>(T diamondObject) where T : DCO.Interfaces.IInsPolicyDetailObject => diamondObject.DetailStatusCode = (int)DCE.StatusCode.Deleted;

  //private static int GetPayPlanId(En_State state,int termId,PaymentInfo paymentInfo,int billingMethodId)
  //{
  //  var statePayPlan = StatePayPlanType.FromStr(state.ToString());
  private static void SetDiaAddress(DCO.Address diaAddress,Address address)
  {
    if(address.Street != null)
    {
      var matchAddress = _PORegex.Match(address.Street);
      if(matchAddress.Success)
      {
        // This is a PO Box
        if(matchAddress.Groups.Count > 2)
        {
          diaAddress.POBox = matchAddress.Groups[2].Value;
          diaAddress.HouseNumber = "";
          diaAddress.StreetName = "";
        }
      }
      else
      {
        // This is not a PO Box
        // Create the regex for street
        matchAddress = _StreetRegex.Match(address.Street);
        if(matchAddress.Success)
        {
          // Now convert the address info over
          diaAddress.POBox = "";
          diaAddress.HouseNumber = matchAddress.Groups[1].Value;
          diaAddress.StreetName = (matchAddress.Groups[2].Value != null) ? _TextInfo.ToTitleCase(matchAddress.Groups[2].Value.ToLower()) : matchAddress.Groups[2].Value;
        }
        else
        {
          diaAddress.POBox = "";
          diaAddress.StreetName = (address.Street != null) ? _TextInfo.ToTitleCase(address.Street.ToLower()) : address.Street;
        }
      }
    }
    diaAddress.ApartmentNumber = "";
    diaAddress.City = _TextInfo.ToTitleCase(address.City.ToLower());
    diaAddress.StateId = address.State.ToInt();
    diaAddress.Zip = address.Zip;
    diaAddress.County = address.County;
    diaAddress.DetailStatusCode = (int)DCE.StatusCode.Active;
  }

  private static void SetDiaAddressFromDiaAddress(DCO.Address diaAddressTo,DCO.Address diaAddressFrom)
  {
    diaAddressTo.StreetName = diaAddressFrom.StreetName;
    diaAddressTo.POBox = diaAddressFrom.POBox;
    diaAddressTo.HouseNumber = diaAddressFrom.HouseNumber;
    diaAddressTo.ApartmentNumber = diaAddressFrom.ApartmentNumber;
    diaAddressTo.City = diaAddressFrom.City;
    diaAddressTo.StateId = diaAddressFrom.StateId;
    diaAddressTo.County = diaAddressFrom.County;
    diaAddressTo.Zip = diaAddressFrom.Zip;
    diaAddressTo.DetailStatusCode = diaAddressFrom.DetailStatusCode;
  }

  private static void SetDiaEmail(DCO.InsCollection<DCO.Email> diaEmails,string email)
  {
    // Try to find the email in the collection
    var diaEmail = diaEmails.FirstOrDefault(e => e.TypeId == DCE.EMailType.Home);
    // Only make or convert the email if the falcon email exists
    if(!string.IsNullOrEmpty(email))
    {
      // If we didn't find the email then make it
      if(diaEmail == null)
      {
        diaEmail = CreateActiveDiamondInsPolicyDetailTableObject<DCO.Email>();
        diaEmails.Add(diaEmail);
      }

      // Set the rest of the email data
      diaEmail.TypeId = DCE.EMailType.Home;
      diaEmail.Address = email;
    }
    else // If the falcon email doesn't exist then delete the Diamond email
    {
      // If we found the email then remove it
      if(diaEmail != null)
      {
        MarkForDelete(diaEmail);
      }
    }
  }

  private static void SetDiaName(DCO.Name diaNameTo,DCO.Name diaNameFrom)
  {
    diaNameTo.FirstName = diaNameFrom.FirstName;
    diaNameTo.MiddleName = diaNameFrom.MiddleName;
    diaNameTo.LastName = diaNameFrom.LastName;
    diaNameTo.BirthDate = diaNameFrom.BirthDate;
    diaNameTo.NameId = diaNameFrom.NameId;
    diaNameTo.TypeId = diaNameFrom.TypeId;
    diaNameTo.DLStateId = diaNameFrom.DLStateId;
    diaNameTo.SexId = diaNameFrom.SexId;
    diaNameTo.MaritalStatusId = diaNameFrom.MaritalStatusId;
    diaNameTo.DetailStatusCode = diaNameFrom.DetailStatusCode;
  }

  private static void SetDiaNameAndAddress(DCO.Entity entityTo,DCO.Entity entityFrom)
  {
    entityTo.Address ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Address>();
    SetDiaAddressFromDiaAddress(entityTo.Address,entityFrom.Address);
    entityTo.Name ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Name>();
    SetDiaName(entityTo.Name,entityFrom.Name);
  }

  private static void SetDiaPhones(DCO.InsCollection<DCO.Phone> diaPhones,List<Phone> phones)
  {
    //Mark delete for phones that dont exist in the instance
    foreach(var diaPhone in diaPhones)
    {
      var phone = phones.Find(d => d.Type == PhoneNumberType.From(diaPhone.TypeId));

      if(phone is null)
      {
        MarkForDelete(diaPhone);
      }
    }

    //Now loop through instance phones and add/update diamond phones
    foreach(var phone in phones)
    {
      var diaPhone = diaPhones.Find(d => d.TypeId == (DCE.PhoneType)phone.Type.GetDiaId());

      //if diamond phone exists, update it or else just add new diamond phone.
      if(diaPhone is not null)
      {
        diaPhone.Number = phone.GetConvertedNumber();
      }
      else if(phone.Number is not null)
      {
        diaPhones.Add(CreateActiveDiamondInsPolicyDetailTableObject<DCO.Phone>().With(p =>
        {
          p.Number = phone.GetConvertedNumber();
          p.TypeId = (DCE.PhoneType)phone.Type.GetDiaId();
        }));
      }
    }
  }

  private static void UpdateDiaCcAndEftInfo(DCO.Policy.Image image)
  {
    image.Policy.CreditCardData = new()
    {
      PolicyId = image.PolicyId,
      AccountNumber = string.Empty,
      ExpirationDate = string.Empty,
      CreditCardType = -1, // none/deleted
    };
    image.Policy.EFT = new()
    {
      PolicyId = image.PolicyId,
      PolicyImageNum = image.PolicyImageNum,
      AccountNumber = string.Empty,
      RoutingNumber = string.Empty,
      BankAccountTypeId = -1, // none/deleted
    };
  }

  private static void UpdateOtherDiaVehicleFields(Vehicle vehicle,Address garageAddress,DCO.Policy.Vehicle diaVehicle,PolicyCoverages policyCoverages,En_State state,string effectiveDate)
  {
    //CDAP-1775 This flag corresponds to the 'Capacity Over 1 Ton' checkbox in Diamond. Reset the flag before rating.
    diaVehicle.ExcludedDesignatedVehicle = false;

    diaVehicle.VehicleUseTypeId = vehicle.PrimaryUse.GetDiaId();
    diaVehicle.OdometerReading = vehicle.OdometerReading;
    diaVehicle.OdometerDate = new(effectiveDate);
    diaVehicle.HasManualDiscount = vehicle.PhotoDiscount;

    // Look to see if we want there to be a garage address
    if(garageAddress is not null)
    {
      diaVehicle.GaragingAddress ??= CreateActiveDiamondInsPolicyDetailTableObject<DCO.Policy.GaragedAddress>();
      SetDiaAddress(diaVehicle.GaragingAddress.Address,garageAddress);
    }
    else
    {
      // Remove if exists when we don't need
      if(diaVehicle.GaragingAddress is not null)
      {
        MarkForDelete(diaVehicle.GaragingAddress);
      }
    }

    AddOrUpdateCoverages();
    //TODO: Update Lienholder and AddtionalInterest fields

    void AddOrUpdateCoverages()
    {
      if(_DiamondApi.Vehicle.TryGetSymbolsForVin(vehicle.Vin,out var symbols))
      {
        CreateVehicleSymbol(FDEnums.VehicleSymbolCoverageTypeId.Comprehensive,symbols.Comprehensive,diaVehicle);
        CreateVehicleSymbol(FDEnums.VehicleSymbolCoverageTypeId.Collision,symbols.Collision,diaVehicle);
        CreateVehicleSymbol(FDEnums.VehicleSymbolCoverageTypeId.Liability,symbols.Liability,diaVehicle);
      }
      vehicle.Coverages ??= VehicleCoverages.GetDefaultCoverage(state);
      var coverages = vehicle.Coverages;

      CreateVehicleCoverage(FDEnums.CoverageCode.BodilyInjury,policyCoverages.BI,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.PropertyDamage,policyCoverages.PD,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.UMBI,policyCoverages.UMBI,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.UIMBI,policyCoverages.UIMBI,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.PIP,policyCoverages.PIP,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.MedicalPayments,policyCoverages.MP,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.Comprehensive,coverages.ComColDeductible,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.Collision,coverages.ComColDeductible,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.UMPD,coverages.UMPD,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.UMPDDeductible,coverages.UMPDDeductible,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.Towing,coverages.Towing == En_YesNo.Yes,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.RentalReimbursement,coverages.Rental,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.SafetyEquipment,coverages.SafetyEquipment == En_YesNo.Yes,diaVehicle);
      CreateVehicleCoverage(FDEnums.CoverageCode.WorkLossBenefitWaiver,policyCoverages.WorkLossBenefitsWaiver == En_YesNo.Yes,diaVehicle);
    }
  }

  private static void UpdateVersions(DCO.Policy.Image image,En_State state,DateTime effectiveDate)
  {
    var versions = _DiamondApi.Static.GetVersionResult(state,effectiveDate);
    image.AddFormsVersionId = versions.AddFormVersionId;
    image.RatingVersionId = versions.RatingVersionId;
    image.UnderwritingVersionId = versions.UnderwritingVersionId;
    image.VersionId = versions.VersionId;
  }

  #endregion Private Methods
}
