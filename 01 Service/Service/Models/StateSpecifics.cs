using ConsumerServiceHost.Models.Dropdowns;

namespace ConsumerServiceHost.Models;

public class StateSpecifics
{
  #region Public Properties

  public StateDropdowns Dropdowns { get; set; }
  public string EffectiveDate { get; set; }
  public List<UnderwritingQuestion> UnderwritingQuestions { get; set; }

  #endregion Public Properties
}
