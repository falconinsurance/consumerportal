using ConsumerServiceHost.Apis;
using DiamondServices.Apis;
using DiamondServices.Service;
using DocuSignData.DB;
using DoucuSignCommon.Model;
using Falcon.Core.Models;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;
using FEnums = Falcon.Enums;

using M = Falcon.Core.Models;

namespace ConsumerServiceHost.Models;

internal static class ConvertToInstance
{
  #region Private Fields

  private const string _ESIGN_EDITION = "esign";
  private const string _NODISPLAY_EDITION = "nodisplay";
  private const string _WETSIGN_EDITION = "wetsign";
  private static readonly IRelax<IConsumerApi> _ConsumerApiRelaxed = RelaxFactory.Create(AppResolver.Get<IConsumerApi>);
  private static readonly IRelax<IDbDocuSign> _DbDocuSignRelaxed = RelaxFactory.Create(AppResolver.Get<IDbDocuSign>);
  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(AppResolver.Get<IDiamondApi>);

  #endregion Private Fields

  #region Private Properties

  private static IConsumerApi _ConsumerApi => _ConsumerApiRelaxed.Value;
  private static IDbDocuSign _DbDocuSign => _DbDocuSignRelaxed.Value;
  private static IDiamondApi _DiamondApi => _DiamondApiRelaxed.Value;

  #endregion Private Properties

  #region Internal Methods

  internal static Instance Convert(ISession consumerSession,DCO.Policy.Image image,bool boundPolicy)
  {
    if(image == null)
    {
      return null;
    }
    Instance instance = new();
    SetAgency(instance.Agency,image);
    SetPolicy(instance.Agency.State,instance.Policy,image,boundPolicy,consumerSession);
    SetPolicyHolder(instance.PolicyHolder,image.PolicyHolder);
    SetDrivers(instance.Drivers,instance.Agency.State,image,consumerSession);
    SetVehicles(instance.InsVehicles,instance.Agency.State,image);
    //instance.WriteToTempAsJson("InstanceFromImage");
    return instance;
  }

  #endregion Internal Methods

  #region Private Methods

  private static DateTime ConvertToFalcon(this DCO.InsDateTime insDateTime) =>
    insDateTime.IsEmpty() || insDateTime.IsNothing() ?
    new() :
    new(insDateTime.Year,insDateTime.Month,insDateTime.Day);

  private static Address GetAddresInfo(DCO.Address diaAddress) => new()
  {
    Street = diaAddress.HouseNumber.IsNullOrWhiteSpace() ? diaAddress.StreetName : diaAddress.HouseNumber + " " + diaAddress.StreetName,
    State = (En_State)diaAddress.StateId,
    County = diaAddress.County,
    Zip = !string.IsNullOrEmpty(diaAddress.Zip) ? diaAddress.Zip.Substring(0,5) : diaAddress.Zip,
    City = diaAddress.City
  };

  private static Document GetDocument(DCO.Printing.PrintForm printForm) => new()
  {
    Description = printForm.Description,
    FormCategoryTypeId = printForm.FormCategoryTypeId,
    FormNumber = printForm.FormNumber,
    FormId = printForm.PolicyFormNum,
    PolicyId = printForm.PolicyId,
    PolicyImageNumber = printForm.PolicyImageNum,
    KeyValue = printForm.KeyValue,
    Recipient = printForm.PrintRecipients[0].Description,
    Version = printForm.EditionVersion
  };

  private static Name GetNameInfo(DCO.Name diaName) => new Name().With(c =>
  {
    c.DateOfBirth = diaName.BirthDate.ToString("MM/dd/yyyy");
    c.First = diaName.FirstName;
    c.Last = diaName.LastName;
    c.Middle = diaName.MiddleName;
    c.Gender = Gender.FromDiaId(diaName.SexId,En_Gender.Male);
    c.MaritalStatus = MaritalStatus.FromDiaId(diaName.MaritalStatusId,En_MaritalStatus.Single);
  });

  private static bool IsValidUSState(int stateId) => ((En_State)stateId).IsValidUSState();

  private static void SetAgency(Agency agency,DCO.Policy.Image image)
  {
    agency.State = _DiamondApi.Static.GetState(image.VersionId);
    agency.Id = image.AgencyId;
    agency.ProducerId = image.AgencyProducerId;
  }

  private static void SetBillingItem(BillingItem billingItem,decimal total,decimal down,decimal installment)
  {
    billingItem.Total = total;
    billingItem.Down = down;
    billingItem.Installment = installment;
  }

  private static void SetCoverages(PolicyCoverages coverages,DCO.InsCollection<DCO.Policy.Vehicle> vehicles,En_State state)
  {
    vehicles.ForEach(vehicle =>
    {
      vehicle.Coverages.ForEach(coverage =>
      {
        var coverageId = (FDEnums.CoverageCode)coverage.CoverageCodeId;
        switch(coverageId)
        {
          case FDEnums.CoverageCode.BodilyInjury:
            coverages.BI = BILimit.FromDiaId(coverage.CoverageLimitId,En_BILimit.TwentyForty);
            break;

          case FDEnums.CoverageCode.PropertyDamage:
            coverages.PD = PDLimit.FromDiaId(coverage.CoverageLimitId);
            break;

          case FDEnums.CoverageCode.UMBI:
            coverages.UMBI = UMBILimit.FromDiaId(coverage.CoverageLimitId);
            break;

          case FDEnums.CoverageCode.UIMBI:
            coverages.UIMBI = coverage.WrittenPremium > 0 ? UIMBILimit.FromDiaId(coverage.CoverageLimitId) : coverages.UIMBI;
            break;

          case FDEnums.CoverageCode.PIP:
            coverages.PIP = PIPLimit.FromDiaId(coverage.CoverageLimitId,En_PIPLimit.None);
            break;

          case FDEnums.CoverageCode.MedicalPayments:
            coverages.MP = MPLimit.FromDiaId(coverage.CoverageLimitId,En_MPLimit.None);
            break;

          case FDEnums.CoverageCode.WorkLossBenefitWaiver:
            coverages.WorkLossBenefitsWaiver = coverage.Checkbox ? En_YesNo.Yes : En_YesNo.NA;
            if(state.In(En_State.UT))
            {
              coverages.WorkLossBenefitsWaiver = coverage.Checkbox ? En_YesNo.Yes : En_YesNo.No;
            }
            break;
        }
      });
    });
  }

  //private static void SetCurrentPayPlan(PayplanItem payPlanItem,bool boundPolicy,DCO.Policy.Image image,ISession session)
  //{
  //  var billingData = boundPolicy ? _DiamondApi.Billing.LoadBillingData(session,image.PolicyId) : _DiamondApi.Billing.LoadBillingDataPreview(session,image.PolicyId,image.PolicyImageNum,image.CurrentPayplanId);

  //  var payPlan = _DiamondApi.Billing.LoadPayPlan(image.CurrentPayplanId);

  //  var state = _DiamondApi.Static.GetState(image.VersionId);

  //  payPlanItem.setObj(image,billingData,state,payPlan.NumInstalls);
  //}

  private static void SetDrivers(List<Driver> drivers,En_State state,DCO.Policy.Image image,ISession consumerSession)
  {
    foreach(var diaDriver in image.LOB.RiskLevel.Drivers)
    {
      if(diaDriver.DetailStatusCode != (int)DCE.StatusCode.Deleted)
      {
        var driver = GetDriver(diaDriver);
        var driverExclusionReason = diaDriver.DriverExcludeReasonId != 0 ? DriverExclusionReason.FromDiaId(diaDriver.DriverExcludeReasonId,En_DriverExclusionReason.NA) : En_DriverExclusionReason.NoVal;

        UpdateLicenseInfo(driver.License,diaDriver.Name.DLN,diaDriver.LicenseStatusId,diaDriver.Name.DLStateId);
        UpdateFilingInfo(driver.Filing,diaDriver.FilingInformation);
        UpdateEmploymentInfo(driver.Employment,diaDriver);
        UpdateStateSpecificFields(driver);
        SetCourseDiscountInfo(driver.CourseDiscount,diaDriver);
        SetViolations(driver.Violations,diaDriver.AccidentViolations);

        drivers.Add(driver);
      }
    }

    Driver GetDriver(DCO.Policy.Driver diaDriver) => new()
    {
      Id = diaDriver.DriverNum,
      License = new License().With(license =>
      {
        license.Months36 = diaDriver.LicenseTypeId == (int)FDEnums.LicenseTypeId.GreaterThanEqualTo36Months ? En_YesNo.Yes : En_YesNo.No;
      }),
      Name = GetNameInfo(diaDriver.Name),
      Relationship = DriverRelationship.FromDiaId(diaDriver.RelationshipTypeId,En_DriverRelationship.Other),
      Type = (En_Driver)diaDriver.DriverExcludeTypeId,
      //DriverExclusionReason = diaDriver.DriverExcludeReasonId != 0 ? DriverExclusionReason.FromDiaId(diaDriver.DriverExcludeReasonId,En_DriverExclusionReason.NA) : En_DriverExclusionReason.NoVal,
      IsUDR = diaDriver.ReasonExcludedDescription == "UDR" //This is set in Decisioning (RiskUDR class)
    };

    void UpdateLicenseInfo(License license,string dialicenseNum,int diaLicenseStatusId,DCE.State diaDLStateId)
    {
      var tempL = _DiamondApi.Security.DecryptLicenseNumber(consumerSession,dialicenseNum,image.PolicyId);
      if(!string.IsNullOrEmpty(tempL))
      {
        license.Number = tempL;
      }

      var diaState = StateType.From(diaDLStateId);
      license.State = diaState.IsValidUSState() ? diaState : En_State.Other;

      switch(diaLicenseStatusId)
      {
        case (int)FDEnums.LicenseStatus.NotLicensed:
        case (int)FDEnums.LicenseStatus.None:
        case (int)FDEnums.LicenseStatus.Unknown:
          license.Type = En_License.NotLicensed;
          license.State = En_State.Other;
          break;

        case (int)FDEnums.LicenseStatus.Expired:
          license.Type = En_License.Expired;
          break;

        case (int)FDEnums.LicenseStatus.TemporaryLearnerPermit:
          license.Type = En_License.Permit;
          license.State = license.State == En_State.Other ? state : license.State;
          break;

        case 10: // TVDL
          license.Type = En_License.TVDL;
          license.State = license.State == En_State.Other ? state : license.State;
          break;

        case 11: // StateID
          license.Type = En_License.StateID;
          license.State = license.State == En_State.Other ? state : license.State;
          break;

        case (int)FDEnums.LicenseStatus.Suspended:
        case (int)FDEnums.LicenseStatus.Revoked:
          license.Type = En_License.Suspended;
          license.State = license.State == En_State.Other ? state : license.State;
          break;

        case (int)FDEnums.LicenseStatus.Valid:
          if(state == diaState)
          {
            license.Type = LicenseType.From(diaState,En_License.NotLicensed);
            license.State = state;
          }
          else
          {
            license.Type = diaDLStateId switch
            {
              DCE.State.NA_Other => En_License.International,
              _ => En_License.OutOfState,
            };
          }

          break;

        default:
          throw new Exception("DriverLicense conversion (image to instance): Unrecognized LicenseStatusId.");
      }
    }
    static void UpdateFilingInfo(Filing filing,DCO.Policy.FilingInfo diaFiling)
    {
      if(diaFiling is not null)
      {
        filing.IsSR22 = diaFiling.SR22;
        filing.IsSR50 = diaFiling.SR50;
      }
    }
    static void UpdateEmploymentInfo(Employment employment,DCO.Policy.Driver diaDriver)
    {
      employment.Occupation = !string.IsNullOrEmpty(diaDriver.Name?.PositionTitle) ? diaDriver.Name.PositionTitle : employment.Occupation;
      employment.Employer = !string.IsNullOrEmpty(diaDriver.Employment?.Occupation) ? diaDriver.Employment.Occupation : employment.Employer;
    }
    void UpdateStateSpecificFields(Driver driver)
    {
      driver.Relationship = state == En_State.AZ && driver.Relationship == En_DriverRelationship.Insured ? En_DriverRelationship.Policyholder : driver.Relationship;
      //driver.DriverExclusionReason = driver.DriverExclusionReason == En_DriverExclusionReason.NotLicensed && state == En_State.UT ? En_DriverExclusionReason.NoValidLicense : driver.DriverExclusionReason;
    }
    static void SetCourseDiscountInfo(CourseDiscount courseDiscount,DCO.Policy.Driver diaDriver)
    {
      courseDiscount.AccidentPrevention = GetCourse(diaDriver.AccPrevCourse,diaDriver.AccPrevCourseDate);
      courseDiscount.DefensiveDriver = GetCourse(diaDriver.DefDriver,diaDriver.DefDriverDate);
      courseDiscount.MatureDriver = GetCourse(diaDriver.SeniorDiscount,diaDriver.AccPreventionCourse);

      Course GetCourse(bool flag,DCO.InsDateTime courseDate)
      {
        if(flag)
        {
          return new() { Date = courseDate.ConvertToFalcon().ToString("MM/dd/yyyy"),Value = En_YesNo.Yes };
        }
        return new() { Value = En_YesNo.No };
      }
    }
    static void SetViolations(List<M.Violation> violations,DCO.InsCollection<DCO.Policy.AccidentViolation> diaViolations)
    {
      foreach(var diaViolation in diaViolations)
      {
        if(diaViolation.DetailStatusCode != (int)DCE.StatusCode.Deleted)
        {
          var violation = GetViolation(diaViolation);
          violations.Add(violation);
        }
      }

      M.Violation GetViolation(DCO.Policy.AccidentViolation diaViolation) => new()
      {
        Id = diaViolation.ViolationNum.Id,
        Type = FEnums.Violation.FromDiaId(diaViolation.AccidentsViolationsTypeId),
        Date = diaViolation.ConvictionDate.ConvertToFalcon().ToString("MM/dd/yyyy")
      };
    }
  }

  //private static void SetMainPayment(BillingItem billingItem,PayplanItem payPlanItem,DCO.Policy.Image image,ISession session,En_State state)
  //{
  //  var payPlans = _DiamondApi.Billing.LoadAllPayPlans(session,image);

  //  var currentPayPlan = payPlans.FirstOrDefault(p => p.PayPlanId == image.CurrentPayplanId);
  //  var isPayInFullPayPlan = currentPayPlan.PayPlanId == 1
  //    || currentPayPlan.PayPlanId == (int)FDEnums.BillingPayPlan.FullPay
  //    || currentPayPlan.PayPlanId == (int)FDEnums.BillingPayPlan.FullPayPremiumFinance;

  //  var numOfActualInstallments = currentPayPlan.NumInstalls - 1;
  //  decimal installmentFeeAmount = 0;

  //  if(!isPayInFullPayPlan)
  //  {
  //    installmentFeeAmount = currentPayPlan.ServiceChargeAmount;
  //  }

  //  var SR22Fee = payPlanItem.SR22Fee.Total;

  //  switch(state)
  //  {
  //    case En_State.AZ:
  //    case En_State.CO:
  //      var theftProtectionFee = payPlanItem.TheftProtectionFee.Total;

  //      SetBillingItem(billingItem
  //        ,getTotalAZAndCO(currentPayPlan)
  //        ,currentPayPlan.DownPaymentAmount - SR22Fee + payPlanItem.AgencyFee.Down
  //        ,currentPayPlan.InstallmentAmount + ((numOfActualInstallments > 0) ? installmentFeeAmount : 0) + payPlanItem.AgencyFee.Installment);

  //      decimal getTotalAZAndCO(DCO.Billing.PayPlanPreview currentPayPlan)
  //      {
  //        return currentPayPlan.DownPaymentAmount - SR22Fee
  //          + (currentPayPlan.InstallmentAmount * numOfActualInstallments)
  //          + payPlanItem.AgencyFee.Down;
  //      }
  //      break;

  //    case En_State.IL:
  //      SetBillingItem(billingItem
  //        ,currentPayPlan.DownPaymentAmount + currentPayPlan.TotalInstallmentAmount + numOfActualInstallments + installmentFeeAmount
  //        ,currentPayPlan.DownPaymentAmount
  //        ,currentPayPlan.InstallmentAmount + installmentFeeAmount);
  //      break;

  //    case En_State.IN:
  //      SetBillingItem(billingItem
  //        ,getTotalIN(currentPayPlan)
  //        ,currentPayPlan.DownPaymentAmount + payPlanItem.AgencyFee.Down
  //        ,currentPayPlan.InstallmentAmount + installmentFeeAmount + payPlanItem.AgencyFee.Installment);

  //      decimal getTotalIN(DCO.Billing.PayPlanPreview currentPayPlan)
  //      {
  //        var totalFee = (installmentFeeAmount + payPlanItem.AgencyFee.Installment) * numOfActualInstallments;
  //        return currentPayPlan.DownPaymentAmount
  //          + (currentPayPlan.InstallmentAmount * numOfActualInstallments)
  //          + payPlanItem.AgencyFee.Down
  //          + totalFee;
  //      }
  //      break;

  //    case En_State.OK:
  //      const int policyFeeDownOK = 15;

  //      SetBillingItem(billingItem
  //        ,getTotalOK(currentPayPlan)
  //        ,0
  //        ,0);

  //      decimal getTotalOK(DCO.Billing.PayPlanPreview currentPayPlan)
  //      {
  //        var policyFee = (numOfActualInstallments > 0) ? installmentFeeAmount : 0;
  //        return currentPayPlan.DownPaymentAmount - policyFee + (currentPayPlan.InstallmentAmount * numOfActualInstallments)
  //          + policyFeeDownOK + payPlanItem.AgencyFee.Down
  //          + (policyFee * numOfActualInstallments)
  //          + policyFeeDownOK
  //          + payPlanItem.AgencyFee.Down
  //          + (policyFee * numOfActualInstallments);
  //      }
  //      break;

  //    case En_State.TX:
  //      const int policyFeeDownTX = 5;

  //      SetBillingItem(billingItem
  //        ,getTotalTX(currentPayPlan)
  //        ,currentPayPlan.DownPaymentAmount + payPlanItem.AgencyFee.Down
  //        ,currentPayPlan.InstallmentAmount + payPlanItem.AgencyFee.Installment);

  //      decimal getTotalTX(DCO.Billing.PayPlanPreview currentPayPlan)
  //      {
  //        return currentPayPlan.DownPaymentAmount
  //          - policyFeeDownTX
  //          - payPlanItem.SR22Fee.Total
  //          + payPlanItem.TotalFees.Total;
  //      }
  //      break;

  //    case En_State.UT:
  //      var policyFee = (numOfActualInstallments > 0) ? installmentFeeAmount : 0;

  //      SetBillingItem(billingItem
  //        ,getTotalUT(currentPayPlan)
  //        ,currentPayPlan.DownPaymentAmount + payPlanItem.AgencyFee.Down
  //        ,currentPayPlan.InstallmentAmount + policyFee + payPlanItem.AgencyFee.Installment);
  //      break;

  //      decimal getTotalUT(DCO.Billing.PayPlanPreview currentPayPlan)
  //      {
  //        var policyFees = (numOfActualInstallments > 0) ? policyFeeDownTX : payPlanItem.PolicyFee.Total;
  //        return currentPayPlan.DownPaymentAmount
  //          + (currentPayPlan.InstallmentAmount * numOfActualInstallments)
  //          + payPlanItem.AgencyFee.Down
  //          + (policyFee * numOfActualInstallments);
  //      }

  //    default:
  //      break;
  //  }
  //}

  private static void SetModifiersAndPolicyType(Modifiers modifiers,Policy policy,DCO.Policy.PolicyLevel policyLevel,DCO.InsCollection<DCO.Policy.Vehicle> vehicles)
  {
    //TODO:
    foreach(var diaModifier in policyLevel.Modifiers)
    {
      switch(diaModifier.ModifierTypeId)
      {
        case (int)FDEnums.ModifierTypeId.TransferDiscount:
          // This is a Transfer Discount Modifier
          if(policyLevel.PriorCarrier != null)
          {
            modifiers.TransferDiscount.SetObj(policyLevel.PriorCarrier.PriorPolicy,policyLevel.PriorCarrier.Name.CommercialName1,policyLevel.PriorCarrier.PriorExpirationDate.ConvertToFalcon());
          }
          break;

        case (int)FDEnums.ModifierTypeId.HomeownersDiscount:
          // This is a Home Owner Discount Modifier
          modifiers.HomeownersDiscount = diaModifier.CheckboxSelected ? En_YesNo.Yes : En_YesNo.No;
          break;

        case 27:
          // This is a Direct Bill Discount Modifier
          modifiers.DirectBillDiscount = En_YesNo.Yes;
          break;

        case (int)FDEnums.ModifierTypeId.ConsentToReceiveDocumentsElectronically:
          // This is a Consent To Receive Documents Electronically Modifier
          modifiers.ReceiveElectronicDocuments = diaModifier.CheckboxSelected ? En_YesNo.Yes : En_YesNo.No;
          break;

        case (int)FDEnums.ModifierTypeId.PolicyType:
          policy.Type = diaModifier.ModifierOptionId == (int)FDEnums.ModifierOptionId.Limited ? En_Policy.Limited : En_Policy.Owner;

          break;

        case (int)FDEnums.ModifierTypeId.ReturnedMail:
          //policy.Metadata.ReturnedMail = modifier.CheckboxSelected;
          break;

        // Converted From Limited
        case 33:
          //policy.IsConvertedFromLimited = true;
          break;
      }
    }

    if(vehicles.Any(v => v.NonOwned))
    {
      policy.Type = En_Policy.NonOwner;
    }
  }

  private static void SetPolicy(En_State state,Policy policy,DCO.Policy.Image image,bool boundPolicy,ISession session)
  {
    policy.EffectiveDate = image.EffectiveDate.ToString("MM/dd/yyyy");
    policy.ExpirationDate = image.ExpirationDate.ToString("MM/dd/yyyy");
    policy.Id = image.PolicyId;
    policy.ImageNum = image.PolicyImageNum;
    policy.Number = image.PolicyNumber;
    policy.Term = TermLength.FromDiaId(image.PolicyTermId);
    policy.VersionId = image.VersionId;
    policy.RatingVersionId = image.RatingVersionId;
    SetModifiersAndPolicyType(policy.Modifiers ??= new(),policy,image.LOB.PolicyLevel,image.LOB.RiskLevel.Vehicles);
    SetCoverages(policy.Coverages = new PolicyCoverages().SetGetDefaults(state),image.LOB.RiskLevel.Vehicles,state);
    SetPaymentInfo(policy.PaymentInfo);
    SetUnderwritingResponses(policy.UnderwritingResponses,image.LOB.PolicyLevel.PolicyUnderwritings);

    var imageNumber = image.PolicyImageNum;
    var printForms = _DiamondApi.Printing.LoadPrintForms(session,image.PolicyId,imageNumber);

    PopulateAllDocuments(policy.Documents);

    void PopulateAllDocuments(Documents documents)
    {
      printForms.ForEach(p => AddIfAllowed(documents,p));
      AddFalconDocuments();
      if(boundPolicy)
      {
        UpdatePolicyIdCards();
        AddESignedDocuments();
      }

      void AddESignedDocuments()
      {
        var envelope = _DbDocuSign.GetFromPolicyInfo(new() { PolicyId = policy.Id,ImageNum = policy.ImageNum });
        if(envelope?.Signers.All(s => s.Status == EnSignerStatus.signing_complete) == true)
        {
          var desc = envelope.Descriptions.FirstOrDefault(c => c.Contains("Policy Application"));
          //Adding only if the documents are signed.
          documents.Regular.Add(new()
          {
            Description = string.IsNullOrEmpty(desc) ? Apis.Tools.ESIGN : desc,
            PolicyId = envelope.PolicyId,
            PolicyImageNumber = envelope.ImageNum
          });
        }
      }

      void UpdatePolicyIdCards()
      {
        try
        {
          var idCards = documents.Regular.Where(c => c.FormNumber.Contains("-004") && !c.FormNumber.EndsWith("-004A"))
        .OrderByDescending(c => c.PolicyImageNumber).ToList();

          foreach(var diaVehicle in image.LOB.RiskLevel.Vehicles)
          {
            var vehicle = _DiamondApi.Vehicle.GetVehicles(diaVehicle.Vin).FirstOrDefault();
            var idCard = idCards.Find(v => v.KeyValue == diaVehicle.VehicleNum.Id.ToString());
            if(idCard != null)
            {
              idCard.Description = $"ID Card - {vehicle.Year} {vehicle.Make.Description} {vehicle.Model.Description}";
            }
          }
        }
        catch
        {
        }
      }

      void AddFalconDocuments()
      {
        // Only want to call AddFalconDocuments once when Application is present
        if(!printForms.Any(c => c.FormNumber.EndsWith("-002")))
          return;

        var docs = _DiamondApi.DbFalcon.SqlSet("Exec [Docs].[spGetDocuments] @PolicyId=@1",c => c.AddValues(image.PolicyId));
        if(docs.Count == 0)
          return;
        foreach(var doc in docs)
        {
          var document = new Document
          {
            Description = $"{doc["Description"]}",
            FormId = -100,
            FormNumber = $"{doc["State"]}-{doc["FormNumber"]}",
            PolicyId = image.PolicyId,
            PolicyImageNumber = image.PolicyImageNum
          };
          if(doc.ToBool("IsWetSign"))
          {
            documents.WetSign.AddIfNot(document,c => c.FormNumber == document.FormNumber);
          }
          else
          {
            documents.Regular.AddIfNot(document,c => c.FormNumber == document.FormNumber);
          }
        }
      }

      void AddIfAllowed(Documents documents,DCO.Printing.PrintForm printForm)
      {
        // Don't process Claims letters, if set to No Display
        var flag = true;
        if(printForm.FormNumber.Contains("-C") || printForm.EditionVersion.Trim().ToLowerInvariant() == _NODISPLAY_EDITION)
        {
          flag = false;
        }

        // We don't have to do anything if there's an insured version of this document
        if(printForm.PrintRecipients.Count > 0)
        {
          // Skip the forms that aren't of type print
          if(printForm.PrintRecipients[0].OutputTypeId != (int)Diamond.Common.Enums.Printing.OutputType.Print)
          {
            flag = false;
          }
          // Skip the forms that have an insured copy of the same form
          if(printForm.PrintRecipients[0].PrintRecipientId != (int)Diamond.Common.Enums.Printing.PrintRecipient.Insured)
          {
            var insuredPrintForm = printForms.FirstOrDefault(k => (k.PrintRecipients.Count > 0) && (k.PrintRecipients[0].PrintRecipientId == (int)Diamond.Common.Enums.Printing.PrintRecipient.Insured) && (k.FormNumber.ToLowerInvariant().Trim() == printForm.FormNumber.ToLowerInvariant().Trim()) && (k.EditionVersion.ToLowerInvariant().Trim() == printForm.EditionVersion.ToLowerInvariant().Trim()));
            if(insuredPrintForm != null)
            {
              flag = false;
            }
          }
        }

        //we found the document - determine its type.
        var edition = printForm.EditionVersion.Trim().ToLowerInvariant();

        // Put the document in the correct queue depending on it's type
        if(flag && !string.IsNullOrEmpty(edition))
        {
          flag = false;
          if(edition == _ESIGN_EDITION)
          {
            documents.ESign.Add(GetDocument(printForm));
          }
          else if(edition == _WETSIGN_EDITION)
          {
            documents.WetSign.Add(GetDocument(printForm));
          }
        }

        //Adding all other documents to regular
        if(flag)
        {
          documents.Regular.Add(GetDocument(printForm));
        }
      }
    }

    void SetPaymentInfo(PaymentInfo paymentInfo)
    {
      SetPayPlans(paymentInfo.Payplans);
      SetRecurring(paymentInfo);
      void SetPayPlans(Payplans payplans)
      {
        var diaPayplans = _DiamondApi.Billing.LoadAllPayPlans(session,image);
        if(diaPayplans is not null && diaPayplans.Count > 0)
        {
          var stateSpecDropdowns = _ConsumerApi.StateSpec.GetStateSpecifics(state,En_Language.EN,policy.EffectiveDate.ToDate()).Dropdowns.PolicyOptions.PayPlanType.Items;
          foreach(var item in diaPayplans)
          {
            var type = PayPlanType.FromDiaId(item.PayPlanId);
            if(stateSpecDropdowns.Find(c => c.Value == (int)type) != null)
            {
              var billingData = boundPolicy ? _DiamondApi.Billing.LoadBillingData(session,image.PolicyId) : _DiamondApi.Billing.LoadBillingDataPreview(session,image.PolicyId,image.PolicyImageNum,item.PayPlanId);
              var payplanTypeId = _DiamondApi.Static.GetPayplanTypeId(item.PayPlanId);
              var payplan = new PayplanItem().With(a => a.Build(item,type,payplanTypeId,billingData,state));
              if(payplan.Tag != "Recurring")
              {
                //adding only non-recurring plans
                payplans.Collection.Add(payplan);
              }
            }
          }
          payplans.Current = payplans.Collection.Find(c => c.Id == image.CurrentPayplanId);
          payplans.Collection = payplans.Collection.OrderByDescending(c => c.Tag == "PayInFull").ThenByDescending(c => c.Tag == "Installments").ToList();
        }
      }

      void SetRecurring(PaymentInfo paymentInfo)
      {
        paymentInfo.RecurringPayment = paymentInfo.Payplans.Current.Tag == "Recurring" ? new() : null;
      }
    }
  }

  private static void SetPolicyHolder(PolicyHolder policyHolder,DCO.Policy.PolicyHolder diaPolicyHolder)
  {
    policyHolder.Address = GetAddresInfo(diaPolicyHolder.Address);
    policyHolder.Name = GetNameInfo(diaPolicyHolder.Name);
    AddPhoneInfos(policyHolder.Phones,diaPolicyHolder.Phones);
    policyHolder.Email = diaPolicyHolder.Emails.FirstOrDefault(c => c.TypeId == DCE.EMailType.Home)?.Address;

    void AddPhoneInfos(List<Phone> phones,DCO.InsCollection<DCO.Phone> diaPhones)
    {
      foreach(var diaPhone in diaPhones)
      {
        if(diaPhone != null && diaPhone.DetailStatusCode != (int)DCE.StatusCode.Deleted)
        {
          phones.Add(new()
          {
            Type = PhoneNumberType.FromDiaId((int)diaPhone.TypeId),
            Number = diaPhone.Number,
          });
        }
      }
    }
  }

  private static void SetRecurringPayment(Payment recurringPayment,DCO.Policy.Image image,ISession consumerSession)
  {
    recurringPayment.Type = PaymentType.FromDiaId(image.CurrentPayplanId);
    switch(recurringPayment.Type)
    {
      case En_Payment.ACH:
        var eft = image.Policy.EFT;
        if(!string.IsNullOrEmpty(eft.RoutingNumber))
        {
          recurringPayment.Eft = new()
          {
            AccountNumber = _DiamondApi.Security.DecryptEft(consumerSession,eft.AccountNumber,eft.PolicyId),
            Type = BankAccountType.FromDiaId(eft.BankAccountTypeId),
            RoutingNumber = eft.RoutingNumber,
            FullName = eft.AccountHolder
          };
        }
        break;

      case En_Payment.CreditCard:
        var cc = image.Policy.CreditCardData;
        if(cc.CreditCardDataId > 0)
        {
          var (token, last4, expirationDate, CreditCardTypeId) = _DiamondApi.Static.GetDefaultCreditCardTokenData(cc.PolicyId);
          recurringPayment.CreditCard = new CreditCard()
          {
            Token = token,
            Last4 = last4,
            Type = PaymentCardType.FromDiaId(cc.CreditCardDataId),
          }.With(c => c.Expiration.SetObj(expirationDate));
        }
        break;

      default:
        break;
    }
  }

  private static void SetUnderwritingResponses(List<UnderwritingResponse> underwritingResponses,DCO.InsCollection<DCO.Policy.PolicyUnderwriting> diaUnderwritings) => diaUnderwritings.ForEach(diaUnderwriting => underwritingResponses.Add(new()
  {
    DiamondId = diaUnderwriting.PolicyUnderwritingCodeId,
    Value = (En_AnswerValue)diaUnderwriting.PolicyUnderwritingAnswer,
    Answer = diaUnderwriting.PolicyUnderwritingExtraAnswer
  }));

  private static void SetVehicles(InsVehicles instVehicles,En_State state,DCO.Policy.Image image)
  {
    //check if a garrage address is set and then add it to instance vehicles
    var diaGarageAddress = image.LOB.RiskLevel.Vehicles.FirstOrDefault(v => !string.IsNullOrEmpty(v.GaragingAddress?.Address?.Zip));
    if(diaGarageAddress != null)
    {
      instVehicles.Address = GetAddresInfo(diaGarageAddress.GaragingAddress.Address);
    }
    foreach(var diaVehicle in image.LOB.RiskLevel.Vehicles)
    {
      var vehicle = GetVehicle(diaVehicle);
      var v = _DiamondApi.Vehicle.GetVehicles(diaVehicle.Vin).FirstOrDefault();
      vehicle.Make = v.Make;
      vehicle.Model = v.Model;
      vehicle.Year = v.Year;
      //TODO: improve this
      //CDAP-2662 v.BodyType is not always correct for some vehicles, so using this explicit call to get the right body type
      vehicle.BodyType = _DiamondApi.Vehicle.GetBodyTypes(vehicle.Year,vehicle.Make,vehicle.Model).FirstOrDefault(c => c.Id == v.BodyType.Id);
      vehicle.Symbol = v.Symbol;
      if(diaVehicle.Vin.Length == 17)
      {
        vehicle.Vin = diaVehicle.Vin;
      }
      vehicle.PrimaryUse = VehicleUse.From(diaVehicle.VehicleUseTypeId);
      SetCoverages(vehicle.Coverages = new VehicleCoverages().SetGetDefaults(state),diaVehicle.Coverages);
      vehicle.OdometerReading = diaVehicle.OdometerReading;
      vehicle.PhotoDiscount = diaVehicle.HasManualDiscount;
      instVehicles.Vehicles.Add(vehicle);
    }

    static Vehicle GetVehicle(DCO.Policy.Vehicle diaVehicle) => new()
    {
      Id = diaVehicle.VehicleNum,
      //Vin = diaVehicle.Vin,
    };

    static VehicleCoverages SetCoverages(VehicleCoverages coverages,DCO.InsCollection<DCO.Coverage> diaCoverages)
    {
      foreach(var diaCoverage in diaCoverages)
      {
        switch(diaCoverage.CoverageCodeId)
        {
          case (int)FDEnums.CoverageCode.Comprehensive:
          case (int)FDEnums.CoverageCode.Collision:
            coverages.ComColDeductible = ComColDeductible.FromDiaId(diaCoverage.CoverageLimitId,En_ComColDeductible.NoInsurance);
            break;

          case (int)FDEnums.CoverageCode.UMPD:
            coverages.UMPD = UMPDLimit.FromDiaId(diaCoverage.CoverageLimitId);
            break;

          case (int)FDEnums.CoverageCode.UMPDDeductible:
            coverages.UMPDDeductible = UMPDDeductible.FromDiaId(diaCoverage.CoverageLimitId,En_UMPDDeductible.None);
            break;

          case (int)FDEnums.CoverageCode.Towing:
            coverages.Towing = diaCoverage.Checkbox ? En_YesNo.Yes : En_YesNo.No;
            break;

          case (int)FDEnums.CoverageCode.RentalReimbursement:
            coverages.Rental = RentalType.FromDiaId(diaCoverage.CoverageLimitId);
            break;

          case (int)FDEnums.CoverageCode.SafetyEquipment:
            coverages.SafetyEquipment = diaCoverage.Checkbox ? En_YesNo.Yes : En_YesNo.No;
            break;
        }
      }
      return coverages;
    }
  }

  #endregion Private Methods
}
