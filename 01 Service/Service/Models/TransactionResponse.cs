using Falcon.Core.Models;

namespace ConsumerServiceHost.Models;

public class TransactionResponse
{
  #region Public Properties

  public string Error { get; set; }
  public Instance Instance { get; set; }
  public bool Success { get; set; } = false;

  #endregion Public Properties
}
