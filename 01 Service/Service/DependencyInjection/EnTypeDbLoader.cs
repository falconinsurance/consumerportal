using Falcon.FalconDB;
using DB = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.DependencyInjection;

public class EnTypeDbLoader:IEnTypeLoader
{
  #region Private Fields
  private readonly IRelax<IFalconHandler> _FalconHandlerRelaxed = RelaxFactory.Create(() => AppResolver.Get<IFalconHandler>());
  #endregion Private Fields

  #region Public Methods

  public EnTypeDic Load() => Load_Collection();

  #endregion Public Methods

  #region Private Methods

  private EnTypeDic Load_Collection()
  {
    var set = _FalconHandlerRelaxed.Value.SqlSet("Exec Enum.spGetEnTypes");
    return MapSet(set);
  }

  private EnTypeDic MapSet(DB.RecordSet set)
  {
    var res = new EnTypeDic();
    var groups = set.CastJson<Item>().GroupBy(c => c.Type);
    foreach(var group in groups)
    {
      var list = new List<EnType>();
      foreach(var item in group)
      {
        list.Add(new EnType
        {
          DiaId = DeSerialize<DiamondId>(item.DiaId),
          Enum = item.Enum,
          Id = item.Id,
          Keys = DeSerialize<Keys>(item.Keys),
          Labels = DeSerialize<Labels>(item.Labels),
          Val = DeSerialize<Values>(item.Val),
          Ext = DeSerialize<EnTypeExt>(item.Ext),
        });
      }
      res[group.Key] = list;
    }

    return res;

    static T DeSerialize<T>(string input)
      => string.IsNullOrWhiteSpace(input) ? default : FormatConvert.DeserializeJson<T>(input);
  }

  #endregion Private Methods

  #region Private Classes

  private class Item
  {
    #region Public Properties
    public string DiaId { get; init; }
    public string Enum { get; init; }
    public string Ext { get; init; }
    public int Id { get; init; }
    public string Keys { get; init; }
    public string Labels { get; init; }
    public string Type { get; init; }
    public string Val { get; init; }
    #endregion Public Properties
  }
  #endregion Private Classes
}
