using System.Reflection;
using DecCommon.DB;
using DecUdr.DB;
using DocuSignData.DB;
using Falcon.ConsumerDB;
using Falcon.Core.Settings;
using Falcon.FalconDB;
using static Falcon.DependencyInjection.ServiceExt;

namespace ConsumerServiceHost.DependencyInjection;

internal static class Dependency
{
  #region Private Fields
  private static bool _IsInit = false;
  private static bool _IsRegisterd = false;
  #endregion Private Fields

  #region Internal Methods

  internal static void Init() => InitAsync().GetAwaiter().GetResult();

  internal static async Task InitAsync()
  {
    if(!_IsRegisterd)
      throw new("Please register first");
    if(_IsInit)
      return;
    _IsInit = true;
    await InitializeAll();
  }

  internal static void Register()
  {
    if(_IsRegisterd)
      return;
    _IsRegisterd = true;
    _ = new ServiceContainer()
      .SetModule<ServicesHostModule>().Build();
  }

  #endregion Internal Methods

  #region Private Methods

  private static void AppInfo(bool isConsoleMode)
  {
    if(!isConsoleMode)
      return;
    var setting = AppResolver.Get<ConfigSettings>();
    Console.WriteLine("Starting Service...");
    Console.WriteLine("---------------------------------------------");
    Console.WriteLine($"{"WebApi Host",-25}=> {setting.WebApiHost}/CP/{{controller}}/{{action}}");
    Console.WriteLine($"{"Software Version",-25}=> {Assembly.GetExecutingAssembly().GetName().Version}");
    Console.WriteLine($"{"DataBase",-25}=> {setting.DbServer}");
    Console.WriteLine($"{"DiamondService Host",-25}=> {setting.DiamondHost}");
    Console.WriteLine("---------------------------------------------");
  }

  private static void ApplicationLoadEnded(bool isConsoleMode)
  {
    if(!isConsoleMode)
      return;
    var tempFile = new FileInfo("c:\\Temp\\Consumer\\Consumer-Check.txt");
    tempFile.Create();
  }

  private static void ApplicationLoadStarted(bool isConsoleMode)
  {
    if(!isConsoleMode)
      return;
    Console.WriteLine("Consumer Services Host Console Mode");
    var tempFolder = new DirectoryInfo("c:\\Temp\\Consumer");
    if(!tempFolder.Exists)
    {
      tempFolder.Create();
    }
    var files = tempFolder.GetFiles("Consumer-Check.txt");
    foreach(var file in files)
    {
      file.Delete();
    }
  }

  private static async Task InitializeAll()
  {
    var isConsoleMode = Environment.UserInteractive;
    ApplicationLoadStarted(isConsoleMode);
    await InizializeConfigHandlerAsync(isConsoleMode);
    await InizializeDBAsync(isConsoleMode);
    await InizializeEnumsAsync(isConsoleMode);
    await InizializeApis(isConsoleMode);
    AppInfo(isConsoleMode);
    ApplicationLoadEnded(isConsoleMode);
  }

  private static async Task InizializeApis(bool isConsoleMode)
  {
    var tasksFunc = new List<Func<Task>>() {
     ()=>RunAsync(isConsoleMode,"IDiamondApi",AppResolver.Get<DiamondServices.Apis.IDiamondApi>().Initialize)
    };
    await RunGroupAsync("Apis",isConsoleMode,tasksFunc);
  }

  private static async Task InizializeConfigHandlerAsync(bool isConsoleMode)
  {
    var tasksFunc = new List<Func<Task>>()
    {
      ()=>RunAsync(isConsoleMode,"ILogHandler", ()=>{
       //await AppResolver.Get<IConfigHandler>().InitAsync();
       _= AppResolver.Get<ILogHandler>();
      }),
    };
    await RunGroupAsync("ConfigHandler",isConsoleMode,tasksFunc);
  }

  private static async Task InizializeDBAsync(bool isConsoleMode)
  {
    var tasksFunc = new List<Func<Task>>() {
        ()=>RunAsync(isConsoleMode,"IUdrHandler",  AppResolver.Get<IUdrHandler>().InitAsync),
        ()=>RunAsync(isConsoleMode,"ICommonHandler",AppResolver.Get<ICommonHandler>().InitAsync),
        ()=>RunAsync(isConsoleMode,"IFalconHandler",AppResolver.Get<IFalconHandler>().InitAsync),
        ()=>RunAsync(isConsoleMode,"IConsumerHandler",AppResolver.Get<IConsumerHandler>().InitAsync),
        ()=>RunAsync(isConsoleMode,"IDbDocuSign",  AppResolver.Get<IDbDocuSign>().InitAsync),
      };
    await RunGroupAsync("Databases",isConsoleMode,tasksFunc);
  }

  private static async Task InizializeEnumsAsync(bool isConsoleMode)
  {
    var tasksFunc = new List<Func<Task>>() {
        ()=>RunAsync(isConsoleMode,"IEnTypeLoader",()=>{
        EnTypeHelper.SetLoader(AppResolver.Get<IEnTypeLoader>);
          _= EnTypeHelper.Collection;
        }),
      };
    await RunGroupAsync("Enums",isConsoleMode,tasksFunc);
  }

  #endregion Private Methods
}
