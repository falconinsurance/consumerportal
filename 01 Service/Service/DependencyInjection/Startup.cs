using System.Web.Http;
using ConsumerServiceHost.Middleware;
using Owin;

namespace ConsumerServiceHost;

public class Startup
{
  #region Public Methods

  // This code configures Web API. The Startup class is specified as a type
  // parameter in the WebApp.Start method.
  public void Configuration(IAppBuilder appBuilder)
  {
    // Configure Web API for self-host.
    var config = new HttpConfiguration();
    config.MapHttpAttributeRoutes();
    config.Routes.MapHttpRoute(
        name: "DefaultApi",
        routeTemplate: "CP/{controller}/{action}/{id}",
        defaults: new { id = RouteParameter.Optional }
    );
    appBuilder.UseConsole();
    appBuilder.UseWebApi(config);
  }

  #endregion Public Methods
}
