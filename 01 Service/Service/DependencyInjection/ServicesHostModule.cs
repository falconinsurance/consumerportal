using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.DependencyInjection;
using DiamondServices.Apis;
using DiamondServices.Service;
using DocuSignData.DB;
using DocuSignService;
using DocuSignService.Service;
using Falcon.ConsumerDB;
using Falcon.Core.Settings;
using Falcon.Data.SmartyStreets.DB;
using Falcon.FalconDB;
using Falcon.Service.SmartyStreets;

namespace ConsumerServiceHost;

public class ServicesHostModule:IServiceModule
{
  #region Private Fields

  private readonly ConfigSettings _ConfigSettings = ConfigSettings.Default;
  private IServiceBuilder _Builder;

  #endregion Private Fields

  #region Public Methods

  public void Load(IServiceBuilder builder)
  {
    _Builder = builder;
    RegisterSettings();
    RegisterLogHandler();
    RegisterConsumerHandler();
    RegisterSmartyStreetsHandler();
    RegisterNotificationFalconHttpClient();
    RegisterOneInc();
    RegisterDiamondService();
    RegisterSmartyStreetsService();
    RegisterFalconHandler();
    RegisterDocuSign();
    builder.SetModule<ApplicationModule>();
    builder.SetModule(c => new DecisioningModule(c.Get<ConfigSettings>()));
  }

  #endregion Public Methods

  #region Private Methods

  private void RegisterConsumerHandler()
    => _Builder.AopSetSingle<IConsumerHandler>(_ => new ConsumerHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Consumer)));

  private void RegisterDiamondService()
    => _Builder.AopSetSingle<IDiamondService>(_ => new DiamondService(_ConfigSettings.DiamondHost));

  private void RegisterDocuSign()
  {
    _Builder.AopSetSingle<IDbDocuSign>(_ => new DbDocuSign(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Services)));
    _Builder.AopSetSingle<IDocuSignApi>(_ =>
    {
      //Setting the consumer portal url here, since docusign tries to send us a response after user completes signing.
      //It communicates throught the portal url.
      GlobalESignService.ReturnUrl = _ConfigSettings.GetEsignReturnUrl();
      return new DocuSignApi(_ConfigSettings.IsProduction);
    });
  }

  private void RegisterFalconHandler()
    => _Builder.AopSetSingle<IFalconHandler>(_ => new FalconHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Falcon)));

  private void RegisterNotificationFalconHttpClient()
    => _Builder.AopSetSingle<IFalconHttpClient>(_ => new FalconHttpClient(_ConfigSettings.GetNotificationServiceUrl()));

  private void RegisterLogHandler()
      => _Builder.SetSingle<ILogHandler>(_ => new LogHandler(_ConfigSettings.LogOption));

  private void RegisterOneInc()
    => _Builder.AopSetSingle<IOneIncApi>(_ => new OneIncApi(_ConfigSettings.IsProduction));

  private void RegisterSettings() => _Builder.SetSingle(_ => _ConfigSettings);

  private void RegisterSmartyStreetsHandler()
    => _Builder.AopSetSingle<ISmartyStreetsHandler>(_ => new SmartyStreetsHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Services)));

  private void RegisterSmartyStreetsService()
      => _Builder.AopSetSingle<ISmartyStreetsService>(_ => new SmartyStreetsService());

  #endregion Private Methods
}
