using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Controllers.Consumer;
using ConsumerServiceHost.Controllers.Init;
using ConsumerServiceHost.Controllers.Quoting;
using ConsumerServiceHost.Controllers.SmartyStreets;
using ConsumerServiceHost.Controllers.VehicleInfo;
using DiamondServices.Apis;

namespace ConsumerServiceHost.DependencyInjection;

public class ApplicationModule:IServiceModule
{
  #region Private Fields

  private IServiceBuilder _Builder;

  #endregion Private Fields

  #region Public Methods

  public void Load(IServiceBuilder builder)
  {
    _Builder = builder;
    RegisterConsumerApi();
    RegisterDiamondApi();
    RegisterEnTypeLoader();
    RegisterWorkerInit();
    RegisterWorkerConsumer();
    RegisterWorkerQuoting();
    RegisterWorkerVehicleInfo();
    RegisterWorkerSmartyStreets();
  }

  #endregion Public Methods

  #region Private Methods

  private void RegisterConsumerApi() => _Builder.AopSetSingle<IConsumerApi>(_ => new ConsumerApi());

  private void RegisterDiamondApi() => _Builder.AopSetSingle<IDiamondApi>(_ => new DiamondApi());

  private void RegisterEnTypeLoader() => _Builder.AopSetSingle<IEnTypeLoader>(_ => new EnTypeDbLoader());

  private void RegisterWorkerConsumer() => _Builder.AopSetSingle<IWrkConsumer>(_ => new WrkConsumer());

  private void RegisterWorkerInit() => _Builder.AopSetSingle<IWrkInit>(_ => new WrkInit());

  private void RegisterWorkerQuoting() => _Builder.AopSetSingle<IWrkQuoting>(_ => new WrkQuoting());

  private void RegisterWorkerVehicleInfo() => _Builder.AopSetSingle<IWrkVehicleInfo>(_ => new WrkVehicleInfo());

  private void RegisterWorkerSmartyStreets() => _Builder.AopSetSingle<IWrkSmartyStreets>(_ => new WrkSmartyStreets());

  #endregion Private Methods
}
