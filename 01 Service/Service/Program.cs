// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ConsumerServiceHost;

internal static class Program
{
  #region Internal Methods

  internal static void Main()
  {
    using IHost host = BuildHost();
    host.Run();
  }

  #endregion Internal Methods

  #region Private Methods

  private static IHost BuildHost() => Host.CreateDefaultBuilder()
       .UseWindowsService()
       .ConfigureServices(SetServices)
       .Build();

  private static void SetServices(HostBuilderContext hostBuilderContext,IServiceCollection services)
  {
    services.AddHostedService<ServicesHost>();
  }

  #endregion Private Methods
}
