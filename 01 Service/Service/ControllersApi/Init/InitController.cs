using System.Web.Http;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.ConsumerDB;
using DB = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Controllers.Init;

public class InitController:ApiController
{
  #region Private Fields

  private static readonly IRelax<IWrkInit> _WorkerRelaxed = RelaxFactory.Create(AppResolver.Get<IWrkInit>);

  #endregion Private Fields

  #region Private Properties

  private IWrkInit _Worker => _WorkerRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  [HttpPost] public Task<FalconResponse<List<Agency>>> GetAgencies(DB.Record request) => Run(c => c.GetAgencies(request.To<Guid>("AgencyGroupId")));

  [HttpPost] public Task<FalconResponse<Dictionary<string,string>>> GetLabels(DB.Record request) => Run(c => c.GetLabelsAsync(request.To("Language",En_Language.EN)));

  [HttpPost] public Task<FalconResponse<StateSpecifics>> GetStateSpecifics(DB.Record request) => Run(c => c.GetStateSpecificsAsync(request.To("State",En_State.NA),request.To("Language",En_Language.EN)));

  #endregion Public Methods

  #region Private Methods

  private Task<T> Run<T>(Func<IWrkInit,Task<T>> func) => Task.Run(async () => await func(_Worker).ConfigureAwait(false));

  #endregion Private Methods
}
