using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Controllers.Init;

public interface IWrkInit
{
  #region Public Methods

  Task<FalconResponse<List<Agency>>> GetAgencies(Guid agencyGroupId);


  Task<FalconResponse<Dictionary<string,string>>> GetLabelsAsync(En_Language en_Language);

  Task<FalconResponse<StateSpecifics>> GetStateSpecificsAsync(En_State state,En_Language language);

  #endregion Public Methods
}
