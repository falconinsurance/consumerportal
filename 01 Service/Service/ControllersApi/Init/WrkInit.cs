using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DiamondServices.Apis;
using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Controllers.Init;

public class WrkInit:IWrkInit
{
  #region Private Fields

  private static readonly IRelax<IConsumerApi> _ConsumerApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IConsumerApi>());
  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IDiamondApi>());
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(WrkInit));

  #endregion Private Fields

  #region Private Properties

  private static IConsumerApi _ConsumerApi => _ConsumerApiRelaxed.Value;
  private static IDiamondApi _DiamondApi => _DiamondApiRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  public Task<FalconResponse<List<Agency>>> GetAgencies(Guid agencyGroupId) => _Log.TaskWrapFalconResponse(() => _ConsumerApi.Agency.GetAgencies(agencyGroupId));

  public Task<FalconResponse<Dictionary<string,string>>> GetLabelsAsync(En_Language en_Language) => _Log.TaskWrapFalconResponse(() => _ConsumerApi.Translator.GetLabelLookupsDictionary(en_Language));

  public Task<FalconResponse<StateSpecifics>> GetStateSpecificsAsync(En_State state,En_Language language) => _Log.TaskWrapFalconResponse(() => GetStateSpecifics(state,language));
  private StateSpecifics GetStateSpecifics(En_State state,En_Language language)
  {
    var stateSpecs = _ConsumerApi.StateSpec.GetStateSpecifics(state,language,_DiamondApi.SystemDate);
    stateSpecs.EffectiveDate = _DiamondApi.SystemDate.ToString("MM/dd/yyyy");
    return stateSpecs;
  }

  #endregion Public Methods

  #region Private Methods

  private static Task<T> TaskWrap<T>(Func<T> func) => Task.FromResult<T>(func());

  #endregion Private Methods
}
