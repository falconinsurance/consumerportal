namespace ConsumerServiceHost.ControllersApi.Caching;

public class ClsValue<T>
{
  #region Public Properties

  public T Value { get; set; }

  #endregion Public Properties

  #region Public Constructors

  public ClsValue(T value)
  {
    Value = value;
  }

  #endregion Public Constructors
}
