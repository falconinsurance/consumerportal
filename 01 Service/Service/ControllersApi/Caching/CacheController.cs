using System.Web.Http;

namespace ConsumerServiceHost.ControllersApi.Caching;

public class CacheController:ApiController
{
  #region Public Methods

  [HttpPost]
  public bool Clear(ClsValue<string> key)
  {
    AppCache.Clear(key.Value);
    return true;
  }

  [HttpPost]
  public string[] GetKeys() => AppCache.GetKeys();

  [HttpPost]
  public object GetValue(ClsValue<string> key) => AppCache.GetValue(key.Value);

  #endregion Public Methods
}
