using System.Net;
using System.Web.Http;

namespace ConsumerServiceHost.ControllersApi.Diagnostics;

[RoutePrefix("Diagnostics")]
public class DiagnosticsController:ApiController
{
  #region Private Fields
  private const string _HealthCheckFilePath = @"c:\consumer-service-status.txt";
  #endregion Private Fields

  #region Public Methods

  [HttpGet, Route("HealthCheck")]
  public IHttpActionResult Check() => Ok(ServiceHealthSummary.Up());

  [HttpGet, Route("HealthCheckFile")]
  public IHttpActionResult CheckFile() => FileExists() ? Ok(ServiceHealthSummary.Up()) : Content(HttpStatusCode.Gone,ServiceHealthSummary.Down());

  #endregion Public Methods

  #region Private Methods

  private static bool FileExists() => File.Exists(_HealthCheckFilePath);

  #endregion Private Methods
}
