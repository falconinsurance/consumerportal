using System.Reflection;

namespace ConsumerServiceHost.ControllersApi.Diagnostics;

public class ServiceHealthSummary
{
  #region Private Fields
  private static string _StaticVersion = null;
  #endregion Private Fields

  #region Public Constructors

  public ServiceHealthSummary(string status)
  {
    Timestamp = DateTime.Now;
    Version = _StaticVersion ??= Assembly.GetExecutingAssembly().GetName().Version.ToString();
    Status = status;
  }

  #endregion Public Constructors

  #region Public Properties
  public string Status { get; set; }

  public DateTime Timestamp { get; set; }

  public string Version { get; set; }
  #endregion Public Properties

  #region Public Methods

  public static ServiceHealthSummary Down() => new("Down");

  public static ServiceHealthSummary Up() => new("Up");

  #endregion Public Methods
}
