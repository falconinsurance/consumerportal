using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Controllers.Consumer;

public class WrkConsumer:IWrkConsumer
{
  #region Private Fields

  private static readonly IConsumerHandler _IConsumerHandler = AppResolver.Get<IConsumerHandler>();

  #endregion Private Fields

  #region Public Methods

  public Task<Dictionary<Guid,AgencyGroup>> GetAgencyGroups() => TaskWrap(_IConsumerHandler.GetAgenciesGroups);

  #endregion Public Methods

  #region Private Methods

  private static Task<T> TaskWrap<T>(Func<T> func) => Task.FromResult<T>(func());

  #endregion Private Methods
}
