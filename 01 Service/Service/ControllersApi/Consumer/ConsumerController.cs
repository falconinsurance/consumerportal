using System.Web.Http;
using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Controllers.Consumer;

[RoutePrefix("Consumer")]
public class ConsumerController:ApiController
{
  #region Private Fields
  private static readonly IRelax<IWrkConsumer> _WorkerRelaxed = RelaxFactory.Create(AppResolver.Get<IWrkConsumer>);
  #endregion Private Fields

  #region Private Properties
  private IWrkConsumer _Worker => _WorkerRelaxed.Value;
  #endregion Private Properties

  #region Public Methods

  [HttpPost, Route("GetAgencyGroups")]
  public Task<Dictionary<Guid,AgencyGroup>> GetAgencyGroups() => Run(c => c.GetAgencyGroups());

  #endregion Public Methods

  #region Private Methods

  private Task<T> Run<T>(Func<IWrkConsumer,Task<T>> func) => Task.Run(async () => await func(_Worker).ConfigureAwait(false));

  #endregion Private Methods
}
