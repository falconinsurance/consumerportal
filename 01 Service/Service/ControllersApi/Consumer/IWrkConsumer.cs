using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Controllers.Consumer;

public interface IWrkConsumer
{
  #region Public Methods

  Task<Dictionary<Guid,AgencyGroup>> GetAgencyGroups();

  #endregion Public Methods
}
