using System.Web.Http;
using ConsumerServiceHost.Controllers.Quoting;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;
using Falcon.Tools.VehicleInfoCache;
using DB = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Controllers.VehicleInfo;

public class VehicleInfoController:ApiController
{
  #region Private Fields

  private static readonly IRelax<IWrkVehicleInfo> _WorkerRelaxed = RelaxFactory.Create(AppResolver.Get<IWrkVehicleInfo>);
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(VehicleInfoController));

  #endregion Private Fields

  #region Private Properties

  private static IWrkVehicleInfo _Worker => _WorkerRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  [HttpPost] public Task<FalconResponse<IdDescEntity[]>> GetBodyTypes(DB.Record request) => Run(c => c.GetBodyTypesAsync(request.To<int>("Year"),request.To<IdDescEntity>("Make"),request.To<IdDescEntity>("Model")));

  [HttpPost] public Task<FalconResponse<List<VehicleDropdowns>>> GetDropdowns(DB.Record request) => Run(c => c.GetDropdownsAsync(request.To<Vehicle[]>("Vehicles")));

  [HttpPost] public Task<FalconResponse<IdDescEntity[]>> GetMakes(DB.Record request) => Run(c => c.GetMakesAsync(request.To<int>("Year")));

  [HttpPost] public Task<FalconResponse<IdDescEntity[]>> GetModels(DB.Record request) => Run(c => c.GetModelsAsync(request.To<int>("Year"),request.To<IdDescEntity>("Make")));

  [HttpPost] public Task<FalconResponse<IdDescEntity[]>> GetSymbols(DB.Record request) => Run(c => c.GetSymbolsAsync(request.To<int>("Year"),request.To<IdDescEntity>("Make"),request.To<IdDescEntity>("Model"),request.To<IdDescEntity>("BodyType")));

  [HttpPost] public Task<FalconResponse<int[]>> GetYears() => Run(c => c.GetYearsAsync());

  [HttpPost] public Task<FalconResponse<bool>> ValidateVin(Vehicle request) => Run(c => c.ValidateVinAsync(request.RunValidation(_Log)));

  #endregion Public Methods

  #region Private Methods

  private Task<T> Run<T>(Func<IWrkVehicleInfo,Task<T>> func) => Task.Run(async () => await func(_Worker).ConfigureAwait(false));

  #endregion Private Methods
}
