using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DiamondServices.Apis;
using Falcon.Core.Models;
using Falcon.Tools.VehicleInfoCache;

namespace ConsumerServiceHost.Controllers.VehicleInfo;

public class WrkVehicleInfo:IWrkVehicleInfo
{
  #region Private Fields

  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IDiamondApi>());

  private static readonly ILog _Log = AppResolver.GetLogger(nameof(WrkVehicleInfo));

  #endregion Private Fields

  #region Private Properties

  private static IVehicleApi _DiamondApiVehicle => _DiamondApiRelaxed.Value.Vehicle;

  #endregion Private Properties

  #region Public Methods

  public Task<FalconResponse<IdDescEntity[]>> GetBodyTypesAsync(int year,IdDescEntity make,IdDescEntity model) => _Log.TaskWrapFalconResponse(() => _DiamondApiVehicle.GetBodyTypes(year,make,model).OrderByDesc());

  public Task<FalconResponse<List<VehicleDropdowns>>> GetDropdownsAsync(Vehicle[] vehicles) => _Log.TaskWrapFalconResponse(() => GetAllVehicleDropdowns(vehicles));

  public Task<FalconResponse<IdDescEntity[]>> GetMakesAsync(int year) => _Log.TaskWrapFalconResponse(() => _DiamondApiVehicle.GetMakes(year).OrderByDesc());

  public Task<FalconResponse<IdDescEntity[]>> GetModelsAsync(int year,IdDescEntity make) => _Log.TaskWrapFalconResponse(() => _DiamondApiVehicle.GetModels(year,make).OrderByDesc());

  public Task<FalconResponse<IdDescEntity[]>> GetSymbolsAsync(int year,IdDescEntity make,IdDescEntity model,IdDescEntity bodyType) => _Log.TaskWrapFalconResponse(() => _DiamondApiVehicle.GetSymbols(year,make,model,bodyType).OrderByDesc());

  public Task<FalconResponse<int[]>> GetYearsAsync() => _Log.TaskWrapFalconResponse(() => _DiamondApiVehicle.GetYears().OrderByDescending(c => c).ToArray());

  public Task<FalconResponse<bool>> ValidateVinAsync(Vehicle vehicle) => _Log.TaskWrapFalconResponse(() => ValidateVin(vehicle));

  #endregion Public Methods

  #region Private Methods

  private List<VehicleDropdowns> GetAllVehicleDropdowns(Vehicle[] vehicles)
  {
    var obj = new List<VehicleDropdowns>();
    vehicles.ForEach(vehicle =>
    {
      var m = new VehicleDropdowns
      {
        Vehicle = vehicle,
        Makes = vehicle.Year > 0 ? _DiamondApiVehicle.GetMakes(vehicle.Year).OrderByDesc() : null,
        Models = vehicle.Year > 0 && !string.IsNullOrEmpty(vehicle.Make.Id) ? _DiamondApiVehicle.GetModels(vehicle.Year,vehicle.Make).OrderByDesc() : null,
        BodyTypes = vehicle.Year > 0 && !string.IsNullOrEmpty(vehicle.Make.Id) && !string.IsNullOrEmpty(vehicle.Model.Id) ? _DiamondApiVehicle.GetBodyTypes(vehicle.Year,vehicle.Make,vehicle.Model).OrderByDesc() : null,
        Symbols = vehicle.Year > 0 && !string.IsNullOrEmpty(vehicle.Make.Id) && !string.IsNullOrEmpty(vehicle.Model.Id) && !string.IsNullOrEmpty(vehicle.BodyType.Id) ? _DiamondApiVehicle.GetSymbols(vehicle.Year,vehicle.Make,vehicle.Model,vehicle.BodyType).OrderByDesc() : null,
      };
      obj.Add(m);
    });
    return obj;
  }

  private bool ValidateVin(Vehicle vehicle)
  {
    var v = _DiamondApiVehicle.GetVehicles(vehicle.Vin).FirstOrDefault();
    return v != null
      && v.Year == vehicle.Year
      && v.Make == vehicle.Make
      && v.Model == vehicle.Model
      && v.BodyType == vehicle.BodyType
      && v.Symbol == vehicle.Symbol;
  }

  #endregion Private Methods
}
