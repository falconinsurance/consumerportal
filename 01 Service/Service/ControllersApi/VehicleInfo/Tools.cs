using Falcon.Tools.VehicleInfoCache;

namespace ConsumerServiceHost.Controllers.VehicleInfo;

public static class Tools
{
  #region Public Methods

  public static IdDescEntity[] OrderByDesc(this IdDescEntity[] IdDescEntitys) => IdDescEntitys.OrderBy(c => c.Description).ToArray();

  #endregion Public Methods
}
