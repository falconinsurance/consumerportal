using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;
using Falcon.Tools.VehicleInfoCache;

namespace ConsumerServiceHost.Controllers.VehicleInfo;

public interface IWrkVehicleInfo
{
  #region Public Methods

  Task<FalconResponse<IdDescEntity[]>> GetBodyTypesAsync(int year,IdDescEntity make,IdDescEntity model);

  Task<FalconResponse<List<VehicleDropdowns>>> GetDropdownsAsync(Vehicle[] vehicles);

  Task<FalconResponse<IdDescEntity[]>> GetMakesAsync(int year);

  Task<FalconResponse<IdDescEntity[]>> GetModelsAsync(int year,IdDescEntity make);

  Task<FalconResponse<IdDescEntity[]>> GetSymbolsAsync(int year,IdDescEntity make,IdDescEntity model,IdDescEntity bodyType);

  Task<FalconResponse<int[]>> GetYearsAsync();

  Task<FalconResponse<bool>> ValidateVinAsync(Vehicle vehicle);

  #endregion Public Methods
}
