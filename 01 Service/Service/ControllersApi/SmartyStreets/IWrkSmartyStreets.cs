using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;
using Falcon.Data.SmartyStreets.DB;

namespace ConsumerServiceHost.Controllers.SmartyStreets;

public interface IWrkSmartyStreets
{
  #region Public Methods

  Task<FalconResponse<List<Address>>> GetAddressAutocompleteAsync(Address req);

  Task<FalconResponse<SuggestedAddress>> GetAddressAsync(Instance instance,EnAddressType addressType);

  Task<bool> LogSelectedAddressAsync(Instance req);

  Task<bool> LogValidatedAddressAsync(Instance req,EnAddressType addressType,bool ignored);

  #endregion Public Methods
}
