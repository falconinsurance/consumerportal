using System.Web.Http;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;
using Falcon.Data.SmartyStreets.DB;
using DB = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Controllers.SmartyStreets;

public class SmartyStreetsController:ApiController
{
  #region Private Fields

  private static readonly IRelax<IWrkSmartyStreets> _WorkerRelaxed = RelaxFactory.Create(AppResolver.Get<IWrkSmartyStreets>);
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(SmartyStreetsController));

  #endregion Private Fields

  #region Private Properties

  private static IWrkSmartyStreets _Worker => _WorkerRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  [HttpPost] public Task<FalconResponse<List<Address>>> GetAddressAutocomplete(Address request) => Run(c => c.GetAddressAutocompleteAsync(request));

  [HttpPost] public Task<FalconResponse<SuggestedAddress>> GetAddress(DB.Record request) => Run(c => c.GetAddressAsync(request.To<Instance>("Instance"),request.To<EnAddressType>("Type")));

  [HttpPost] public Task<bool> LogSelectedAddress(Instance request) => Run(c => c.LogSelectedAddressAsync(request));

  [HttpPost] public Task<bool> LogValidatedAddress(DB.Record request) => Run(c => c.LogValidatedAddressAsync(request.To<Instance>("Instance"),request.To<EnAddressType>("Type"),request.To<bool>("Ignore")));

  #endregion Public Methods

  #region Private Methods

  private Task<T> Run<T>(Func<IWrkSmartyStreets,Task<T>> func) => Task.Run(async () => await func(_Worker).ConfigureAwait(false));

  #endregion Private Methods
}
