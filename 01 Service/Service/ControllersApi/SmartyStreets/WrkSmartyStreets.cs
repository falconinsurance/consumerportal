using System.Runtime.CompilerServices;
using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DiamondServices.Apis;
using DiamondServices.Service;
using Falcon.Core.Models;
using Falcon.Service.SmartyStreets;
using SmartyObj = Falcon.Data.SmartyStreets.DB;

namespace ConsumerServiceHost.Controllers.SmartyStreets;

public class WrkSmartyStreets:IWrkSmartyStreets
{
  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IDiamondApi>());

  private static ISession _ConsumerSession => _DiamondApi.Login.Session_ConsumerService;
  private static IDiamondApi _DiamondApi => _DiamondApiRelaxed.Value;

  #region Private Fields

  private static readonly IRelax<ISmartyStreetsService> _SmartyStreetsServiceRelaxed = RelaxFactory.Create(() => AppResolver.Get<ISmartyStreetsService>());
  private static readonly SmartyObj.ISmartyStreetsHandler _SmartyStreetsHandler = AppResolver.Get<SmartyObj.ISmartyStreetsHandler>();
  private static readonly string[] _SecondaryAddress = { "apt","ste","unit" };
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(WrkSmartyStreets));

  #endregion Private Fields

  private static ISmartyStreetsService _SmartyStreetsService => _SmartyStreetsServiceRelaxed.Value;

  #region Private Properties

  public Task<FalconResponse<List<Address>>> GetAddressAutocompleteAsync(Address req) => _Log.TaskWrapFalconResponse(() => GetAddressAutocomplete(req));

  public Task<FalconResponse<SuggestedAddress>> GetAddressAsync(Instance instance,SmartyObj.EnAddressType addressType) => _Log.TaskWrapFalconResponse(() => GetAddress(instance,addressType));

  public Task<bool> LogSelectedAddressAsync(Instance req) => LogSelectedAddress(req);

  private Task<bool> LogSelectedAddress(Instance instance)
  {
    var req = new SmartyObj.AutocompleteSelection
    {
      AgencyId = instance.Agency.Id,
      PolicyId = instance.Policy.Id,
      UserId = _ConsumerSession.UserId,
      SelectedAddress = instance.PolicyHolder.Address.GetFull()
    };
    return LogWrap(req);
  }

  public Task<bool> LogValidatedAddressAsync(Instance req,SmartyObj.EnAddressType addressType,bool ignored) => LogValidatedAddress(req,addressType,ignored);

  private Task<bool> LogValidatedAddress(Instance instance,SmartyObj.EnAddressType addressType,bool ignored)
  {
    var req = new SmartyObj.ValidationSelection
    {
      Address = instance.PolicyHolder.Address.GetFull(),
      AddressType = addressType,
      PolicyId = instance.Policy.Id,
      AgencyId = instance.Agency.Id,
      UserId = _ConsumerSession.UserId,
      Ignored = ignored,
    };
    return LogWrap(req);
  }

  private List<Address> GetAddressAutocomplete(Address addr)
  {
    var req = new GetAddressReq
    {
      street = addr.Street,
      city = addr.City,
      state = addr.State.ToString(),
      zipcode = addr.Zip
    };
    var result = _SmartyStreetsService.GetAutocomplete(req);
    var response = result.Items.Take(10).Select(c => new Address
    {
      Street = !string.IsNullOrEmpty(c.Street2) ? $"{c.Street} {c.Street2}" : c.Street,
      City = c.City,
      State = c.State.ToEnum<En_State>(),
      Zip = c.Zip,
    }).ToList();
    return response;
  }

  private SuggestedAddress GetAddress(Instance instance,SmartyObj.EnAddressType addressType)
  {
    var addr = instance.PolicyHolder.Address;
    if(addressType == SmartyObj.EnAddressType.Garage)
    {
      addr = instance.InsVehicles.Address;
    }
    var req = new GetAddressReq
    {
      street = addr.Street,
      city = addr.City,
      state = addr.State.ToString(),
      zipcode = addr.Zip
    };
    if(req.street.ToLower().EndsWithAny(_SecondaryAddress))
    {
      return new SuggestedAddress
      {
        MissingOrWrongSecondaryInfo = true,
        ValidAddressWithUnknownApt = false,
      };
    }
    var result = _SmartyStreetsService.GetAddress(req);
    LogWrap(new SmartyObj.SmartyStreets
    {
      AddressType = addressType,
      AgencyId = instance.Agency.Id,
      PolicyId = instance.Policy.Id,
      Request = new StreetAddress
      {
        Street1 = req.street,
        Street2 = req.street2,
        City = req.city,
        State = req.state,
        ZipCode = req.zipcode,
      },
      Response = result.Address,
      ResponseRaw = result.Raw,
      UserId = _ConsumerSession.UserId,
    });

    //if address found
    if(result.Address != null && result.Address.CorrectedAddress != null)
    {
      if(!result.Address.CorrectedAddress.ToConsumerAddress().Compare(addr))
        return new SuggestedAddress(result.Address);
      else
        return new SuggestedAddress
        {
          WasSuccess = true,
        };
    }

    //address not found.
    return new SuggestedAddress();
  }

  private bool AreEquivalent(string s1,string s2)
  {
    if(string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2))
    {
      return true;
    }

    return string.Compare(s1,s2,StringComparison.OrdinalIgnoreCase) == 0;
  }

  private bool AreZipcodesEquivalent(string searchedZip,string correctedZip)
  {
    var searched = new ZipCode(searchedZip);
    var corrected = new ZipCode(correctedZip);

    return searched.Zip == corrected.Zip;
  }

  private Task<bool> LogWrap<T>(T obj = default,[CallerMemberName] string methodName = null)
  {
    bool res;
    try
    {
      _SmartyStreetsHandler.Add(obj);
      res = true;
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex,obj: obj,methodName: methodName);
      res = false;
    }
    return Task.FromResult(res);
  }

  #endregion Private Properties
}
