using System.Web.Http;

namespace ConsumerServiceHost.Controllers.Logger;

public class LoggerController:ApiController
{
  #region Private Fields
  private static readonly ILogHandler _LogHandler = AppResolver.Get<ILogHandler>();
  #endregion Private Fields
  #region Public Methods

  [HttpPost]
  public bool LogWeb(LogValue record)
  {
    record?.With(c => c.Application = "ConsumerPortalJs");
    Task.Run(() => _LogHandler.Write(record));
    return true;
  }

  #endregion Public Methods
}
