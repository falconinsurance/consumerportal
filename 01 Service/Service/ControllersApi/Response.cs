namespace ConsumerServiceHost.Controllers;

public class Response<T>
{
  #region Public Properties
  public T Data { get; set; }
  public string ErrorMessage { get; set; }
  public string Exception { get; set; }
  #endregion Public Properties
}

public class Response:Response<bool>
{
  #region Public Constructors

  public Response()
  {
    Data = true;
  }

  #endregion Public Constructors

  #region Public Methods

  public static Response<T> Create<T>(Func<T> func,string errorMesssage = null)
  {
    var ret = new Response<T>();
    try
    {
      ret.Data = func();
    }
    catch(Exception ex)
    {
      ret.Exception = ex.ToString();
      ret.ErrorMessage = errorMesssage;
    }
    return ret;
  }

  public static Task<Response<T>> CreateAsync<T>(Func<T> func,string errorMesssage = null)
  {
    var ret = new Response<T>();
    try
    {
      ret.Data = func();
    }
    catch(Exception ex)
    {
      ret.Exception = ex.ToString();
      ret.ErrorMessage = errorMesssage;
    }
    return Task.FromResult(ret);
  }

  #endregion Public Methods
}
