using DiamondServices.Apis;
using Falcon.Core.Models;

namespace ConsumerServiceHost.Controllers.Quoting;

public static class Tools
{
  #region Public Methods

  public static int GetPayPlanId1(IDiamondApi diamondApi,En_State state,int termId,PayplanItem currentPayPlanItem,En_SR22Payment sr22,int billingMethodId)
  {
    var statePayPlan = StatePayPlanType.FromStr(state.ToString());
    if(!state.IsSR22State())
    {
      sr22 = En_SR22Payment.SR22No;
    }
    if(billingMethodId == 1)
    {
      return 1;
    }
    if(currentPayPlanItem is not null && currentPayPlanItem.Type != En_PayPlan.NoVal)
    {
      return currentPayPlanItem.Type.GetMultiKeyIntersectionId(
        currentPayPlanItem.PaymentType == En_Payment.NoVal ? En_Payment.AgencySweep : currentPayPlanItem.PaymentType,
        sr22 == En_SR22Payment.NoVal ? En_SR22Payment.SR22No : sr22,
        statePayPlan);
    }
    return GetStateDefaultPayPlanId();

    int GetStateDefaultPayPlanId()
    {
      var payplan = state.WeWrite() ? En_PayPlan.Pay_05_16P : En_PayPlan.NoVal;
      if(payplan > En_PayPlan.NoVal)
      {
        return payplan.GetMultiKeyIntersectionId(En_Payment.AgencySweep,sr22 == En_SR22Payment.NoVal ? En_SR22Payment.SR22No : sr22,statePayPlan);
      }
      var payPlans = diamondApi.Static.GetPayPlans(state,termId);
      if(payPlans.Count > 0)
      {
        var temp = payPlans.Select(c => PayPlanType.FromDiaId(c).GetEnType()).FirstOrDefault(c => c.Id > 3);
        return temp?.Id ?? 1;
      }
      return 1;
    }
  }

  #endregion Public Methods
}
