using System.Net.Http;
using System.Web.Http;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;
using DB = Falcon.Tools.DBHandler;

namespace ConsumerServiceHost.Controllers.Quoting;

public class QuotingController:ApiController
{
  #region Private Fields

  private static readonly ILog _Log = AppResolver.GetLogger(nameof(QuotingController));
  private static readonly IRelax<IWrkQuoting> _WorkerRelaxed = RelaxFactory.Create(AppResolver.Get<IWrkQuoting>);

  #endregion Private Fields

  #region Private Properties

  private static IWrkQuoting _Worker => _WorkerRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  [HttpPost] public Task<FalconResponse<Instance>> BindPolicy(DB.Record request) => Run(c => c.BindPolicyAsync(request.To<Instance>("Instance").RunValidation(_Log),request.To<Payment>("Payment")));

  [HttpGet] public Task<HtmlContent> DocuSignResp(Guid id) => Run(c => c.GetESignResponseAsync(id,Request.GetQueryNameValuePairs().ToLookup(x => x.Key,x => x.Value)));

  [HttpPost] public Task<FalconResponse<List<string>>> GetCounties(DB.Record request) => Run(c => c.GetCountiesAsync(request.To<DateTime>("EffectiveDate"),request.To<En_State>("State"),request.To<string>("Zip")));

  [HttpGet] public Task<HttpFile> GetDocumentAsHttpResp([FromUri] string id = null) => Run(c => c.GetDocumentAsHttpRespAsync(id));

  [HttpGet] public Task<HttpFile> GetDocumentsAsHttpResp([FromUri] string id = null) => Run(c => c.GetDocumentsAsHttpRespAsync(id));

  [HttpPost] public Task<FalconResponse<List<ESignItem>>> GetDocuSignItems(DB.Record request) => Run(c => c.GetDocuSignItemsAsync(request.To<Instance>("Instance").RunValidation(_Log),request.To<Guid>("SignerId")));

  [HttpPost] public Task<FalconResponse<string>> GetOneIncSaveUrl() => Run(c => c.GetOneIncSaveUrlAsync());

  [HttpPost] public Task<FalconResponse<string>> GetOneIncToken() => Run(c => c.GetOneIncTokenAsync());

  [HttpPost] public Task<FalconResponse<Instance>> GetRate(Instance request) => Run(c => c.GetRateAsync(request.RunValidation(_Log)));

  [HttpPost] public Task<FalconResponse<bool>> RegisterUser(InsRegistration request) => Run(c => c.RegisterUserAsync(request.RunValidation(_Log)));

  [HttpPost] public Task<bool> SendWelcomeEmail(InsRegistration request) => Run(c => c.SendWelcomeEmailAsync(request.RunValidation(_Log)));

  #endregion Public Methods

  #region Private Methods

  private Task<T> Run<T>(Func<IWrkQuoting,Task<T>> func)
  {
    return Task.Run(async () => await func(_Worker).ConfigureAwait(false));
  }

  #endregion Private Methods
}
