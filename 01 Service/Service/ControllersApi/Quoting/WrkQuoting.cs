using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DiamondServices.Apis;
using DiamondServices.Service;
using Falcon.ConsumerDB;
using Falcon.Core.Models;

namespace ConsumerServiceHost.Controllers.Quoting;

public class WrkQuoting:IWrkQuoting
{
  #region Private Fields

  private static readonly IRelax<IConsumerApi> _ConsumerApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IConsumerApi>());
  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(() => AppResolver.Get<IDiamondApi>());

  private static readonly ILog _Log = AppResolver.GetLogger(nameof(WrkQuoting));

  #endregion Private Fields

  #region Private Properties

  private static IConsumerApi _ConsumerApi => _ConsumerApiRelaxed.Value;
  private static ISession _ConsumerSession => _DiamondApi.Login.Session_ConsumerService;
  private static IDiamondApi _DiamondApi => _DiamondApiRelaxed.Value;

  #endregion Private Properties

  #region Public Methods

  public Task<FalconResponse<Instance>> BindPolicyAsync(Instance instance,Payment payment) => _Log.TaskWrapFalconResponse(() => BindPolicy(instance,payment));

  public Task<FalconResponse<List<string>>> GetCountiesAsync(DateTime effDate,En_State state,string zip) => _Log.TaskWrapFalconResponse(() => _DiamondApi.Static.GetCounties(state,zip));

  public Task<HttpFile> GetDocumentAsHttpRespAsync(string code) => TaskWrap(() => _ConsumerApi.Documents.GetDocumentAsHttpResp(_ConsumerSession,code));

  public Task<HttpFile> GetDocumentsAsHttpRespAsync(string code) => TaskWrap(() => _ConsumerApi.Documents.GetDocumentsAsHttpResp(_ConsumerSession,code));

  public Task<FalconResponse<List<ESignItem>>> GetDocuSignItemsAsync(Instance instance,Guid signerId) => _Log.TaskWrapFalconResponse(() => _ConsumerApi.ESign.GetDocuSignItems(_ConsumerSession,instance,signerId));

  public Task<HtmlContent> GetESignResponseAsync(Guid id,ILookup<string,string> lookup) => TaskWrap(() => _ConsumerApi.ESign.GetESignResponse(_ConsumerSession,id,lookup));

  public Task<FalconResponse<string>> GetOneIncSaveUrlAsync() => _Log.TaskWrapFalconResponse(_ConsumerApi.OneInc.GetSaveUrl);

  public Task<FalconResponse<string>> GetOneIncTokenAsync() => _Log.TaskWrapFalconResponse(GetOneIncToken);

  public Task<FalconResponse<Instance>> GetRateAsync(Instance instance) => _Log.TaskWrapFalconResponse(() => GetRate(instance));

  public Task<FalconResponse<bool>> RegisterUserAsync(InsRegistration insRegistration) => _Log.TaskWrapFalconResponse(() => _ConsumerApi.Policy.RegisterUser(insRegistration));

  public Task<bool> SendWelcomeEmailAsync(InsRegistration insRegistration) => TaskWrap(() => _ConsumerApi.Policy.SendWelcomeEmail(insRegistration.Email,insRegistration.PolicyNumber,insRegistration.Name.Full));

  #endregion Public Methods

  #region Private Methods

  private static Task<T> TaskWrap<T>(Func<T> func) => Task.FromResult<T>(func());

  private FalconResponse<Instance> BindPolicy(Instance instance,Payment payment)
  {
    var response = new FalconResponse<Instance>();
    var (newImage, oldImage) = instance.ToImage(_ConsumerSession,_DiamondApi.SystemDate,true);
    if(newImage == null && newImage == oldImage)
    {
      return response.With(c => c.Assign(instance,"L_Block_rate"));
    }
    payment.Type = payment.CreditCard != null && !string.IsNullOrEmpty(payment.CreditCard.Token) ? En_Payment.CreditCard : (payment.Eft != null && !string.IsNullOrEmpty(payment.Eft.RoutingNumber) ? En_Payment.ACH : En_Payment.NoVal);
    payment.Source = "ConsumerPortal";

    var res = _ConsumerApi.Policy.Bind(_ConsumerSession,newImage,payment,instance.Agency.State);

    return response.With(c => c.Assign(res.Instance,res.Error));
  }

  private FalconResponse<string> GetOneIncToken()
  {
    var response = new FalconResponse<string>();
    var tempToken = _ConsumerApi.OneInc.GetSessionId();

    if(!string.IsNullOrWhiteSpace(tempToken))
    {
      _Log.Info(msg: $"Payment request succesful. Token={tempToken}");
      response.Result = tempToken;
    }
    else
    {
      var labelKey = _ConsumerApi.Translator.GetLabelKey(EnTranslateKey.ErrorCCPaymentReq);
      _Log.Warn(msg: labelKey);
      response.Error.Message = labelKey;
    }
    return response;
  }

  private FalconResponse<Instance> GetRate(Instance instance)
  {
    var response = new FalconResponse<Instance>();
    //Convert instance to images (new image and old/loaded image)
    var (newImage, oldImage) = instance.ToImage(_ConsumerSession,_DiamondApi.SystemDate,true);
    if(newImage == null && newImage == oldImage)
    {
      return response.With(c => c.Assign(instance,"L_Block_rate"));
    }
    var res = _ConsumerApi.Policy.GetRate(_ConsumerSession,instance,newImage,oldImage);

    return response.With(c => c.Assign(res,null));
  }

  #endregion Private Methods
}
