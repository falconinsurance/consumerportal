using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using Falcon.Core.Models;

namespace ConsumerServiceHost.Controllers.Quoting;

public interface IWrkQuoting
{
  #region Public Methods

  Task<FalconResponse<Instance>> BindPolicyAsync(Instance instance,Payment payment);

  Task<FalconResponse<List<string>>> GetCountiesAsync(DateTime effDate,En_State state,string zip);

  Task<HttpFile> GetDocumentAsHttpRespAsync(string code);

  Task<HttpFile> GetDocumentsAsHttpRespAsync(string code);

  Task<FalconResponse<List<ESignItem>>> GetDocuSignItemsAsync(Instance instance,Guid signerId);

  Task<HtmlContent> GetESignResponseAsync(Guid id,ILookup<string,string> lookup);

  Task<FalconResponse<string>> GetOneIncSaveUrlAsync();

  Task<FalconResponse<string>> GetOneIncTokenAsync();

  Task<FalconResponse<Instance>> GetRateAsync(Instance request);
  Task<FalconResponse<bool>> RegisterUserAsync(InsRegistration insRegistration);
  Task<bool> SendWelcomeEmailAsync(InsRegistration insRegistration);

  #endregion Public Methods
}
