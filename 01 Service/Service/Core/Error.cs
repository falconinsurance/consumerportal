namespace ConsumerServiceHost.Core;

public class Error
{
  #region Public Properties
  public string Exception { get; set; }
  public bool HasError => !(string.IsNullOrEmpty(Exception) && string.IsNullOrEmpty(Message));
  public string Message { get; set; }
  #endregion Public Properties
}
