namespace ConsumerServiceHost.Core;

public interface IFalconResponse<TResult>
{
  #region Public Properties
  Error Error { get; }
  TResult Result { get; }
  #endregion Public Properties
}
