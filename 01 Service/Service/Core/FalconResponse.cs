namespace ConsumerServiceHost.Core;

public class FalconResponse<T>:IFalconResponse<T>
{
  #region Public Properties

  public Error Error { get; set; }

  public T Result { get; set; }

  public EnStatus Status { get; set; }

  #endregion Public Properties

  #region Public Constructors

  public FalconResponse()
  {
  }

  public FalconResponse(Exception e)
  {
    Error = new Error
    {
      Exception = e.ToString(),
    };
  }

  #endregion Public Constructors

  #region Public Methods

  public void Assign(T result,string error)
  {
    Result = result;

    if(!string.IsNullOrEmpty(error))
    {
      AssignError(error);
    }
  }

  public void AssignError(Exception e)
  {
    Error = new()
    {
      Exception = e.ToString(),
    };
    Status = EnStatus.Error;
  }

  public void AssignError(string error)
  {
    Error = new()
    {
      Message = error,
    };
    Status = EnStatus.FailedOther;
  }

  #endregion Public Methods
}

public class FalconResponse:FalconResponse<bool>
{
  #region Public Constructors

  public FalconResponse()
  {
  }

  public FalconResponse(Exception e) : base(e)
  {
  }

  #endregion Public Constructors
}
