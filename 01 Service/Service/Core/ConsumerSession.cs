using Diamond.Common.Services;
using DiamondServices.Service;

namespace ConsumerServiceHost.Core;

public class ConsumerSession:BaseSession
{
  #region Public Constructors

  public ConsumerSession()
  {
  }

  public ConsumerSession(DiamondSecurityToken token,string trustedSecurityToken = null) => SetSession(token,trustedSecurityToken);

  #endregion Public Constructors
}
