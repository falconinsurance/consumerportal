namespace ConsumerServiceHost.Core;

public enum EnStatus
{
  Success = 0,
  Error = 1,
  FailedOther = 2
}
