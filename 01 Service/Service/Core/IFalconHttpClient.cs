namespace ConsumerServiceHost.Core;

public interface IFalconHttpClient
{
  #region Public Methods

  Res PostJson<Res>(string uri,object reqObject);

  #endregion Public Methods
}
