namespace ConsumerServiceHost.Core;

public interface IFalconRequest<TData>
{
  #region Public Properties
  TData Data { get; set; }
  string Key { get; set; }
  #endregion Public Properties
}
