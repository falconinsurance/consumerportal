namespace ConsumerServiceHost.Core;

public class FalconRequest<TData>:IFalconRequest<TData>
{
  #region Public Properties
  public TData Data { get; set; }
  public string Key { get; set; }
  #endregion Public Properties
}
