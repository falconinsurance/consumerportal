using System.Runtime.CompilerServices;

namespace ConsumerServiceHost.Core;

internal static class ServiceHelper
{
  #region Internal Methods

  internal static Task<FalconResponse<T>> TaskWrapFalconResponse<T>(this ILog log,Func<T> func,object obj = default,[CallerMemberName] string methodName = null)
  {
    var result = new FalconResponse<T>();
    try
    {
      result.Result = func();
    }
    catch(Exception ex)
    {
      result.AssignError(ex);
      result.Status = EnStatus.Error;
      log.Error(ex: ex,obj: obj,methodName: methodName);
    }

    return Task.FromResult(result);
  }

  internal static Task<FalconResponse<T>> TaskWrapFalconResponse<T>(this ILog log,Func<FalconResponse<T>> func,object obj = default,[CallerMemberName] string methodName = null)
  {
    var result = new FalconResponse<T>();
    try
    {
      var resp = func();
      result = resp;
    }
    catch(Exception ex)
    {
      result.AssignError(ex);
      result.Status = EnStatus.Error;
      log.Error(ex: ex,obj: obj,methodName: methodName);
    }

    return Task.FromResult(result);
  }

  #endregion Internal Methods
}
