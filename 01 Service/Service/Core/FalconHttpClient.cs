using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace ConsumerServiceHost.Core;

public class FalconHttpClient:IFalconHttpClient
{
  #region Private Fields

  private readonly Uri _BaseAddress;
  private readonly TimeSpan _Timeout;
  private HttpClient _HttpClient;

  #endregion Private Fields

  #region Private Properties

  private static JsonMediaTypeFormatter JsonFormatter => new() { SerializerSettings = { NullValueHandling = NullValueHandling.Ignore,DefaultValueHandling = DefaultValueHandling.Ignore } };

  private HttpClient Client => _HttpClient ??= new(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip })
  {
    BaseAddress = _BaseAddress,
    Timeout = _Timeout
  };

  #endregion Private Properties

  #region Public Constructors

  public FalconHttpClient(string baseAddress)
  {
    _BaseAddress = new Uri(baseAddress);
    _Timeout = TimeSpan.FromMinutes(30);
  }

  #endregion Public Constructors

  #region Public Methods

  public Res PostJson<Res>(string uri,object reqObject) => PostJsonAsync<Res>(uri,reqObject).GetAwaiter().GetResult();

  #endregion Public Methods

  #region Private Methods

  private async Task<Res> PostJsonAsync<Res>(string uri,object reqObject)
  {
    try
    {
      var response = await Client.PostAsync(uri,reqObject,JsonFormatter).ConfigureAwait(false);
      if(response.IsSuccessStatusCode)
        return await response.Content.ReadAsAsync<Res>().ConfigureAwait(false);
      else
        throw new($"PostJsonAsync:  \n{response.StatusCode}\n{response.ReasonPhrase}");
    }
    catch
    {
      throw;
    }
  }

  #endregion Private Methods
}
