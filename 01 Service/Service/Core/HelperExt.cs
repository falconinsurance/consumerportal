using System.Globalization;
using System.Runtime.CompilerServices;

namespace ConsumerServiceHost;

public static class HelperExt
{
  #region Public Methods

  public static T RunValidation<T>(this T obj,ILog log,[CallerMemberName] string methodName = null) where T : IValidate
  {
    if(obj is null)
    {
      return default;
    }
    try
    {
      if(obj.TryValidate(out var failedItems))
      {
        log.Warn(obj: new { obj,failedItems },note: "Validation",methodName: "V-" + methodName);
      }
    }
    catch(Exception ex)
    {
      log.Warn(obj: obj,note: "Validation-Error",methodName: "V-" + methodName,ex: ex);
    }
    return obj;
  }

  public static DateTime ToDate(this string dateString) => DateTime.ParseExact(dateString,"MM/dd/yyyy",CultureInfo.InvariantCulture);

  #endregion Public Methods
}
