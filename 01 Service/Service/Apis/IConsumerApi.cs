namespace ConsumerServiceHost.Apis;

public interface IConsumerApi
{
  #region Public Properties

  IAgencyApi Agency { get; }
  IDocumentsApi Documents { get; }
  IESignApi ESign { get; }
  IOneIncApi OneInc { get; }
  IPolicyApi Policy { get; }
  INotificationApi Notification { get; }
  IStateSpecApi StateSpec { get; }
  ITranslatorApi Translator { get; }

  #endregion Public Properties
}
