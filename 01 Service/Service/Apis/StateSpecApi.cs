using ConsumerServiceHost.Models;
using ConsumerServiceHost.Models.Dropdowns;

namespace ConsumerServiceHost.Apis;

public class StateSpecApi:BaseApi, IStateSpecApi
{
  #region Private Fields
  private static readonly ClassCache _Cache = new();
  #endregion Private Fields

  #region Public Constructors

  public StateSpecApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public StateSpecifics GetStateSpecifics(En_State state,En_Language language,DateTime effectiveDate)
  {
    var version = DiamondApi.Static.GetVersionId(state,effectiveDate);
    return Request();
    StateSpecifics Request()
    {
      var p = new StateSpecifics
      {
        Dropdowns = GetStateDropdown(state,language),
        UnderwritingQuestions = GetUnderwritingQuestions(state,version,language)
      };
      return p;
    }
  }

  #endregion Public Methods

  #region Private Methods

  private SqlStateDropdowns GetDropdowns(En_State state) => FalconHandler.SqlFirstOrDefault("EXEC [Enum].[SpGetStateDropDown] @State=@1;",c => c.AddValues(state.ToInt()),SqlDropdowns.Cast);

  private StateDropdowns GetStateDropdown(En_State state,En_Language language)
  {
    return _Cache.SetGet(Request,new(state,language));
    StateDropdowns Request()
    {
      var p = GetDropdowns(state);
      FilterDropdowns(p,state);
      return StateDropdowns.Convert(p,state,language);
    }
  }

  private void FilterDropdowns(SqlStateDropdowns dropdowns,En_State state)
  {
    //removing comcoll, since we are laucnhing off with liability only.
    dropdowns.VehicleOptions.ComColDeductible.Remove();
    //removing excluded drivers in Utah, since there are some extra esign hopes people don't want to deal with
    if(state == En_State.UT)
    {
      dropdowns.DriverOptions.DriverType.Items.Remove(En_Driver.Excluded);
    }
  }

  private List<UnderwritingQuestion> GetUnderwritingQuestions(En_State state,int version,En_Language language = En_Language.EN)
  {
    return _Cache.SetGet(Request,new(state,version,language));
    List<UnderwritingQuestion> Request()
    {
      return FalconHandler.Sql("EXEC [Enum].[SpGetUnderwritingQuestions] @State=@1, @Lang=@2, @Version=@3",
c => c.AddValues(state.ToInt(),language.ToString(),version),UnderwritingQuestion.Cast).ToList();
    }
  }

  #endregion Private Methods
}
