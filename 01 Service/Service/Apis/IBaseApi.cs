namespace ConsumerServiceHost.Apis;

public interface IBaseApi
{
  #region Public Properties
  IConsumerApi ConsumerApi { get; }
  #endregion Public Properties
}
