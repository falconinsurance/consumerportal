using ConsumerServiceHost.Models;
using DiamondServices.Service;
using DocuSignData.DB;
using DocuSignService.Service;
using DoucuSignCommon.Model;
using Falcon.Core.Models;

namespace ConsumerServiceHost.Apis;

public class ESignApi:BaseApi, IESignApi
{
  #region Private Fields

  private static readonly IRelax<IDbDocuSign> _IDbDocuSignRelaxed = RelaxFactory.Create(AppResolver.Get<IDbDocuSign>);
  private static readonly IRelax<IDocuSignApi> _IDocuSignApiRelaxed = RelaxFactory.Create(AppResolver.Get<IDocuSignApi>);
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(ESignApi));

  #endregion Private Fields

  #region Private Properties

  private static IDbDocuSign _DbDocuSign => _IDbDocuSignRelaxed.Value;
  private static IDocuSignApi _DocuSignApi => _IDocuSignApiRelaxed.Value;

  #endregion Private Properties

  #region Public Constructors

  public ESignApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public List<ESignItem> GetDocuSignItems(ISession session,Instance instance,Guid signerId)
  {
    var docuSignItems = new List<ESignItem>();
    Envelope envelope = null;

    //SignerId is empty when when user re-rates quote and sends a new instance in the request, so that new envelope with new signerIds are created.
    if(signerId == Guid.Empty)
    {
      var orderedDocuments = new List<byte[]>();
      var descriptions = new List<string>();
      var policy = instance.Policy;

      //get all the esign documents
      foreach(var document in policy.Documents.ESign.OrderByDescending(f => f.FormId))
      {
        var printForms = DiamondApi.Printing.LoadPrintForms(session,document.PolicyId,document.PolicyImageNumber);

        var printForm = printForms.FirstOrDefault(p => p.Matches(document));

        var bytes = DiamondApi.Printing.GetDocument(session,document.PolicyId,document.PolicyImageNumber,printForm);

        //orderedDocuments.Add(new MemoryStream(bytes));

        orderedDocuments.Add(bytes);
        descriptions.Add($"{document.Description} ({document.FormNumber})");
      }

      var subject = $"New Business-{policy.Number}-{policy.ImageNum}";

      //create signer names
      var signersWithName = new SignersWithName().With(s =>
      {
        s.AddToEnd($"{instance.Drivers[0].Name.First} {instance.Drivers[0].Name.Last}",EnSigner.Insured);
        if(instance.Agency.State.In(En_State.IN))
        {
          var spouse = instance.Drivers.Find(d => d.Relationship == En_DriverRelationship.Spouse && d.Type == En_Driver.Excluded);
          if(spouse is not null)
          {
            s.AddToEnd($"{spouse.Name.First} {spouse.Name.Last}",EnSigner.Spouse);
          }
        }
      });

      //create envelope
      envelope = _DocuSignApi.CreateEnvelope(orderedDocuments,subject,policy.EffectiveDate.ToDate(),signersWithName.GetSigners());

      envelope
      .AssignPolicyInfo(policy.Id,policy.ImageNum,1)
      .AssignDescriptions(descriptions);

      //Deleting existing envelope to tblEnvelope
      DeleteExistingEnvelope(policy.Id);
      //Adding the new envelope to tblEnvelope
      _DbDocuSign.AddOrUpdateEnvelope(envelope);
    }

    //else if signerId exists, then we return the same envelope, so we get the current signing status of the envelope.
    else
    {
      envelope = _DbDocuSign.GetFromSignerId(new() { SignerId = signerId });
    }

    foreach(var item in envelope.Signers)
    {
      //make sure to add only Insured/Spouse docusign
      if(item.Signer != EnSigner.Agent)
      {
        docuSignItems.Add(new(item,
          ConsumerApi.Translator.GetLabelKey(item.Status.GetFriendlyMessage()),
          item.Status == EnSignerStatus.Default || item.Status == EnSignerStatus.tt_expired ? _DocuSignApi.GetUrl(envelope.EnvelopeId,item)?.Url : null));
      }
    }

    return docuSignItems;
  }

  public byte[] GetEsignedDocumentBytes(int policyId,int imageNum)
  {
    var envelope = _DbDocuSign.GetFromPolicyInfo(new() { PolicyId = policyId,ImageNum = imageNum });
    var document = _DocuSignApi.GetDocumentAsBytes(envelope.EnvelopeId);
    return document;
  }

  public HtmlContent GetESignResponse(ISession session,Guid id,ILookup<string,string> lookup)
  {
    const string mainContent = "<html><body style=\"font-family: arial\"><div style=\"font-weight: bold; font-size: 20px\">";
    try
    {
      var responseEvent = lookup["event"].LastOrDefault();
      if(TryGetSignerInfo(id,out var envelope,out var signer))
      {
        if(!signer.Status.In(EnSignerStatus.signing_complete,EnSignerStatus.Decline))
        {
          signer.Status = responseEvent switch
          {
            "signing_complete" => EnSignerStatus.signing_complete,
            "decline" => EnSignerStatus.Decline,
            "viewing_complete" => EnSignerStatus.signing_complete,
            "ttl_expired" => EnSignerStatus.tt_expired,
            _ => EnSignerStatus.Default
          };
        }

        if(envelope.Signers.All(c => c.Status == EnSignerStatus.signing_complete))
        {
          envelope.Status = EnEnvelopeStatus.Completed;

          if(envelope.AttachmentId == 0)
          {
            //get document from docusign
            var document = _DocuSignApi.GetDocumentAsBytes(envelope.EnvelopeId);

            //attach to diamond
            envelope = AttachDocumentAndGetEnvelope(new()
            {
              Document = document,
              EnvelopeId = envelope.EnvelopeId,
              ImageNum = envelope.ImageNum,
              PolicyId = envelope.PolicyId,
            },session);
          }
        }
        if(envelope.Signers.Any(c => c.Status == EnSignerStatus.Decline))
        {
          envelope.Status = EnEnvelopeStatus.Declined;
        }

        if(envelope.AttachmentId == 0)
        {
          _DbDocuSign.AddOrUpdateEnvelope(envelope);
        }
      }
      var h1 = responseEvent switch
      {
        "signing_complete" => "eSign Completed Successfully",
        "decline" => "eSign Declined",
        "ttl_expired" => "eSign Expired",
        _ => "Something went wrong with the eSignature process"
      };
      var h2 = responseEvent switch
      {
        "signing_complete" => "You can safely close this window",
        "decline" => "You will need to wetsign the documents and submit to Falcon Underwriting",
        "ttl_expired" => "Please close this window and click “launch the e-sign process” again. If you continue to have issues contact Falcon Underwriting.",
        _ => "Please try again or contact Falcon Underwriting to continue"
      };

      var content = $"{mainContent}{h1}</div><div style=\"font-weight: normal; font-size: 16px; padding-top: 10px;\">{h2}</div></body></html>";

      return new HtmlContent(content);
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex);
      var errorContent = $"{mainContent}Something went wrong while signing the documents.</div></body></html>";
      return new HtmlContent(errorContent);
    }
  }

  public string GetESignUrl(ISession session,Instance instance)
  {
    var orderedDocuments = new List<byte[]>();
    var descriptions = new List<string>();
    var policy = instance.Policy;

    //get all the esign documents
    foreach(var document in policy.Documents.ESign)
    {
      var printForms = DiamondApi.Printing.LoadPrintForms(session,document.PolicyId,document.PolicyImageNumber);

      var printForm = printForms.FirstOrDefault(p => p.Matches(document));

      //TODO: Inject agency signature

      var bytes = DiamondApi.Printing.GetDocument(session,document.PolicyId,document.PolicyImageNumber,printForm);

      orderedDocuments.Add(bytes);
      descriptions.Add($"{document.Description} ({document.FormNumber})");
    }

    var subject = $"New Business-{policy.Number}-{policy.ImageNum}";

    //create signer names
    var signersWithName = new SignersWithName().With(s => s.AddToEnd($"{instance.Drivers[0].Name.First} {instance.Drivers[0].Name.Last}",EnSigner.Insured));

    //create envelope
    var newEnvelope = _DocuSignApi.CreateEnvelope(orderedDocuments,subject,policy.EffectiveDate.ToDate(),signersWithName.GetSigners());

    newEnvelope
    .AssignPolicyInfo(policy.Id,policy.ImageNum,1)
    .AssignDescriptions(descriptions);

    //Adding the envelope to tblEnvelope
    _DbDocuSign.AddOrUpdateEnvelope(newEnvelope);

    //var signerInfo = Array.Find(newEnvelope.Signers,c => c.Signer.ToString() == request.Data.UserKey) ?? Array.Find(newEnvelope.Signers,c => c.Status == EnSignerStatus.Default);
    var signerInfo = newEnvelope.Signers;

    //generate and return esign url
    return _DocuSignApi.GetUrl(newEnvelope.EnvelopeId,signerInfo[0])?.Url;
  }

  #endregion Public Methods

  #region Private Methods

  private Envelope AttachDocumentAndGetEnvelope(DocuSignRequest request,ISession session)
  {
    Envelope envelope = null;
    if(request.EnvelopeId != default)
    {
      envelope = _DbDocuSign.GetFromEnvelopeId(request);
    }
    else if(request.PolicyId > 0 && request.ImageNum > 0)
    {
      envelope = _DbDocuSign.GetFromPolicyInfo(request);
    }

    envelope.Status = EnEnvelopeStatus.Completed;

    foreach(var signer in envelope.Signers)
    {
      signer.Status = EnSignerStatus.signing_complete;
    }

    if(envelope.AttachmentId > 0)
      return envelope;

    var desc = envelope.GetDescription();
    var attachmentId = DiamondApi.Attachment.AttachFileAndGetId(session,request.Document,request.PolicyId,request.ImageNum,"esigned-documents.pdf",desc);

    if(attachmentId > 0)
    {
      envelope.AttachmentId = attachmentId;

      //delete the task for new business
      if(request.ImageNum == 1)
      {
        _ = Task.Run(() =>
        {
          //get the id and delete task
          var deleteWorkflowId = _DbDocuSign.SqlFirstOrDefault<int>("EXEC DocuSign.spGetPolicyTaskWorkflowId @PolicyId=@1,@PolicyImageNum=@2",c => c.AddValues(request.PolicyId,request.ImageNum));
          if(deleteWorkflowId > 0)
          {
            return DiamondApi.Workflow.Delete(session,deleteWorkflowId);
          }
          return false;
        });
      }
      return _DbDocuSign.AddOrUpdateEnvelope(envelope);
    }
    return envelope;
  }

  private void DeleteExistingEnvelope(int policyId)
  {
    const string sqlQuery = """
      DELETE X
      FROM      [DocuSign].[tblEnvelope] X
      LEFT JOIN Diamond..Policy          P(NOLOCK) ON P.policy_id = X.PolicyId
      WHERE X.PolicyId = @1
        AND X.ImageNum = 1
        AND P.quotesource_id = 8
      """;

    _DbDocuSign.Exec(sqlQuery,c => c.Add(policyId));
  }

  private DateTime GetRenewalPaymentDueDate(int policyId)
  {
    var imageInfo = FalconHandler.SqlSet("EXEC falcon.dbo.spGetActiveAndFutureImages @PolicyId=@1",c => c.AddValues(policyId));

    //filter on the active image and return the EffectiveDate
    var activeImageInfo = imageInfo?.Where(c => c.To<int>("Future") == 0)?.FirstOrDefault();
    return activeImageInfo?.To<DateTime>("EffectiveDate") ?? new DateTime();
  }

  private bool TryGetSignerInfo(Guid signerId,out Envelope envelope,out SignerInfo signer)
  {
    envelope = _DbDocuSign.GetFromSignerId(new() { SignerId = signerId });
    signer = envelope?.Signers.FirstOrDefault(c => c.ClientUserId == signerId);
    return signer is not null;
  }

  #endregion Private Methods
}
