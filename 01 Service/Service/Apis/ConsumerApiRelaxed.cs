namespace ConsumerServiceHost.Apis;

internal class ConsumerApiRelaxed
{
  #region Internal Properties

  internal IRelax<IAgencyApi> Agency { get; }
  internal IRelax<IDocumentsApi> Documents { get; }

  internal IRelax<IESignApi> ESign { get; }

  internal IRelax<IOneIncApi> OneInc { get; }

  internal IRelax<IPolicyApi> Policy { get; }

  internal IRelax<INotificationApi> Notification { get; }

  internal IRelax<IStateSpecApi> StateSpec { get; }

  internal IRelax<ITranslatorApi> Translator { get; }

  #endregion Internal Properties

  #region Public Constructors

  public ConsumerApiRelaxed(IConsumerApi consumerApi)
  {
    Agency = RelaxFactory.Create(() => new AgencyApi(consumerApi) as IAgencyApi);
    Documents = RelaxFactory.Create(() => new DocumentsApi(consumerApi) as IDocumentsApi);
    StateSpec = RelaxFactory.Create(() => new StateSpecApi(consumerApi) as IStateSpecApi);
    OneInc = RelaxFactory.Create(() => AppResolver.Get<IOneIncApi>());
    Policy = RelaxFactory.Create(() => new PolicyApi(consumerApi) as IPolicyApi);
    Notification = RelaxFactory.Create(() => new NotificationApi(consumerApi) as INotificationApi);
    ESign = RelaxFactory.Create(() => new ESignApi(consumerApi) as IESignApi);
    Translator = RelaxFactory.Create(() => new TranslatorApi(consumerApi) as ITranslatorApi);
  }

  #endregion Public Constructors
}
