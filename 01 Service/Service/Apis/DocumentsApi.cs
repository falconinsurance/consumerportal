using System.Runtime.CompilerServices;
using ConsumerServiceHost.Models;
using DiamondServices.Service;
using Falcon.Core.Models;
using Falcon.Tools.ZipUtility;

namespace ConsumerServiceHost.Apis;

public class DocumentsApi:BaseApi, IDocumentsApi
{
  #region Private Fields

  private const string _DOC_ERROR = "Something went wrong while retrieving the document(s).";
  private const string _POLICY_DOC = "Policy Documents.zip";
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(DocumentsApi));

  #endregion Private Fields

  #region Public Constructors

  public DocumentsApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public HttpFile GetDocumentAsHttpResp(ISession session,string code) =>
    LogWrap(() =>
    {
      var document = code.Decode<Document>();
      var bytes = getDocumentAsBytes(session,document);
      return new HttpFile(bytes,$"{document.Description}.pdf");
    });

  public HttpFile GetDocumentsAsHttpResp(ISession session,string code) =>
    LogWrap(() =>
    {
      var documents = code.Decode<Document[]>();
      using var zipUtil = new ZipUtil();
      foreach(var document in documents)
      {
        var byteArr = getDocumentAsBytes(session,document);
        zipUtil.AddEntry(byteArr,$"{document.Description}.pdf");
      }
      return new HttpFile(zipUtil.GetBytes(),_POLICY_DOC);
    });

  #endregion Public Methods

  #region Private Methods

  private byte[] getDocumentAsBytes(ISession session,Document document)
  {
    //Check if it is an esigned document. All props of 'Document' for Esign are null/empty except 'Description'
    if(IsESigned(document))
    {
      return ConsumerApi.ESign.GetEsignedDocumentBytes(document.PolicyId,document.PolicyImageNumber);
    }
    //logic for all other documents
    var printForms = DiamondApi.Printing.LoadPrintForms(session,document.PolicyId,document.PolicyImageNumber);
    var printForm = printForms.FirstOrDefault(p => p.Matches(document));
    return DiamondApi.Printing.GetDocument(session,document.PolicyId,document.PolicyImageNumber,printForm);
  }

  private bool IsESigned(Document document) => string.IsNullOrEmpty(document.FormNumber)
        && string.IsNullOrEmpty(document.Version) &&
        string.IsNullOrEmpty(document.Recipient);

  private HttpFile LogWrap(Func<HttpFile> func,[CallerMemberName] string methodName = null)
  {
    try
    {
      return func();
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex,methodName: methodName);
      return new HttpFile(errorMessage: _DOC_ERROR);
    }
  }

  #endregion Private Methods
}
