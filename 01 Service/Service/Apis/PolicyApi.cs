using System.Text.RegularExpressions;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DBTools;
using DecCommon.DB;
using DecCommon.Model;
using DecUdr.DB;
using DiamondServices.Service;
using Falcon.ConsumerDB;
using Falcon.Core.Models;
using Falcon.Decision;
using DCE = Diamond.Common.Enums;
using DCO = Diamond.Common.Objects;

namespace ConsumerServiceHost.Apis;

public class PolicyApi:BaseApi, IPolicyApi
{
  #region Private Fields

  private static readonly ClassCache _Cache = new();
  private static readonly IRelax<ICommonHandler> _CommonHandlerRelaxed = RelaxFactory.Create(() => AppResolver.Get<ICommonHandler>());
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(PolicyApi));
  private static readonly IRelax<IUdrHandler> _UdrHandlerRelaxed = RelaxFactory.Create(() => AppResolver.Get<IUdrHandler>());

  #endregion Private Fields

  #region Public Properties

  public ICommonHandler _CommonHandler => _CommonHandlerRelaxed.Value;
  public IUdrHandler _UdrHandler => _UdrHandlerRelaxed.Value;

  #endregion Public Properties

  #region Private Properties

  private static IDecisionRisk _DecisionRisk => AppResolver.Get<IDecisionRisk>();

  #endregion Private Properties

  #region Public Constructors

  public PolicyApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public TransactionResponse Bind(ISession session,DCO.Policy.Image image,Payment payment,En_State state)
  {
    //TODO: check Recurring payments
    // Check with BillingSweepAgencyId

    var transactionResponse = new TransactionResponse();

    if(BuildApplyCashIsValid(session,image,payment,transactionResponse,out var applyCash,out var action))
    {
      var response = DiamondApi.Policy.SaveRateIssue(session,image,applyCash);

      action?.Invoke();

      SavePayment(image,payment,response);

      //getting the right image by checking for future endorsement
      var finalImage = image.PolicyImageNum == response.ResponseData.Image.PolicyImageNum ? response.ResponseData.Image : image;
      transactionResponse.Success = response.ResponseData.IssueSuccessful && !response.DiamondValidation.ValidationItems.Any(v => v.ItemType == DCO.ValidationItemType.ValidationError);

      if(transactionResponse.Success)
      {
        transactionResponse.Instance = finalImage.ToInstance(session,true);
        CreateWorkflowItems(session,image);
        CreateManualTransactionItems(session,image,state);
        CreateUDRNoteItem(session,image);
        var policyHolder = transactionResponse.Instance.PolicyHolder;
        CreateSmsSubscription(finalImage.PolicyNumber,finalImage.PolicyId,transactionResponse.Instance.Policy.PaymentInfo.Payplans.Current.Type,policyHolder.Phones.FirstOrDefault(p => p.Type == En_PhoneNumber.Cellular));
        SendWelcomeEmail(policyHolder.Email,finalImage.PolicyNumber,policyHolder.Name.Full);
      }
      else
      {
        transactionResponse.Instance = finalImage.ToInstance(session,false);
        //Add validation errors/warnings from diamond response.
        AddValidationErrorsAndWarnings(transactionResponse.Instance,response.DiamondValidation.ValidationItems);
      }
    }

    return transactionResponse;
  }

  public Instance GetRate(ISession session,Instance instance,DCO.Policy.Image newImage,DCO.Policy.Image oldImage)
  {
    var risk = instance.RiskCheck.ToEnum<EnRiskCheck>();
    //Send both the images to decisioning check
    var decisioningResult = risk == EnRiskCheck.None ? null : _DecisionRisk.Check(new ParamDecision(
      risk,
      instance.AttachedTypes,
      newImage,
      oldImage));

    var newInstance = instance;

    switch(risk)
    {
      case EnRiskCheck.None:
      case EnRiskCheck.ConsumerAppLvl0:
      case EnRiskCheck.ConsumerAppLvl3:
      case EnRiskCheck.ConsumerAppLvl4:
        //Submit the new image to diamond
        var diaResponse = DiamondApi.Policy.Submit(session,newImage,instance.Agency.State);
        //If rating for successful, we get back a new image.
        //Update the policy info in Decisioning and convert the image to a new instance
        if(diaResponse.Image is DCO.Policy.Image image)
        {
          //Promote policy (changes quote number to a policy number), if we have Q in the Quote number.
          if(risk == EnRiskCheck.ConsumerAppLvl4 && image.Policy?.CurrentPolicy?.StartsWith("Q") == true)
          {
            //promote and submit again for updating policy number in eSign documents
            diaResponse.Image = DiamondApi.Policy.Promote(session,diaResponse.Image.PolicyId,diaResponse.Image.PolicyImageNum);
          }
          decisioningResult?.UpdatePolicyId(diaResponse.Image.PolicyId);
          newInstance = diaResponse.Image.ToInstance(session);
        }
        //Add validation errors/warnings from diamond response.
        AddValidationErrorsAndWarnings(newInstance,diaResponse.ValidationItems);
        break;

      case EnRiskCheck.ConsumerAppLvl1:
      case EnRiskCheck.ConsumerAppLvl2:
        //Creates new instance from loaded image and decisioning result.
        newInstance = newImage.ToInstance(session);
        break;

      default:
        throw new("Risk level for rating is not set.");
    }

    //Add validation errors/warnings from decision result.
    if(decisioningResult is not null)
    {
      AddValidationErrorsAndWarnings(newInstance,decisioningResult);
    }
    return newInstance;
  }

  public FalconResponse<bool> RegisterUser(InsRegistration insRegistration)
  {
    var response = new FalconResponse<bool>();

    var clientId = DiamondApi.Security.GetRegistrationClientId(insRegistration.PolicyNumber,insRegistration.ZipCode);
    if(clientId > 0)
    {
      //validation items are returned when save user fails.
      var validationItems = DiamondApi.Administration.SaveUser(insRegistration.Email,insRegistration.Password,clientId);
      if(validationItems is not null)
      {
        if(validationItems.Any(c => c.Message.Contains("already exists in the system")))
        {
          return response.With(c => c.Assign(false,ConsumerApi.Translator.GetLabelKey(EnTranslateKey.ErrorAccExists)));
        }
      }
      else
      {
        FalconHandler.Sql(sqlQuery: "DELETE FROM IP.tblPortalUser WHERE EmailAddress =@1;",c => c.AddValues(insRegistration.Email));

        //create email subscription
        Task.Run(() =>
        {
          return ConsumerApi.Notification.UpdateEmailSubscription(insRegistration.PolicyNumber,insRegistration.Email);
        });

        return response.With(c => c.Result = true);
      }
    }
    return response.With(c => c.Assign(false,ConsumerApi.Translator.GetLabelKey(EnTranslateKey.ErrorRegistration)));
  }

  public bool SendWelcomeEmail(string email,string policyNumber,string name)
  {
    if(!email.IsNull())
    {
      var request = new
      {
        FullName = name,
        PolicyNumber = policyNumber,
        EmailAddress = email
      };
      Task.Run(() => ConsumerApi.Notification.SendNewPolicyWelcomeEmail(SqlRecord.FromObject(request)));
      return true;
    }
    return false;
  }

  #endregion Public Methods

  #region Private Methods

  private void AddValidationErrorsAndWarnings(Instance instance,IList<DCO.ValidationItem> validationItems)
  {
    var errors = validationItems?.Where(vi => vi.ItemType == DCO.ValidationItemType.ValidationError);
    if(errors?.Any() == true)
    {
      errors.ForEach(e => instance.Validation.Errors.Add(e.Message));
      CleanValidationErrorMessages(instance.Validation);
    }
    var warnings = validationItems.Where(vi => vi.ItemType == DCO.ValidationItemType.ValidationWarning);
    if(warnings?.Any() == true)
    {
      warnings.ForEach(validationItem =>
      {
        var msg = IsAcceptableDiamondWarning(validationItem.Message,instance.Agency.State,instance.Policy.Type);

        // Only add if message returned is non-null and isn't already a part of the Warnings list in cases where Diamond sends a warning twice.
        if(msg != null && !instance.Validation.Warnings.Any(x => x == msg))
        {
          instance.Validation.Warnings.Add(validationItem.Message);
        }
      });
    }
  }

  private void AddValidationErrorsAndWarnings(Instance instance,IDecisionRisk decisionRick)
  {
    var res = decisionRick.Result;
    var param = decisionRick.Param;

    instance.RiskCheck = param.RiskCheck.ToString();

    instance.AttachedTypes.DriversViolations = param.DriversViolations;

    if(res.PortalWarning?.Count > 0)
    {
      foreach(var message in res.PortalWarning)
      {
        instance.Validation.Warnings.Add(message);
      }
    }

    if(!res.Pass && res.PortalError?.Count > 0)
    {
      foreach(var message in res.PortalError)
      {
        instance.Validation.Errors.Add(message);
      }
    }
  }

  private bool BuildApplyCashIsValid(ISession session,DCO.Policy.Image image,Payment payment,TransactionResponse transactionResponse,out DCO.Billing.ApplyCash applyCash,out Action action)
  {
    action = null;

    applyCash = new DCO.Billing.ApplyCash()
    {
      PolicyId = image.PolicyId,
      PolicyImageNum = image.PolicyImageNum,
      CheckDate = DiamondApi.SystemTime.ToInsDateTime(),
      AccountPayment = false,
      BillingAccountId = 0,
      CashType = (int)DCE.Billing.BillingCashType.Payment,
      CashAmount = payment.Amount,
    };

    //Update other apply cash fields based on payment type
    return payment.Type switch
    {
      En_Payment.CreditCard => UpdateApplyCashWithCreditCard(payment.CreditCard,applyCash,action),
      En_Payment.ACH => UpdateApplyCashWithEFT(payment.Eft,applyCash,transactionResponse),
      En_Payment.AgencySweep => UpdateApplyCashWithCash(applyCash),
      _ => false,
    };

    bool UpdateApplyCashWithCreditCard(CreditCard creditCard,DCO.Billing.ApplyCash applyCash,Action action)
    {
      var diaCreditCardData = new DCO.Billing.CreditCard
      {
        ExpirationDate = creditCard.Expiration.Date,
        AccountNumber = creditCard.Last4,
        PaymentProfileId = creditCard.Token,
        CreditCardType = creditCard.Type.GetDiaId(),
        CreditCardVendorId = 3,
        PolicyId = image.PolicyId,
        DeductionDay = 1,
        // Dummy Info
        CVSCode = "999",
        CreditCardHolder = new()
        {
          Name = new()
          {
            FirstName = "Token",
            LastName = "Placeholder"
          },
          Address = new()
          {
            HouseNumber = "123",
            StreetName = "Placeholder St.",
            City = "New York",
            StateId = (int)(DCE.State.NY),
            Zip = "55555-5555"
          }
        }
      };

      var clientId = image?.Policy?.Client?.ClientId ?? 0;
      DiamondApi.CreditCard.SaveCCToken(session,image,clientId,diaCreditCardData);
      var profileId = GetDefaultPaymentProfileId(clientId) ?? diaCreditCardData.PaymentProfileId;
      var profileIdResponse = DiamondApi.CreditCard.SaveCustomerAndPaymentProfiles(session,image,clientId,profileId);
      diaCreditCardData.CustomerProfileId ??= profileIdResponse?.ResponseData?.CustomerProfileId ?? "";
      DiamondApi.Billing.SaveCreditCardInfo(session,image.PolicyId,diaCreditCardData);

      applyCash.CreditCardData = diaCreditCardData;
      applyCash.CashInSource = (int)DCE.Billing.BillingCashInSource.CreditCard;

      action = () => DiamondApi.CreditCard.DeletePaymentProfile(session,image.PolicyId,clientId,diaCreditCardData.CustomerProfileId,true);

      return true;
    }

    bool UpdateApplyCashWithEFT(Eft eft,DCO.Billing.ApplyCash applyCash,TransactionResponse transactionResponse)
    {
      if(!GetRoutingNumbers().Any(c => c == eft.RoutingNumber))
      {
        transactionResponse.Success = false;
        transactionResponse.Error = ConsumerApi.Translator.GetLabelKey(EnTranslateKey.ErrorInvalidRoutingNumber);
        return false;
      }
      var encryptedAccountNumber = DiamondApi.Security.Encrypt(session,eft.AccountNumber,image.PolicyId);
      var diaEft = new DCO.EFT.Eft()
      {
        AccountHolder = eft.FullName,
        AccountNumber = encryptedAccountNumber,
        RoutingNumber = eft.RoutingNumber,
        BankAccountTypeId = eft.Type.GetDiaId(),
        PolicyId = image.PolicyId,
        PolicyImageNum = image.PolicyImageNum,
        DeductionDay = 1,
      };
      var eftResponse = DiamondApi.Billing.SaveEft(session,diaEft);
      applyCash.EftAccountId = eftResponse.ResponseData.EftAccountId;
      applyCash.CashInSource = (int)DCE.Billing.BillingCashInSource.Eft;
      return true;
    }

    bool UpdateApplyCashWithCash(DCO.Billing.ApplyCash applyCash)
    {
      applyCash.CashInSource = (int)DCE.Billing.BillingCashInSource.AgencyEft;
      return true;
    }
  }

  private void CleanValidationErrorMessages(Validation validation)
  {
    // Different pattern combinations of uselss Diamond validations
    var avpRegex = new Regex("^Error in RateCoverage_BodilyInjury Unable to locate Y Axis",RegexOptions.IgnoreCase);
    var invalidDOBRegex = new Regex(@"^Error in RateCoverage_(BodilyInjury|PropertyDamage) Unable to locate Table\[ClassCodeFactors\] ""Driver Characteristics"" Value\[MS\]",RegexOptions.IgnoreCase);
    var redMountainRegex = new Regex("^Error in table lookup. Table: (VehicleHistoryScoreFactor)",RegexOptions.IgnoreCase);

    foreach(var vi in validation.Errors)
    {
      // Check for any matches
      if(avpRegex.IsMatch(vi))
      {
        validation.Errors = ["Driver is under the age of 15 and should be Excluded."];
      }
      if(invalidDOBRegex.IsMatch(vi))
      {
        validation.Errors = ["Invalid Date of Birth."];
      }
      if(redMountainRegex.IsMatch(vi))
      {
        validation.Errors = ["Underwriting Failed. Policy did not rate."];
      }
    }
  }

  private void CreateManualTransactionItems(ISession session,DCO.Policy.Image image,En_State state)
    => Task.Run(() =>
    {
      var manualTransactionItems = GetManualTransactionItems(image.PolicyId);

      if(manualTransactionItems?.Count() > 0)
      {
        var policyFormNum = GetPolicyFormNumber(image.PolicyId,image.PolicyImageNum);
        foreach(var manualTransactionItem in manualTransactionItems)
        {
          DiamondApi.PolicyForm.AddAndPrintByFormVersion(session,
            image,
            manualTransactionItem.Remarks,
            manualTransactionItem.AdditionalInfo,
            manualTransactionItem.FormVersionId,
            manualTransactionItem.DueDateDays,
            policyFormNum,
            state);
        }
        Task.Run(() => RemoveManualTransactionItem(manualTransactionItems));
      }
    });

  private void CreateSmsSubscription(string policyNumber,int policyId,En_PayPlan payPlanType,Phone phone)
  {
    if(!payPlanType.In(En_PayPlan.None,En_PayPlan.PayInFullPremiumFinanced,En_PayPlan.PayInFull) && phone != null)
    {
      Task.Run(() => ConsumerApi.Notification.UpdateSmsSubscription(policyNumber,policyId,phone.GetConvertedNumber()));
    }
  }

  private void CreateUDRNoteItem(ISession session,DCO.Policy.Image image)
  {
    if(image.PolicyImageNum == 1)
    {
      var persons = _UdrHandler.GetPersons(image.PolicyId);
      if(persons.Count > 0)
      {
        const string title = "Undisclosed Drivers";
        var note = $"{title}:";
        foreach(var person in persons)
        {
          note = string.Concat(note,"\nName:" + person.First + " " + person.Last + ", DOB:" + person.DOB.ToShortDateString() + ", Decision:" + person.Status);
        }
        note = note.TrimEnd(',');
        DiamondApi.Notes.Create(session,image.PolicyId,note,title,128);
      }
    }
  }

  private void CreateWorkflowItems(ISession session,DCO.Policy.Image image)
    => Task.Run(() =>
    {
      var workFlowItems = GetWorkflowItems(image.PolicyId);

      if(workFlowItems?.Count() > 0)
      {
        foreach(var workFlowItem in workFlowItems)
        {
          DiamondApi.Workflow.Create(session,
            image,workFlowItem.Remarks,
            workFlowItem.DueDateDays,
            DCE.Workflow.WorkflowType.PolicyDiary,
            workFlowItem.WorkflowQueue);
        }

        Task.Run(() => RemoveWorkflowItems(workFlowItems));
      }
    });

  private string GetDefaultPaymentProfileId(int clientId)
  {
    var result = DiamondApi.DbFalcon.Sql("SELECT TOP(1) default_payment_profile_identifier FROM Diamond..CreditCardTokenClientInfo(NOLOCK) WHERE client_id = @1;",c => c.AddValues(clientId)).ToArray();
    return result.FirstOrDefault()?["default_payment_profile_identifier"]?.ToString();
  }

  private IEnumerable<ManualTransactionItem> GetManualTransactionItems(int policyId) => _CommonHandler.Sql<ManualTransactionItem>("Select * from tblManualTransactionItem where PolicyId=@1;",c => c.AddValues(policyId))
           .GroupBy(g => new { g.PolicyId,g.Remarks })
         .Select(g => g.First());

  private int GetPolicyFormNumber(int policyId,int policyImageNumber) => _CommonHandler.Sql<int>("SELECT COUNT(*) FROM Diamond..PolicyForm(NOLOCK) WHERE policy_id =@1 AND policyimage_num =@2;",c => c.AddValues(policyId,policyImageNumber)).ToArray()[0] + 1;

  private IEnumerable<string> GetRoutingNumbers()
  {
    return _Cache.SetGet(Request,new("EFTRoutingNumbers"));
    IEnumerable<string> Request()
    {
      return DiamondApi.DbFalcon.Sql("EXEC [dbo].[spGetValidRoutingNumbers];").Select(c => c["RoutingNumber"].ToString());
    }
  }

  private IEnumerable<WorkFlowItem> GetWorkflowItems(int policyId) => _CommonHandler.Sql<WorkFlowItem>("Select * from tblWorkFlowItem where PolicyId=@1;",c => c.AddValues(policyId))
               .GroupBy(g => new { g.PolicyId,g.Remarks })
         .Select(g => g.First());

  private string IsAcceptableDiamondWarning(string warningMessage,En_State state,En_Policy policyType)
  {
    // Different pattern combinations of Diamond validation warnings we want
    var evenMsgRegex = new Regex("^Uneven number of Married/Civil Union drivers. Spouse must be listed on the policy.",RegexOptions.IgnoreCase);
    var unacceptableMsgRegex = new Regex("Unacceptable Vehicle surcharge",RegexOptions.IgnoreCase);
    var unacceptablePhysDamMsgRegex = new Regex("(?i)(.*comp.*coll.*)",RegexOptions.IgnoreCase);
    var unacceptablePhysDamAdjustedDeductibleMsgRegex = new Regex("1,000",RegexOptions.IgnoreCase);
    var unacceptableDriversRiskSurcharge = new Regex("able Risk Surcharge",RegexOptions.IgnoreCase);

    // These are for rater sourced quotes as the AP should prevent this situation from occuring
    var medPayRemovedRegex = new Regex("Medical Payments have been removed",RegexOptions.IgnoreCase);
    var limitCannotExceedBIRegex = new Regex("limit cannot exceed Bodily Injury limit and has been updated",RegexOptions.IgnoreCase);
    var minimumLimitsRegex = new Regex("have been reset to their minimum limits.",RegexOptions.IgnoreCase);

    // Even Number of Married Drivers
    if(evenMsgRegex.IsMatch(warningMessage) && (state != En_State.UT || (state == En_State.UT && policyType == En_Policy.Owner))) // Temporarily don't do this in Utah
    {
      return warningMessage;
    }
    // Surcharge Endorsed Vehicles
    if(unacceptableMsgRegex.IsMatch(warningMessage))
    {
      var vehicleDesc = warningMessage.Substring(0,warningMessage.IndexOf("-") + 1);
      if(unacceptablePhysDamMsgRegex.IsMatch(warningMessage) && unacceptablePhysDamAdjustedDeductibleMsgRegex.IsMatch(warningMessage))
      {
        if(unacceptablePhysDamAdjustedDeductibleMsgRegex.IsMatch(warningMessage))
        {
          return $"{vehicleDesc} Due to vehicle being unacceptable for COMP/COLL coverage, the deductible has been updated to $1,000 and a surcharge has been applied.";
        }
        else
        {
          return $"{vehicleDesc} Due to vehicle being unacceptable for COMP/COLL coverage, a surcharge has been applied.";
        }
      }
      else
      {
        return $"{vehicleDesc} Due to vehicle being unacceptable, a surcharge has been applied.";
      }
    }
    if(medPayRemovedRegex.IsMatch(warningMessage) || limitCannotExceedBIRegex.IsMatch(warningMessage) || minimumLimitsRegex.IsMatch(warningMessage))
      return warningMessage;
    // Unacceptable Driver Surchage
    if(unacceptableDriversRiskSurcharge.IsMatch(warningMessage))
    {
      return warningMessage.Replace("Unaccetpable","Unacceptable");
    }
    return null;
  }

  private void RemoveManualTransactionItem(IEnumerable<ManualTransactionItem> manualTransactionItems) => _CommonHandler.Exec("DELETE FROM [dbo].[tblManualTransactionItem]  WHERE PolicyId=@1;",c => c.AddValues(manualTransactionItems.First().PolicyId));

  private void RemoveWorkflowItems(IEnumerable<WorkFlowItem> workFlowItems) => _CommonHandler.Exec("DELETE FROM [dbo].[tblWorkFlowItem]  WHERE PolicyId=@1;",c => c.AddValues(workFlowItems.First().PolicyId));

  private void SavePayment(DCO.Policy.Image image,Payment payment,Diamond.Common.Services.Messages.PolicyService.SaveRateIssue.Response response)
  {
    var paymentProfile = new PaymentProfile
    {
      Amount = payment.Amount,
      ImageNum = image.PolicyImageNum,
      PaymentType = payment.Type,
      PolicyId = image.PolicyId,
      Source = payment.Source,
      Success = response.ResponseData.IssueSuccessful,
      DateAdded = DiamondApi.SystemDate,
      Method = "BindPolicy"
    };
    Task.Run(() => DiamondApi.DbFalcon.Sql("Exec SpSavePayment @PaymetProfile=@1",c => c.AddJson(paymentProfile)));
  }

  #endregion Private Methods
}
