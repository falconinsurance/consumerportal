using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Apis;

public interface ITranslatorApi
{
  #region Public Methods

  string GetLabelKey(EnTranslateKey translateKey);

  Dictionary<string,string> GetLabelLookupsDictionary(En_Language en_Language);

  #endregion Public Methods
}
