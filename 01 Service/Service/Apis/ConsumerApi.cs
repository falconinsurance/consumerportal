namespace ConsumerServiceHost.Apis;

public class ConsumerApi:IConsumerApi
{
  #region Private Fields

  private static ConsumerApiRelaxed _Relaxed;

  #endregion Private Fields

  #region Public Properties

  public IAgencyApi Agency => _Relaxed.Agency.Value;

  public IDocumentsApi Documents => _Relaxed.Documents.Value;

  public IESignApi ESign => _Relaxed.ESign.Value;

  public IOneIncApi OneInc => _Relaxed.OneInc.Value;

  public IPolicyApi Policy => _Relaxed.Policy.Value;

  public INotificationApi Notification => _Relaxed.Notification.Value;

  public IStateSpecApi StateSpec => _Relaxed.StateSpec.Value;

  public ITranslatorApi Translator => _Relaxed.Translator.Value;

  #endregion Public Properties

  #region Public Constructors

  public ConsumerApi()
  {
    if(_Relaxed is not null)
      throw new Exception($"You can not create {nameof(ConsumerApi)} more than once.");
    _Relaxed ??= new ConsumerApiRelaxed(this);
  }

  #endregion Public Constructors
}
