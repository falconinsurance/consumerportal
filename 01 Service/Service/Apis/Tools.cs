using DoucuSignCommon.Model;
using Falcon.ConsumerDB;
using Falcon.Core.Models;
using Falcon.Service.SmartyStreets;
using Newtonsoft.Json;

using CoreModels = Falcon.Core.Models;

using DCO = Diamond.Common.Objects;

namespace ConsumerServiceHost.Apis;

public static class Tools
{
  #region Public Fields

  public const string ESIGN = "eSigned Document";

  #endregion Public Fields

  #region Public Methods

  public static T Decode<T>(this string input)
  {
    var data = Convert.FromBase64String(input);
    var decodedString = Encoding.UTF8.GetString(data);
    return JsonConvert.DeserializeObject<T>(decodedString);
  }

  public static EnTranslateKey GetFriendlyMessage(this EnSignerStatus signerStatus) => signerStatus switch
  {
    EnSignerStatus.Decline => EnTranslateKey.DocuSignDeclined,
    EnSignerStatus.signing_complete => EnTranslateKey.DocuSignComplete,
    EnSignerStatus.tt_expired => EnTranslateKey.DocuSignExpired,
    _ => EnTranslateKey.DocuSignDefault,
  };

  public static bool Matches(this DCO.Printing.PrintForm printForm,Document document)
  {
    var flag = true;
    flag &= printForm.FormNumber.ToLowerInvariant().Trim() == document.FormNumber.ToLowerInvariant().Trim();
    flag &= printForm.PolicyFormNum == document.FormId;
    flag &= printForm.EditionVersion.ToLowerInvariant().Trim() == document.Version?.ToLowerInvariant().Trim();
    flag &= printForm.PrintRecipients[0].Description == document.Recipient;
    flag &= printForm.PolicyImageNum == document.PolicyImageNumber;
    return flag;
  }

  public static T ToEnum<T>(this string value) => (T)Enum.Parse(typeof(T),value);

  private static bool AreEquivalent(string s1,string s2)
  {
    if(string.IsNullOrEmpty(s1) && string.IsNullOrEmpty(s2))
    {
      return true;
    }
    if(!string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2))
    {
      return string.Compare(s1,s2,StringComparison.OrdinalIgnoreCase) == 0;
    }
    return false;
  }

  public static CoreModels.Address ToConsumerAddress(this StreetAddress addr) => new()
  {
    Street = !string.IsNullOrEmpty(addr.Street2) ? $"{addr.Street1} {addr.Street2}" : addr.Street1,
    City = addr.City,
    State = addr.State.ToEnum<En_State>(),
    Zip = addr.ZipCode.Substring(0,5),
    County = addr.County.ToUpper()
  };

  public static bool Compare(this CoreModels.Address srcSmarty,CoreModels.Address target) => AreEquivalent(srcSmarty.Street,target.Street) && AreEquivalent(srcSmarty.City,target.City) &&
      srcSmarty.State == target.State && AreEquivalent(srcSmarty.County,target.County)
      && AreEquivalent(srcSmarty.Zip.Substring(0,5),target.Zip);

  #endregion Public Methods
}
