namespace ConsumerServiceHost.Apis;

public interface IOneIncApi
{
  #region Public Methods

  string GetSaveUrl();

  string GetSessionId();

  #endregion Public Methods
}
