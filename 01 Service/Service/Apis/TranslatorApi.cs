using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Apis;

public class TranslatorApi:BaseApi, ITranslatorApi
{
  #region Private Fields

  private static readonly ClassCache _Cache = new();

  #endregion Private Fields

  #region Public Constructors

  public TranslatorApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public string GetLabelKey(EnTranslateKey translateKey)
  {
    return _Cache.SetGet(Request,new(translateKey));
    string Request() => GetLabelLookups().Find(c => c.Type == translateKey)?.Key ?? default;
  }

  public Dictionary<string,string> GetLabelLookupsDictionary(En_Language language)
  {
    return _Cache.SetGet(Request,new(language));
    Dictionary<string,string> Request() => GetLabelLookups().ToDictionary(c => c.Key,c => language == En_Language.ES ? c.Es ?? c.En : c.En);
  }

  #endregion Public Methods

  #region Private Methods

  private List<LabelLookup> GetLabelLookups()
  {
    return _Cache.SetGet(Request);
    List<LabelLookup> Request() => ConsumerHandler.Sql("SELECT * FROM tblLabelLookup;",null,LabelLookup.Cast).ToList();
  }

  #endregion Private Methods
}
