using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Apis;

public class AgencyApi:BaseApi, IAgencyApi
{
  #region Private Fields

  private static ClassCache _Cache = new();

  #endregion Private Fields

  #region Public Constructors

  public AgencyApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public List<Agency> GetAgencies(Guid agencyGroupId)
  {
    var agencies = _Cache.SetGet(Request);

    if(agencies.TryGetValue(agencyGroupId,out var list))
    {
      return list;
    }
    return [];

    Dictionary<Guid,List<Agency>> Request()
    {
      var result = ConsumerHandler.Sql("SELECT A.*, AG.State FROM tblAgency A INNER JOIN tblAgencyGroup AG ON A.AgencyGroupId = AG.Id;",null,Agency.Cast);
      var ret = result.GroupBy(g => g.AgencyGroupId).ToDictionary(c => c.Key,c => c.Select(i => i).ToList());
      return ret;
    }
  }

  public Agency GetAgency(int agencyId, int producerId)
  {
    var agencies = _Cache.SetGet(Request);
    return agencies.First(c => c.Id == agencyId && c.ProducerId == producerId);

    IList<Agency> Request() => ConsumerHandler.SqlList("SELECT A.* FROM tblAgency A(NOLOCK);",null,Agency.Cast);
  }

  #endregion Public Methods
}
