using System.Runtime.CompilerServices;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DBTools;
using Falcon.Data.Notification;

namespace ConsumerServiceHost.Apis;

public class NotificationApi:BaseApi, INotificationApi
{
  private const string _Uri = "/ApiNotification/";
  private static readonly ClassCache _Cache = new();
  private static readonly IRelax<IFalconHttpClient> _FalconHttpClientRelaxed = RelaxFactory.Create(() => AppResolver.Get<IFalconHttpClient>());
  private static readonly ILog _Log = AppResolver.GetLogger(nameof(NotificationApi));

  public IFalconHttpClient _FalconHttpClient => _FalconHttpClientRelaxed.Value;

  public NotificationApi(IConsumerApi consumerApi) : base(consumerApi)
  {
  }

  public SqlRecord SendNewPolicyWelcomeEmail(SqlRecord request)
  {
    try
    {
      return _FalconHttpClient.PostJson<SqlRecord>(_Uri + "SendNewPolicyWelcomeEmail",request);
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex,msg: "Error sending welcome email to newly bound policy holder.",obj: request);
      return [];
    }
  }

  public Subscription UpdateEmailSubscription(string policyNumber,string email)
  {
    var id = 0;
    var request = CreateNewSubscription(policyNumber).With(s =>
    {
      var sub = s.UserSubscription[0];
      sub.EmailAddress = email;
      sub.EmailSubscriptions =
      [
        new EmailSubscriptions {
          Id = id,
          LanguageType = EnLanguageType.English,
          EmailAddress = email,
          PromotionalItems = true,
        }.With(c=>SetRemindersBase(c))
      ];
    });
    return Submit(request);
  }

  public Subscription UpdateSmsSubscription(string policyNumber,int policyId,string phoneNumber)
  {
    var id = 0;
    var request = CreateNewSubscription(policyNumber).With(s =>
    {
      s.PolicyId = policyId;
      var sub = s.UserSubscription[0];
      sub.SmsSubscriptions =
      [
        new SmsSubscriptions {
          Id = id,
          LanguageType = EnLanguageType.Default,
          PhoneNumber = phoneNumber,
          ValidCellPhone = true,
        }.With(c=>SetRemindersBase(c))
      ];
    });
    return Submit(request);
  }

  private Subscription CreateNewSubscription(string policyNumber) => new Subscription
  {
    PolicyNumber = policyNumber,
    UserSubscription =
    [
      new UserSubscription
      {
        EmailAddress = "",
        IsActive = true,
      }
    ]
  };

  private void SetRemindersBase(RemindersBase remindersBase)
  {
    remindersBase.ActiveCancelReminder = true;
    remindersBase.ActiveDeclinedPayment = true;
    remindersBase.ActiveExpiringCard = true;
    remindersBase.ActivePayment = true;
    remindersBase.ActivePaymentConfirmation = true;
    remindersBase.ActivePendingCancel = true;
    remindersBase.IsPrimary = true;
    remindersBase.OptIn = true;
  }

  private Subscription Submit(Subscription request,[CallerMemberName] string type = "")
  {
    try
    {
      var result = _FalconHttpClient.PostJson<NotificationResponse<Subscription>>(_Uri + "UpdateSubscription",request);
      return result.Request;
    }
    catch(Exception ex)
    {
      _Log.Warn(ex: ex,msg: $"Error inserting/updating user subscription. SubscriptionId={request?.Id}. Exception={ex.Message}.",note: type);
      return new Subscription();
    }
  }
}
