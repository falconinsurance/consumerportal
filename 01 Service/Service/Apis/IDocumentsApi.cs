using ConsumerServiceHost.Models;
using DiamondServices.Service;

namespace ConsumerServiceHost.Apis;

public interface IDocumentsApi
{
  #region Public Methods

  HttpFile GetDocumentAsHttpResp(ISession session,string code);

  HttpFile GetDocumentsAsHttpResp(ISession session,string code);

  #endregion Public Methods
}
