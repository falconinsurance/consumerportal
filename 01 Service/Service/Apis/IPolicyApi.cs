using ConsumerServiceHost.Core;
using ConsumerServiceHost.Models;
using DiamondServices.Service;
using Falcon.Core.Models;
using DCO = Diamond.Common.Objects;

namespace ConsumerServiceHost.Apis;

public interface IPolicyApi
{
  #region Public Methods

  TransactionResponse Bind(ISession session,DCO.Policy.Image image,Payment payment,En_State state);

  Instance GetRate(ISession session,Instance instance,DCO.Policy.Image newImage,DCO.Policy.Image oldImage);

  FalconResponse<bool> RegisterUser(InsRegistration insRegistration);
  bool SendWelcomeEmail(string email,string policyNumber,string name);

  #endregion Public Methods
}
