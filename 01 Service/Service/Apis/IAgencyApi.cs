using Falcon.ConsumerDB;

namespace ConsumerServiceHost.Apis
{
  public interface IAgencyApi
  {
    #region Public Methods

    List<Agency> GetAgencies(Guid agencyGroupId);

    Agency GetAgency(int agencyId,int producerId);

    #endregion Public Methods
  }
}
