using DBTools;
using Falcon.Data.Notification;

namespace ConsumerServiceHost.Apis;

public interface INotificationApi
{
  #region Public Methods

  Subscription UpdateEmailSubscription(string policyNumber,string email);

  Subscription UpdateSmsSubscription(string policyNumber,int policyId,string phoneNumber);

  SqlRecord SendNewPolicyWelcomeEmail(SqlRecord request);

  #endregion Public Methods
}
