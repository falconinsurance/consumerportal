using DiamondServices.Apis;
using Falcon.ConsumerDB;
using Falcon.FalconDB;

namespace ConsumerServiceHost.Apis;

public abstract class BaseApi:IBaseApi
{
  #region Private Fields
  private static readonly IRelax<IConsumerHandler> _ConsumerHandlerRelaxed = RelaxFactory.Create(AppResolver.Get<IConsumerHandler>);
  private static readonly IRelax<IDiamondApi> _DiamondApiRelaxed = RelaxFactory.Create(AppResolver.Get<IDiamondApi>);
  private static readonly IRelax<IFalconHandler> _FalconHandlerRelaxed = RelaxFactory.Create(AppResolver.Get<IFalconHandler>);
  #endregion Private Fields

  #region Protected Constructors

  protected BaseApi(IConsumerApi consumerApi)
  {
    ConsumerApi = consumerApi;
  }

  #endregion Protected Constructors

  #region Public Properties
  public IConsumerApi ConsumerApi { get; }
  public IConsumerHandler ConsumerHandler => _ConsumerHandlerRelaxed.Value;
  public IDiamondApi DiamondApi => _DiamondApiRelaxed.Value;
  public IFalconHandler FalconHandler => _FalconHandlerRelaxed.Value;
  #endregion Public Properties
}
