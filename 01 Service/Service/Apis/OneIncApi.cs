using System.Net.Http;
using Newtonsoft.Json;

namespace ConsumerServiceHost.Apis;

public class OneIncApi:IOneIncApi
{
  #region Private Fields
  private const string _PORTAL_KEY = "33564886-9af1-4b88-a2b1-8aa72e74aba1";
  private const string _PORTAL_KEY_TEST = "58e5024f-ecc0-40a5-bac8-4ffd5e9f8cd8";
  private const string _PORTAL_URI = "portalone";
  private const string _PORTAL_URI_TEST = "stgportalone";

  private readonly HttpClient _Client;
  private readonly string _SaveUrl;
  #endregion Private Fields

  #region Public Constructors

  public OneIncApi(bool isProduction)
  {
    var baseUri = $"{(isProduction ? _PORTAL_URI : _PORTAL_URI_TEST)}.processonepayments.com/api/api";
    _SaveUrl = $"https://{baseUri}/Card/Save";
    var portalKey = isProduction ? _PORTAL_KEY : _PORTAL_KEY_TEST;
    var oneIncSessionUri = new Uri($"https://{baseUri}/Session/Create?PortalOneAuthenticationKey={portalKey}");
    _Client = new() { BaseAddress = oneIncSessionUri };
  }

  #endregion Public Constructors

  #region Public Methods

  public string GetSaveUrl() => _SaveUrl;

  public string GetSessionId() => GetSessionIdAsync().Result;

  #endregion Public Methods

  #region Private Methods

  private async Task<string> GetSessionIdAsync()
  {
    var res = await _Client.GetStringAsync("").ConfigureAwait(false);
    if(!string.IsNullOrWhiteSpace(res))
    {
      var result = JsonConvert.DeserializeObject<OneIncSessionResponse>(res);
      return result.ResponseCode == "Success" ? result.PortalOneSessionKey : null;
    }
    return null;
  }

  #endregion Private Methods
}

public class OneIncSessionResponse
{
  #region Public Properties
  public string PortalOneSessionKey { get; set; }
  public string ResponseCode { get; set; }
  public string ResponseMessage { get; set; }
  #endregion Public Properties
}
