using ConsumerServiceHost.Models;
using DiamondServices.Service;
using Falcon.Core.Models;

namespace ConsumerServiceHost.Apis;

public interface IESignApi
{
  #region Public Methods

  List<ESignItem> GetDocuSignItems(ISession session,Instance instance,Guid signerId);

  byte[] GetEsignedDocumentBytes(int policyId,int imageNum);

  HtmlContent GetESignResponse(ISession Session,Guid id,ILookup<string,string> lookup);

  string GetESignUrl(ISession session,Instance instance);

  #endregion Public Methods
}
