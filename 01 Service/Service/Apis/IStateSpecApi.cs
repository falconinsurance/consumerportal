using ConsumerServiceHost.Models;

namespace ConsumerServiceHost.Apis;

public interface IStateSpecApi
{
  #region Public Methods

  StateSpecifics GetStateSpecifics(En_State state,En_Language language,DateTime effectiveDate);

  #endregion Public Methods
}
