using ConsumerServiceHost.DependencyInjection;
using Falcon.Core.Settings;
using Microsoft.Extensions.Hosting;
using Microsoft.Owin.Hosting;

namespace ConsumerServiceHost;

public class ServicesHost:BackgroundService
{
  #region Private Fields

  private static ILog _Log;
  private readonly string _WebApiHost;

  #endregion Private Fields

  #region Public Constructors

  public ServicesHost()
  {
    Dependency.Register();
    Dependency.Init();
    _Log = AppResolver.GetLogger(nameof(ConsumerServiceHost));
    _WebApiHost = AppResolver.Get<ConfigSettings>().WebApiHost;
  }

  #endregion Public Constructors

  #region Protected Methods

  protected override Task ExecuteAsync(CancellationToken stoppingToken)
  {
    SetWebApiHost();
    _Log.Other(action: "Start");
    return Task.CompletedTask;
  }

  #endregion Protected Methods

  #region Private Methods

  private void SetWebApiHost()
  {
    IDisposable webApp = null;
    try
    {
      webApp = WebApp.Start<Startup>(_WebApiHost);
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex);
      webApp?.Dispose();
      Thread.Sleep(100);
      SetWebApiHost();
    }
  }

  #endregion Private Methods
}
