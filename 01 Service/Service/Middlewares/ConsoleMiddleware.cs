using Microsoft.Owin;

namespace ConsumerServiceHost.Middleware;

internal class MiddlewareConsole:OwinMiddleware
{
  #region Public Constructors

  public MiddlewareConsole(OwinMiddleware next) : base(next)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public override async Task Invoke(IOwinContext context)
  {
    Console.WriteLine(context.Request.Path.Value);
    await Next.Invoke(context).ConfigureAwait(false);
  }

  #endregion Public Methods
}
