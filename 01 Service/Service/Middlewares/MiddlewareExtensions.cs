namespace ConsumerServiceHost.Middleware;
using Owin;

public static class MiddlewareExtensions
{
  #region Internal Methods

  internal static IAppBuilder UseConsole(this IAppBuilder _IAppBuilder)
  {
    if(Environment.UserInteractive)
    {
      _IAppBuilder.Use<MiddlewareConsole>();
    }
    return _IAppBuilder;
  }

  #endregion Internal Methods
}
