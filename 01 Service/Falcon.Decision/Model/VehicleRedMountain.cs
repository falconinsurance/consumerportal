using DecRedMountain.Settings;

namespace Falcon.Decision;

public class VehicleRedMountain
{
  #region Public Properties
  public EnRedMountainScore Score { get; set; }
  public string Vin { get; set; }
  #endregion Public Properties

  #region Public Methods

  public override bool Equals(object obj) => obj is VehicleRedMountain vehicleRedMountain && Vin?.Equals(vehicleRedMountain.Vin,StringComparison.OrdinalIgnoreCase) == true;

  public override int GetHashCode() => Vin?.ToUpper().GetHashCode() ?? 0;

  #endregion Public Methods
}
