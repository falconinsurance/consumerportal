namespace Falcon.Decision;

internal static class FalconNoteStatic
{
  #region Private Fields

  private static readonly Dictionary<EnNote,DiaNote> Dic = new()
  {
    [EnNote.None] = new DiaNote { Id = 0,Title = "Not Available" },
    [EnNote.Decision] = new DiaNote { Id = 132,Title = "Decision Notes" },
    [EnNote.Warning] = new DiaNote { Id = 133,Title = "Warning" },
    [EnNote.FollowUp] = new DiaNote { Id = 0,Title = "Follow Up" },
    [EnNote.Validations] = new DiaNote { Id = 132,Title = "Falcon Validations" },
    [EnNote.AgencyBlock] = new DiaNote { Id = 132,Title = "Agency Block" },
    [EnNote.History] = new DiaNote { Id = 132,Title = "Falcon History" },
    [EnNote.Hugo] = new DiaNote { Id = 132,Title = "Hugo Results" },
    [EnNote.IsoRisk] = new DiaNote { Id = 133,Title = "IsoRisk Results" },
    [EnNote.CarFax] = new DiaNote { Id = 132,Title = "CarFax Results" },
    [EnNote.RedMountain] = new DiaNote { Id = 132,Title = "Red Mountain Results" },
    [EnNote.MVR] = new DiaNote { Id = 133,Title = "MVR Results" },
    [EnNote.APlus] = new DiaNote { Id = 133,Title = "APlus Results" },
    [EnNote.Guidewire] = new DiaNote { Id = 132,Title = "Guidewire Results" },
  };

  #endregion Private Fields

  #region Internal Methods

  internal static DiaNote GetDiaNote(this EnNote type) => Dic.TryGetValue(type,out DiaNote result) ? result : Dic[EnNote.None];

  internal static DiaNote GetTypeNote(EnNote type) => Dic.TryGetValue(type,out DiaNote result) ? result : Dic[EnNote.None];

  internal static FalconNote SetNote(this EnNote type,string value) => new() { _Type = type,_Value = value?.Trim() ?? "" };

  #endregion Internal Methods
}
