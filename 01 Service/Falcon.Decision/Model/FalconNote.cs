namespace Falcon.Decision;

public class FalconNote
{
  #region Public Fields
  public EnNote _Type;
  public string _Value;
  #endregion Public Fields

  #region Private Properties
  private string ValueCleaned => _Value?.Trim().ToUpper() ?? "";
  #endregion Private Properties

  #region Public Methods

  public FalconNote Clean()
  {
    _Value = _Value.Trim();
    return this;
  }

  public override bool Equals(object obj)
  {
    if(obj is FalconNote note)
    {
      return (note._Type, note.ValueCleaned).Equals((_Type, ValueCleaned));
    }

    return false;
  }

  public override int GetHashCode() => (_Type, _Value?.ToUpper()).GetHashCode();

  #endregion Public Methods
}
