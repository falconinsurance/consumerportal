using DecRedMountain.Settings;

namespace Falcon.Decision;

public class VehicleGuideWire
{
  #region Public Properties
  public EnGuideWireScore Score { get; set; }
  public string Vin { get; set; }
  #endregion Public Properties

  #region Public Methods

  public override bool Equals(object obj) => obj is VehicleGuideWire vehicleGuideWire && Vin?.Equals(vehicleGuideWire.Vin,StringComparison.OrdinalIgnoreCase) == true;

  public override int GetHashCode() => Vin?.ToUpper().GetHashCode() ?? 0;

  #endregion Public Methods
}
