namespace Falcon.Decision;

public class FalconModifier
{
  #region Public Properties
  public int DetailStatusCode { get; set; }
  public int ModifierGroupId { get; set; }
  public int ModifierLevelId { get; set; }
  public string ModifierOptionDescription { get; set; }
  public int ModifierOptionId { get; set; }
  public int ModifierTypeId { get; set; }
  public int ParentModifierTypeId { get; set; }
  #endregion Public Properties
}
