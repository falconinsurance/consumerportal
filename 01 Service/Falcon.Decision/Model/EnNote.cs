namespace Falcon.Decision;

public enum EnNote
{
  None = 0,
  Decision = 1,
  Warning = 2,
  FollowUp = 3,
  Validations = 4,
  AgencyBlock = 5,
  History = 6,
  Hugo = 7,
  IsoRisk = 8,
  CarFax = 9,
  RedMountain = 10,
  MVR = 11,
  APlus = 12,
  Guidewire = 13,
  Verisk_CVInfo = 14,
  Verisk_CVWarn = 15,
}
