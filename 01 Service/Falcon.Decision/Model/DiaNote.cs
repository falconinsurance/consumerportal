namespace Falcon.Decision;

public class DiaNote
{
  #region Public Properties
  public int Id { set; get; }
  public string Title { set; get; }
  #endregion Public Properties
}
