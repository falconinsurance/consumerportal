//using System;
//using System.Collections.Generic;
//using System.Linq;
//using AgencyApi.Objects;
//using DecIsoRisk.DB;
//using DecIsoRisk.Service;
//using DecIsoRisk.Table;
//using Falcon.AgencyServices.Core;
//using Falcon.Decision.Helper;
//using Falcon.Tools;
//using FalconConfig;
//using FalconConfig.Model;
//using Tools.Logger;
//using Lib = Diamond.C0057.Common.Library;

// namespace Falcon.Decision.Risks;

//  public partial class RiskIso:RiskCommon
//  {
//    #region Private Fields
//    private const EnPolicyCode PolicyCodeFilter = EnPolicyCode.FullCoverage;
//    private static readonly ILog _Log = App.GetLogger(nameof(RiskIso));
//    private static readonly CacheIso Cache = CacheIso.Me;
//    private readonly IIsoHandler _IsoHandler;
//    #endregion Private Fields

//    #region Public Constructors

//    public RiskIso()
//    {
//      _IsoHandler = AppResolver.Get<IIsoHandler>();
//    }

//    #endregion Public Constructors

//    #region Public Methods

//    //private  int? TestScore;
//    public override void Check()
//    {
//      try
//      {
//        var fromCache = true;
//        var photoModel = EnPhotoModel.Default;
//        var setting = GetSetting();
//        _IsoHandler.DeleteItem(new IsoRisk { PolicyId = Local.PolicyId });
//        if(setting?.Skip != false)
//          return;

//        var request = ToIsoRisk();
//        var key = GetCacheKey(request);
//        var cacheContent = Cache.Get(key);
//        var response = cacheContent?.Response;
//        var parsedResponse = _IsoHandler.Parse(response);

//        if(parsedResponse.IsNull)
//        {
//          fromCache = false;
//          response = _IsoHandler.Call(request);
//          parsedResponse = _IsoHandler.Parse(response);
//        }
//        else if(cacheContent?.LocalResult != null)
//        {
//          LocalResult = cacheContent.LocalResult;
//        }
//        if(!_IsoHandler.IsProduction)
//          RunTestValue(request,parsedResponse);
//        if(!parsedResponse.IsNull)
//        {
//          var result = setting.GetResult(parsedResponse,Local.CutOffScore);
//          photoModel = result.PhotoModel;
//          var score = result.Score < Local.CutOffScore ? result.Score : 100;
//          LocalResult.Pass = result.Pass && score < 100;
//          LocalResult.Score = score;
//          SetNotes(result);
//        }
//        //Add/Update the cache
//        Cache[key] = new CacheContent { LocalResult = LocalResult,Response = response };
//        request.RequestHeader.Authentication = null;
//        SaveToDb(photoModel,setting,request,response,parsedResponse,key,fromCache);
//        UpdateDiamond();
//      }
//      catch(Exception ex)
//      {
//        _Log.Exception(ex: ex);
//      }
//      // Methods
//      SettingIso GetSetting()
//         => _IDecHandler.GetSettingCs<SettingIso>(ParamRisk.Create(policyCode: Local.PolicyCode & PolicyCodeFilter,riskLevel: Local.RiskLevel,state: Local.State));

//      void SetNotes(ClsResult result)
//      {
//        if(result.Pass)
//        {
//          switch(result.PhotoModel)
//          {
//            case EnPhotoModel.FollowUp:
//              LocalResult.AddMessage(diamondNote: EnNote.FollowUp.SetNote($"Risk: {Message.DiamondNote.PhotoFollowUp}"));
//              ParamDecision.PhotoModelAddWorkFlowItem(
//                  policyCode: Local.PolicyCode,
//                  policyId: Local.PolicyId,
//                  policyImgNum: Local.ImageNumber ,
//                  enRiskService: EnRiskService.IsoRisk
//                  );
//              break;

//            case EnPhotoModel.Warning:
//              LocalResult.AddMessage(diamondNote: EnNote.IsoRisk.SetNote($"Risk: {Message.DiamondNote.PhotoWarning}"));
//              break;

//            default:
//              ParamDecision.RemoveWorkFlowItem(policyId: Local.PolicyId,enRiskService: EnRiskService.IsoRisk);
//              break;
//          }
//        }
//        else
//        {
//          LocalResult.AddMessage(
//              diamondNote: EnNote.Decision.SetNote($"Risk: {Message.DiamondNote.FailHighScore}"),
//              portalError: Message.PortalError.DecisionError(Local));
//        }
//      }
//      void RunTestValue(RMSRequest request,ParsedIsoRisk parsedResponse)
//      {
//        var id = request.SubjectsSection[0].PersonName.FirstName;
//        parsedResponse.Score = _IsoHandler.SqlFirstOrDefault<int?>("Select Value From tblValueInt Where Id =@1;",c => c.AddValues(id)) ?? 1000;
//        //parsedResponse.IsNull = false;
//      }
//    }

//    #endregion Public Methods

//    #region Private Methods

//    private string GetCacheKey(RMSRequest request)
//         => request.SubjectsSection.Select(c => $"{c.PersonName.FirstName}-{c.PersonName.LastName}-{c.PersonDetail.DateOfBirth}-{Local.Territory}").First();

//    private IsoRisk.ClsNotes GetNotes()
//    {
//      if(LocalResult.Pass)
//        return default;
//      var tmp = new IsoRisk.ClsNotes();
//      tmp.AddRange(LocalResult.DiamondNotes.Select(c => $"{c.Type} : {c.Value}"));
//      return tmp;
//    }

//    private string GetRelationShip(AgencyApi.Enums.En_DriverRelationship aPIEnumValue) => aPIEnumValue switch
//    {
//      AgencyApi.Enums.En_DriverRelationship.Insured => "PR",
//      AgencyApi.Enums.En_DriverRelationship.Spouse => "SP",
//      AgencyApi.Enums.En_DriverRelationship.Brother_Sister => "SI",
//      AgencyApi.Enums.En_DriverRelationship .Child=> "CH",
//      AgencyApi.Enums.En_DriverRelationship.Parent => "PR",
//      _ => "OT",
//    };

//    private void SaveToDb(EnPhotoModel photoModel,SettingIso setting,RMSRequest request,Result<RMSResponse> response,ParsedIsoRisk parsedResponse,string key,bool fromCache)
//    {
//      if(!Local.ValidUser)
//        return;
//      var tblIsoRisk = GetIsoRisk();
//      var tblIsoRiskError = GetIsoRiskError();
//      var tblIsoRiskDetails = GetIsoRiskDetails();

//      return;

//      #region Methods

//      IsoRisk GetIsoRisk()
//      {
//        var table = default(IsoRisk);
//        try
//        {
//          table = new IsoRisk
//          {
//            AgencyId = Local.AgencyId,
//            Environment = _IsoHandler.IsProduction ? EnEnvironment.Production : EnEnvironment.Test,
//            FalconScore = LocalResult.Score,
//            FromCache = fromCache,
//            IsoKey = key,
//            Notes = GetNotes(),
//            Pass = LocalResult.Pass,
//            PhotoModel = photoModel,
//            PolicyCode = Local.PolicyCode,
//            PolicyId = Local.PolicyId,
//            Request = request,
//            Response = response.Value,
//            Risk = setting.Risk,
//            RiskLevel = setting.RiskLevel,
//            Score = parsedResponse.Score,
//            Status = $"{(_IsoHandler.IsProduction ? EnEnvironment.Production : EnEnvironment.Test)}",
//            UserId = Local.UserId,
//            Ver = setting.Ver,
//          };
//          if(table.PolicyId > 0)
//          {
//            _IsoHandler.Add(table);
//          }
//          else
//          {
//            Local.UpdatePolicyIdAdd(policyId =>
//            {
//              table.PolicyId = policyId;
//              _IsoHandler.Add(table);
//            });
//          }
//        }
//        catch(Exception ex)
//        {
//          throw _Log.Exception(obj: table,ex: ex,msg: "DbDecIsoRisk.Add(IsoRisk); See object");
//        }
//        return table;
//      }
//      IsoRiskDetails GetIsoRiskDetails()
//      {
//        var table = default(IsoRiskDetails);
//        if(parsedResponse.IsNull)
//          return table;
//        var reasonCodes = response?.Value?.ScoreDetails?.SelectMany(c => c.ReasonCodeDetails?.Select(x => x.ReasonCode)).Distinct().ToArray();
//        try
//        {
//          table = new IsoRiskDetails
//          {
//            Id = tblIsoRisk.Id,
//            PolicyId = Local.PolicyId,
//            AgencyId = Local.AgencyId,
//            IsoScore = parsedResponse.Score,
//            Id01 = GetReasonCode("ID-1.01"),
//            Id02 = GetReasonCode("ID-1.02"),
//            Id03 = GetReasonCode("ID-1.03"),
//            Id04 = GetReasonCode("ID-1.04"),
//            Id05 = GetReasonCode("ID-1.05"),
//            Id06 = GetReasonCode("ID-1.06"),
//            Id07 = GetReasonCode("ID-1.07"),
//            Id08 = GetReasonCode("ID-1.08"),
//            Id09 = GetReasonCode("ID-1.09"),
//            Id10 = GetReasonCode("ID-1.10"),
//            Id11 = GetReasonCode("ID-1.11"),
//            Id12 = GetReasonCode("ID-1.12"),
//            Id13 = GetReasonCode("ID-1.13"),
//            Id14 = GetReasonCode("ID-1.14"),
//            Id15 = GetReasonCode("ID-1.15"),
//            Id16 = GetReasonCode("ID-1.16"),
//            Id17 = GetReasonCode("ID-1.17"),
//            Id18 = GetReasonCode("ID-1.18"),
//            Id19 = GetReasonCode("ID-1.19"),
//            Id20 = GetReasonCode("ID-1.20"),
//            Id21 = GetReasonCode("ID-1.21"),
//            Id22 = GetReasonCode("ID-1.22"),
//            Id23 = GetReasonCode("ID-1.23"),
//            Id24 = GetReasonCode("ID-1.24"),
//            Id25 = GetReasonCode("ID-1.25"),
//            Id26 = GetReasonCode("ID-1.26"),
//            Id27 = GetReasonCode("ID-1.27"),
//            Id28 = GetReasonCode("ID-1.28"),
//            Id29 = GetReasonCode("ID-1.29"),
//            Id30 = GetReasonCode("ID-1.30"),
//            Id31 = GetReasonCode("ID-1.31"),
//            Id32 = GetReasonCode("ID-1.32"),
//            Id33 = GetReasonCode("ID-1.33"),
//            Id34 = GetReasonCode("ID-1.34"),
//            Id35 = GetReasonCode("ID-1.35"),
//            Id36 = GetReasonCode("ID-1.36"),
//            Id37 = GetReasonCode("ID-1.37"),
//            Id38 = GetReasonCode("ID-1.38"),
//            Id39 = GetReasonCode("ID-1.39"),
//            Ag01 = GetReasonCode("AG-2.01"),
//            Ag02 = GetReasonCode("AG-2.02"),
//            Ag03 = GetReasonCode("AG-2.03"),
//            Ag04 = GetReasonCode("AG-2.04"),
//            Ag05 = GetReasonCode("AG-2.05"),
//            Ag06 = GetReasonCode("AG-2.06"),
//            Ag07 = GetReasonCode("AG-2.07"),
//            Ag08 = GetReasonCode("AG-2.08"),
//            Ag09 = GetReasonCode("AG-2.09"),
//            Ag10 = GetReasonCode("AG-2.10"),
//            Ag11 = GetReasonCode("AG-2.11"),
//            Ag12 = GetReasonCode("AG-2.12"),
//            Ag13 = GetReasonCode("AG-2.13"),
//            Ag14 = GetReasonCode("AG-2.14"),
//            Ag15 = GetReasonCode("AG-2.15"),
//            Ag16 = GetReasonCode("AG-2.16"),
//            Ag17 = GetReasonCode("AG-2.17"),
//            Ag18 = GetReasonCode("AG-2.18"),
//            Ag19 = GetReasonCode("AG-2.19"),
//            Ag20 = GetReasonCode("AG-2.20"),
//            Ag21 = GetReasonCode("AG-2.21"),
//            Ag22 = GetReasonCode("AG-2.22"),
//            Ag23 = GetReasonCode("AG-2.23"),
//            Ag24 = GetReasonCode("AG-2.24"),
//            Ds01 = GetReasonCode("DS-3.01"),
//            Ds02 = GetReasonCode("DS-3.02"),
//            Ds03 = GetReasonCode("DS-3.03"),
//            Ds04 = GetReasonCode("DS-3.04"),
//            Ds05 = GetReasonCode("DS-3.05"),
//            Ds06 = GetReasonCode("DS-3.06"),
//            Ds07 = GetReasonCode("DS-3.07"),
//            Ds08 = GetReasonCode("DS-3.08"),
//            Ds09 = GetReasonCode("DS-3.09")
//          };
//          if(table.PolicyId > 0)
//          {
//            _IsoHandler.Add(table);
//          }
//          else
//          {
//            Local.UpdatePolicyIdAdd(policyId =>
//            {
//              table.PolicyId = policyId;
//              _IsoHandler.Add(table);
//            });
//          }
//        }
//        catch(Exception ex)
//        {
//          throw _Log.Exception(obj: table,ex: ex,msg: "DbDecIsoRisk.Add(IsoRiskDetails); See object");
//        }
//        return table;
//        //Methods
//        bool GetReasonCode(string e) => reasonCodes?.Any(c => c == e) == true;
//      }
//      IsoRiskError GetIsoRiskError()
//      {
//        var table = default(IsoRiskError);
//        if(!parsedResponse.IsNull)
//          return table;
//        try
//        {
//          table = new IsoRiskError
//          {
//            AgencyId = tblIsoRisk.AgencyId ?? 0,
//            Environment = tblIsoRisk.Environment,
//            PolicyId = tblIsoRisk.PolicyId ?? 0,
//            UserId = tblIsoRisk.UserId ?? 0
//          };
//          if(table.PolicyId > 0)
//          {
//            _IsoHandler.Add(table);
//          }
//          else
//          {
//            Local.UpdatePolicyIdAdd(policyId =>
//            {
//              table.PolicyId = policyId;
//              _IsoHandler.Add(table);
//            });
//          }
//        }
//        catch(Exception ex)
//        {
//          throw _Log.Exception(obj: table,ex: ex,msg: "DbDecIsoRisk.Add(IsoRiskError); See object");
//        }
//        return table;
//      }

//      #endregion Methods
//    }

//    private RMSRequest ToIsoRisk()
//    {
//      var subject = default(SubjectDetails);
//      var addresses = default(List<Address>);
//      string relationship = null;
//      var subjectNumber = 0;
//      var hasPrimary = false;
//      var timeStamp = DateTime.Now;
//      var request = new RMSRequest
//      {
//        SchemaVersion = "1.0",
//        RequestHeader = new RequestHeader
//        {
//          CustomerReferenceNumber = $"{timeStamp:yyMMddHHmmssfff}-{Local.AppInstance.AgencyCode}-{Local.DiamondImage.Policy.PolicyId}",
//          RequestId = 1010,
//          InovatusPreviousTranId = "",
//          Timestamp = timeStamp.ToString()
//        },
//        SubjectsSection = new List<SubjectDetails>()
//      };

//      var subjectsSection = request.SubjectsSection;
//      var mailingAddress = GetAddress(address: Local.AppInstance.MailingAddress,addressId: EnAddressId.Mailing);
//      var garageAddress = GetAddress(address: Local.AppInstance.GarageAddress,addressId: EnAddressId.Current);
//      if(garageAddress is null && mailingAddress != null)
//      {
//        mailingAddress.AddressIdVal = EnAddressId.Current;
//      }
//      Local.AppInstance.Drivers.ForEach(action: d =>
//      {
//        subjectNumber++;
//        subject = new SubjectDetails();
//        addresses = new List<Address>();
//        garageAddress.NotNull(x => addresses.Add(x));
//        mailingAddress.NotNull(x => addresses.Add(x));
//        subject.Addresses = addresses;
//        subject.PersonDetail = new PersonalDetails
//        {
//          Age = GetAge(d.DateOfBirth),
//          DateOfBirthVal = d.DateOfBirth,
//        };
//        subject.PersonName = new PersonName
//        {
//          FirstName = ExtHelper.CleanName(d.FirstName),
//          LastName = ExtHelper.CleanName(d.LastName),
//          MiddleName = ExtHelper.CleanName(d.MiddleName),
//        };
//        relationship = GetRelationShip(d.Relationship);
//        if(hasPrimary && "PR" == relationship)
//        {
//          relationship = "OT";
//        }
//        subject.RelationshipToInsured = relationship;
//        if(!hasPrimary && "PR" == relationship)
//        {
//          hasPrimary = true;
//        }
//        subject.SubjectNumber = subjectNumber.ToString();
//        subjectsSection.Add(subject);
//      });
//      var primaryIndex = subjectsSection.IndexOf(subjectsSection.Find(c => c.RelationshipToInsured == "PR"));
//      if(primaryIndex == -1 && subjectsSection?.Count > 0)
//      {
//        subjectsSection[0].RelationshipToInsured = "PR";
//      }
//      if(primaryIndex > 0)
//      {
//        var item = subjectsSection[primaryIndex];
//        subjectsSection.RemoveAt(primaryIndex);
//        subjectsSection.Insert(0,item);
//        subjectNumber = 0;
//        foreach(var subjectItem in subjectsSection)
//        {
//          subjectNumber++;
//          subjectItem.SubjectNumber = subjectNumber.ToString();
//        }
//      }
//      return request;
//      //methods
//      static int? GetAge(DateTime date)
//      {
//        DateTime dob = date;
//        var today = DateTime.Today;
//        var age = today.Year - dob.Year;
//        if(dob.AddYears(age) > today)
//          age--;
//        return age;
//      }

//      static Address GetAddress(StreetAddress address,EnAddressId addressId)
//      {
//        Address add = null;
//        if(address?.Street1.IsNullOrWhiteSpace() == false)
//        {
//          add = new Address
//          {
//            AddressIdVal = addressId,
//            AddressLine1 = ExtHelper.TrimNull(address.Street1),
//            AddressLine2 = ExtHelper.TrimNull(address.Street2),
//            City = address.City,
//            State = address.State.ToString(),
//            Zip5 = address.ZipCode,
//          };
//        }
//        return add;
//      }
//    }

//    private void UpdateDiamond()
//    {
//      const int riskLevelTypeId = (int)Lib.Enumerations.ModifierTypeId.ISORiskVerification;
//      var pass = LocalResult.Pass;
//      var modifiers = Local.DiamondImage.LOB.PolicyLevel.Modifiers;
//      if(!modifiers.Any(m => m.ModifierTypeId == riskLevelTypeId))
//      {
//        modifiers.Add(Lib.Utility.CreateModifier(riskLevelTypeId,Local.VersionData));
//      }
//      var modifier = modifiers.First(m => m.ModifierTypeId == riskLevelTypeId);
//      modifier.ModifierOptionId = (int)(pass ? Lib.Enumerations.ModifierOptionId.Pass : Lib.Enumerations.ModifierOptionId.Fail);
//    }

//    #endregion Private Methods
//  }
