//using System;
//using System.Linq;
//using DecIsoRisk.DB;
//using DecIsoRisk.Table;
//using Falcon.Tools;

// namespace Falcon.Decision.Risks;

//  public partial class RiskIso
//  {
//    #region Public Classes

//    public class CacheIso:CacheTimedDb<CacheIso,string,CacheContent>
//    {
//      #region Private Fields
//      private readonly IIsoHandler _IIsoHandler = AppResolver.Get<IIsoHandler>();
//      #endregion Private Fields

//      #region Public Methods

//      public override void Init()
//      {
//        var sqlServer = _IDecHandler.GetServer(EnDatabase.DecIsoRisk);
//        ItemDuration = sqlServer.Duration;
//        DbDelete = LocalDbDelete;
//        DbInsert = LocalDbInsert;
//        DbSelect = LocalDbSelect;
//        DbUpdate = LocalDbUpdate;
//      }

//      #endregion Public Methods

//      #region Private Methods

//      private void LocalDbDelete(string key) => _IIsoHandler.Delete(new CacheStr { Id = key?.Trim().ToUpper() });

//      private void LocalDbInsert(string key,TimedTValue value)
//      {
//        if(key is null)
//          return;
//        var tbl = new CacheStr
//        {
//          Duration = (int)value.Duration.TotalMinutes,
//          Id = key.Trim().ToUpper(),
//        };
//        tbl.SetJson(value.Value);
//        _IIsoHandler.Add(tbl);
//      }

//      private TimedTValue LocalDbSelect(string key)
//      {
//        var result = default(TimedTValue);
//        if(key is null)
//          return result;
//        var tbl = _IIsoHandler.Use(db => db.Set<CacheStr>().FirstOrDefault(c => c.Id == key.Trim().ToUpper()));
//        if(tbl != null)
//        {
//          return new TimedTValue
//          {
//            Duration = TimeSpan.FromMinutes(tbl.Duration),
//            Value = tbl.GetJson<CacheContent>()
//          };
//        }
//        return result;
//      }

//      private void LocalDbUpdate(string key,TimedTValue value)
//      {
//        if(key is null)
//          return;
//        var tbl = new CacheStr
//        {
//          Duration = (int)value.Duration.TotalMinutes,
//          Id = key.ToUpper(),
//        };
//        tbl.SetJson(value.Value);
//        _IIsoHandler.Update(tbl);
//      }

//      #endregion Private Methods
//    }
//    #endregion Public Classes
//  }
