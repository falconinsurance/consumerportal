namespace Falcon.Decision.Risks
{
  public class ViolationInstance
  {
    public En_Violation Violation { get; internal set; }
    public DateTime Date { get; internal set; }
  }
}
