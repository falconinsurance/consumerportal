using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using DecRedMountain.DB;
using DecRedMountain.GwService;
using DecRedMountain.Model;
using DecRedMountain.RmService;
using DecRedMountain.Settings;
using Diamond.Common.Enums;
using Diamond.Common.Objects.Policy;
using Falcon.Decision.Helper;
using Falcon.Tools.VehicleInfoCache;
using static Falcon.Decision.ParamDecision;
using DiaVehicle = Diamond.Common.Objects.Policy.Vehicle;
using Lib = Diamond.C0057.Common.Library;

using LibCo = Diamond.Common.Enums;

namespace Falcon.Decision.Risks;

public class RiskRedMountain:RiskCommon
{
  #region Private Fields

  private static readonly IRedMountainHandler _IRedMountainHandler = AppResolver.Get<IRedMountainHandler>();
  private static readonly ILog _Log = AppResolver.Get<ILogHandler>().GetLogger(nameof(RiskRedMountain));
  private static readonly IVehicleInfoCache _VehicleInfoCache = AppResolver.Get<IVehicleInfoCache>();
  private GwRequest _GuideWireRequest;
  private bool _RunGuideWire;
  private bool _RunRedMountain;
  private SettingGuideWire _SettingGw;
  private SettingRedMountain _SettingRm;

  #endregion Private Fields

  #region Private Properties

  private HashSet<VehicleGuideWire> SetGuideWire { get; } = [];
  private HashSet<VehicleRedMountain> SetRedMountain { get; } = [];

  #endregion Private Properties

  #region Public Methods

  public override void Check()
  {
    try
    {
      var vehicles = GetVehicles()?.Where(c => c.Vehicle is not null).ToArray() ?? Array.Empty<ClsVehicle>();

      _SettingRm = GetSettingRm();
      _RunRedMountain = !_SettingRm.Skip;
      _SettingGw = GetSettingGw();
      _RunGuideWire = !_SettingGw.Skip;

      if(Local.IsNonOwner)
      {
        UpdateDiamond();
        return;
      }

      // Start fresh
      if(Local.IsEndorsement)
      {
        ParamDecision.RemoveWorkFlowItem(policyId: Local.PolicyId,enRiskService: EnRiskService.RedMountain,policyCode: Local.PolicyCode);
      }

      if(_SettingRm.Skip && _SettingGw.Skip)
        return;
      if(_RunGuideWire)
      {
        _GuideWireRequest = BuildGwRequestCommon();
      }
      // Assign all Record as Deleted if exists
      var score = 0;
      foreach(var vehicle in vehicles)
      {
        var guid = Tools.DBHandler.GuidTools.GetGuid();
        var resultRm = SettingRedMountain.NoScore;

        if(_RunRedMountain)
        {
          try
          {
            resultRm = CheckVehicleRM(vehicle,guid);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
        if(_RunGuideWire && vehicle.Vin10?.Length > 9)
        {
          try
          {
            _ = CheckVehicleGW(vehicle,resultRm,guid);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
        if(!Local.IsEndorsement)
        {
          score = resultRm.FalconScore < Local.CutOffScore ? resultRm.FalconScore : 100;
          if(score > 99)
          {
            LocalResult.Pass = false;
            LocalResult.Score = 100;
            SetNotes(LocalResult);
            break;
          }
        }
      }
      UpdateDiamond();
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex,obj: Local);
    }
    //Methods
    void SetNotes(DecisionResult result)
    {
      if(!result.Pass)
      {
        LocalResult.AddMessage(
            diamondNote: EnNote.RedMountain.SetNote($"Risk: {Message.DiamondNote.FailHighScore}"),
            portalError: Message.PortalError.DecisionError(Local,EnRiskService.RedMountain));
      }
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static int GetVehicleAge(ClsVehicle vehicleInformation)
  {
    var age = vehicleInformation.Vehicle?.Year ?? 0;
    return age > 0 ? age : VinHelper.GetModelYear(vehicleInformation.Vin10);
  }

  private T AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    return data;

    async Task AddAsync()
    {
      try
      {
        await _IRedMountainHandler.AddAsync(data);
      }
      catch(Exception ex)
      {
        throw _Log.Exception(obj: data,msg: $"_IRedMountainHandler.Add({typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private GwRequest BuildGwRequest(ClsVehicle vehicle,SettingRedMountain.RowResult resultRm) =>
    _GuideWireRequest.Clone().With(c =>
      {
        c.Input.RedMountainScore = resultRm.RMScore.ToInt();
        c.Input.BodyTypeId = vehicle.BodyType?.Id ?? (_VehicleInfoCache.TryGetBodyTypeForVin(vehicle.Vin,out var value) ? value.ToString() : "24");
        c.Input.CovHasCollision = vehicle.ComColDeductible > 0;
        c.SetRecordId(Local.PolicyId,Local.PolicyEffectiveDate,Local.ImageNumber,Local.ImageEffectiveDate,vehicle.Id);
      });

  private GwRequest BuildGwRequestCommon()
  {
    var driverCount = Local.Drivers.Length;
    var driverInsuredCount = Local.DriversInsured.Length;
    var todayYear = DateTime.Today.Year;
    var vehiclesOldestAge = todayYear - Local.Vehicles.Min(GetVehicleAge);
    var vehiclesYoungstAge = todayYear - Local.Vehicles.Max(GetVehicleAge);

    return new()
    {
      Input = new()
      {
        DriverCount = driverCount,
        ExcludedDriverCount = driverCount - driverInsuredCount,
        LiabilityOnly = Local.IsLiability,
        MarriedDriverCount = Local.DriversInsured.Count(c => c.MaritalStatus == En_MaritalStatus.Married),
        OldestVehicleYears = vehiclesOldestAge,
        PolicyTenureMonths = Local.Transaction == EnPolicyTransaction.NewPolicy ? 0 : _IRedMountainHandler.GetPolicyTenure(Local.PolicyId),
        PolicyTerm = $"{(int)Local.TermLength} Month",
        QuoteSource = Local.QuoteSource,
        RenewalVersion = Local.DiamondImage.RenewalVersion,
        StateCode = Local.State.ToString(),
        TransactionType = Local.Transaction switch
        {
          EnPolicyTransaction.NewPolicy => "New Business",
          EnPolicyTransaction.RenewPolicy => "Renewal",
          _ => Local.Transaction.ToString(),
        },
        TransferDiscount = GetTransferDiscount(),
        UnlicensedDriverCount = Local.DriversInsured.Count(c => !c.IsLicense),
        VehicleCount = Local.Vehicles.Length,
        VehicleTenureMonths = 0,//TODO: Fix it Renewal only
        YoungestVehicleYears = vehiclesYoungstAge,
        //IL Model
        PolicyBillTo = GetPolicyBillTo(),
        DriverMaleCount = GetGenderCount(1),
        DriverFemaleCount = GetGenderCount(2),
        CovMedLimitAmount = GetCovMedLimitAmount(),
        DriverPointsMax = GetDriverPointsMax(),
      }
    };
    //int GetDriverPointsMax() => Local.DiamondImage.LOB.RiskLevel.Drivers.Where(x => !x.ExcludedDriver).Max(d => d.Points);

    int GetDriverPointsMax()
    {
      // Get a better default w/o going to Diamond in IL for Rate Call 1
      if(Local.State == En_State.IL)
      {
        switch(Local.RiskCheck)
        {
          case EnRiskCheck.AgencyAppXmlEZLynx:
          case EnRiskCheck.AgencyAppXmlITC:
          case EnRiskCheck.AgencyAppXmlQuickQuote:
          case EnRiskCheck.AgencyAppXmlQuomation:
          case EnRiskCheck.AgencyAppLvl0:
            if(Local.DiamondImage.LOB.RiskLevel.Drivers.Any(v => v.AccidentViolations.Any(x => x.ConvictionDate > DateTime.Now.AddYears(-3)
             && (x.AccidentsViolationsTypeId.In(En_Violation.AtFaultAccident.ToInt(),En_Violation.DisplayingAlteredDL.ToInt(),En_Violation.DrivingWithDLSuspended.ToInt(),
              En_Violation.DrivingWithExpiredDL.ToInt(),En_Violation.DUI.ToInt(),En_Violation.FailureToYield.ToInt(),En_Violation.FalseAccidentReport.ToInt(),
              En_Violation.Fleeing.ToInt(),En_Violation.HitAndRun.ToInt(),En_Violation.MotorVehicleFelony.ToInt(),En_Violation.NegligentHomicide.ToInt(),
              En_Violation.NoDL.ToInt(),En_Violation.OpenContainer.ToInt(),En_Violation.Racing.ToInt(),En_Violation.RecklessDriving.ToInt(),En_Violation.RefusalOfAlcoholTest.ToInt(),
              En_Violation.Speeding_15p.ToInt(),En_Violation.Texting.ToInt(),En_Violation.UnlawfulUseOfDriversLicense.ToInt(),En_Violation.ViolatingDLRestriction.ToInt()))
             && x.DetailStatusCode != (int)StatusCode.Deleted)))
              return 1; // Any number > zero finding any violation typed as above within the last 3 years

            // "All other moving violations" require at least 2 to trigger. Returning 1 vs 2 doesn't
            // really matter, as long as it is non-zero and non-negative.
            var otherViolations = 0;
            foreach(var driver in Local.DiamondImage.LOB.RiskLevel.Drivers.ToList())
            {
              otherViolations += driver.AccidentViolations.Count(x => x.ConvictionDate > DateTime.Now.AddYears(-3) && x.AccidentsViolationsTypeId.In(En_Violation.Other.ToInt()) && x.DetailStatusCode != (int)StatusCode.Deleted);
            }
            if(otherViolations > 1)
              return 2;

            break;
        }
      }

      return Local.DiamondImage.LOB.RiskLevel.Drivers.Where(x => !x.ExcludedDriver && x.DetailStatusCode == 1).Max(d => d.Points);
    }

    int GetCovMedLimitAmount()
    {
      var covId = Local.DiamondImage.LOB.RiskLevel.Vehicles.FirstOrDefault()?.Coverages.FirstOrDefault(x => x.CoverageCodeId == 6 && x.DetailStatusCode == 1)?.CoverageLimitId ?? -1;
      return covId switch
      {
        11 => 1000,
        12 => 2000,
        15 => 5000,
        166 => 500,
        _ => 0,
      };
    }

    int GetGenderCount(int sexId) => Local.DiamondImage.LOB.RiskLevel.Drivers.Count(x => x.Name.SexId == sexId && x.DriverExcludeTypeId != 3 && x.DetailStatusCode == 1);

    string GetPolicyBillTo() => Local.DiamondImage.Policy.Account.BillMethodId == 2 ? "DirectBill" : "AgencyBill";
    bool GetTransferDiscount()
    {
      var expdate = Local.DiamondImage?.LOB?.PolicyLevel?.PriorCarrier?.PriorExpirationDate;
      if(expdate is null)
        return false;
      return (Local.PolicyEffectiveDate - (DateTime)expdate).TotalDays <= (Local.State.In(En_State.IL) ? 20 : 30);
    }
  }

  private SettingGuideWire.RowResult CheckVehicleGW(ClsVehicle vehicle,SettingRedMountain.RowResult resultRm,Guid guid)
  {
    var req = BuildGwRequest(vehicle,resultRm);
    var value = _IRedMountainHandler.CallGw(req) ?? GwResponse._Zero;
    var temp = new GwTemResult() { Response = value,Request = req,LossRaioEstimate = value.Output.LossRatioEstimate,Score = value.GetScore() ?? -1 };
    var settingResult = _SettingGw.GetResult(value.GetScore());

    // Only Check Pass/Fail logic on NBS
    if(Local.IsNewBusiness && !settingResult.Pass)
    {
      LocalResult.Pass = false;
      LocalResult.Score = 100;
      LocalResult.AddMessage(diamondNote: EnNote.Guidewire.SetNote($"Guidewire: {Message.DiamondNote.FailHighScore}"),
        portalError: Message.PortalError.DecisionError(Local));
    }

    temp.GwScore = settingResult.GWScore;
    temp.PhotoModel = settingResult.PhotoModel;

    SaveToGw(vehicle,temp,guid);

    return settingResult;
  }

  private SettingRedMountain.RowResult CheckVehicleRM(ClsVehicle vehicle,Guid guid)
  {
    // Red Mountain errors on lower case letters in VIN
    var vin = vehicle.Vin10.ToUpper();
    var isValidVin = VehicleVin.IsValid(vin);
    var value = isValidVin ? _IRedMountainHandler.CallRm(new() { Model = ((EnState)Local.State).ToModel(),Vin = vin }) : RmResponse._Zero;
    var cacheContent = new RmTemResult() { Response = value,Score = value.GetScore() ?? -1 };
    var settingResult = _SettingRm.GetResult(value.GetScore());
    if(!settingResult.Pass)
    {
      LocalResult.Pass = false;
      LocalResult.Score = 100;
      LocalResult.AddMessage(diamondNote: EnNote.RedMountain.SetNote($"RedMountain: {Message.DiamondNote.FailHighScore} Vehicle {vehicle?.Vehicle?.Year} {vehicle?.Vehicle?.Make} {vehicle?.Vehicle?.Model} {vin} is unacceptable for Comp/Col coverage."),
        portalError: Message.PortalError.DecisionError(Local));
    }
    if(vehicle.IsFullCoverage && !Local.IsEndorsementQuoteOnly)
    {
      switch(Local.State)
      {
        case En_State.IN:
        case En_State.AZ:
        case En_State.OK:
        case En_State.TX:
        case En_State.UT:
        case En_State.CO:
          if(settingResult.PhotoModel == EnPhotoModel.FollowUp)
          {
            CreateTask(vehicle);
          }
          break;
      }
    }
    else
    {
      //if policy has full coverage don't remove the workflow item.
      if((Local.PolicyCode & EnPolicyCode.FullCoverage) != EnPolicyCode.FullCoverage)
      {
        ParamDecision.RemoveWorkFlowItem(policyId: Local.PolicyId,enRiskService: EnRiskService.RedMountain,policyCode: Local.PolicyCode);
      }
    }

    cacheContent.RMScore = settingResult.RMScore;
    cacheContent.PhotoModel = settingResult.PhotoModel;

    SaveToRm(vehicle,cacheContent,guid);

    return settingResult;
  }

  private void CreateTask(ClsVehicle vehicle)
  {
    var source = Local.RiskCheck switch
    {
      EnRiskCheck.AgencyAppLvl0 => "QuoteInfo",
      EnRiskCheck.AgencyAppLvl1 => "Application",
      EnRiskCheck.AgencyAppEndorsement => "Endorsement",
      EnRiskCheck.AgencyAppXmlITC => "ITC",
      EnRiskCheck.AgencyAppXmlQuickQuote => "QuickQuote",
      EnRiskCheck.AgencyAppXmlQuomation => "Quomation",
      EnRiskCheck.AgencyAppXmlEZLynx => "EZLynx",
      _ => "Other"
    };

    LocalResult.AddMessage(
      diamondNote: EnNote.FollowUp.SetNote($"RedMountain: [{source}] - {vehicle.Year} {vehicle.Make?.Description ?? "No Make"} {vehicle.Model?.Description ?? "No Model"} - {vehicle.Vin ?? "No VIN"} - {Message.DiamondNote.PhotoFollowUp}"),
      portalWarning: Message.WorkflowRemark.PhotosRequired + " Thank you.");

    ParamDecision.PhotoModelAddWorkFlowItem(
           policyCode: Local.PolicyCode,
           policyId: Local.PolicyId,
           policyImgNum: Local.ImageNumber,
           enRiskService: EnRiskService.RedMountain);
  }

  private GwData GetDataGw(ClsVehicle vehicle,SettingGuideWire setting,GwTemResult temp)
     => new()
     {
       AgencyId = Local.AgencyId,
       Deleted = false,
       Environment = _IRedMountainHandler.IsProduction ? EnEnvironment.Production : EnEnvironment.Test,
       FalconScore = temp?.GwScore ?? EnGuideWireScore.NoScore,
       LossRaioEstimate = temp?.LossRaioEstimate ?? 0,
       Pass = true,
       PhotoModel = temp?.PhotoModel ?? EnPhotoModel.Default,
       PolicyCode = vehicle.IsFullCoverage ? EnPolicyCode.FullCoverage : EnPolicyCode.Default,
       PolicyId = Local.PolicyId,
       Request = temp?.Request,
       Response = temp?.Response,
       Risk = setting.Risk,
       RiskLevel = Local.RiskLevel,
       Score = temp?.Score ?? 0,
       State = (EnState)Local.State,
       TransactionId = Local.Transaction,
       UserId = Local.UserId,
       Ver = setting.Ver,
       Vin = vehicle.Vin10
     };

  private RmData GetDataRm(ClsVehicle vehicle,SettingRedMountain setting,RmTemResult temp,bool fromCache)
     => new()
     {
       AgencyId = Local.AgencyId,
       Deleted = false,
       Environment = _IRedMountainHandler.IsProduction ? EnEnvironment.Production : EnEnvironment.Test,
       FalconScore = temp?.RMScore ?? EnRedMountainScore.NoScore,
       FromCache = fromCache,
       Pass = true,
       PhotoModel = temp?.PhotoModel ?? EnPhotoModel.Default,
       PolicyCode = vehicle.IsFullCoverage ? EnPolicyCode.FullCoverage : EnPolicyCode.Default,
       PolicyId = Local.PolicyId,
       ResponseJson = temp?.Response,
       Risk = setting.Risk,
       RiskLevel = Local.RiskLevel,
       Score = temp?.Score ?? 0,
       State = (EnState)Local.State,
       TransactionId = Local.Transaction,
       Type = ((EnState)Local.State).ToModel(),
       UserId = Local.UserId,
       Ver = setting.Ver,
       Vin = vehicle.Vin10
     };

  private SettingGuideWire GetSettingGw() => _IDecHandler.GetSettingCs<SettingGuideWire>(new ParamRisk { PolicyCode = Local.PolicyCode & EnPolicyCode.FullCoverage,RiskLevel = Local.RiskLevel,State = (EnState)Local.State });

  private SettingRedMountain GetSettingRm()
          => _IDecHandler.GetSettingCs<SettingRedMountain>(
        new ParamRisk { RiskLevel = Local.RiskLevel,State = (EnState)Local.State,PolicyCode = Local.PolicyCode }
        .With(c => c.PolicyCode &= Local.State.In(En_State.TX) ? (EnPolicyCode.FullCoverage | EnPolicyCode.Endorsement) : EnPolicyCode.FullCoverage));

  private ClsVehicle[] GetVehicles()
  {
    // Endorsements for Standard Policies
    if(Local.Transaction == EnPolicyTransaction.Endorsement && Local.ExistingParamDecision != null)
    {
      var vehiclesNew = Local?.Vehicles.ToList();
      var vehiclesOld = Local.ExistingParamDecision?.Vehicles;
      if(vehiclesOld != null) // Non-Owner converting to Standard
      {
        foreach(var item in vehiclesOld)
          vehiclesNew.Remove(vehiclesNew.Find(c => c.Vin10 == item.Vin10 && c.IsFullCoverage == item.IsFullCoverage));
      }
      return vehiclesNew.ToArray();
    }

    return Local?.Vehicles;
  }

  private void SaveToGw(ClsVehicle vehicle,GwTemResult temp,Guid guid)
  {
    if(!Local.ValidUser)
      return;
    _Log.Wrap(_ => SetAddGw(vin: vehicle.Vin10,score: temp?.GwScore ?? EnGuideWireScore.NoScore),args: new[] { vehicle });
    var data = GetDataGw(vehicle: vehicle,setting: _SettingGw,temp: temp);
    data.Id = guid;
    GwError error = null;
    try
    {
      if(data.PolicyId > 0)
      {
        AddToTables();
      }
      else
      {
        Local.UpdatePolicyIdAdd(policyId =>
        {
          data.PolicyId = policyId;
          AddToTables();
        });
      }
    }
    catch(Exception ex)
    {
      _Log.Error(obj: data,ex: ex,msg: "RedMountain.Add(tblGuideWire); See object");
    }

    #region Methods

    void AddToTables()
    {
      AddWithPolicyId(data);
      if(data.FalconScore == EnGuideWireScore.ServiceOff)
      {
        error = new GwError
        {
          AgencyId = data.AgencyId,
          Environment = data.Environment,
          Id = data.Id,
          PolicyId = data.PolicyId,
          UserId = data.UserId,
          Vin = data.Vin
        };
        AddWithPolicyId(error);
      }
    }

    #endregion Methods
  }

  private void SaveToRm(ClsVehicle vehicle,RmTemResult temp,Guid guid)
  {
    if(!Local.ValidUser)
      return;
    SetAddRm(vin: vehicle.Vin10,score: temp?.RMScore ?? EnRedMountainScore.NoScore);
    var data = GetDataRm(vehicle: vehicle,setting: _SettingRm,temp: temp,fromCache: temp.Response?.FromCache ?? false);
    data.Id = guid;
    RmError error = null;
    try
    {
      if(data.PolicyId > 0)
      {
        AddToTables();
      }
      else
      {
        Local.UpdatePolicyIdAdd(policyId =>
        {
          data.PolicyId = policyId;
          AddToTables();
        });
      }
    }
    catch(Exception ex)
    {
      _Log.Error(obj: data,ex: ex,msg: "RedMountain.Add(tblRedMountain); See object");
    }

    #region Methods

    void AddToTables()
    {
      Task.Run(() => _IRedMountainHandler.Add(data));
      if(data.FalconScore == EnRedMountainScore.ServiceOff)
      {
        error = new RmError
        {
          AgencyId = data.AgencyId,
          Environment = data.Environment,
          Id = data.Id,
          PolicyId = data.PolicyId,
          UserId = data.UserId,
          Vin = data.Vin
        };
        Task.Run(() => _IRedMountainHandler.Add(error));
      }
    }

    #endregion Methods
  }

  private void SetAddGw(string vin,EnGuideWireScore score)
  {
    if(score == EnGuideWireScore.NoScore)
      return;
    var item = SetGuideWire.FirstOrDefault(c => string.Equals(c.Vin,vin,StringComparison.OrdinalIgnoreCase));
    if(item is null)
      SetGuideWire.Add(new() { Score = score,Vin = vin.ToUpper() });
  }

  private void SetAddRm(string vin,EnRedMountainScore score)
  {
    if(score == EnRedMountainScore.NoScore)
      return;
    var item = SetRedMountain.FirstOrDefault(c => string.Equals(c.Vin,vin,StringComparison.OrdinalIgnoreCase));
    if(item is null)
      SetRedMountain.Add(new() { Score = score,Vin = vin.ToUpper() });
  }

  private void UpdateDiamond()
  {
    if(Local.Transaction == EnPolicyTransaction.Endorsement)
    {
      var vehicles = Local.Vehicles;
      foreach(var vehicle in vehicles)
      {
        if(_RunRedMountain)
        {
          try
          {
            UpdateAppInstance_Rm(vehicleInfo: vehicle);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
        if(_RunGuideWire)
        {
          try
          {
            UpdateAppInstance_Gw(vehicleInfo: vehicle);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
      }
    }
    else
    {
      var vehicles = Local.DiamondImage.LOB.RiskLevel.Vehicles.Where(c => c.DetailStatusCode != 2);
      foreach(var vehicle in vehicles)
      {
        if(_RunRedMountain)
        {
          try
          {
            UpdateImage_Rm(vehicle: vehicle);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
        if(_RunGuideWire)
        {
          try
          {
            UpdateImage_Gw(vehicle: vehicle);
          }
          catch(Exception ex)
          {
            _Log.Error(obj: vehicle,ex: ex);
          }
        }
      }
    }
    SetRedMountain.Clear();
    SetGuideWire.Clear();
    return;
    //Method(s)
    void UpdateAppInstance_Gw(ClsVehicle vehicleInfo)
    {
      const int guideWireModType = (int)Lib.Enumerations.ModifierTypeId.RAFSegment;
      var vin = vehicleInfo.Vin;
      var guideWire = SetGuideWire.FirstOrDefault(c => c.Vin.Equals(vin,StringComparison.OrdinalIgnoreCase));
      if(guideWire is null)
        return;
      var modifiers = vehicleInfo.Modifiers ??= [];
      if(!modifiers.Any(m => m.ModifierTypeId == guideWireModType))
      {
        if(Local.CreateModifier(guideWireModType) is null)
          return;
        var temp = new FalconModifier
        {
          ModifierGroupId = 7,//Vehicle Modifiers
          ModifierTypeId = guideWireModType,
          DetailStatusCode = (int)Insuresoft.RuleEngine.Common.RuleBase.StatusCode.Active,//Active
          ModifierLevelId = (int)LibCo.Modifiers.ModifierLevel.Vehicle,//Vehicle
          ParentModifierTypeId = guideWireModType,
          ModifierOptionDescription = EnGuideWireScore.NotOrdered.IntStr()
        };
        modifiers.Add(temp);
      }
      var modifier = modifiers.Find(m => m.ModifierTypeId == guideWireModType);
      modifier.ModifierOptionDescription = guideWire.Score.IntStr();
    }
    void UpdateAppInstance_Rm(ClsVehicle vehicleInfo)
    {
      var redMountainModType = Local.State switch
      {
        En_State.CO => (int)Lib.Enumerations.ModifierTypeId.HistorySegment,
        _ => (int)Lib.Enumerations.ModifierTypeId.ExperienceLevel,
      };

      var vin = vehicleInfo.Vin;
      var redMountain = SetRedMountain.FirstOrDefault(c => c.Vin.Equals(vin,StringComparison.OrdinalIgnoreCase));
      if(redMountain is null)
        return;
      var modifiers = vehicleInfo.Modifiers ??= [];
      if(!modifiers.Any(m => m.ModifierTypeId == redMountainModType))
      {
        if(Local.CreateModifier(redMountainModType) is null)
          return;
        var temp = new FalconModifier
        {
          ModifierGroupId = 7,//Vehicle Modifiers
          ModifierTypeId = redMountainModType,
          DetailStatusCode = (int)Insuresoft.RuleEngine.Common.RuleBase.StatusCode.Active,//Active
          ModifierLevelId = (int)LibCo.Modifiers.ModifierLevel.Vehicle,//Vehicle
          ParentModifierTypeId = redMountainModType,
          ModifierOptionDescription = EnRedMountainScore.NotOrdered.IntStr()
        };
        modifiers.Add(temp);
      }
      var modifier = modifiers.First(m => m.ModifierTypeId == redMountainModType);
      modifier.ModifierOptionDescription = redMountain.Score.IntStr();
    }
    void UpdateImage_Gw(DiaVehicle vehicle)
    {
      const int guideWireModType = (int)Lib.Enumerations.ModifierTypeId.RAFSegment;
      var vin = vehicle.Vin;
      var guideWire = SetGuideWire.FirstOrDefault(c => c.Vin.Equals(vin,StringComparison.OrdinalIgnoreCase));

      if(Local.IsNonOwner)
        guideWire = new VehicleGuideWire() { Score = EnGuideWireScore.ServiceOff }; // Temporary, need to change to NotApplicable when Diamond has it
      if(guideWire is null)
        return;

      var modifiers = vehicle.Modifiers;
      if(!modifiers.Any(m => m.ModifierTypeId == guideWireModType))
      {
        if(Local.CreateModifier(guideWireModType) is Modifier temp)
        {
          if(temp is null)
          {
            return;
          }
          temp.ModifierGroupId = 7;//Vehicle Modifiers
          temp.ModifierTypeId = guideWireModType;
          temp.DetailStatusCode = (int)Insuresoft.RuleEngine.Common.RuleBase.StatusCode.Active;//Active
          temp.ModifierLevelId = (int)LibCo.Modifiers.ModifierLevel.Vehicle;//Vehicle
          temp.ParentModifierTypeId = guideWireModType;
          temp.ModifierOptionDescription = EnRedMountainScore.NotOrdered.IntStr();
          modifiers.Add(temp);
        }
      }
      var modifier = modifiers.First(m => m.ModifierTypeId == guideWireModType);
      modifier.ModifierOptionDescription = guideWire.Score.IntStr();
    }
    void UpdateImage_Rm(DiaVehicle vehicle)
    {
      var redMountainModType = Local.State switch
      {
        En_State.CO => (int)Lib.Enumerations.ModifierTypeId.HistorySegment,
        _ => (int)Lib.Enumerations.ModifierTypeId.ExperienceLevel,
      };
      var vin = vehicle.Vin;
      var redMountain = SetRedMountain.FirstOrDefault(c => c.Vin.Equals(vin,StringComparison.OrdinalIgnoreCase));

      if(Local.IsNonOwner)
        redMountain = new VehicleRedMountain() { Score = EnRedMountainScore.NotApplicable };
      if(redMountain is null)
        redMountain = new VehicleRedMountain() { Score = EnRedMountainScore.NoScore };

      var modifiers = vehicle.Modifiers;
      if(!modifiers.Any(m => m.ModifierTypeId == redMountainModType))
      {
        if(Local.CreateModifier(redMountainModType) is Modifier temp)
        {
          temp.ModifierGroupId = 7;//Vehicle Modifiers
          temp.ModifierTypeId = redMountainModType;
          temp.DetailStatusCode = (int)Insuresoft.RuleEngine.Common.RuleBase.StatusCode.Active;//Active
          temp.ModifierLevelId = (int)LibCo.Modifiers.ModifierLevel.Vehicle;//Vehicle
          temp.ParentModifierTypeId = redMountainModType;
          temp.ModifierOptionDescription = EnRedMountainScore.NotOrdered.IntStr();
          modifiers.Add(temp);
        }
      }
      var modifier = modifiers.First(m => m.ModifierTypeId == redMountainModType);
      modifier.ModifierOptionDescription = redMountain.Score.IntStr();
    }
  }

  #endregion Private Methods
}
