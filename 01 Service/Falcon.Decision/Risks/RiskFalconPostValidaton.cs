using Decisioning.Enums;
using Falcon.Decision.Helper;
using static Falcon.Decision.ParamDecision;

namespace Falcon.Decision.Risks;

public class RiskFalconPostValidaton:RiskCommon
{
  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.FalconPostValidation,Local.State))
      return;
    if(Local.Is_CP_Payment)
    {
      return;
    }

    RiskHelper.RulePermitDriverAsPolicyHolder(this);

    switch(Local.RiskCheck)
    {
      case EnRiskCheck.ConsumerAppLvl1:
      case EnRiskCheck.ConsumerAppLvl2:
        ValidateDriversLicenseStatusForDiaValidation();
        break;

      default:
        break;
    }
  }

  #endregion Public Methods

  #region Protected Methods

  protected bool DriverNewOrChanged(ClsDriver driverInfo)
  {
    // Exit quick if new driver
    if(driverInfo.Id < 1)
      return true;

    // Check for changed driver, ex. Excluded -> Insured
    if(Local.ExistingParamDecision != null)
    {
      return Local.ExistingParamDecision.Drivers.Any(epi => epi.FirstName.IsEqual(driverInfo.FirstName)
           && epi.LastName.IsEqual(driverInfo.LastName)
           && epi.DateOfBirth.IsEqual(driverInfo.DateOfBirth)
           && epi.DriverType == En_Driver.Excluded && driverInfo.DriverType == En_Driver.Insured);
    }
    return false;
  }

  #endregion Protected Methods

  #region Private Methods

  private void ValidateDriversLicenseStatusForDiaValidation()
  {
    switch(Local.State)
    {
      case En_State.AZ:
        foreach(var driver in Local.Drivers)
        {
          // Diamond Validation will fire if Out of State License is Expired, Not Licensed, or Suspended OR AZ License is Suspended without SR-22
          if((driver.DriverType == En_Driver.Insured
              && driver.LicenseType.In(En_License.Expired,En_License.NotLicensed,En_License.Suspended)
              && (!Local.IsEndorsement || DriverNewOrChanged(driver))
              && driver.LicenseState != En_State.AZ)
            || (driver.DriverType == En_Driver.Insured
              && driver.LicenseType == En_License.Suspended
              && (!Local.IsEndorsement || DriverNewOrChanged(driver))
              && !driver.IsSR22
              && driver.LicenseState == En_State.AZ))
          {
            if(Local.IsEndorsement)
            {
              LocalResult.AddMessage(portalWarning: "</br>");
            }
            else
            {
              LocalResult.Pass = false;
              LocalResult.AddMessage(portalError: "</br>");
            }
          }
        }
        break;
    }
  }

  #endregion Private Methods
}
