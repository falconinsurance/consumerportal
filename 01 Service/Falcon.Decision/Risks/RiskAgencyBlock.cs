using DecAgencyBlock.DB;
using Decisioning.Enums;
using Falcon.Decision.Helper;

namespace Falcon.Decision.Risks;

public class RiskAgencyBlock:RiskCommon
{
  #region Private Fields
  private readonly IAgencyBlockHandler _IAgencyBlockHandler;
  #endregion Private Fields

  #region Public Constructors

  public RiskAgencyBlock()
  {
    _IAgencyBlockHandler = AppResolver.Get<IAgencyBlockHandler>();
  }

  #endregion Public Constructors

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.AgencyBlock,Local.State))
      return;

    if(DenyRisk() || Local.IsEndorsement)
      return;

    var result = _IAgencyBlockHandler.GetAgencyScore(Local.AgencyId,Local.PolicyCode,Local.TermLength.GetId());
    if(result == EnCheckResult.Pass)
    {
      LocalResult.Score = 0;
      LocalResult.Pass = true;
      return;
    }

    LocalResult.Score = 100;
    LocalResult.Pass = false;
    switch(result)
    {
      case EnCheckResult.Fail_AgencyBlock_PolicyCode:
        LocalResult.AddMessage(
               diamondNote: EnNote.AgencyBlock.SetNote($"Agency Block: {Message.DiamondNote.FailPolicyCode}"),
               portalError: Message.PortalError.DecisionError(Local));
        break;
      //case Tools.EnCheckResult.Fail_AgencyBlock_Terms:
      default:
        LocalResult.AddMessage(
               diamondNote: EnNote.AgencyBlock.SetNote($"Agency Block: {Message.DiamondNote.FailPolicyTerm}"),
               portalError: Message.PortalError.TermError);
        break;
    }
  }

  #endregion Public Methods

  #region Private Methods

  private bool DenyRisk()
  {
    if(Local.RiskLevel != EnRiskLevel.RiskBlock)
    {
      return false;
    }
    LocalResult.Pass = false;
    LocalResult.Score = 100;
    LocalResult.AddMessage(portalError: Message.PortalError.DecisionError(Local));
    return true;
  }

  #endregion Private Methods
}
