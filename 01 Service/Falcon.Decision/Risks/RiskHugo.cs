using DecHugo;
using DecHugo.Class;
using DecHugo.Model;
using DecHugo.Settings;
using Decisioning.Config;
using Decisioning.Enums;
using Diamond.Common.Enums;
using Diamond.Common.Objects;
using Diamond.Common.Objects.Policy;
using Falcon.Decision.Helper;
using static Falcon.Decision.ParamDecision;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

namespace Falcon.Decision.Risks;

public class RiskHugo:RiskCommon
{
  #region Private Fields

  private static readonly DateTime _DateTime_20110101 = DateTime.Parse("2011-01-01");
  private static readonly IHugoHandler _Handler = AppResolver.Get<IHugoHandler>();
  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskHugo));
  private bool _EnableEvasion;
  private bool _IsEndorsement;

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.Hugo,Local.State))
      return;
    var setting = GetSetting();
    _EnableEvasion = setting.EnableEvasion && Local.IsNewBusiness;
    if(setting.Skip)
      return;
    if(CheckEvasion(setting))
      return;
    var drivers = GetDrivers();
    var driversOffenses = _Handler.SearchOffense(drivers,Local.State.ToString())?.ToArray();
    if(driversOffenses?.Any() == true)
    {
      AddDrivers(setting,driversOffenses);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static IEnumerable<OffenseSearch> GetOffenses(IEnumerable<OffenseSearch> offenses,params EnUnderwritingCode[] codes)
      => offenses.Where(c => c.UnderwritingCodeId.In(codes));

  private static int GetOffensesCount(IEnumerable<OffenseSearch> offenses,int ageMin,int ageMax)
        => (from offense in offenses let age = offense.DateOfOffense.AgeYear() where age >= ageMin && age < ageMax select 1).Count();

  private static int GetScoreIL(IEnumerable<OffenseSearch> offenses)
  {
    // offenses one driver
    int count;
    // Unknown
    var unknowns = GetOffenses(offenses,EnUnderwritingCode.Unknown,EnUnderwritingCode.InsuranceFraud);
    if(unknowns.Any())
    {
      //Any Unknown
      return 100;
    }
    //DWI
    var duis = GetOffenses(offenses,EnUnderwritingCode.DUI);
    if(duis.Any())
    {
      //No more than 1 DWI in the last 10 years
      count = GetOffensesCount(duis,0,10);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 DWI's total
      count = duis.Count();
      if(count > 2)
      {
        return 100;
      }
    }

    // Felony
    var felonies = GetOffenses(offenses,EnUnderwritingCode.Felony);
    if(felonies.Any())
    {
      //No Felony Offenses
      count = felonies.Count();
      if(count > 0)
      {
        return 100;
      }
    }
    // Misdemeanor
    var misdemeanors = GetOffenses(offenses,EnUnderwritingCode.Misdemeanor);
    if(misdemeanors.Any())
    {
      //No more than 1 Misdemeanor Offense in the last 5 years
      count = GetOffensesCount(misdemeanors,0,5);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 Misdemeanors in the last 10 years
      count = GetOffensesCount(misdemeanors,0,10);
      if(count > 2)
      {
        return 100;
      }
    }
    return 0;
  }

  private static int GetScoreOK(IEnumerable<OffenseSearch> offenses)
  {
    // offenses one driver
    int count;
    // Unknown
    var unknowns = GetOffenses(offenses,EnUnderwritingCode.Unknown,EnUnderwritingCode.InsuranceFraud);
    if(unknowns.Any())
    {
      //Any Unknown
      return 100;
    }
    //DWI
    var duis = GetOffenses(offenses,EnUnderwritingCode.DUI);
    if(duis.Any())
    {
      //No more than 1 DWI in the last 5 years
      count = GetOffensesCount(duis,0,5);
      if(count > 1)
      {
        return 100;
      }
    }

    // Felony
    var felonies = GetOffenses(offenses,EnUnderwritingCode.Felony);
    if(felonies.Any())
    {
      //No Felony Offenses during the last 10 years.
      count = GetOffensesCount(felonies,0,10);
      if(count > 0)
      {
        return 100;
      }
      //No more than 1 Felony between 10 and 20 years.
      count = GetOffensesCount(felonies,10,20);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 Felonies total.
      count = felonies.Count();
      if(count > 2)
      {
        return 100;
      }
    }

    // Misdemeanor
    var misdemeanors = GetOffenses(offenses,EnUnderwritingCode.Misdemeanor);
    if(misdemeanors.Any())
    {
      //No more than 1 Misdemeanor Offense in the last 5 years.
      count = GetOffensesCount(misdemeanors,0,5);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 Misdemeanors in last 10 years.
      count = GetOffensesCount(misdemeanors,0,10);
      if(count > 2)
      {
        return 100;
      }
    }
    return 0;
  }

  private static int GetScoreTX(IEnumerable<OffenseSearch> offenses)
  {
    int count;
    // Unknown
    var unknowns = GetOffenses(offenses,EnUnderwritingCode.Unknown,EnUnderwritingCode.InsuranceFraud);
    if(unknowns.Any())
    {
      //Any Unknown
      return 100;
    }
    //DWI
    var duis = GetOffenses(offenses,EnUnderwritingCode.DUI);
    // offenses one driver
    if(duis.Any())
    {
      //No more than 1 DWI in the last 10 years
      count = GetOffensesCount(duis,0,10);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 DWI's total
      count = duis.Count();
      if(count > 2)
      {
        return 100;
      }
    }

    // Felony
    var felonies = GetOffenses(offenses,EnUnderwritingCode.Felony);
    if(felonies.Any())
    {
      //No Felony Offenses during the last 10 years
      count = GetOffensesCount(felonies,0,10);
      if(count > 0)
      {
        return 100;
      }
      //No more than 1 Felony between 10 and 20 years
      count = GetOffensesCount(felonies,10,20);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 Felonies total
      count = felonies.Count();
      if(count > 2)
      {
        return 100;
      }
    }

    // Misdemeanor
    var misdemeanors = GetOffenses(offenses,EnUnderwritingCode.Misdemeanor);
    if(misdemeanors.Any())
    {
      //No more than 1 Misdemeanor Offense in the last 5 years
      count = GetOffensesCount(misdemeanors,0,5);
      if(count > 1)
      {
        return 100;
      }
      //No more than 2 Misdemeanors in the last 10 years
      count = GetOffensesCount(misdemeanors,0,10);
      if(count > 2)
      {
        return 100;
      }
    }
    return 0;
  }

  private void AddDrivers(SettingHugo setting,OffenseSearch[] driversOffenses)
  {
    var driversGroup = driversOffenses.GroupBy(c => c.DriverId).ToArray();

    var tblHugo = GetHugoTable(setting);
    var tblClaim = GetClaimTable(setting);

    var maxScore = 0;
    //Add Drivers
    foreach(var group in driversGroup)
    {
      //Remove duplicate
      var offenses = group
          .GroupBy(g => new { g.DateOfOffense,g.UnderwritingCodeId })
          .Select(i => i.First());
      var score = GetScore(offenses);
      if(score > maxScore)
        maxScore = score;
      var first = group.First();
      var groupHasHugo = group.Any(c => c.UnderwritingCodeId != EnUnderwritingCode.AtFaultAccident);
      var groupHasClaim = group.Any(c => c.UnderwritingCodeId == EnUnderwritingCode.AtFaultAccident);
      if(groupHasHugo)
      {
        var tblHugoDriver = new HugoDriver
        {
          DriverId = group.Key,
          StatusId = _IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote,
          Score = score
        };
        tblHugo.Drivers.Add(tblHugoDriver);
      }
      if(groupHasClaim)
      {
        var tblClaimDriver = new ClaimDriver
        {
          DriverId = group.Key,
          StatusId = _IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote,
          Score = score,
        };
        tblClaim.Drivers.Add(tblClaimDriver);
      }

      foreach(var offense in group)
      {
        if(ShouldAddMessage(offense))
        {
          LocalResult.AddMessage(portalWarning: $"Additional violation found: {first.FirstName.ToUpperCase()} {first.LastName.ToUpperCase()} - {offense.DateOfOffense:MM/dd/yyyy} - {offense.UnderwritingCodeId}");
        }
      }

      if(LocalResult.PortalError.Count == 0 && LocalResult.PortalWarning.Count > 0)
        LocalResult.AddMessage(portalWarning: "\n\nClose this window to continue.");

      if(_IsEndorsement)
        UpdateAppInstanceDriver(driverOffense: group.ToArray());
      else
        UpdateImageDriver(driverOffense: group.ToArray());
    }
    tblHugo.Score = maxScore;
    tblHugo.Pass = maxScore < 100;
    tblClaim.Score = maxScore;
    tblClaim.Pass = maxScore < 100;
    if(Local.ValidUser)
    {
      if(tblHugo.Drivers?.Count > 0)
      {
        if(tblHugo.PolicyId > 0)
        {
          Task.Run(() => SaveHugo(tblHugo));
        }
        else
        {
          Local.UpdatePolicyIdAdd(policyId => _ = Task.Run(() => SaveHugo(tblHugo.With(c => c.PolicyId = policyId))));
        }
      }
      if(tblClaim.Drivers?.Count > 0)
      {
        if(tblClaim.PolicyId > 0)
        {
          Task.Run(() => _Handler.Add(tblClaim));
        }
        else
        {
          Local.UpdatePolicyIdAdd(policyId => _ = Task.Run(() => _Handler.Add(tblClaim.With(c => c.PolicyId = policyId))));
        }
      }
    }
  }

  /// <summary>
  /// Check Hugo Violation, true if Evasion [Vin] found only
  /// </summary>
  private bool CheckEvasion(SettingHugo setting)
  {
    // Check Evasion is Active
    if(!_EnableEvasion)
      return false;

    var vins = Local.GetVins();
    //Check if vins exists
    if(!(vins?.Length > 0))
      return false;

    (bool Found, Evasion Evasion, string Policy, string[] Names) found = default;
    var vinFound = "";
    var blocked = false;
    foreach(var vin in vins)
    {
      found = _Handler.CheckEvasion(vin,(EnState)Local.State);
      if(found.Found)
      {
        vinFound = vin;
        blocked = (DateTime.Now - found.Evasion.TimeStamp).Days < setting.ThresholdDays;
        break;
      }
    }
    //if not found
    if(!found.Found)
      return false;
    //if found

    //Check driver exists or not

    if(CheckEvasionDriver(found.Names))
    {
      _EnableEvasion = false;
      return false;
    }

    var hugoEvasion = new HugoEvasion
    {
      HugoId = found.Evasion.HugoId,
      IsBlocked = blocked,
      PolicyId = Local.PolicyId,
      PolicyIdRef = found.Evasion.PolicyIdRef,
      StateId = (EnState)Local.State,
      Vin = vinFound,
    };
    if(hugoEvasion.PolicyId > 0)
    {
      Task.Run(() => _Handler.Add(hugoEvasion));
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId => _Handler.Add(hugoEvasion.With(c => c.PolicyId = policyId)));
    }

    LocalResult.AddMessage(diamondNote: EnNote.Hugo.SetNote($"Risk: Hugo Evasion Detected - [{found.Policy}]"));
    if(blocked)
    {
      LocalResult.Pass = false;
      LocalResult.Score = 100;
      LocalResult.AddMessage(portalError: Message.PortalError.DecisionError(Local));
      return true;
    }
    return false;
  }

  private bool CheckEvasionDriver(string[] names)
  {
    return Local.State.In(En_State.OK) ? CheckOK() : CheckOther();

    bool CheckOK()
    {
      var FoundName = names.Select(c => c.Substring(0,c.Length - 3).ToUpper());
      var Driver = Local.DriversInsured.Select(c => $"{c.FirstName} {c.LastName} {c.DateOfBirth:yyyy-MM}".ToUpper());
      return Driver.Any(c => FoundName.Contains(c));
    }
    bool CheckOther()
    {
      var FoundName = names.Select(c => c.ToUpper());
      var Driver = Local.DriversInsured.Select(c => $"{c.FirstName} {c.LastName} {c.DateOfBirth:yyyy-MM-dd}".ToUpper());
      return Driver.Any(c => FoundName.Contains(c));
    }
  }

  private Claim GetClaimTable(SettingHugo setting) =>
     //Start New Hugo
     new()
     {
       AgencyId = Local.AgencyId,
       Pass = LocalResult.Pass,
       PolicyId = Local.PolicyId,
       Status = $"{(_IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote)}",
       UserId = Local.UserId,
       PolicyCode = Local.PolicyCode,
       RiskLevel = setting.RiskLevel,
     };

  private IEnumerable<Search> GetDrivers()
  {
    var drivers = new List<Search>();
    if(Local.Transaction == EnPolicyTransaction.Endorsement)
    {
      if(Local.ExistingParamDecision != null)
      {
        var addedDrivers = Local.DriversInsured.Select(c => new
        {
          c.DateOfBirth,
          c.FirstName,
          c.LastName,
          DriverType = c.IsExcluded ? En_Driver.Excluded : En_Driver.Insured // Matches value in falcon enum
        }).ToList();

        // Remove all drivers that exist in old instance and new instance
        addedDrivers.RemoveAll(qd => Local.ExistingParamDecision.Drivers.ToList().Any(
            ed => ed.FirstName.IsEqual(qd.FirstName)
            && ed.LastName.IsEqual(qd.LastName)
            && ed.DateOfBirth.IsEqual(qd.DateOfBirth)
            && (ed.DriverType == qd.DriverType || qd.DriverType == En_Driver.Excluded)
));

        drivers = addedDrivers.ConvertAll(x => new Search
        {
          FirstName = x.FirstName,
          LastName = x.LastName,
          DateOfBirth = x.DateOfBirth
        });
        _IsEndorsement = true;
      }
    }
    else
    {
      drivers = Local.DriversInsured.Select(c => new Search
      {
        DateOfBirth = c.DateOfBirth,
        FirstName = c.FirstName,
        LastName = c.LastName,
      }).ToList();
    }
    return drivers?.WithItem(c => { c.FirstClean = ExtHelper.CleanName(c.FirstName); c.LastClean = ExtHelper.CleanName(c.LastName); });
  }

  private Hugo GetHugoTable(SettingHugo setting) =>
     //Start New Hugo
     new()
     {
       AgencyId = Local.AgencyId,
       Pass = LocalResult.Pass,
       PolicyId = Local.PolicyId,
       Status = $"{(_IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote)}",
       UserId = Local.UserId,
       PolicyCode = Local.PolicyCode,
       RiskLevel = setting.RiskLevel,
     };

  private int GetScore(IEnumerable<OffenseSearch> offenses)
  {
    var score = Local.State switch
    {
      En_State.IL => GetScoreIL(offenses),
      En_State.IN => GetScoreTX(offenses),
      En_State.OK => GetScoreOK(offenses),
      En_State.TX => GetScoreTX(offenses),
      En_State.UT => GetScoreUT(offenses),
      _ => 0,
    };
    return score > 18 ? 100 : score;
  }

  private int GetScoreUT(IEnumerable<OffenseSearch> offenses) => GetScoreTX(offenses) + GetScoreUT_20110101(offenses);

  private int GetScoreUT_20110101(IEnumerable<OffenseSearch> offenses)
  {
    var totalOffenses = offenses.Where(c => c.DateOfOffense > _DateTime_20110101);
    if(totalOffenses.Any())
    {
      var note = $"Criminal Background Check Failure: {totalOffenses.FirstOrDefault()?.FirstName} {totalOffenses.FirstOrDefault()?.LastName}\n";
      foreach(var offense in totalOffenses)
        note += $"- {offense.DateOfOffense:MM/dd/yyyy} - Felony Reported\n";

      LocalResult.Score = 100;
      LocalResult.Pass = false;
      LocalResult.AddMessage(
          diamondNote: EnNote.Decision.SetNote(note),
          portalError: Message.PortalError.DecisionError(Local)
          );
      return 100;
    }
    return 0;
  }

  private SettingHugo GetSetting()
  {
    SettingHugo setting;
    try
    {
      setting = _IDecHandler.GetSettingCs<SettingHugo>(new ParamRisk { RiskLevel = Local.RiskLevel,State = (EnState)Local.State })
        ?? new SettingHugo().With(_ => _Log.Error(msg: "Hugo is Null",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode }));
    }
    catch(Exception)
    {
      _Log.Error(msg: "Hugo Setting 1",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode });
      setting = new SettingHugo();
    }

    return setting;
  }

  private void SaveHugo(Hugo tblHugo)
  {
    _Handler.Add(tblHugo);
    if(_EnableEvasion && !tblHugo.Pass)
    {
      foreach(var vin in Local.GetVins())
      {
        _Handler.Add(new Evasion
        {
          HugoId = tblHugo.Id,
          PolicyIdRef = (int)tblHugo.PolicyId,
          Id = vin,
          StateId = (EnState)Local.State
        });
      }
    }
  }

  private bool ShouldAddMessage(OffenseSearch offense) => Local.State != En_State.UT || offense.DateOfOffense <= DateTime.Parse("2011-01-01");

  private void UpdateAppInstanceDriver(IEnumerable<OffenseSearch> driverOffense)
  {
    var appInstanceDrivers = Local.Drivers;
    foreach(var offense in driverOffense)
    {
      Array.Find(appInstanceDrivers,d =>
          d?.FirstName.IsEqual(offense.FirstName) == true
          && d.LastName.IsEqual(offense.LastName)
          && d.DateOfBirth.IsEqual(offense.DateOfBirth)
          )?.With(c => Merge(c,offense));
    }
    return;
    void Merge(ClsDriver driver,OffenseSearch offense)
    {
      var diaId = RiskHelper.GetDiaIdFromUndeWritingCode(this,offense.UnderwritingCodeId);
      if(diaId < 0)
        return;
      if(Local.State.In(En_State.UT) && offense.DateOfOffense > _DateTime_20110101)
        return;
      var appInstanceViolationsId = Violation.FromDiaId(diaId);
      var offenseDate = offense.DateOfOffense;
      if(appInstanceViolationsId != En_Violation.NoVal && !driver.Violations.Any(v => v.Violation == appInstanceViolationsId && v.Date.Equals(offenseDate)))
      {
        driver.Violations.Add(new() { Violation = appInstanceViolationsId,Date = offenseDate });
      }
    }
  }

  private void UpdateImageDriver(IEnumerable<OffenseSearch> driverOffense)
  {
    var diamondDrivers = Local.DiamondImage.LOB.RiskLevel.Drivers;
    foreach(var offense in driverOffense)
    {
      diamondDrivers
        .Find(d =>
         d?.Name?.FirstName.IsEqual(offense.FirstName) == true
         && d.Name.LastName.IsEqual(offense.LastName)
         && d.Name.BirthDate.IsEqual(offense.DateOfBirth)
         )
        ?.With(c => Merge(c,offense));
    }
    return;
    void Merge(Driver driver,OffenseSearch offense)
    {
      var diaId = RiskHelper.GetDiaIdFromUndeWritingCode(this,offense.UnderwritingCodeId);
      if(diaId < 0)
        return;
      if(Local.State.In(En_State.UT) && offense.DateOfOffense > _DateTime_20110101)
        return;

      driver.AccidentViolations ??= [];
      var offenseDate = offense.DateOfOffense.ToInsDateTime();
      if(!driver.AccidentViolations.Any(v => v.AccidentsViolationsTypeId == diaId && v.ConvictionDate.Equals(offenseDate) && v.DetailStatusCode != (int)StatusCode.Deleted))
      {
        // if we don't already have the violation, add it
        var violation = new AccidentViolation
        {
          DetailStatusCode = (int)StatusCode.Active,
          ConvictionDate = offenseDate,
          AvDate = offenseDate,
          AccidentsViolationsTypeId = diaId,
          ViolationSourceId = (int)FDEnums.ViolationSourceId.Other
        };
        driver.AccidentViolations.Add(violation);
      }
    }
  }

  #endregion Private Methods
}
