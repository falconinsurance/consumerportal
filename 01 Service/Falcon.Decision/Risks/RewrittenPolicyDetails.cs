namespace Falcon.Decision.Risks
{
  public class RewrittenPolicyDetails
  {
    #region Public Properties
    public int? FromPolicyId { get; set; }
    public string FromPolicyNumber { get; set; }
    public string FromPolicyStatusDescription { get; set; }
    public int? FromPolicyStatusId { get; set; }
    public int? ToPolicyId { get; set; }

    public string ToPolicyNumber { get; set; }

    public string ToPolicyStatusDescription { get; set; }
    public int? ToPolicyStatusId { get; set; }
    #endregion Public Properties
  }
}
