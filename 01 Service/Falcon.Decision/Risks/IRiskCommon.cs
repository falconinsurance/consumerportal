using ArgNotNull = Falcon.DependencyInjection.ArgNotNull;
using SkipError = Falcon.DependencyInjection.SkipError;

namespace Falcon.Decision.Risks;

public interface IRiskCommon:IDisposable
{
  #region Public Properties

  ParamDecision Local { get; }
  DecisionResult LocalResult { get; set; }

  #endregion Public Properties

  #region Public Methods

  [SkipError]
  void Check();

  void Start([ArgNotNull] ParamDecision param,DecisionResult result);

  #endregion Public Methods
}
