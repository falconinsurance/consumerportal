using DecUdr.DB;
using DecUdr.Model.Schema;
using Falcon.Core.Settings;

namespace Falcon.Decision.Risks;

public partial class RiskUDR:RiskCommon
{
  #region Public Classes

  public class CacheUDR:CacheTimedDb<CacheUDR,string,List<ClsRetIndivLayout>>
  {
    #region Private Fields

    private static readonly ConfigSettings _Config = AppResolver.Get<ConfigSettings>();
    private static readonly IUdrHandler _Handler = AppResolver.Get<IUdrHandler>();

    #endregion Private Fields

    #region Public Methods

    public override void Init()
    {
      var sqlServer = _Config.GetDBConnection(nameof(EnDatabase.Services));
      ItemDuration = TimeSpan.FromMinutes(30);
      DbDelete = LocalDbDelete;
      DbInsert = LocalDbInsert;
      DbSelect = LocalDbSelect;
      DbUpdate = LocalDbUpdate;
    }

    #endregion Public Methods

    #region Private Methods

    private T AddWithPolicyId<T>(T data) where T : class
    {
      _ = AddAsync();
      return data;

      async Task AddAsync()
      {
        try
        {
          await Task.Run(() => _Handler.Add(data));
        }
        catch(Exception ex)
        {
          throw _Log.Exception(obj: data,msg: $"_Handler.Add({typeof(T).Name}); See object",ex: ex);
        }
      }
    }

    private void LocalDbDelete(string key = null) => _Handler.Delete(new DecUdr.Model.CacheStr { Id = key?.ToUpper() });

    private void LocalDbInsert(string key,TimedTValue value)
    {
      if(key == null)
        return;
      var tbl = new DecUdr.Model.CacheStr
      {
        Duration = (int)value.Duration.TotalMinutes,
        Id = key.Trim().ToUpper(),
      };
      tbl.SetJson(value.Value);
      AddWithPolicyId(tbl);
    }

    private TimedTValue LocalDbSelect(string key)
    {
      var temp = default(TimedTValue);
      if(key == null)
        return temp;

      var tbl = _Handler.SqlFirstOrDefault<DecUdr.Model.CacheStr>("SELECT TOP (1) * FROM [Services].[Udr].[tblCacheStr](NOLOCK) WHERE Id=@1",c => c.AddValues(key));
      if(tbl != null)
      {
        return new TimedTValue
        {
          Duration = TimeSpan.FromMinutes(tbl.Duration),
          Value = tbl.GetJson<List<ClsRetIndivLayout>>()
        };
      }
      return temp;
    }

    private void LocalDbUpdate(string key,TimedTValue value)
    {
      if(key == null)
        return;
      var tbl = new DecUdr.Model.CacheStr
      {
        Duration = (int)value.Duration.TotalMinutes,
        Id = key.Trim().ToUpper(),
      };
      tbl.SetJson(value.Value);
      _Handler.Update(tbl);
    }

    #endregion Private Methods
  }

  #endregion Public Classes
}
