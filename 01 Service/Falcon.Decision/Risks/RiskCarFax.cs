using DecCarFax.DB;
using DecCarFax.Model;
using DecCarFax.Service;
using DecCarFax.Settings;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using Falcon.Decision.Helper;
using En = Falcon.Enums;
using Indicator = DecCarFax.Service.CarFaxResponse.NestedIndicator;

namespace Falcon.Decision.Risks;

public partial class RiskCarFax:RiskCommon
{
  #region Private Fields

  private static readonly ICarFaxHandler _CarFaxHandler = AppResolver.Get<ICarFaxHandler>();
  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskCarFax));

  #endregion Private Fields

  #region Private Properties

  private HashSet<string> CarFaxSet { get; } = [];

  #endregion Private Properties

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.CarFax,Local.State))
      return;
    switch(Local.Transaction)
    {
      case EnPolicyTransaction.NewPolicy:
        {
          CheckNewPolicy();
          break;
        }
      case EnPolicyTransaction.Endorsement:
        {
          CheckEndorsement();
          break;
        }
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static bool CheckDateDiff(Indicator indicator,DateTime today,int days)
  {
    var result = false;
    if(indicator?.LastDate != null)
    {
      var ts = today - indicator.LastDate;
      if(ts.HasValue)
      {
        result = ts.Value.TotalDays <= days;
      }
    }
    return result;
  }

  private static CarFax.ClsNotes GetNotes(DecisionResult result)
  {
    if(result.Pass)
    {
      return default;
    }
    var tmp = new CarFax.ClsNotes();
    tmp.AddRange(result.DiamondNotes.Select(c => $"{c._Type} : {c._Value}"));
    return tmp;
  }

  private T AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    return data;

    async Task AddAsync()
    {
      try
      {
        await Task.Run(() => _CarFaxHandler.Add(data));
      }
      catch(Exception ex)
      {
        throw _Log.Exception(obj: data,msg: $"_CarFaxHandler.Add({typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private void CarFaxAdd(string vin) => _ = CarFaxSet.Add(vin);

  private void CheckEndorsement()
  {
    try
    {
      if(DenyPolicy())
        return;
      var setting = GetSetting(policyCode: EnPolicyCode.FullCoverage | EnPolicyCode.Endorsement);
      if(setting?.Skip != false)
        return;
      var vins = GetVinsEndorsement();
      if(vins?.Any() != true)
        return;
      foreach(var vin in vins)
      {
        // Assign all Record as Deleted if exists
        _CarFaxHandler.DeleteItem(new CarFax { PolicyId = Local.PolicyId,Vin = vin });
        var result = CheckVinEndorsement(vin,setting);
        LocalResult.AddResult(result);
      }
    }
    catch(Exception ex)
    {
      throw _Log.Exception(ex: ex);
    }

    #region Methods

    string[] GetVinsEndorsement()
    {
      if(!Local.IsEndorsementQuoteOnly)
      {
        if(Local.ExistingParamDecision is null)
          return null;
        // Check Only Vehicles with FullCoverage
        var vehiclesNew = Local.Vehicles?.Where(c => c.IsFullCoverage).Select(c => c.Vin10);
        //Old List
        var vehiclesOld = Local.ExistingParamDecision?.Vehicles?.Select(c => c.Vin);
        //Only vehicle on new and not exists on the old allowed to return
        if(vehiclesOld is null)
        {
          return vehiclesNew.ToArray();
        }

        return vehiclesNew.Where(Vnew => !vehiclesOld.Any(Vold => Vold == Vnew)).ToArray();
      }
      else
      {
        if(Local.ExistingParamDecision is null)
          return null;

        var vehiclesAddingFullCoverage = new List<string>();
        foreach(var oldVehicle in Local.ExistingParamDecision?.Vehicles)
        {
          var newVehicle = Local.Vehicles?.Where(n => n.Vin10 == oldVehicle.Vin).FirstOrDefault();
          // If oldVehicle added full coverage when it had none or new vehicle has a lower deductible than old vehicle (adding coverage by definition)
          if((oldVehicle.ComColDeductible == En.En_ComColDeductible.NoInsurance && newVehicle.IsFullCoverage) || (oldVehicle.ComColDeductible > newVehicle.ComColDeductible))
            vehiclesAddingFullCoverage.Add(newVehicle.Vin10);
        }

        return vehiclesAddingFullCoverage.ToArray();
      }
    }
    DecisionResult CheckVinEndorsement(string vin,SettingCarFax setting)
    {
      var tempResult = new DecisionResult { CutOffScore = Local.CutOffScore };
      var continueNext = true;
      var key = vin;


      var (parsedResponse, response) = _CarFaxHandler.ParsedResponse(vin,$"{DateTime.Now:yyMMddHHmmssfff}-{Local.AgencyCode}-{Local.PolicyId}-{vin}");
      var fromCache = response.Value.FromCache;

      if(parsedResponse.IsServerOff)
        continueNext = false;
      if(continueNext && parsedResponse.IsVinError)
      {
        LocalResult.Score = 100;
        LocalResult.Pass = false;
        LocalResult.AddMessage(
            diamondNote: EnNote.CarFax.SetNote($"CarFax: Vin :{vin} Not Found"),
            portalError: $"Vehicle with VIN <b>{vin}</b> is invalid, please correct."
            );
        continueNext = false;
      }
      if(continueNext)
      {
        var result = setting.GetResult(parsedResponse,Local.CutOffScore);
        var score = result.Score < Local.CutOffScore ? result.Score : 100;
        var val = response?.Value?.Components?.DemographicInformationWithLastOdo?.Description;
        tempResult.Score = score;
        tempResult.Pass = score < 100;
        var hasBrand = parsedResponse.HasBrand();
        var diamondNote = hasBrand ? $"Branded title on {val?.Year} {val?.Make} {val?.Model} {vin}" : Message.DiamondNote.FailHighScore;
        if(!tempResult.Pass)
        {
          CarFaxAdd(vin);
          tempResult.AddMessage(
              diamondNote: EnNote.CarFax.SetNote($"CarFax: {diamondNote}"),
              portalError: hasBrand ? $"We are unable to bind physical damage on this vehicle: <b>{val?.Year} {val?.Make} {val?.Model} {vin}</b>, due to a branded title.<br/>You may continue with liability only." : null,
              portalWarning: hasBrand ? null : $"The vehicle: <b>{val?.Year} {val?.Make} {val?.Model} {vin}</b>, is unacceptable for COMP/COLL coverage due to vehicle history.<br/>A surcharge has been applied and the deductible has been updated to $1,000.<br/>You will avoid this surcharge by removing COMP/COLL coverage.");
        }
      }

      //Add/Update the cache
      SaveToDB(vin,setting,response,tempResult,parsedResponse,fromCache);
      return tempResult;
    }

    #endregion Methods
  }

  private void CheckNewPolicy()
  {
    try
    {
      if(DenyPolicy())
        return;
      var setting = GetSetting(policyCode: EnPolicyCode.FullCoverage);
      if(setting?.Skip != false)
        return;
      var vins = GetVinsNewPolicy();
      if(vins?.Any() != true)
        return;
      _CarFaxHandler.DeleteItem(new CarFax { PolicyId = Local.PolicyId });
      // Assign all Record as Deleted if exists
      var score = 0;
      foreach(var vin in vins)
      {
        var result = CheckVinNewPolicy(vin,setting);
        if(result.Score > score)
        {
          LocalResult = result;
          score = result.Score;
        }
        if(!result.Pass)
          break;
      }
    }
    catch(Exception ex)
    {
      throw _Log.Exception(ex: ex);
    }

    #region Methods

    string[] GetVinsNewPolicy() => Local.State switch
    {
      En_State.TX => Local.Vehicles?.Select(c => c.Vin10).ToArray(),// Check all Vehicles
      _ => Local.Vehicles?.Where(c => c.IsFullCoverage).Select(c => c.Vin10).ToArray(),// Check Only Vehicles with FullCoverage
    };

    DecisionResult CheckVinNewPolicy(string vin,SettingCarFax setting)
    {
      ClsResult result = null;
      var tempResult = new DecisionResult { CutOffScore = Local.CutOffScore };
      var continueNext = true;
      var key = vin;


      var (parsedResponse, response) = _CarFaxHandler.ParsedResponse(vin,$"{DateTime.Now:yyMMddHHmmssfff}-{Local.AgencyCode}-{Local.PolicyId}-{vin}");
      var fromCache = response.Value.FromCache;
      if(parsedResponse.IsServerOff)
        continueNext = false;
      if(continueNext && parsedResponse.IsVinError)
      {
        tempResult.Score = 100;
        tempResult.Pass = false;
        tempResult.AddMessage(
            diamondNote: EnNote.CarFax.SetNote($"CarFax: Vin :{vin} Not Found"),
            portalError: $"Vehicle with VIN <b>{vin}</b> is invalid, please correct."
            );
        continueNext = false;
      }
      if(continueNext)
      {
        result = setting.GetResult(parsedResponse,Local.CutOffScore);
        var score = result.Score < Local.CutOffScore ? result.Score : 100;
        var val = response?.Value?.Components?.DemographicInformationWithLastOdo?.Description;
        tempResult.Score = score;
        tempResult.Pass = score < 100;

        var diamondNote = (parsedResponse.HasBrand()) ? $"Branded title on {val?.Year} {val?.Make} {val?.Model} {vin}" : $"{Message.DiamondNote.FailHighScore} Vehicle {val?.Year} {val?.Make} {val?.Model} {vin} is unacceptable for Comp/Col coverage.";

        if(tempResult.Pass)
        {
          if(Array.Find(Local.Vehicles,x => x.Vin == vin)?.IsFullCoverage == true
              && Local.State == En_State.TX
              && parsedResponse.HasBrand())
          {
            tempResult.Score = 100;
            tempResult.Pass = false;
            var portalError = $"We are unable to bind physical damage on this vehicle: <b>{val?.Year} {val?.Make} {val?.Model} {vin}</b>, due to a branded title.<br/>You may continue with liability only.";
            tempResult.AddMessage(
                diamondNote: EnNote.CarFax.SetNote($"CarFax: {diamondNote}"),
                portalError: portalError
            );
          }
        }
        else
        {
          var portalError = Message.PortalError.DecisionError(Local);
          if(setting.DisplayLiability && (EnPolicyCode.FullCoverage & Local.PolicyCode) == EnPolicyCode.FullCoverage)
          {
            portalError = parsedResponse.HasBrand()
              ? $"We are unable to bind physical damage on this vehicle: <b>{val?.Year} {val?.Make} {val?.Model} {vin}</b>, due to a branded title.<br/>You may continue with liability only."
              : $"We are unable to bind physical damage on this vehicle: <b>{val?.Year} {val?.Make} {val?.Model} {vin}</b>, due to the vehicle history.<br/>You may continue with liability only.";
          }

          tempResult.AddMessage(
              diamondNote: EnNote.CarFax.SetNote($"CarFax: {diamondNote}"),
              portalError: portalError
         );
        }
      }

      //Add/Update the cache
      SaveToDB(vin,setting,response,tempResult,parsedResponse,fromCache);

      return tempResult;
    }

    #endregion Methods
  }

  private bool DenyPolicy()
  {
    if(Local.RiskLevel != EnRiskLevel.RiskBlock)
    {
      return false;
    }
    LocalResult.Pass = false;
    LocalResult.Score = 100;
    LocalResult.AddMessage(portalError: Message.PortalError.DecisionError(Local));
    return true;
  }

  private SettingCarFax GetSetting(EnPolicyCode policyCode)
      => _IDecHandler.GetSettingCs<SettingCarFax>(ParamRisk.Create(policyCode: Local.PolicyCode & policyCode,riskLevel: Local.RiskLevel,state: (EnState)Local.State));

  private void SaveToDB(string vin,SettingCarFax setting,Result<CarFaxResponse> response,DecisionResult result,ParsedCarFax parsedResponse,bool fromCache)
  {
    if(!Local.ValidUser)
      return;
    var tblCarFax = GetTblCarFax();
    GetCarFaxError();

    #region Methods

    CarFax GetTblCarFax()
    {
      var table = new CarFax
      {
        AgencyId = Local.AgencyId,
        Deleted = false,
        Environment = _CarFaxHandler.IsProduction ? EnEnvironment.Production : EnEnvironment.Test,
        FalconScore = result.Score,
        FromCache = fromCache,
        Notes = GetNotes(result),
        Pass = result.Pass,
        PhotoModel = result.Pass ? EnPhotoModel.Default : EnPhotoModel.Fail,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        Response = response.Value,
        Risk = setting.Risk,
        RiskLevel = Local.RiskLevel,
        UserId = Local.UserId,
        Ver = setting.Ver,
        Vin = vin
      };
      AddWithPolicyId(table);
      return table;
    }
    void GetCarFaxError()
    {
      if(!parsedResponse.IsServerOff)
        return;
      AddWithPolicyId(tblCarFax.ToCarFaxError());
    }

    #endregion Methods
  }

  #endregion Private Methods
}
