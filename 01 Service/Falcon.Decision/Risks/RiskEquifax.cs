using DecEquifax.DB;
using DecEquifax.Model;
using DecEquifax.Settings;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;

namespace Falcon.Decision.Risks;

public class RiskEquifax:RiskCommon
{
  #region Private Fields

  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskEquifax));
  private readonly IEquifaxHandler _Handler = AppResolver.Get<IEquifaxHandler>();

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.Equifax,Local.State))
      return;
    try
    {
      var setting = GetSetting();
      if(setting.Skip)
        return;
      var req = GetRequest();
      var res = _Handler.Call(req);
      var resVal = res?.Value;

      var equifax = new Equifax
      {
        AgencyId = Local.AgencyId,
        Environment = _Handler.Environmen,
        FalconScore = EnEquifaxScore.NoHit,
        FromCache = res?.Value.FromCache ?? false,
        Pass = true,
        PhotoModel = EnPhotoModel.Default,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        Request = req?.SerializeJson(),
        Response = res?.SerializeJson(),
        RiskLevel = Local.RiskLevel,
        Score = resVal?.Score ?? -1,
        State = (EnState)Local.State,
        TransactionId = Local.Transaction,
        UserId = Local.UserId,
        Ver = setting.Ver,
      };
      if(resVal?.Score is null)
      {
        //Add Error
        AddWithPolicyId(equifax.With(c => c.FalconScore = EnEquifaxScore.ServiceOff).ToError());
      }
      else if(res.Value.ServerStatus != EnStatusCode.Success)
      {
        //Add Error
        AddWithPolicyId(equifax.With(c => c.FalconScore = EnEquifaxScore.ThinFile).ToError());
      }
      else
      {
        (var Row, _) = setting.GetResult(resVal.Score);
        equifax.With(c =>
        {
          c.FalconScore = Row.EfScore;
          c.PhotoModel = Row.PhotoModel;
        });
      }
      RiskHelper.UpdateDiamondModifierPolicyEquifax(this,equifax.FalconScore);
      AddWithPolicyId(equifax);
    }
    catch(Exception ex)
    {
      _Log.Exception(ex: ex);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private void AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    async Task AddAsync()
    {
      try
      {
        await Task.Run(() => _Handler.Add(data));
      }
      catch(Exception ex)
      {
        _Log.Exception(obj: data,msg: $"EquifaxHandler.Add(tbl{typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private EFReq GetRequest()
  {
    var holder = Array.Find(Local.Drivers,c => c.DriverType != En_Driver.Excluded);
    var address = Local.MailingAddress;
    return new EFReq
    {
      AdCity = address.City,
      AdState = address.State,
      AdStreet = address.Street1,
      AdZip = address.ZipCode,
      DOB = holder.DateOfBirth,
      First = holder.FirstName,
      Last = holder.LastName,
      Middle = holder.MiddleName,
    };
  }

  private SettingEquifax GetSetting()
  {
    SettingEquifax setting;
    try
    {
      setting = _IDecHandler.GetSettingCs<SettingEquifax>(ParamRisk.Create(state: (EnState)Local.State)) ?? new SettingEquifax().With(c =>
      {
        c.Skip = true;
        _Log.Error(msg: "Equifax Setting is null",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode });
      });
    }
    catch(Exception ex)
    {
      _Log.Error(msg: "Equifax Setting",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },ex: ex);
      setting = new SettingEquifax().With(c => c.Skip = true);
    }
    return setting;
  }

  #endregion Private Methods
}
