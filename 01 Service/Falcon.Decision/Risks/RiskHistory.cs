using DecHistory.DB;
using DecHistory.Model;
using DecHistory.Table;
using Decisioning.Enums;
using Falcon.Decision.Helper;

namespace Falcon.Decision.Risks;

public class RiskHistory:RiskCommon
{
  #region Private Fields

  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskHistory));
  private readonly IHistoryHandler _IHistoryHandler;
  private bool IsEndorsement;

  #endregion Private Fields

  #region Public Constructors

  public RiskHistory()
  {
    _IHistoryHandler = AppResolver.Get<IHistoryHandler>();
  }

  #endregion Public Constructors

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.History,Local.State))
      return;
    var drivers = GetDrivers();
    var driversOffenses = _IHistoryHandler.SearchOffense(drivers)?.ToArray();
    if(driversOffenses?.Any() != true)
    {
      //cancel if nothing found
      return;
    }
    AddDrivers();

    //Methods
    IEnumerable<Search> GetDrivers()
    {
      var tempDrivers = Local.DriversInsured.Select(c => new Search
      {
        DateOfBirth = c.DateOfBirth,
        FirstName = c.FirstName,
        LastName = c.LastName,
      }).ToList();
      if(Local.Transaction == EnPolicyTransaction.Endorsement)
      {
        if(Local.ExistingParamDecision != null)
        {
          // Remove all drivers that exist in old instance and new instance
          tempDrivers.RemoveAll(qd => Local.ExistingParamDecision.Drivers.ToList().Any(ed => ed.FirstName.IsEqual(qd.FirstName) && ed.LastName.IsEqual(qd.LastName) && ed.DateOfBirth.IsEqual(qd.DateOfBirth)));
          IsEndorsement = true;
        }
      }
      return tempDrivers;
    }

    void AddDrivers()
    {
      var driversGroup = driversOffenses.GroupBy(c => c.DriverId).ToArray();
      var tblHistory = GetHistoryTable();
      var notes = "";
      //Add Drivers
      foreach(var group in driversGroup)
      {
        var first = group.First();
        notes += $"\n{first.LastName}, {first.FirstName}";
        foreach(var item in group)
        {
          notes += $"\n\t{item.Desc}";
        }
        var tblHistoryDriver = new TblHistoryDriver
        {
          DriverId = group.Key,
          StatusId = IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote,
          Score = 100
        };

        tblHistory.Drivers.Add(tblHistoryDriver);
      }
      tblHistory.Score = 100;
      tblHistory.Pass = false;
      AddWithPolicyId(tblHistory);
      LocalResult.Score = 100;
      LocalResult.Pass = false;
      var remarks = $"History Risk: {Message.DiamondNote.FailHighScore}{notes}";
      if(IsEndorsement)
      {
        //Local.AddWorkFlowItem(remarks: remarks,dueDateDays: 3,workflowQueue: EnWorkflowQueue.UnderwritingExceptions);
        //LocalResult.AddMessage(
        //   diamondNote: DecisionResult.GetNote(type: NoteType.Decision,value: remarks));
        LocalResult.AddMessage(
           diamondNote: EnNote.History.SetNote(remarks),
           portalError: "Driver is unacceptable, contact Falcon for further information.");
      }
      else
      {
        LocalResult.AddMessage(
           diamondNote: EnNote.History.SetNote(remarks),
           portalError: Message.PortalError.DecisionError(Local));
      }
      return;
    }
  }

  private T AddWithPolicyId<T>(T data) where T : TblHistory
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    return data;

    async Task AddAsync()
    {
      try
      {
        await Task.Run(() => _IHistoryHandler.Add(data));
      }
      catch(Exception ex)
      {
        throw _Log.Exception(obj: data,msg: $"_IHistoryHandler.Add({typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  #endregion Public Methods

  #region Private Methods

  private TblHistory GetHistoryTable()
         => new()
         {
           AgencyId = Local.AgencyId,
           Pass = LocalResult.Pass,
           PolicyId = Local.PolicyId,
           Status = $"{(IsEndorsement ? EnHugoStatus.Endorsement : EnHugoStatus.Quote)}",
           UserId = Local.UserId,
           PolicyCode = Local.PolicyCode,
           RiskLevel = Local.RiskLevel,
         };

  #endregion Private Methods
}
