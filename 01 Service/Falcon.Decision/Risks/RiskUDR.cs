using Decisioning.Enums;
using DecUdr.DB;
using DecUdr.Model;
using DecUdr.Model.Schema;
using Diamond.C0057.Common.Library.Enumerations;
using Diamond.Common;
using Diamond.Common.Enums;
using Diamond.Common.Objects;
using Falcon.Decision.Helper;
using MaritalStatus = Diamond.C0057.Common.Library.Enumerations.MaritalStatus;

namespace Falcon.Decision.Risks;

public partial class RiskUDR:RiskCommon
{
  #region Private Fields

  private static readonly CacheUDR _Cache = CacheUDR.Me;
  private static readonly DateTime _DefaultDate = new(1900,1,1);
  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskUDR));
  private static readonly IUdrHandler _UdrHandler = AppResolver.Get<IUdrHandler>();

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.UDR,Local.State))
      return;
    List<ClsRetIndivLayout> _Residents = null;
    UdrResponse response = null;
    try
    {
      //Limited or NonOwner Skip
      if(Local.IsLimited || Local.IsNonOwner)
        return;
      var infoNameAddress = GetInfoNameAddress();
      //No information Skip
      if(infoNameAddress == null)
        return;
      //Build request
      var request = _UdrHandler.BuildRequest(infoNameAddress);

      var key = request.GetKey();

      var CheckStatus = _UdrHandler.SqlFirstOrDefault<UdrStatus>("SELECT * FROM Udr.tblUdrStatus WHERE PolicyId=@1;",c => c.AddValues(Local.PolicyId));
      if(CheckStatus?.Key == key)
      {
        return;
      }

      //Checking the cache first
      _Residents = _Cache.Get(key);

      if(_Residents == null)
      {
        //Get response
        response = _UdrHandler.Call(request);
        //Check Response if null save error
        if(response == null)
        {
          SaveDBError(request);
          return;
        }
        _Residents = response?.Residents ?? [];
      }
      var undisclosedDrivers = GetUndisclosedDriver(_Residents);

      // Update UDR status
      var udrStatusResidents = new UdrStatus
      {
        PolicyId = Local.PolicyId,
        Key = _UdrHandler.BuildRequest(infoNameAddress).GetKey()
      };
      foreach(var r in undisclosedDrivers)
      {
        udrStatusResidents.Persons.Add(new Person
        {
          DOB = r.DOB >= _DefaultDate ? r.DOB : _DefaultDate,
          First = r.First,
          Last = r.Last,
          Status = EnUDRStatus.Exclude
        });
      }
      _ = Task.Run(() => _UdrHandler.Add(udrStatusResidents));

      //Add/Update the cache
      _Cache[key] = _Residents;
      //Save to Db and update image
      SaveDB(request,response);
      UpdateImage(undisclosedDrivers);
    }
    catch(Exception ex)
    {
      _Log.Error(ex: ex,obj: Local);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private InfoNameAddress GetInfoNameAddress()
  {
    var mailingAddress = Local.MailingAddress;
    var driver = Local.DriversInsured.FirstOrDefault();
    if(mailingAddress is null || driver is null)
      return default;
    return new InfoNameAddress
    {
      AddrStreet = mailingAddress.Street1,
      AddrCity = mailingAddress.City,
      AddrState = mailingAddress.State,
      AddrZip = mailingAddress.ZipCode,
      NameFirst = driver.FirstName,
      NameLast = driver.LastName
    };
  }

  private IEnumerable<ClsRetIndivLayout> GetNewUndisclosedDrivers(IEnumerable<ClsRetIndivLayout> undisclosedDrivers)
  {
    var dimondDriver = Local.DiamondImage.LOB.RiskLevel.Drivers.Where(c => c.ReasonExcludedDescription == "UDR");

    return undisclosedDrivers.Where(c => !dimondDriver.Any(d => string.Equals(d.Name.FirstName,c.First,StringComparison.OrdinalIgnoreCase) && string.Equals(d.Name.LastName,c.Last,StringComparison.OrdinalIgnoreCase))).ToList();
  }

  private IEnumerable<ClsRetIndivLayout> GetUndisclosedDriver(IEnumerable<ClsRetIndivLayout> residents)
  {
    if(residents == null)
      residents = new List<ClsRetIndivLayout>();
    var topSortedLastName = Local.Drivers[0].LastName.Trim();
    //if not production mock values
    if(!_UdrHandler.IsProduction)
    {
      residents = new MockUndisclosedDrivers(residents,topSortedLastName).GetList();
    }
    return residents
       .Where(r => !Local.Drivers.Any(d =>
          string.Equals(r.First.Trim(),d.FirstName.Trim(),StringComparison.OrdinalIgnoreCase)
          && string.Equals(r.Last.Trim(),d.LastName.Trim(),StringComparison.OrdinalIgnoreCase)
          && (r.DOB == default || r.DOB.Date == d.DateOfBirth.Date)
        ))
        .OrderByDescending(r => string.Equals(r.Last.Trim(),topSortedLastName,StringComparison.OrdinalIgnoreCase))
        .ThenBy(r => r.DOB == default ? 0 : 1)
        .ThenBy(r => r.Last)
        .ThenBy(r => r.First)
        .GroupBy(g => new { First = g.First?.ToLower(),Last = g.Last?.ToLower() })
        .Select(g => g.FirstOrDefault());
  }

  private void RemoveUndisclosedDriversFromImage(IEnumerable<ClsRetIndivLayout> undisclosedDrivers)
  {
    var drivers = Local.DiamondImage.LOB.RiskLevel.Drivers;
    var driversToRemove = drivers
       .Where(c =>
          c.DriverExcludeTypeId == (int)DriverExcludeTypeId.Excluded
          && c.ReasonExcludedDescription == "UDR"
          && !undisclosedDrivers.Any(a => string.Equals(a.Last,c.Name.LastName,StringComparison.OrdinalIgnoreCase) && string.Equals(a.First,c.Name.FirstName,StringComparison.OrdinalIgnoreCase))
       ).ToArray();
    if(driversToRemove.Length == 0)
      return;
    foreach(var driver in driversToRemove)
    {
      driver.DetailStatusCode = 2;
      drivers.Remove(driver);
    }
  }

  private void SaveDB(UdrRequest request,UdrResponse response)
  {
    var tblUdr = new Udr
    {
      UserId = Local.UserId,
      AgencyId = Local.AgencyId,
      PolicyId = Local.PolicyId,
      IsProduction = _UdrHandler.IsProduction
    }
    .With(request)
    .With(response);
    if(tblUdr.PolicyId > 0)
    {
      _ = Task.Run(() => _UdrHandler.Add(tblUdr));
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        tblUdr.PolicyId = policyId;
        _ = Task.Run(() => _UdrHandler.Add(tblUdr));
      });
    }
  }

  private void SaveDBError(UdrRequest request)
  {
    var tblUdrError = new UdrError
    {
      UserId = Local.UserId,
      AgencyId = Local.AgencyId,
      PolicyId = Local.PolicyId,
      IsProduction = _UdrHandler.IsProduction
    }
           .With(request);
    if(tblUdrError.PolicyId > 0)
    {
      Task.Run(() => _UdrHandler.Add(tblUdrError));
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        tblUdrError.PolicyId = policyId;
        _UdrHandler.Add(tblUdrError);
      });
    }
  }

  private void UpdateImage(IEnumerable<ClsRetIndivLayout> undisclosedDrivers)
  {
    var newUndisclosedDrivers = GetNewUndisclosedDrivers(undisclosedDrivers);
    var oldUndisclosedDrivers = undisclosedDrivers.Where(c => !newUndisclosedDrivers.Any(n => string.Equals(n.First,c.First) && string.Equals(n.Last,c.Last))).ToList();

    RemoveUndisclosedDriversFromImage(undisclosedDrivers);

    if(!undisclosedDrivers.Any())
      return;
    var count = oldUndisclosedDrivers.Count;
    if(count < 10 && newUndisclosedDrivers.Any())
    {
      foreach(var driver in newUndisclosedDrivers)
      {
        count++;
        var diamondDriver = new Diamond.Common.Objects.Policy.Driver
        {
          Name = new Name
          {
            DetailStatusCode = (int)StatusCode.Active,
            TypeId = (int)NameType.Personal,
            FirstName = ExtHelper.ToTitleCase(driver.First),
            LastName = ExtHelper.ToTitleCase(driver.Last),
            BirthDate = new InsDateTime(driver.DOB.ToString("yyyy-MM-dd")),
            DLN = "N/A",
            SexId = (int)SexId.Male,
            MaritalStatusId = (int)MaritalStatus.Single
          },
          DriverExcludeTypeId = (int)DriverExcludeTypeId.Excluded,
          LicenseStatusId = (int)LicenseStatus.None,
          RelationshipTypeId = (int)RelationShipTypeId.OtherRelationToPolicyholder,
          ReasonExcludedDescription = "UDR",
        };
        diamondDriver.IsLocked(true);

        Local.DiamondImage.LOB.RiskLevel.Drivers.Add(diamondDriver);
        if(count == 10)
          break;
      }
    }

    if(count > 0)
    {
      //LocalResult.AddMessage(portalWarning: Message.PortalWarning.Udr);
    }
  }

  #endregion Private Methods

  #region Private Classes

  private class MockUndisclosedDrivers
  {
    #region Private Fields

    private static readonly Dictionary<string,int> _WordsToInts = new()
    {
      ["one"] = 1,
      ["two"] = 2,
      ["three"] = 3,
      ["four"] = 4,
      ["five"] = 5,
      ["six"] = 6,
      ["seven"] = 7,
      ["eight"] = 8,
      ["nine"] = 9,
      ["ten"] = 10
    };

    private readonly string _LastName;
    private readonly IEnumerable<ClsRetIndivLayout> _UndisclosedDrivers;

    #endregion Private Fields

    #region Public Constructors

    public MockUndisclosedDrivers(IEnumerable<ClsRetIndivLayout> undisclosedDrivers,string lastName)
    {
      _UndisclosedDrivers = undisclosedDrivers;
      _LastName = lastName?.ToLower().Trim();
    }

    #endregion Public Constructors

    #region Internal Methods

    internal IEnumerable<ClsRetIndivLayout> GetList()
    {
      if(_WordsToInts.ContainsKey(_LastName))
      {
        var num = _WordsToInts[_LastName];
        for(var count = 0;count < num;count++)
        {
          var c = (char)(count + 65);
          yield return new ClsRetIndivLayout { DOBStr = new DateTime(1990 + count,1 + count,1 + count).ToString("yyyyMMdd"),First = $"undisclosed{c}",Last = $"driver{c}" };
        }
      }
    }

    #endregion Internal Methods
  }

  #endregion Private Classes
}
