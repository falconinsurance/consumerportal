using Decisioning.Config;
using Decisioning.Enums;

namespace Falcon.Decision.Risks;

public abstract class RiskCommon:IRiskCommon
{
  #region Public Fields

  public static readonly IDecHandler _IDecHandler = AppResolver.Get<IDecHandler>();
  public static readonly ILogHandler _ILogHandler = AppResolver.Get<ILogHandler>();

  #endregion Public Fields

  #region Private Fields

  private bool _DisposedValue = false;

  #endregion Private Fields

  #region Public Properties

  public ParamDecision Local { get; private set; }

  public DecisionResult LocalResult { get; set; }

  #endregion Public Properties

  #region Public Methods

  public virtual void Check() => throw new Exception("please implement [public override void Check()] in your class");

  public void Dispose()
  {
    DisposeLocal();
    Dispose(disposing: true);
    GC.SuppressFinalize(this);
  }

  public void Start(ParamDecision param,DecisionResult result)
  {
    Init(param);
    try
    {
      Check();
      result.AddResult(result: LocalResult);
      param.Result = result;
      RiskHelper.CheckCombinedAndUpdateDiamondNote(this,result,param.Session);
    }
    catch(Exception ex)
    {
      _ILogHandler.Error(ex: ex);
    }
  }

  #endregion Public Methods

  #region Protected Methods

  protected static bool SkipRiskCheck(EnRiskService enRiskService,En_State enState)
  {
    var _lookup = StaticCache.SetGet("Config.tblAvailableRisk",GetValues);
    return _lookup.TryGetValue(enRiskService,out var statesLookup) && statesLookup.TryGetValue((EnState)enState,out var isNotAvailable) && isNotAvailable;

    static Dictionary<EnRiskService,Dictionary<EnState,bool>> GetValues() => _IDecHandler.Sql("SELECT RiskServiceId, StateId, Available FROM Config.tblAvailableRisk")
     .Select(c => new { RiskService = c.To<EnRiskService>("RiskServiceId"),State = c.To<EnState>("StateId"),IsNotAvailable = !c.To<bool>("Available") })
     .GroupBy(g => g.RiskService)
     .ToDictionary(d => d.Key,d =>
     d.ToDictionary(d2 => d2.State,d2 => d2.IsNotAvailable));
  }

  protected virtual void Dispose(bool disposing)
  {
    if(!_DisposedValue && disposing)
    {
    }
    _DisposedValue = true;
  }

  protected virtual void DisposeLocal()
  {
  }

  #endregion Protected Methods

  #region Private Methods

  private void Init(ParamDecision param)
  {
    Local = param;
    LocalResult = new DecisionResult { CutOffScore = param.CutOffScore };
  }

  #endregion Private Methods
}
