using DecAPlus.DB;
using DecAPlus.Model;
using DecAPlus.Settings;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using Diamond.Common.Enums;
using Diamond.Common.Objects;
using Diamond.Common.Objects.Policy;
using Falcon.Decision.Helper;
using static Decisioning.VeriskCommon.Verisk;
using static Falcon.Tools.FormatConvert;
using FCM = Falcon.Core.Models;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

namespace Falcon.Decision.Risks;

public class RiskAPlus:RiskCommon
{
  #region Private Fields

  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskAPlus));
  private readonly IAPlusHandler _Handler = AppResolver.Get<IAPlusHandler>();

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.APlus,Local.State))
      return;
    var setting = GetSetting();
    if(setting.Skip)
      return;

    var driversViolations = Local.DriversViolations ??= new FCM.DrvsViolations();
    var req = GetRequest();
    var key = req.GetKey();
    if(driversViolations.Status == "Accepted")
    {
      //Save To Diamond + update table APlusPopup
      UpdateDiamond(driversViolations);
      UpdateAPlusPopup(driversViolations);
      CreateDiamondNote(req,driversViolations);
      CreateDiamondTask(driversViolations);
      return;
    }

    //Check if already Accepted before and the key is the same if yes don't continue
    driversViolations = DeserializeJson<FCM.DrvsViolations>(_Handler.GetDrvsViolations(Local.PolicyId)) ?? new FCM.DrvsViolations();
    if(driversViolations.Status == "Accepted")
    {
      //Do Nothing
      return;
    }

    var aplus = new APlus
    {
      AgencyId = Local.AgencyId,
      Environment = _Handler.Environment,
      NumDriver = req.Drivers.Count,
      FromCache = false,
      PolicyCode = Local.PolicyCode,
      PolicyId = Local.PolicyId,
      Request = req.SerializeJson(),
      Response = null,
      RiskLevel = Local.RiskLevel,
      State = (EnState)Local.State,
      TransactionId = Local.Transaction,
      UserId = Local.UserId,
      Ver = setting.Ver,
    };
    if(aplus.NumDriver == 0)
    {
      // Record No MVR to database
      AddWithPolicyId(aplus);
      return;
    }
    var res = _Handler.Call(req);
    aplus.With(c =>
    {
      var val = res?.Value ?? new APlusRes();
      c.Response = val.SerializeJson();
      c.FromCache = val.FromCache;
    });
    // Save To database
    AddWithPolicyId(aplus);
    if(res?.Value?.ServerStatus != EnStatusCode.Success)
    {
      AddWithPolicyId(aplus.ToError());
      return;
    }

    UpdateAPlusPopup(key,res?.Value,setting);

    // If all violations fell off because they were below AmountCutoff, still want a note
    if(!(bool)res?.Value.HasViolations && _Handler.Call(req).Value.HasViolations)
      CreateDiamondNote(req,driversViolations);
  }

  #endregion Public Methods

  #region Private Methods

  private void AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    async Task AddAsync()
    {
      try
      {
        await Task.Run(() => _Handler.Add(data));
      }
      catch(Exception ex)
      {
        _Log.Exception(obj: data,msg: $"APlusHandler.Add(tbl{typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private void CreateDiamondNote(APlusReq req,FCM.DrvsViolations driversViolations)
  {
    var res = _Handler.Call(req);
    var note = "APlus Results:\n";
    var decisionCount = 0;

    if(res?.Value == null)
    {
      note += "Service didn't return successfully or was down during quoting.\n";
    }
    else if(res.Value.HasViolations)
    {
      note += "Response from APlus\n";
      foreach(var violation in res?.Value.Violations)
      {
        note += $"- Name: {violation.First} {violation.Last}, Carrier: {violation.CarrierName}, Claim Number: {violation.ClaimNumber}, Type: AFA, Loss Date: {violation.LossDate}, Total Amount: ${(int)violation.Amount}\n";
      }
      note += "Decision(s) made\n";
      foreach(var driver in driversViolations.Drivers.Where(v => v.AnyViolation))
      {
        note += $"- Name: {driver.First} {driver.Last}\n";
        foreach(var violation in driver.Violations)
        {
          note += $"  - Claim Number: {violation.ClaimNumber}, Type: AFA, Loss Date: {violation.Date}, Decision: {violation.Status}\n";
          decisionCount++;
        }
      }

      // Handle when decision count differs from response violation count meaning some claims got removed based on AmountCutoff
      if(decisionCount == 0)
        note += "All losses found below AFA threshold, not added as violation";
      else if(decisionCount != res.Value.Violations.Count)
        note += "\nSome losses found below AFA threshold, not added as violation";
    }
    else
    {
      note += "No violations found.";
    }

    LocalResult.AddMessage(diamondNote: EnNote.APlus.SetNote(note));
  }

  private void CreateDiamondTask(FCM.DrvsViolations driversViolations)
  {
    // Task for 2 or more accepted violations
    var totalViolations = driversViolations.Drivers.Sum(d => d.Violations.Count(v => v.Status.In("Accepted")));
    if(totalViolations >= 2)
    {
      ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
      {
        Remarks = Message.WorkflowRemark.ClaimAccepted,
        WorkflowQueue = EnWorkflowQueue.UnderwritingExceptions,
        DueDateDays = 30,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        PolicyImgNum = Local.ImageNumber,
        Source = EnRiskService.APlus,
      });
    }
    // Task for Disputed and Duplicates
    foreach(var driver in driversViolations.Drivers)
    {
      if(driver.Violations.Any(v => v.Status.In("Dispute","Duplicate")))
      {
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = Message.WorkflowRemark.ClaimDisputed,
          WorkflowQueue = EnWorkflowQueue.RequestedInformation,
          DueDateDays = 30,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.ImageNumber,
          Source = EnRiskService.APlus,
        });
      }
    }
  }

  private void CreateDiamondTaskNotBound(List<Offense> offenses)
  {
    // Task for 2 or more accepted violations
    if(offenses.Count == 0)
      return;
    var remarks = "APlus-Not bound Violation";
    foreach(var offense in offenses)
    {
      remarks += $"<br/> Name:{offense.FirstName} {offense.LastName}, Offense Data:{offense.DateOfOffense:MM/dd/yyyy} ";
    }

    ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
    {
      Remarks = remarks,
      WorkflowQueue = EnWorkflowQueue.UnderwritingExceptions,
      DueDateDays = 3,
      PolicyCode = Local.PolicyCode,
      PolicyId = Local.PolicyId,
      PolicyImgNum = Local.ImageNumber,
      Source = EnRiskService.APlus,
    });
  }

  private List<ParamDecision.ClsDriver> GetDrivers()
  {
    var drivers = Local.Drivers.ToList();
    if(Local.IsEndorsement)
    {
      if(Local.ExistingParamDecision != null)
      {
        // Remove all drivers that exist in old instance and new instance and DriverType didn't change OR changed from Insured to Excluded
        drivers.RemoveAll(qt => Local.ExistingParamDecision.Drivers.ToList().Any(
            epi => epi.FirstName.IsEqual(qt.FirstName)
            && epi.LastName.IsEqual(qt.LastName)
            && epi.DateOfBirth.IsEqual(qt.DateOfBirth)
            && (epi.DriverType == qt.DriverType || qt.DriverType == En_Driver.Excluded)
        ));

        return drivers;
      }
      return Enumerable.Empty<ParamDecision.ClsDriver>().ToList();
    }

    return drivers;
  }

  private APlusReq GetRequest()
  {
    var req = new APlusReq();

    var drivers = GetDrivers();
    if(drivers.Count == 0)
      return req;
    foreach(var driver in drivers)
    {
      if(driver?.LicenseState.In(En_State.Other) == true || driver?.LicenseState == En_State.NA)
      {
        req.WithDriver(driver.FirstName,driver.LastName,driver.DateOfBirth);
      }
      else
      {
        req.WithDriver(driver.FirstName,driver.LastName,driver.DateOfBirth,driver.LicenseNumber?.Trim(),driver.LicenseState.ToString());
      }
    }
    var Address = Local.MailingAddress ?? Local.GarageAddress;
    Address?.With(c => req.WithAddress(c.State.ToString(),c.City,c.ZipCode.Substring(0,5),c.Street1,GetStreet2(c)));
    var Phone = Local.HomePhone;
    Phone?.With(c => req.WithPhone(c.AreaCode + c.Exchange + c.Suffix));
    return req;

    static string GetStreet2(ParamDecision.ClsAddress streetAddress)
    {
      if(string.IsNullOrWhiteSpace(streetAddress.Street2))
      {
        return null;
      }
      return streetAddress.Street2.Trim();
    }
  }

  private SettingAPlus GetSetting()
  {
    SettingAPlus setting;
    try
    {
      setting = GetSettingAgency() ?? _IDecHandler.GetSettingCs<SettingAPlus>(ParamRisk.Create(state: (EnState)Local.State)) ?? new SettingAPlus().With(c =>
        {
          c.Skip = true;
          _Log.Error(msg: "APlus Setting is null",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode });
        });
    }
    catch(Exception ex)
    {
      _Log.Error(msg: "APlus Setting",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },ex: ex);
      setting = new SettingAPlus().With(c => c.Skip = true);
    }
    return setting;
  }

  private SettingAPlus GetSettingAgency()
  {
    var map = StaticCache.SetGet("AgenciesAPlus",() => _IDecHandler.SqlSet("SELECT [AgencyId],[Json] FROM [Config].[tblAgencyAPlus](NOLOCK)").Select(c => new { AgencyId = c.To<int>("AgencyId"),Setting = c.To<SettingAPlus>("Json") }));
    return map.FirstOrDefault(c => c.AgencyId == Local.AgencyId)?.Setting;
  }

  private void UpdateAPlusPopup(FCM.DrvsViolations drvsViolations)
  {
    var popup = new APlusPopup
    {
      AgencyId = Local.AgencyId,
      APlusKey = drvsViolations.Key,
      DrvsViolations = drvsViolations.SerializeJson(),
      Environment = _Handler.Environment,
      PolicyId = Local.PolicyId,
      StateId = (EnState)Local.State,
      Status = drvsViolations.Status,
      UserId = Local.UserId,
    };
    _Handler.Add(popup);
  }

  private void UpdateAPlusPopup(string key,APlusRes res,SettingAPlus setting)
  {
    var violations = res?.Violations;

    // Don't use violations under 500 but keep them around for notes, etc.
    violations.RemoveAll(v => v.Amount < setting.AmountCutoff || v.LossDate.AgeYear() >= setting.YearCutoff);

    if(violations?.Any() == true)
    {
      var drvsViolations = new FCM.DrvsViolations
      {
        Key = key,
        Status = "Initialized",
      };
      var Drivers = violations
         .GroupBy(c => new { c.DOB,c.First,c.Last })
         .Select(g =>
           new FCM.DrvViolationItem
           {
             DOB = $"{g.Key.DOB:MM/dd/yyyy}",
             First = g.Key.First,
             Last = g.Key.Last,
             Violations = g.Select(i => new FCM.DrvViolation
             {
               Amount = i.Amount,
               CarrierName = i.CarrierName,
               CarrierNameShort = i.CarrierName.Left(20) + "...",
               ClaimNumber = i.ClaimNumber,
               ClaimNumberShort = "..." + i.ClaimNumber.Right(10),
               Date = $"{i.LossDate:MM/dd/yyyy}",
               Status = "Initialized",
               Type = "APlus"
             }).ToList()
           }
         ).ToHashSet();

      // Add All other Drivers
      foreach(var driver in Local.Drivers.Where(c => !c.IsExcluded))
      {
        Drivers.Add(new FCM.DrvViolationItem { DOB = $"{driver.DateOfBirth:MM/dd/yyyy}",First = driver.FirstName,Last = driver.LastName });
      }
      drvsViolations.Drivers = Drivers.ToList();
      var popup = new APlusPopup
      {
        AgencyId = Local.AgencyId,
        APlusKey = key,
        DrvsViolations = drvsViolations.SerializeJson(),
        Environment = _Handler.Environment,
        PolicyId = Local.PolicyId,
        StateId = (EnState)Local.State,
        Status = drvsViolations.Status,
        UserId = Local.UserId,
      };
      _Handler.Add(popup);
    }
    else
    {
      _Handler.DeleteAPlusPopup(Local.PolicyId);
    }
  }

  private void UpdateAppInstanceDriver(IEnumerable<Offense> driverOffense)
  {
    var diaId = RiskHelper.GetDiaIdFromUndeWritingCode(this,EnUnderwritingCode.AtFaultAccident);
    var appInstanceDrivers = Local.Drivers.ToList();
    var notBoundOffences = new List<Offense>();
    foreach(var offense in driverOffense)
    {
      var driverMatch = appInstanceDrivers
       .Find(d => (d?.FirstName.IsEqual(offense.FirstName) == true && d.LastName.IsEqual(offense.LastName) && d.DateOfBirth.IsEqual(offense.DateOfBirth))
       || (d?.DateOfBirth.IsEqual(offense.DateOfBirth) == true)
       );
      if(driverMatch is null)
      {
        notBoundOffences.Add(offense);
      }
      else
      {
        Merge(driverMatch,offense);
      }
    }
    CreateDiamondTaskNotBound(notBoundOffences);
    return;
    void Merge(ParamDecision.ClsDriver driver,Offense offense)
    {
      var appInstanceViolationsId = Violation.FromDiaId(diaId);
      var offenseDate = offense.DateOfOffense;
      if(appInstanceViolationsId != default && !driver.Violations.Any(v => v.Violation == appInstanceViolationsId && v.Date.Equals(offenseDate)))
      {
        driver.Violations.Add(new() { Violation = appInstanceViolationsId,Date = offenseDate });
      }
    }
  }

  private void UpdateDiamond(FCM.DrvsViolations drvsViolations)
  {
    if(!drvsViolations.AnyDriver)
      return;

    var driverOffense = drvsViolations.Drivers.Where(c => c.AnyViolation)
      .SelectMany(c => c.Violations.Where(v => v.Status.In("Initialized","Accepted"))
        .Select(v => new Offense { FirstName = c.First,LastName = c.Last,DateOfBirth = DateTime.Parse(c.DOB),DateOfOffense = DateTime.Parse(v.Date) }));

    if(driverOffense?.Any() != true)
      return;
    if(Local.IsEndorsement)
      UpdateAppInstanceDriver(driverOffense);
    else
      UpdateImageDriver(driverOffense);
    return;
  }

  private void UpdateImageDriver(IEnumerable<Offense> driverOffense)
  {
    var diaId = RiskHelper.GetDiaIdFromUndeWritingCode(this,EnUnderwritingCode.AtFaultAccident);
    var diamondDrivers = Local.DiamondImage.LOB.RiskLevel.Drivers;
    var notBoundOffences = new List<Offense>();
    foreach(var offense in driverOffense)
    {
      var deriverMatch = diamondDrivers
          .Find(d => (d?.Name?.FirstName.IsEqual(offense.FirstName) == true && d.Name.LastName.IsEqual(offense.LastName) && d.Name.BirthDate.IsEqual(offense.DateOfBirth))
           || (d?.Name?.BirthDate.IsEqual(offense.DateOfBirth) == true));
      if(deriverMatch is null)
      {
        notBoundOffences.Add(offense);
      }
      else
      {
        Merge(deriverMatch,offense);
      }
    }
    CreateDiamondTaskNotBound(notBoundOffences);
    return;
    void Merge(Driver driver,Offense offense)
    {
      driver.AccidentViolations ??= [];
      var offenseDate = offense.DateOfOffense.ToInsDateTime();
      if(!driver.AccidentViolations.Any(v => v.AccidentsViolationsTypeId == diaId && v.ConvictionDate.Equals(offenseDate) && v.DetailStatusCode != (int)StatusCode.Deleted))
      {
        // if we don't already have the violation, add it
        var violation = new AccidentViolation
        {
          DetailStatusCode = (int)StatusCode.Active,
          ConvictionDate = offenseDate,
          AvDate = offenseDate,
          AccidentsViolationsTypeId = diaId,
          ViolationSourceId = (int)FDEnums.ViolationSourceId.Other
        };
        driver.AccidentViolations.Add(violation);
      }
    }
  }

  #endregion Private Methods

  #region Private Classes

  private class Offense
  {
    #region Public Properties

    public DateTime DateOfBirth { get; set; }
    public DateTime DateOfOffense { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }

    #endregion Public Properties
  }

  #endregion Private Classes
}
