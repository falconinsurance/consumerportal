using DecEquifax.DB;
using DecEquifax.Model;
using DecEquifax.Settings;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using Lib = Diamond.C0057.Common.Library;

namespace Falcon.Decision.Risks;

public class RiskEquifaxQuote:RiskCommon
{
  #region Private Fields

  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskEquifaxQuote));
  private static SettingEquifaxQuote _Setting = null;
  private readonly IEquifaxHandler _Handler;

  #endregion Private Fields

  #region Public Constructors

  public RiskEquifaxQuote()
  {
    _Handler = AppResolver.Get<IEquifaxHandler>();
  }

  #endregion Public Constructors

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.EquifaxQuote,Local.State))
      return;
    try
    {
      var segment = EnEquifaxScore.ServiceOff; // Connectivity Issue (Not run)
      var cacheVal = _Handler.GetCache(GetRequest());
      var setting = GetSetting();
      if(cacheVal?.FromCache == true && cacheVal.Score > 0)
      {
        var settingEquifax = SettingEquifax();
        (var Row, _) = settingEquifax.GetResult(cacheVal.Score);
        segment = Row.EfScore;
      }
      else
      {
        // If modifier is already set, we've navigated backward, do nothing
        if(!Local.DiamondImage.LOB.PolicyLevel.Modifiers.Any(m => m.ModifierTypeId == (int)Lib.Enumerations.ModifierTypeId.InsuranceScoreSegment))
        {
          switch(Local.RiskCheck)
          {
            case EnRiskCheck.AgencyAppXmlITC:
            case EnRiskCheck.AgencyAppXmlQuomation:
              segment = setting.Mapping.TryGetValue(Local.RaterCreditScore.ToString(),out var enScore) ? enScore : EnEquifaxScore.ServiceOff;
              LocalResult.AddMessage(portalWarning: "Assumed insurance score used for initial rating. Rate may change after bridging.");
              break;

            case EnRiskCheck.ConsumerAppLvl0:
              segment = setting.Default;
              break;
          }
        }
        else
        {
          segment = int.TryParse(Local.DiamondImage.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == (int)Lib.Enumerations.ModifierTypeId.InsuranceScoreSegment)?.ModifierOptionDescription,out var result) ? (EnEquifaxScore)result : EnEquifaxScore.NoScore;
        }
      }
      RiskHelper.UpdateDiamondModifierPolicyEquifax(this,segment);
      AddWithPolicyId(new EquifaxQuote
      {
        AgencyId = Local.AgencyId,
        FalconScore = segment,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        State = (EnState)Local.State,
        UserId = Local.UserId
      });
    }
    catch(Exception ex)
    {
      throw _Log.Exception(ex: ex);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private void AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    async Task AddAsync()
    {
      try
      {
        await Task.Run(() => _Handler.Add(data));
      }
      catch(Exception ex)
      {
        _Log.Exception(obj: data,msg: $"EquifaxHandler.Add(tbl{typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private EFReq GetRequest()
  {
    var holder = Array.Find(Local.Drivers,c => c.DriverType != En_Driver.Excluded);
    var address = Local.MailingAddress;
    return new EFReq
    {
      AdCity = address.City,
      AdState = address.State,
      AdStreet = address.Street1,
      AdZip = address.ZipCode,
      DOB = holder.DateOfBirth,
      First = holder.FirstName,
      Last = holder.LastName,
      Middle = holder.MiddleName,
    };
  }

  private SettingEquifaxQuote GetSetting() => _Setting ??= _IDecHandler.GetSettingCs<SettingEquifaxQuote>(ParamRisk.Create(state: (EnState)Local.State));

  private SettingEquifax SettingEquifax()
  {
    SettingEquifax setting;
    try
    {
      setting = _IDecHandler.GetSettingCs<SettingEquifax>(ParamRisk.Create(state: (EnState)Local.State)) ?? new SettingEquifax().With(c =>
      {
        c.Skip = true;
        _Log.Error(msg: "EquifaxQuote Setting is null",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode });
      });
    }
    catch(Exception ex)
    {
      _Log.Error(msg: "EquifaxQuote Setting",obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },ex: ex);
      setting = new SettingEquifax();
    }

    return setting;
  }

  #endregion Private Methods
}
