using System.IO;
using System.Text.RegularExpressions;
using DecHugo.Class;
using Decisioning.Enums;
using Falcon.Decision.Helper;
using Falcon.FalconDB;
using static Falcon.Decision.ParamDecision;

namespace Falcon.Decision.Risks;

public class RiskFalconPreValidaton:RiskCommon
{
  #region Private Fields
  private readonly IFalconHandler _IFalconHandler = AppResolver.Get<IFalconHandler>();
  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.FalconPreValidation,Local.State))
      return;
    if(Local.Is_CP_Payment)
    {
      return;
    }
    if(Local.IsNewBusiness)
    {
      ValidateNewBusiness();
    }

    switch(Local.RiskCheck)
    {
      case EnRiskCheck.AgencyAppXmlEZLynx:
      case EnRiskCheck.AgencyAppXmlITC:
      case EnRiskCheck.AgencyAppXmlQuickQuote:
      case EnRiskCheck.AgencyAppXmlQuomation:
        ValidateRaters();
        break;

      case EnRiskCheck.ConsumerAppLvl0:
        ValidatePage_01();
        break;

      case EnRiskCheck.ConsumerAppLvl1:
      case EnRiskCheck.ConsumerAppLvl2:
        ValidatePage_02();
        break;

      default:
        break;
    }
  }

  private void ValidateNewBusiness()
  {
    if(Local.DiamondImage.EffectiveDate < Local.SystemDate)
    {
      Local.DiamondImage.With(c =>
      {
        var date = Local.SystemDate.ToInsDateTime();
        c.TransactionEffectiveDate = date;
        c.EffectiveDate = date;
      });
    }
  }


  #endregion Public Methods

  //Endorsement

  #region Private Methods

  private bool DriverNewOrChanged(ClsDriver driverInfo)
  {
    // Exit quick if new driver
    if(driverInfo.Id < 0)
      return true;

    // Check for changed driver, ex. Excluded -> Insured
    if(Local.ExistingParamDecision != null)
    {
      return Local.ExistingParamDecision.Drivers.Any(epi => epi.FirstName.IsEqual(driverInfo.FirstName)
           && epi.LastName.IsEqual(driverInfo.LastName)
           && epi.DateOfBirth.IsEqual(driverInfo.DateOfBirth)
           && epi.DriverType == En_Driver.Excluded && driverInfo.DriverType == En_Driver.Insured);
    }
    return false;
  }

  private void DuplicateDriversOnPolicyValidation()
  {
    var result = Local.Drivers.GroupBy(c => $"{c.FirstName?.ToUpper()}{c.LastName?.ToUpper()}{c.DateOfBirth:yyMMdd}").Any(g => g.Count() > 1);
    if(result)
    {
      LocalResult.AddMessage(portalError: "Driver already exists, please update existing or contact Falcon Underwriting.");
      LocalResult.Pass = false;
    }
  }

  private List<Search> ExcludePreviouslyInsuredDriver()
  {
    var ExistingDrivers = Local.ExistingParamDecision.Drivers.ToList();
    var QuotedDrivers = Local.Drivers.Select(c => new
    {
      c.DateOfBirth,
      c.FirstName,
      c.LastName,
      c.MaritalStatus,
      DriverTypeId = c.IsExcluded ? 3 : 1 // Matches value in falcon enum
    }).ToList();
    // Want to exclude any drivers that were added as part of the endorsement to prevent erroneous Exclusion tasks
    var DriversAdded = QuotedDrivers.Where(driver => !ExistingDrivers.Any(c =>
      c.FirstName.IsEqual(driver.FirstName)
      && c.LastName.IsEqual(driver.LastName)
      && c.DateOfBirth.IsEqual(driver.DateOfBirth))
    ).ToList();
    // Except when an added excluded driver is marked as the spouse
    DriversAdded.RemoveAll(d => d.MaritalStatus == En_MaritalStatus.Married && d.DriverTypeId == 3);

    QuotedDrivers.RemoveAll(quoted => ExistingDrivers.Any(existing =>
      DriversAdded.Contains(quoted) ||
      (existing.FirstName.IsEqual(quoted.FirstName)
      && existing.LastName.IsEqual(quoted.LastName)
      && existing.DateOfBirth.IsEqual(quoted.DateOfBirth)
      && (existing.DriverType == (En_Driver)quoted.DriverTypeId || quoted.DriverTypeId == 1))
    ));
    return QuotedDrivers.ConvertAll(x => new Search { FirstName = x.FirstName,LastName = x.LastName,DateOfBirth = x.DateOfBirth });
  }

  private bool IsValidRoutingNumber(string recurringRoutingNum)
  {
    if(string.IsNullOrEmpty(recurringRoutingNum))
    {
      return true; // Always pass this check if routing number is not present
    }

    var list = StaticCache.SetGet("EFTRoutingNumbers",() => _IFalconHandler.Sql<string>("EXEC [dbo].[spGetValidRoutingNumbers];"));
    return list.Any(c => c == recurringRoutingNum);
  }

  private void RuleAccidentPrevention()
  {
    switch(Local.State)
    {
      case En_State.OK:
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = Message.WorkflowRemark.AccidentPrevention,
          WorkflowQueue = EnWorkflowQueue.RequestedInformation,
          DueDateDays = 3,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.ImageNumber,
          Source = EnRiskService.FalconPreValidation,
        });
        break;
    }
  }

  private void RuleDefensiveDriver()
  {
    switch(Local.State)
    {
      case En_State.IL:
      case En_State.CO:
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = Message.WorkflowRemark.DefensiveDriver,
          WorkflowQueue = EnWorkflowQueue.RequestedInformation,
          DueDateDays = 3,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.ImageNumber,
          Source = EnRiskService.FalconPreValidation,
        });
        break;
    }
  }

  private void RuleHealthStatementOrExcludedDriver()
  {
    if(Local.HasExcludedDriver && !Local.HasHealthStatement)
    {
      switch(Local.State)
      {
        case En_State.IL:
        case En_State.UT:
          AddDriverExcludedTask();
          break;
      }
    }
    else if(Local.HasHealthStatement)
    {
      switch(Local.State)
      {
        case En_State.IL:
          if(Local.HasExcludedDriver)
            AddDriverExcludedTask();

          ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
          {
            Remarks = Message.WorkflowRemark.DriverExclusionAndHealthStatement,
            WorkflowQueue = EnWorkflowQueue.RequestedInformation,
            DueDateDays = 3,
            PolicyCode = Local.PolicyCode,
            PolicyId = Local.PolicyId,
            PolicyImgNum = Local.ImageNumber,
            Source = EnRiskService.FalconPreValidation,
          });
          break;

        case En_State.IN:
        case En_State.OK:
        case En_State.TX:
          AddHealthStatementTask();
          break;

        case En_State.AZ:
          AddHealthStatementTask();
          ParamDecision.AddManualTransactionItem(new DecCommon.Model.ManualTransactionItem
          {
            FormVersionId = 1080,
            Remarks = Message.WorkflowRemark.HealthStatement,
            AdditionalInfo = Message.WorkflowRemark.HealthStatementDescription,
            DueDateDays = 3,
            PolicyCode = Local.PolicyCode,
            PolicyId = Local.PolicyId,
            PolicyImgNum = Local.ImageNumber,
            Source = EnRiskService.FalconPreValidation,
          });
          break;

        case En_State.UT:
          if(Local.HasExcludedDriver)
            AddDriverExcludedTask();

          AddHealthStatementTask();
          ParamDecision.AddManualTransactionItem(new DecCommon.Model.ManualTransactionItem
          {
            FormVersionId = 1286,
            Remarks = Message.WorkflowRemark.HealthStatement,
            AdditionalInfo = Message.WorkflowRemark.HealthStatementDescription,
            DueDateDays = 3,
            PolicyCode = Local.PolicyCode,
            PolicyId = Local.PolicyId,
            PolicyImgNum = Local.ImageNumber,
            Source = EnRiskService.FalconPreValidation,
          });
          break;

        case En_State.CO:
          AddHealthStatementTask();
          ParamDecision.AddManualTransactionItem(new DecCommon.Model.ManualTransactionItem
          {
            FormVersionId = 1417,
            Remarks = Message.WorkflowRemark.HealthStatement,
            AdditionalInfo = Message.WorkflowRemark.HealthStatementDescription,
            DueDateDays = 3,
            PolicyCode = Local.PolicyCode,
            PolicyId = Local.PolicyId,
            PolicyImgNum = Local.ImageNumber,
            Source = EnRiskService.FalconPreValidation,
          });
          break;
      }
    }
  }

  private void AddDriverExcludedTask() => ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
  {
    Remarks = Message.WorkflowRemark.DriverExcluded,
    WorkflowQueue = EnWorkflowQueue.RequestedInformation,
    DueDateDays = 3,
    PolicyCode = Local.PolicyCode,
    PolicyId = Local.PolicyId,
    PolicyImgNum = Local.ImageNumber,
    Source = EnRiskService.FalconPreValidation,
  });

  private void AddHealthStatementTask() => ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
  {
    Remarks = Message.WorkflowRemark.HealthStatement,
    WorkflowQueue = EnWorkflowQueue.RequestedInformation,
    DueDateDays = 3,
    PolicyCode = Local.PolicyCode,
    PolicyId = Local.PolicyId,
    PolicyImgNum = Local.ImageNumber,
    Source = EnRiskService.FalconPreValidation,
  });

  private void RuleDuplicateVIN()
  {
    if(Local.Vehicles.Select(v => v.Vehicle.Vin).Distinct().Count() != Local.Vehicles.Length)
    {
      LocalResult.AddMessage(portalError: "Duplicate VIN listed on policy, please fix and quote again.  Thank you.");
      LocalResult.Pass = false;
    }
  }

  //private void RuleHomeownersDiscount()
  //{
  //switch(Local.State)
  //{
  //  case En_State.TX:
  //  case En_State.OK:
  //  case En_State.UT:
  //    ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
  //    {
  //      Remarks = Message.WorkflowRemark.HomeownersDiscount,
  //      WorkflowQueue = EnWorkflowQueue.RequestedInformation,
  //      DueDateDays = 3,
  //      PolicyCode = Local.PolicyCode,
  //      PolicyId = Local.PolicyId,
  //      PolicyImgNum = Local.ImageNumber ,
  //      Source = EnRiskService.FalconPreValidation,
  //    });
  //    break;
  //}
  //}

  private void RuleMatureDriver()
  {
    switch(Local.State)
    {
      case En_State.UT:
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = Message.WorkflowRemark.MatureDriver,
          WorkflowQueue = EnWorkflowQueue.RequestedInformation,
          DueDateDays = 3,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.ImageNumber,
          Source = EnRiskService.FalconPreValidation,
        });
        break;
    }
  }

  private void RuleNoLicense()
  {
    switch(Local.State)
    {
      case En_State.IN:
        foreach(var driver in Local.Drivers)
        {
          if(driver.LicenseType.In(En_License.NotLicensed) && driver.DriverType.In(En_Driver.Insured))
          {
            LocalResult.AddMessage(
              portalError: $"All drivers must possess U.S. or International/Foreign License, Driver {driver.FirstName} {driver.LastName} does not qualify.",
              diamondNote: EnNote.Validations.SetNote($"Driver {driver.FirstName} {driver.LastName} was quoted as Not Licensed for New Business at some point."));
            LocalResult.Pass = false;
          }
        }
        break;
    }
  }

  //private void RulePermitDriverAsPolicyHolder()
  //{
  //  var policyHolder = Local.Drivers.FirstOrDefault();
  //  if(policyHolder.Driver.LicenseType == LicenseType.Permit)
  //  {
  //    LocalResult.AddMessage(portalError: $"Permit Driver as Primary Insured is an Unacceptable Risk.");
  //    LocalResult.Pass = false;
  //  }
  //}

  private void RuleRequireSpouse()
  {
    if(!Local.Drivers.Any(d => d.Relationship == En_DriverRelationship.Spouse) && /*CDAP-2187*/!(Local.State.In(En_State.OK) && (Local.PolicyCode & EnPolicyCode.NonOwner) == EnPolicyCode.NonOwner))
    {
      const string msg = "Policy with a married policy holder requires a subsequent driver with a Relationship of Spouse be added.";
      if(Local.Is_CP_Page_2)
      {
        LocalResult.AddMessage(portalWarning: msg);
        LocalResult.Pass = true;
      }
      else
      {
        LocalResult.AddMessage(portalError: msg);
        LocalResult.Pass = false;
      }
    }
  }

  private void RuleSignedApplication()
  {
    switch(Local.State)
    {
      case En_State.AZ:
      case En_State.CO:
      case En_State.IN:
      case En_State.OK:
      case En_State.TX:
      case En_State.UT:
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = Message.WorkflowRemark.ESignFollowUp,
          WorkflowQueue = EnWorkflowQueue.RequestedInformation,
          DueDateDays = 3,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.DiamondImage?.PolicyImageNum ?? 1,
          Source = EnRiskService.FalconPreValidation,
        });
        break;
    }
  }

  private void RuleTaskForUMPDRejection()
  {
    switch(Local.State)
    {
      case En_State.CO:
        foreach(var v in Local.Vehicles)
        {
          // If a vehicle is added (Id < 1) and has UMPD Rejected and all previous vehicles had UMPD, we need to add the Rejection Task
          // Otherwise, we already signed the Rejection from a previous image
          if(v.Id < 1 && (v.UMPDLimit == En_UMPDLimit.Rejected && v.ComColDeductible == En_ComColDeductible.NoInsurance)
            && !Local.ExistingParamDecision.Vehicles.Any(o => o.UMPDLimit == En_UMPDLimit.Rejected && o.ComColDeductible == En_ComColDeductible.NoInsurance))
          {
            AddUMPDRejectionTask();
          }
          // Check for Rejection on Existing vehicles which previous had coverage
          else if(v.UMPDLimit == En_UMPDLimit.Rejected && v.ComColDeductible == En_ComColDeductible.NoInsurance)
          {
            foreach(var ev in Local.ExistingParamDecision.Vehicles)
            {
              if(ev.Vin == v.Vin10 && ev.UMPDLimit > En_UMPDLimit.Rejected)
              {
                AddUMPDRejectionTask();
              }
            }
          }
        }
        break;
    }

    void AddUMPDRejectionTask()
    {
      ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
      {
        Remarks = Message.WorkflowRemark.UMPDRejection,
        WorkflowQueue = EnWorkflowQueue.RequestedInformation,
        DueDateDays = 3,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        PolicyImgNum = Local.ImageNumber,
        Source = EnRiskService.FalconPreValidation,
      });
    }
  }

  private void RuleTransferDiscount()
  {
    var rewrittenDetails = _IFalconHandler.Sql<RewrittenPolicyDetails>("EXEC dbo.spGetRewrittenPolicyInfo @PolicyId=@1;",c => c.AddValues(Local.DiamondImage.PolicyId)).FirstOrDefault();
    var limitedDrivers = _IFalconHandler.Sql<Search>("EXEC [dbo].[spGetCurrentRatedDrivers] @PolicyId = @1;",c => c.AddValues(rewrittenDetails.FromPolicyId)).ToList();

    var foundDriverOnNewPolicy = false;

    foreach(var newDriver in Local.Drivers)
    {
      foreach(var limitedDriver in limitedDrivers)
      {
        if(newDriver.FirstName == limitedDriver.FirstName
          && newDriver.LastName == limitedDriver.LastName
          && newDriver.DateOfBirth == limitedDriver.DateOfBirth
          && newDriver.DriverType == En_Driver.Insured)
        {
          foundDriverOnNewPolicy = true;
        }
      }
    }

    if(!foundDriverOnNewPolicy)
    {
      ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
      {
        Remarks = Message.WorkflowRemark.RewriteToStandardMaterialDifference,
        WorkflowQueue = EnWorkflowQueue.RequestedInformation,
        DueDateDays = 3,
        PolicyCode = Local.PolicyCode,
        PolicyId = Local.PolicyId,
        PolicyImgNum = Local.DiamondImage?.PolicyImageNum ?? 1,
        Source = EnRiskService.FalconPreValidation,
      });
    }
  }

  private void RuleValidateVehicleCount()
  {
    if(Local.IsNonOwner && Local.Drivers.Length > Local.ExistingParamDecision.Drivers.Length)
    {
      LocalResult.AddMessage(portalError: "The Named Insured is the ONLY rated driver Allowed on a Non-Owner policy.");
      LocalResult.Pass = false;
    }
    else if((Local.Vehicles.Length == 0 && !Local.IsSR22 && !Local.State.In(En_State.AZ,En_State.CO))
      || (Local.Vehicles.Length == 0 && !Local.PolicyType.In(En_Policy.NonOwner) && Local.State.In(En_State.AZ,En_State.CO)))
    {
      LocalResult.AddMessage(portalError: "At least one vehicle is required.");
      LocalResult.Pass = false;
    }
  }

  private void RuleValidateDriversLicense()
  {
    var map = StaticCache.SetGet("FalconValidation-DriverLicenseRegex",() => _IFalconHandler.SqlSet("Select State,Pattern From DecMVR.Tools.tblDriverLicenseRegex(NOLOCK);").ToDictionary(k => k.To<string>("State"),v => v.To<string>("Pattern")));
    if(Local.Is_CP_Page_2)
    {
      Validate(Local.Drivers.FirstOrDefault());
    }
    else
    {
      foreach(var driverInfo in Local.Drivers)
      {
        Validate(driverInfo);
      }
    }

    void Validate(ClsDriver driverInfo)
    {
      var checkType = "";
      if(driverInfo.DriverType == En_Driver.Insured
        && !driverInfo.LicenseType.In(En_License.ForeignPassport,En_License.International,En_License.Matricula,En_License.NotLicensed,En_License.Permit,En_License.StateID,En_License.TVDL)
        && (Local.IsNewBusiness || (Local.IsEndorsement && DriverNewOrChanged(driverInfo))))
      {
        checkType = "Drivers License";
      }
      /* In AZ, StateID is the same format as DL so check that */
      else if(driverInfo.DriverType == En_Driver.Insured
        && driverInfo.LicenseState.In(En_State.AZ) && driverInfo.LicenseType.In(En_License.StateID)
        && (Local.IsNewBusiness || (Local.IsEndorsement && DriverNewOrChanged(driverInfo))))
      {
        checkType = "State ID";
      }

      if(checkType.Length > 0)
      {
        if(!map.TryGetValue(driverInfo.LicenseState.ToString(),out var pattern) || !Regex.IsMatch(driverInfo.LicenseNumber,pattern))
        {
          LocalResult.Pass = false;
          LocalResult.AddMessage(portalError: $"{checkType} for {driverInfo.FirstName} {driverInfo.LastName} is invalid.  Please correct the format and try again.");
        }
      }
    }
  }

  //First Page
  private void ValidatePage_01()
  {
    if(Local.IsNewBusiness)
    {
      RiskHelper.RulePermitDriverAsPolicyHolder(this);
    }
  }

  //Complete Fill Form
  private void ValidatePage_02()
  {
    // Start Fresh
    ParamDecision.RemoveWorkFlowItem(policyId: Local.PolicyId,enRiskService: EnRiskService.FalconPreValidation,policyCode: Local.PolicyCode);
    ParamDecision.RemoveManualTransactionItem(policyId: Local.PolicyId,enRiskService: EnRiskService.FalconPreValidation);

    if(Local.IsNewBusiness)
    {
      //commenting this out since, we don't want to add "ESignFollowUp" tasks for agent signatures in diamond/AP
      //RuleSignedApplication();

      RuleNoLicense();
      RuleValidateDriversLicense();

      if(Local.State == En_State.TX)
        UpdateViolationsInTX();

      if(!Local.IsNonOwner)
        RuleDuplicateVIN();

      RiskHelper.RulePermitDriverAsPolicyHolder(this);

      if(Local.Drivers[0].MaritalStatus == En_MaritalStatus.Married)
        RuleRequireSpouse();

      if(Local.HasHealthStatement || Local.HasExcludedDriver)
        RuleHealthStatementOrExcludedDriver();

      if(Local.HasAccidentPrevention)
        RuleAccidentPrevention();

      if(Local.HasDefensiveDriver)
        RuleDefensiveDriver();

      if(Local.HasMatureDriver)
        RuleMatureDriver();

      if(Local.IsFullCoverage)
      {
        switch(Local.State)
        {
          case En_State.AZ:
            if(Local.HasPreexistingDamage || Local.HasSafetyEquipmentCoverage)
            {
              LocalResult.AddMessage(portalWarning: Message.WorkflowRemark.PhotosRequired + " Thank you.");
              ParamDecision.PhotoModelAddWorkFlowItem(policyCode: Local.PolicyCode,policyId: Local.PolicyId,policyImgNum: Local.DiamondImage?.PolicyImageNum ?? 1,enRiskService: EnRiskService.FalconPreValidation);
            }
            break;

          case En_State.IL:
          case En_State.IN:
          case En_State.OK:
          case En_State.UT:
          case En_State.CO:
          case En_State.TX:
            if(Local.HasPreexistingDamage)
            {
              LocalResult.AddMessage(portalWarning: Message.WorkflowRemark.PhotosRequired + " Thank you.");
              ParamDecision.PhotoModelAddWorkFlowItem(policyCode: Local.PolicyCode,policyId: Local.PolicyId,policyImgNum: Local.DiamondImage?.PolicyImageNum ?? 1,enRiskService: EnRiskService.FalconPreValidation);
            }
            break;
        }
      }
    }
  }

  // Raters
  private void ValidateRaters()
  {
    if(Local.IsFullCoverage)
    {
      switch(Local.State)
      {
        case En_State.AZ:
          if(Local.IsFullCoverage && Local.Vehicles.Any(v => v.SafetyEquipment))
            LocalResult.AddMessage(portalWarning: Message.WorkflowRemark.PhotosRequired + " Thank you.");
          break;

        case En_State.IN:
          RuleNoLicense();
          if(Local.IsFullCoverage)
            LocalResult.AddMessage(portalWarning: "Pay In Full Pay Plan will include an additional Policy Fee on Down Payment after bridging.");
          break;

        case En_State.TX:
          UpdateViolationsInTX();
          break;
      }
    }
  }

  // CDAP-2488
  private void UpdateViolationsInTX()
  {
    // New Business
    if(Local.IsNewBusiness)
    {
      foreach(var driver in Local.DiamondImage.LOB.RiskLevel.Drivers)
      {
        foreach(var vio in driver.AccidentViolations)
        {
          if(vio.AccidentsViolationsTypeId.In(5,8,9,39,40,45))
            vio.AccidentsViolationsTypeId = 23;
        }
      }
    }
    else
    {
      foreach(var driver in Local.Drivers)
      {
        if(driver.Id < 0)
        {
          foreach(var vio in driver.Violations)
          {
            if(vio.Violation.In(En_Violation.FalseAccidentReport,En_Violation.FleeingAttemptingToEludePolice,En_Violation.HitAndRun,
              En_Violation.MotorVehicleFelony,En_Violation.RefusingToComply,En_Violation.ObstructingOfficer))
            {
              vio.Violation = En_Violation.Other;
            }
          }
        }
      }
    }
  }
}

#endregion Private Methods

