using DecEquifax.Model;
using DecHugo;
using DiamondServices.Apis;
using Falcon.Decision.Helper;
using static Falcon.Tools.Ext;
using Lib = Diamond.C0057.Common.Library;
using LibCo = Diamond.Common.Enums;

namespace Falcon.Decision.Risks;

public static class RiskHelper //IRiskCommon
{
  #region Private Fields

  private static readonly IDiamondApi _DiamondApi = AppResolver.Get<IDiamondApi>();
  private static readonly IHugoHandler _HugoHandler = AppResolver.Get<IHugoHandler>();

  #endregion Private Fields

  #region Internal Methods

  internal static void CheckCombinedAndUpdateDiamondNote(IRiskCommon risk,DecisionResult result,DiamondServices.Service.ISession session)
  {
    CheckCombined();
    UpdateDiamondNote();

    void CheckCombined()
    {
      if(risk.LocalResult.Pass && !result.Pass)
      {
        result.AddMessage(
            portalError: Message.PortalError.DecisionError(risk.Local)
            );
        risk.LocalResult.AddMessage(
            diamondNote: EnNote.Decision.SetNote($"Combined: {Message.DiamondNote.FailHighScore}")
            );
      }
    }
    void UpdateDiamondNote()
    {
      var id = risk.Local.DiamondImage.Policy.PolicyId;
      if(id > 0)
      {
        UpdateDiamondNoteLocal(id);
      }
      else
      {
        risk.Local.UpdatePolicyIdAdd(UpdateDiamondNoteLocal);
      }
    }

    void UpdateDiamondNoteLocal(int policyId)
    {
      _ = Task.Run(() =>
      {
        if(policyId > 0 && risk.LocalResult.DiamondNotes?.Any() == true)
        {
          var noteGroup = risk.LocalResult.DiamondNotes.GroupBy(c => c._Type);
          foreach(var item in noteGroup)
          {
            var note = item.Key.GetDiaNote();
            _DiamondApi.Notes.Create(session,policyId,string.Join("\n",item.Select(c => c._Value)),note.Title,note.Id);
          }
        }
      });
    }
  }

  internal static int GetDiaIdFromUndeWritingCode(IRiskCommon risk,EnUnderwritingCode underwritingCode)
  {
    var map = StaticCache.SetGet("Hugo-DiamondHugoMapping",() => _HugoHandler.SqlSet("SELECT StateId,FalconId,DiaId FROM [Hugo].[tblMapHugoDiamond](NOLOCK);")
    .Select(c => new { Id = (EnUnderwritingCode)c.To<int>("FalconId"),State = (EnState)c.To<int>("StateId"),DiaId = c.To<int>("DiaId") })
    .GroupBy(c => c.State)
    .ToDictionary(g => g.Key,g => g.ToDictionary(i => i.Id,i => i.DiaId)));
    if(map.TryGetValue((EnState)risk.Local.State,out var DicTemp) && DicTemp.TryGetValue(underwritingCode,out var diaId))
    {
      return diaId;
    }
    return map.TryGetValue(EnState.NotSet,out DicTemp) && DicTemp.TryGetValue(underwritingCode,out diaId) ? diaId : -1;
  }

  internal static void RulePermitDriverAsPolicyHolder(IRiskCommon risk)
  {
    if(risk.Local.State.In(En_State.AZ,En_State.IL,En_State.IN,En_State.OK,En_State.UT))
    {
      CheckOnlyFirstDriver();
    }

    void CheckOnlyFirstDriver()
    {
      var policyHolder = risk.Local.Drivers.FirstOrDefault();
      if(policyHolder.LicenseType == En_License.Permit)
      {
        risk.LocalResult.AddMessage(portalError: "Permit Driver as Primary Insured is an Unacceptable Risk.");
        risk.LocalResult.Pass = false;
      }
    }
  }

  internal static void UpdateDiamondModifierPolicyEquifax(IRiskCommon risk,EnEquifaxScore segment)
  {
    const int riskLevelTypeId = (int)Lib.Enumerations.ModifierTypeId.InsuranceScoreSegment;
    var modifiers = risk.Local.DiamondImage.LOB.PolicyLevel.Modifiers;
    if(!modifiers.Any(m => m.ModifierTypeId == riskLevelTypeId))
    {
      modifiers.Add(risk.Local.CreateModifier(riskLevelTypeId));
    }
    var modifier = modifiers.First(m => m.ModifierTypeId == riskLevelTypeId);
    modifier.DetailStatusCode = (int)Insuresoft.RuleEngine.Common.RuleBase.StatusCode.Active;
    modifier.ModifierLevelId = (int)LibCo.Modifiers.ModifierLevel.Policy;
    modifier.ModifierOptionDescription = $"{(int)segment}";
  }

  #endregion Internal Methods
}
