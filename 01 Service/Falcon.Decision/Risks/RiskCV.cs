using DecCV.DB;
using DecCV.Model;
using DecCV.Service;
using DecCV.Settings;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using Falcon.Decision.Helper;
using Newtonsoft.Json;

namespace Falcon.Decision.Risks;

public class RiskCV:RiskCommon
{
  #region Private Fields

  private static readonly ClassCache _Cache = AppCache.Get(nameof(RiskCV));
  private static readonly ICVHandler _Handler = AppResolver.Get<ICVHandler>();
  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskCV));

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.CV,Local.State))
      return;
    var setting = GetSetting();
    if(setting.Skip)
      return;
    //Remove Existing Tasks
    ParamDecision.RemoveWorkFlowItem(policyId: Local.PolicyId,enRiskService: EnRiskService.CV,policyCode: Local.PolicyCode);
    if(!Local.HasTransferDiscount)
    {
      return;
    }
    _Handler.DeleteItem(policyId: Local.PolicyId);
    var name = Local.DiamondImage.PolicyHolder.Name;
    var address = Local.DiamondImage.PolicyHolder.Address;
    var phone = Local.DiamondImage.PolicyHolder.Phones.FirstOrDefault()?.Number;
    var (dlNumber, dlState, previousPolicyNumber) = GetDLInfo();
    //Build The Request
    var request = new CVReq()
      .WithDriver(first: name.FirstName,last: name.LastName,dob: name.BirthDate,dlNumber,dlState,phone,previousPolicyNumber)
      .WithAddress(street: address.StreetName,city: address.City,state: address.StateAbbreviation,zip: address.Zip.Substring(0,5));
    //Get The Response
    var result = _Handler.Call(request)?.Value ?? new() { Result = new(null) };
    var hasServerStatusError = result.ServerStatus != 200;
    //Parse the Response
    var pass = setting.GetResult(result.Result);
    //save the data to table
    var data = GetTblData(setting,request,result,pass);
    AddWithPolicyId(data);
    if(hasServerStatusError)
    {
      //Save the data to error table and exit
      AddWithPolicyId(data.ToError());
      return;
    }
    // Diamond And AP Interaction https://falconinsgroup.atlassian.net/browse/CDAP-2714
    CreateDiamondNote(result,pass,hasServerStatusError,setting);
  }

  #endregion Public Methods

  #region Private Methods

  private static string GetCarriers(CVRes.clsResult result)
  {
    return string.Concat(result.Policies.Select(DisplayPolicyInfo).Take(2));
    static string DisplayPolicyInfo(CVRes.clsPolicy c) => $"{c.Carrier},' Eff Date: '{c.DateEff:yyyy-MM-dd},' Exp Date: '{c.DateExp:yyyy-MM-dd}\n";
  }

  private T AddWithPolicyId<T>(T data) where T : class, IHasPolicyId
  {
    if(data.PolicyId > 0)
    {
      _ = AddAsync();
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId =>
      {
        data.PolicyId = policyId;
        _ = AddAsync();
      });
    }
    return data;

    async Task AddAsync()
    {
      try
      {
        var json = JsonConvert.SerializeObject(data);
        await _Handler.AddAsync(data);
      }
      catch(Exception ex)
      {
        throw _Log.Exception(obj: data,msg: $"_CVHandler.Add({typeof(T).Name}); See object",ex: ex);
      }
    }
  }

  private void CreateDiamondNote(CVRes res,bool pass,bool hasServerStatusError,SettingCV setting)
  {
    EnNote noteType;
    var diamondNote = "CV Result:\n";
    if(hasServerStatusError)
    {
      noteType = EnNote.Verisk_CVInfo;
      diamondNote += "No data returned – Service Unavailable.\n";
    }
    else
    {
      if(pass)
      {
        //Scenario 1
        noteType = EnNote.Verisk_CVInfo;
        diamondNote += "Data confirmed sufficient prior coverage for transfer discount.\n";
      }
      else
      {
        var portalWarning = "<h3>Transfer Discount.</h3>";
        noteType = EnNote.Verisk_CVWarn;
        var result = res.Result;
        if(!result.HasResult)
        {
          //Scenario 2
          diamondNote += "No data returned – Policy does not qualify for transfer discount.\n";
          portalWarning += "Please provide proof of 6 months of Prior Coverage within 72 hours to avoid policy being uprated.<br>";
          portalWarning += "<i>Por favor propocione prueba de 6 meses de cobertura anterior dentro de las 72 horas para evitar que aumente la tasa de la póliza.</i>";
        }
        else if(result.CurrentLapsedDays > setting.CurrentLapsedDays)
        {
          //Scenario 3
          diamondNote += $"Current lapse > {setting.CurrentLapsedDays} days - Policy does not qualify for transfer discount.\nPrior Carrier(s):\n" + GetCarriers(result);
          portalWarning += $"Prior coverage verification show lapse > {setting.CurrentLapsedDays} days.  Please provide Letter of Experience, documenting 6 months of Prior Coverage, within 72 hours to avoid policy being uprated.<br>";
          portalWarning += $"<i>Previa verificación de cobertura muestra lapso > {setting.CurrentLapsedDays} días. Por favor propocione una carta de experiencia, que documente la cobertura de 6 meses anteriores, dentro de las 72 horas para evitar un aumento en la tasa de la póliza.</i>";
        }
        else if(result.TotalCoveredDays < setting.TotalCoveredDays)
        {
          //Scenario 4
          diamondNote += $"Less than {setting.TotalCoveredDays} days of coverage - Policy does not qualify for transfer discount.\nPrior Carrier(s):\n" + GetCarriers(result);
          portalWarning += "Please provide Letter of Experience, documenting 6 months of Prior Coverage, within 72 hours to avoid policy being uprated.<br>";
          portalWarning += "<i>Por favor propocione una carta de experiencia, que documente 6 meses de cobertura anterior, dentro de las 72 horas para evitar un aumento en la tasa de la póliza.</i>";
        }
        if(setting.EnableDiamondTask)
        {
          //create Diamond Task
          ParamDecision.AddWorkFlowItem(new()
          {
            Remarks = Message.WorkflowRemark.TransferDiscount,
            WorkflowQueue = EnWorkflowQueue.UnderwritingExceptions,
            DueDateDays = 3,
            PolicyCode = Local.PolicyCode,
            PolicyId = Local.PolicyId,
            PolicyImgNum = Local.ImageNumber,
            Source = EnRiskService.CV,
          });
        }
        //create AP warning
        if(setting.EnableAPMessage)
        {
          LocalResult.AddMessage(portalWarning: $"{portalWarning}<br>");
        }
      }
    }
    if(setting.EnableDiamondNote)
    {
      LocalResult.AddMessage(diamondNote: noteType.SetNote(diamondNote));
    }
  }

  private (string Number, string State, string PreviousPolicyNumber) GetDLInfo()
  {
    var previousPolicyNumber = Local.DiamondImage.LOB.PolicyLevel.PriorCarrier?.PriorPolicy;
    var driver = Local.Drivers.FirstOrDefault();
    return driver is null
      ? (null, null, previousPolicyNumber)
      : (driver.LicenseNumber, driver.LicenseState.In(En_State.Other) ? null : driver.LicenseState.ToString(), previousPolicyNumber);
  }

  private SettingCV GetSetting()
  {
    return _Cache.SetGet(GetSettingLocal,new(Local.State));

    SettingCV GetSettingLocal()
    {
      const EnPolicyCode policyCode = EnPolicyCode.Default;
      SettingCV setting;
      try
      {
        setting = _IDecHandler.GetSettingCs<SettingCV>(ParamRisk.Create(policyCode: policyCode,state: (EnState)Local.State)) ?? new SettingCV().With(c =>
        {
          c.Skip = true;
          _Log.Error(obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },msg: "CV Setting is null");
        });
      }
      catch(Exception ex)
      {
        _Log.Error(obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },msg: "CV Setting",ex: ex);
        setting = new SettingCV().With(c => c.Skip = true);
      }
      return setting;
    }
  }

  private CvData GetTblData(SettingCV setting,CVReq req,CVRes res,bool pass)
  {
    var table = new CvData
    {
      AgencyId = Local.AgencyId,
      DaysInforced = res?.Result?.TotalCoveredDays ?? -1,
      DaysRecentLapsed = res?.Result?.CurrentLapsedDays ?? -1,
      Deleted = false,
      Duration = res?.Duration ?? 0,
      Environment = _Handler.Environment,
      FromCache = res?.FromCache ?? false,
      Pass = pass,
      PolicyCode = Local.PolicyCode,
      PolicyId = Local.PolicyId,
      Request = req?.GetRequestBody().SerializeJson(),
      Response = res?.SerializeJson(),
      RiskLevel = Local.RiskLevel,
      State = (EnState)Local.State,
      UserId = Local.UserId,
      Ver = setting.Ver,
    };

    return table;
  }

  #endregion Private Methods
}
