using System.Text.RegularExpressions;
using Decisioning.Common;
using Decisioning.Config;
using Decisioning.Enums;
using DecMvr.DB;
using DecMvr.Model;
using DecMvr.Settings;
using Diamond.Common.Enums;
using Diamond.Common.Objects.Policy;
using Falcon.Decision.Helper;
using Falcon.Tools.DBHandler;
using static Decisioning.VeriskCommon.Verisk;
using static Falcon.Decision.ParamDecision;
using static Falcon.Tools.FormatConvert;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;

namespace Falcon.Decision.Risks;

public class RiskMVR:RiskCommon
{
  #region Private Fields

  private static readonly ClassCache _Cache = AppCache.Get(nameof(RiskMVR));
  private static readonly ILog _Log = _ILogHandler.GetLogger(nameof(RiskMVR));
  private static readonly IMvrHandler _Handler = FResolver.Resolve<IMvrHandler>();
  private static readonly TimeSpan _Refresh = TimeSpan.FromHours(2);
  private bool _EnableEvasion;
  private Action<int> _FixCache;

  #endregion Private Fields

  #region Public Methods

  public override void Check()
  {
    if(SkipRiskCheck(EnRiskService.MVR,Local.State))
      return;
    var setting = GetSetting();
    if(setting.Skip)
      return;
    _EnableEvasion = setting.EnableEvasion && Local.IsNewBusiness;
    //CheckEvasion(setting);
    var req = GetRequest(setting);
    var mvr = new MVR
    {
      AgencyId = Local.AgencyId,
      Environment = _Handler.Environment,
      NumDriver = Local.DriversInsured.Length,
      NumFromCache = 0,
      NumMvr = req.Count,
      Pass = true,
      PhotoModel = EnPhotoModel.Default,
      PolicyCode = Local.PolicyCode,
      PolicyId = Local.PolicyId,
      Request = req.SerializeJson(),
      Response = "[]",
      RiskLevel = Local.RiskLevel,
      Score = 0,
      State = (EnState)Local.State,
      TransactionId = Local.Transaction,
      UserId = Local.UserId,
      Ver = setting.Ver,
      MvrType = (req?.Any(c => c.MvrType == 2) == true) ? 2 : 1,
    };
    // Check for Task Only DL States
    ManualMVRRequired(setting.DlStatesTaskOnly,req);
    if(req.Count == 0)
    {
      if(mvr.NumDriver > 0)
      {
        // Record No MVR to database
        AddWithPolicyId(mvr);
      }
      return;
    }
    var res = _Handler.Call(req);
    _FixCache = (policyId) =>
    {
      foreach(var item in res?.Value ?? Array.Empty<MvrRes>())
      {
        _ = Task.Run(() => _Handler.Add(new MvrKeyLink { MvrId = mvr.Id,Key = item.Key,PolicyId = policyId }));
        if(!item.FromCache)
        {
          _ = Task.Run(() => _Handler.UpdateCachePolicyId(item.Key,policyId));
        }
      }
    };

    RegisterMvrCodeHits(res?.Value);

    if(res?.Value is null || res.Value.Any(c => !c.ServerStatus.In(EnStatusCode.Success)))
    {
      AddWithPolicyId(mvr.ToError().With(c => c.Response = res?.SerializeJson()));
    }

    mvr.With(c =>
    {
      var val = res?.Value ?? Enumerable.Empty<MvrRes>();
      c.Response = val.SerializeJson();
      c.NumFromCache = val.Count(c => c.FromCache);
    });
    // Save To database
    AddWithPolicyId(mvr);
    // Check Response Map Violation to Diamond
    UpdateDiamondDriver(res?.Value);
    EvaluateMVRResults(res?.Value);
  }

  #endregion Public Methods

  #region Private Methods

  private static string GetCodeDescription(int code)
  {
    var map = _Cache.SetGet(_Refresh,Call);
    return map.TryGetValue(code,out var desc) ? desc : "Unknown code from Verisk";

    static Dictionary<int,string> Call() => _Handler.SqlSet("SELECT Id,Description FROM MVR.tblMvrViolation(NOLOCK);").ToDictionary(k => k.To<int>("Id"),v => v.To<string>("Description")?.Trim());
  }

  private static En_License GetLicenseStatus(string dlState,string mvrStr)
  {
    if(dlState.IsNullOrWhiteSpace() || mvrStr.IsNullOrWhiteSpace())
    {
      return En_License.NotLicensed;
    }

    var map = _Cache.SetGet(_Refresh,Call);
    mvrStr = mvrStr.Trim().ToUpper();
    dlState = dlState.Trim().ToUpper();
    var statId = StateType.FromStr(dlState);
    if(map.TryGetValue((int)statId,out var DicTemp) && DicTemp.TryGetValue(mvrStr,out var falconId))
    {
      return (En_License)falconId;
    }
    else
    {
      Task.Run(() => _Handler.Exec("EXEC MVR.spSetNotMappedDLStatus @StateId=@1,@State=@2,@DLStatus=@3",c => c.AddValues((int)statId,dlState,mvrStr)));
      return En_License.NotLicensed;
    }

    static Dictionary<int,Dictionary<string,int>> Call() => _Handler.SqlSet("SELECT StateId,Mvr,FalconId FROM MVR.tblMapMvrDLStatus(NOLOCK);")
      .Select(c => new { StateId = c.To<int>("StateId"),Mvr = c.To<string>("Mvr")?.Trim().ToUpper(),FalconId = c.To<int>("FalconId") })
      .GroupBy(c => c.StateId)
      .ToDictionary(g => g.Key,g => g.ToDictionary(i => i.Mvr,i => i.FalconId));
  }

  private static bool GetRequestDriverCheck(SettingMVR setting,ClsDriver driverInfo)
  {
    var age = driverInfo.DateOfBirth.AgeYear();
    var flag = (Logic)driverInfo.DriverType.In(En_Driver.Excluded);
    flag |= setting.DlStatesBlock?.Any(c => c == (int)driverInfo.LicenseState) == true;
    flag |= setting.LicenseTypesBlock?.Any(c => c == driverInfo.LicenseType.ToString()) == true;
    flag |= age > setting.AgeMax;
    flag |= age < setting.AgeMin;
    flag |= setting.SR22DriverOnly && !driverInfo.IsSR22 && age < setting.SR22DriverOnlyAgeMin;
    return !flag;
  }

  private void AddWithPolicyId<T>(T obj) where T : class, IHasPolicyId
  {
    if(obj.PolicyId > 0)
    {
      _ = AddAsync(obj);
    }
    else
    {
      Local.UpdatePolicyIdAdd(policyId => _ = AddAsync(obj.With(c => c.PolicyId = policyId)));
    }
    async Task AddAsync(T temp)
    {
      try
      {
        await Task.Run(() => _Handler.Add(temp));
        if(temp is MVR mvr && _FixCache is not null)
        {
          _ = Task.Run(() => _FixCache(temp.PolicyId));
        }
      }
      catch(Exception ex)
      {
        _Log.Exception(obj: temp,msg: $"MvrHandler.Add(tbl{temp.GetType().Name}); See object",ex: ex);
      }
    }
  }

  private void EvaluateMVRResults(IEnumerable<MvrRes> value)
  {
    // Build Diamond Note (Before De-Dupe)
    var note = "MVR Results:\n";
    string warn = null;
    string warnDL = null;
    string error = null;
    // Service was down
    if(value is null)
    {
      note += "Service didn't return successfully or was down during quoting.\n";
    }
    else
    {
      foreach(var driver in value)
      {
        //Find The Driver
        var driverPortal = Array.Find(Local.Drivers,c => c?.FirstName.IsEqual(driver.FirstName) == true && c.LastName.IsEqual(driver.LastName));
        note += $"Name: {driver.FirstName} {driver.LastName}, License: [{driver.DLState}, {driver.LicenseStatus}, {driverPortal.LicenseNumber}], Order Number: {driver.OrderNumber}\n";

        // Map to LicenseType if NOT an IndexOfActivity, OrderFail, Error, NotReady
        var licenseType = driver.ReportStatus.In(EnReportStatus.IndexOfActivity,EnReportStatus.OrderFail,EnReportStatus.Error,EnReportStatus.NotReady) ? driverPortal.LicenseType : GetLicenseStatus(driver.DLState,driver.LicenseStatus);
        // Check MVR license type for [Expired,NotLicensed,Suspended,Permit]
        if(licenseType.In(En_License.Expired,En_License.NotLicensed,En_License.Suspended,En_License.Permit))
        {
          // Check Portal license type for [Expired,NotLicensed,Suspended,Permit]
          if(driverPortal?.LicenseType.In(En_License.Expired,En_License.NotLicensed,En_License.Suspended,En_License.Permit) == false)
          {
            //Overwrite license type
            note += $"- Provided Drivers License Status differs from MVR, changed to {licenseType.GetLabel()} from {driverPortal.LicenseType.GetLabel()}.\n";
            warnDL += $"{driver.FirstName} {driver.LastName} changed to {licenseType.GetLabel()}.</br>";
            if(Local.IsEndorsement)
            {
              driverPortal.SetLicenseType(licenseType);
            }
            else
            {
              Local.DiamondImage.LOB.RiskLevel.Drivers?
               .Find(c => !c.ExcludedDriver && c?.Name?.FirstName.IsEqual(driver.FirstName) == true && c.Name.LastName.IsEqual(driver.LastName))
               .With(d => d.LicenseStatusId = licenseType.GetDiaId());
            }
          }
          else
          {
            note += "- Provided Drivers License Status agrees with MVR. No changes made.\n";
          }
        }

        switch(driver.ReportStatus)
        {
          case EnReportStatus.Clear:
            note += "- No Violations Returned, Clear Report\n";
            break;

          case EnReportStatus.NoRecordFound:
            note += "- No MVR found. Check if DL is correct and valid\n";
            break;

          case EnReportStatus.OrderFail:
          case EnReportStatus.Error:
            note += "- System Error: No MVR returned.\n";
            break;

          case EnReportStatus.NotReady:
            note += "- System took too long to respond, No MVR returned. Run manually.\n";
            break;

          case EnReportStatus.IndexOfActivity:
            note += "- No Violations Returned, Clear Report via Index of Activity\n";
            break;

          default:
            if(driver.Violations.Any())
            {
              foreach(var violation in driver.Violations)
              {
                var diaId = GetDiaId(violation);
                var label = diaId > 0 ? Violation.FromDiaId(diaId).GetLabel() : "Violation not mapped to Diamond";
                note += $"- Violation: {label}, Date: {violation.ViolationDate.ToShortDateString()}, Code: {violation.CustomCode} - {GetCodeDescription(violation.CustomCode)}\n";
              }
              note += "\n";
            }
            else
            {
              note += $"Name: {driver.FirstName} {driver.LastName}, License Status: {driver.LicenseStatus}, Order Number: {driver.OrderNumber}\n";
              note += "- No Violations on MVR Report\n";
            }
            break;
        }

        // UW Task if name doesn't match Format for Name isn't consistent, sometimes is Last,First
        // and other times it is First,Last
        if(driver.Name is not null)
        {
          if(!(driver.Name.Contains(driver.FirstName,StringComparison.OrdinalIgnoreCase) && driver.Name.Contains(driver.LastName,StringComparison.OrdinalIgnoreCase)))
          {
            ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
            {
              Remarks = $"Name returned from MVR ({driver.Name}) doesn't match name provided on the application ({driver.LastName}, {driver.FirstName}). Get proof of license and run name through Hugo where applicable.\n",
              WorkflowQueue = EnWorkflowQueue.UnderwritingExceptions,
              DueDateDays = 3,
              PolicyCode = Local.PolicyCode,
              PolicyId = Local.PolicyId,
              PolicyImgNum = Local.ImageNumber,
              Source = EnRiskService.MVR,
            });
          }
        }
      }

      // Building Agency Portal Warning for DL Changes
      if(!string.IsNullOrEmpty(warnDL))
      {
        warn += "<b>Provided Drivers License Status differs from MVR for the following drivers:</b></br></br>";
        warn += $"{warnDL}</br>";
      }

      // Build Agency Portal Warning for Violations (After De-Dupe)
      if(value.Any(d => d.Violations.Any() && d.ReportStatus != EnReportStatus.Clear))
      {
        warn += "<b>Violations from public records were found for the following drivers:</b></br></br>";
        foreach(var driver in value)
        {
          if(driver.Violations.Any() && driver.ReportStatus != EnReportStatus.Clear)
          {
            warn += $"{driver.FirstName} {driver.LastName} - {driver.Violations.Count()} Violation(s)</br>";
            foreach(var violation in driver.Violations)
              warn += $"<li>{violation.ViolationDate.ToShortDateString()} - {Violation.FromDiaId(violation.DiaId).GetLabel()}</li>";
          }
        }
      }

      // Build Agency Portal Error
      if(value.Any(d => d.ReportStatus == EnReportStatus.NoRecordFound))
      {
        error = "<b>The following drivers have drivers licenses that were not found in the MVR database, please verify name, date of birth, and drivers license number or remove the driver:</b></br></br>";
        foreach(var driver in value)
        {
          if(driver.ReportStatus == EnReportStatus.NoRecordFound)
          {
            LocalResult.Pass = false;
            error += $"{driver.FirstName} {driver.LastName}</br>";
          }
        }
      }
    }

    // Clear out ghost warn
    if(warn == "</br>")
      warn = null;

    LocalResult.AddMessage(diamondNote: EnNote.MVR.SetNote(note),portalError: error,portalWarning: warn);
  }

  private int GetDiaId(MvrViolation mvrViolation)
  {
    var map = _Cache.SetGet(Call);
    if(map.TryGetValue((EnState)Local.State,out var DicTemp) && DicTemp.TryGetValue(mvrViolation.CustomCode,out var diaId))
    {
      if(diaId.Regex is null)
      {
        return diaId.DiaId;
      }
      foreach(var item in diaId.Regex)
      {
        var match = Regex.Match(mvrViolation.Detail,item);
        if(match.Success && DateTime.TryParseExact(match.Result("$1"),diaId.Format,null,System.Globalization.DateTimeStyles.None,out var result))
        {
          mvrViolation.With(c =>
          {
            c.DiaId = diaId.DiaId;
            c.ViolationDate = result;
          });

          // CDAP-2488
          if(Local.State == En_State.TX && mvrViolation.DiaId.In(5,8,9,39,40,45))
            mvrViolation.DiaId = 23;

          return mvrViolation.DiaId;
        }
      }
      // Regex existed in the DB but there was no match. Violation should be thrown out, not return
      // what is in DB
      return -1;
    }
    return map.TryGetValue(EnState.NotSet,out DicTemp) && DicTemp.TryGetValue(mvrViolation.CustomCode,out diaId) ? diaId.DiaId : -1;

    static Dictionary<EnState,Dictionary<int,DiaIdMapper>> Call()
      => _Handler.SqlSet("SELECT Id,StateId,DiaId,Regex,[Format] FROM MVR.tblMapViolationMvrDiamond(NOLOCK);")
      .Select(c => new { Id = c.To<int>("Id"),State = (EnState)c.To<int>("StateId"),DiaId = c.To<int>("DiaId"),Format = c.To<string>("Format"),Regex = c.To<List<string>>("Regex") })
      .GroupBy(c => c.State)
      .ToDictionary(g => g.Key,g => g.ToDictionary(i => i.Id,i => new DiaIdMapper { DiaId = i.DiaId,Format = i.Format,Regex = i.Regex }));
  }

  private List<ClsDriver> GetDrivers()
  {
    var drivers = Local.Drivers.ToList();
    if(Local.IsEndorsement)
    {
      if(Local.ExistingParamDecision != null)
      {
        // Remove all drivers that exist in old instance and new instance and DriverType didn't
        // change OR changed from Insured to Excluded
        drivers.RemoveAll(qt => Local.ExistingParamDecision.Drivers.ToList().Any(
            epi => epi.FirstName.IsEqual(qt.FirstName)
            && epi.LastName.IsEqual(qt.LastName)
            && epi.DateOfBirth.IsEqual(qt.DateOfBirth)
            && (epi.DriverType == qt.DriverType || qt.DriverType == En_Driver.Excluded)
        ));

        return drivers;
      }
      return Enumerable.Empty<ClsDriver>().ToList();
    }

    return drivers;
  }

  private IList<MvrReq> GetRequest(SettingMVR setting)
  {
    var result = new List<MvrReq>();
    var skip = false;
    if(setting.PolicyCodesBlock?.Count > 0)
    {
      skip = Local.IsPolicyCodeAny(setting.PolicyCodesBlock.ToArray());
    }
    if(skip && setting.PolicyCodesRun?.Count > 0)
    {
      skip = !Local.IsPolicyCodeAny(setting.PolicyCodesRun.ToArray());
    }

    if(skip)
      return result;
    var Drivers = GetDrivers();
    if(Drivers.Count == 0)
      return result;
    result = Drivers.Where(c => GetRequestDriverCheck(setting,c)).Select(GetRequestConvert).ToList();
    if(result.Any())
    {
      var mvrFull = setting.MvrFull ?? new();
      foreach(var item in result)
      {
        item.MvrType = ((mvrFull.SR22Enabled && Local.IsSR22) || (mvrFull.AgeEnabled && item.DOB.AgeYear() >= mvrFull.AgeMin)) ? 2 /*Full Mvr*/: 1/*IOA Mvr*/;
      }
    }

    return result;
  }

  private MvrReq GetRequestConvert(ClsDriver driverInfo) => new()
  {
    DLNumber = driverInfo.LicenseNumber?.Trim(),
    DLState = driverInfo.LicenseState.ToString(),
    DOB = driverInfo.DateOfBirth,
    FirstName = driverInfo.FirstName,
    LastName = driverInfo.LastName,
  };

  private SettingMVR GetSetting()
  {
    SettingMVR setting;
    try
    {
      var policyCode = Local.State.In(En_State.IL,En_State.IN) ? Local.PolicyCode & EnPolicyCode.SR22 : EnPolicyCode.Default;
      setting = _IDecHandler.GetSettingCs<SettingMVR>(ParamRisk.Create(policyCode: policyCode,state: (EnState)Local.State)) ?? new SettingMVR().With(c =>
     {
       c.Skip = true;
       _Log.Error(obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },msg: "MVR Setting is null");
     });
      GetSettingAgency()?.With(c =>
      {
        if(setting.Skip)
          return;
        if(c.AgeMax != 90)
          setting.AgeMax = c.AgeMax;
        if(c.AgeMin != 16)
          setting.AgeMin = c.AgeMin;
        if(c.DlStatesBlock?.Count > 0)
          setting.DlStatesBlock = c.DlStatesBlock;
        if(c.PolicyCodesBlock?.Count > 0)
          setting.PolicyCodesBlock = c.PolicyCodesBlock;
        if(c.LicenseTypesBlock?.Count > 0)
          setting.LicenseTypesBlock = c.LicenseTypesBlock;
        if(c.PolicyCodesRun?.Count > 0)
          setting.PolicyCodesRun = c.PolicyCodesRun;
      });
    }
    catch(Exception ex)
    {
      _Log.Error(obj: new { Local.State,Local.RiskLevel,Local.PolicyCode },msg: "MVR Setting",ex: ex);
      setting = new SettingMVR().With(c => c.Skip = true);
    }
    return setting;
  }

  private SettingMVR GetSettingAgency()
  {
    var map = _Cache.SetGet(local);
    return map.FirstOrDefault(c => c.AgencyId == Local.AgencyId)?.Setting;

    static IEnumerable<AgencySetting> local()
      => _IDecHandler.SqlSet("SELECT [AgencyId],[Json] FROM [Config].[tblAgencyMvr](NOLOCK)")
      .Select(c => new AgencySetting(c));
  }

  private void ManualMVRRequired(List<int> states,IList<MvrReq> req)
  {
    // Traditional for loop because of modification of the list
    for(var i = 0;i < req.Count;i++)
    {
      var stateId = (int)StateType.FromStr(req[i].DLState);
      if(stateId.In(states))
      {
        ParamDecision.AddWorkFlowItem(new DecCommon.Model.WorkFlowItem
        {
          Remarks = $"The driver {req[i].FirstName} {req[i].LastName} has a {req[i].DLState} license and needs an MVR ran manually.",
          WorkflowQueue = EnWorkflowQueue.UnderwritingExceptions,
          DueDateDays = 3,
          PolicyCode = Local.PolicyCode,
          PolicyId = Local.PolicyId,
          PolicyImgNum = Local.ImageNumber,
          Source = EnRiskService.MVR,
        });
        req.RemoveAt(i);
      }
    }
  }


  private void RegisterMvrCodeHits(IEnumerable<MvrRes> value)
  {
    if(value is not null)
    {
      var lst1 = value
        .Where(d => !d.FromCache && d.Violations?.Any() == true)
        .SelectMany(d => d.Violations.Where(GetIsMatch).Select(v => new { v.CustomCode,d.DLState }))
        .GroupBy(k => k)
        .Select(g => new { Code = g.Key.CustomCode,State = (int)Local.State,DLState = StateType.FromStr(g.Key.DLState),Count = g.Count() });
      var lst2 = value
        .Where(d => !d.FromCache)
        .Select(d => new { CustomCode = d.ReportStatus,d.DLState })
        .GroupBy(k => k)
        .Select(g => new { Code = (int)g.Key.CustomCode,State = (int)Local.State,DLState = StateType.FromStr(g.Key.DLState),Count = g.Count() });
      if(lst1.Any() || lst2.Any())
      {
        _Handler.CodeCount(lst1.Concat(lst2));
      }
    }
    bool GetIsMatch(MvrViolation mvrViolation)
    {
      var map = _Cache.SetGet(Call);
      if(map.TryGetValue((EnState)Local.State,out var DicTemp) && DicTemp.TryGetValue(mvrViolation.CustomCode,out var regex))
      {
        foreach(var item in regex)
        {
          var match = Regex.Match(mvrViolation.Detail,item);
          if(match.Success)
          {
            return true;
          }
        }
        return false;
      }
      return true;
    }
    static Dictionary<EnState,Dictionary<int,List<string>>> Call()
      => _Handler.SqlSet("SELECT Id,StateId,Regex FROM MVR.tblMapViolationMvrDiamond(NOLOCK) WHERE Regex is not Null;")
      .Select(c => new { Id = c.To<int>("Id"),State = (EnState)c.To<int>("StateId"),Regex = c.To<List<string>>("Regex") })
      .GroupBy(c => c.State)
      .ToDictionary(g => g.Key,g => g.ToDictionary(i => i.Id,i => i.Regex));
  }

  private void UpdateAppInstanceDriver(IEnumerable<MvrRes> drivers)
  {
    var diamondDrivers = Local.Drivers;
    foreach(var driver in drivers)
    {
      Array.Find(diamondDrivers,d =>
        d?.FirstName.IsEqual(driver.FirstName) == true
        && d.LastName.IsEqual(driver.LastName)
        && d.DateOfBirth.IsEqual(driver.BirthDate)
        )?.With(c => Merge(c,driver.Violations));
    }
    return;
    void Merge(ClsDriver driver,IEnumerable<MvrViolation> violations)
    {
      foreach(var item in violations)
      {
        var offenseDate = item.ViolationDate;
        var violation = Violation.FromDiaId(item.DiaId);

        if(!driver.Violations.Any(v => v.Violation == violation && v.Date.Equals(offenseDate)))
        {
          // if we don't already have the violation, add it
          driver.Violations.Add(new() { Violation = violation,Date = offenseDate });
        }
      }
    }
  }

  private void UpdateDiamondDriver(IEnumerable<MvrRes> drivers)
  {
    if(drivers is null)
      return;
    // Filter drivers with Violations
    var tempDrivers = drivers.Where(c => c.Violations.Any()) ?? Enumerable.Empty<MvrRes>();
    if(!tempDrivers.Any())
      return;

    // map to Diamond remove all DiamonId <1 and then group based on v.DiaId,v.ViolationDate
    tempDrivers = tempDrivers.Select(d => d.With(td => td.Violations = (td.Violations?
     .Select(v => v.With(tv => tv.DiaId = GetDiaId(tv))))
     .Where(v => v.DiaId > 0)
     .GroupBy(v => new { v.DiaId,v.ViolationDate }).Select(g => g.First())));

    // Filter drivers with Violations again
    tempDrivers = tempDrivers?.Where(c => c.Violations.Any()) ?? Enumerable.Empty<MvrRes>();
    if(!tempDrivers.Any())
      return;
    if(Local.IsEndorsement)
      UpdateAppInstanceDriver(tempDrivers);
    else
      UpdateImageDriver(tempDrivers);
  }

  private void UpdateImageDriver(IEnumerable<MvrRes> drivers)
  {
    var diamondDrivers = Local.DiamondImage.LOB.RiskLevel.Drivers;
    foreach(var driver in drivers)
    {
      diamondDrivers
        .Find(d =>
         d?.Name?.FirstName.IsEqual(driver.FirstName) == true
         && d.Name.LastName.IsEqual(driver.LastName)
         && d.Name.BirthDate.IsEqual(driver.BirthDate)
         )
        ?.With(c => Merge(c,driver.Violations));
    }
    return;
    void Merge(Driver driver,IEnumerable<MvrViolation> violations)
    {
      foreach(var item in violations)
      {
        var offenseDate = item.ViolationDate.ToInsDateTime();
        if(!driver.AccidentViolations.Any(v => v.AccidentsViolationsTypeId == item.DiaId && v.ConvictionDate.Equals(offenseDate) && v.DetailStatusCode != (int)StatusCode.Deleted))
        {
          // if we don't already have the violation, add it
          var violation = new AccidentViolation
          {
            DetailStatusCode = (int)StatusCode.Active,
            ConvictionDate = offenseDate,
            AvDate = offenseDate,
            AccidentsViolationsTypeId = item.DiaId,
            ViolationSourceId = (int)FDEnums.ViolationSourceId.Other
          };
          driver.AccidentViolations.Add(violation);
        }
      }
    }
  }

  #endregion Private Methods

  #region Internal Classes

  internal class AgencySetting
  {
    #region Public Constructors

    public AgencySetting(Record rec)
    {
      AgencyId = rec.To<int>("AgencyId");
      Setting = rec.To<SettingMVR>("Json");
    }

    #endregion Public Constructors

    #region Public Properties

    public int AgencyId { get; set; }
    public SettingMVR Setting { get; set; }

    #endregion Public Properties
  }

  #endregion Internal Classes

  #region Private Classes

  private class DiaIdMapper
  {
    #region Public Properties

    public int DiaId { get; set; }
    public string Format { get; set; }
    public List<string> Regex { get; set; }

    #endregion Public Properties
  }

  #endregion Private Classes
}
