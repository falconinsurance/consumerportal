//using DecCarFax.DB;
//using DecCarFax.Model;
//using Falcon.Core.Settings;

//namespace Falcon.Decision.Risks;

//public partial class RiskCarFax:RiskCommon
//{
//  #region Public Classes

//  public class CacheCarFax:CacheTimedDb<CacheCarFax,string,CacheContent>
//  {
//    #region Private Fields

//    private static readonly ConfigSettings _Config = AppResolver.Get<ConfigSettings>();
//    private readonly ICarFaxHandler _ICarFaxHandler = AppResolver.Get<ICarFaxHandler>();

//    #endregion Private Fields

//    #region Public Methods

//    public override void Init()
//    {
//      var sqlServer = _Config.GetDBConnection(nameof(EnDatabase.DecCarFax));
//      ItemDuration = TimeSpan.FromMinutes(30);
//      DbDelete = LocalDbDelete;
//      DbInsert = LocalDbInsert;
//      DbSelect = LocalDbSelect;
//      DbUpdate = LocalDbUpdate;
//    }

//    #endregion Public Methods

//    #region Private Methods

//    private void LocalDbDelete(string key = null) => _ICarFaxHandler.Delete(new CacheStr { Id = key?.Trim().ToUpper() });

//    private void LocalDbInsert(string key,TimedTValue value)
//    {
//      if(key is null)
//        return;
//      var tbl = new CacheStr
//      {
//        Duration = (int)value.Duration.TotalMinutes,
//        Id = key.Trim().ToUpper(),
//      };
//      tbl.SetJson(value.Value);
//      _ = Task.Run(() => _ICarFaxHandler.Add(tbl));
//    }

//    private TimedTValue LocalDbSelect(string key)
//    {
//      var temp = default(TimedTValue);
//      if(key is null)
//        return temp;

//      var tbl = _ICarFaxHandler.Get<CacheStr>(s => s.Where(c => c.Id == key.Trim().ToUpper())).FirstOrDefault();
//      if(tbl != null)
//      {
//        return new TimedTValue
//        {
//          Duration = TimeSpan.FromMinutes(tbl.Duration),
//          Value = tbl.GetJson<CacheContent>()
//        };
//      }
//      return temp;
//    }

//    private void LocalDbUpdate(string key,TimedTValue value)
//    {
//      if(key is null)
//        return;
//      var tbl = new CacheStr
//      {
//        Duration = (int)value.Duration.TotalMinutes,
//        Id = key.Trim().ToUpper(),
//      };
//      tbl.SetJson(value.Value);
//      _ICarFaxHandler.Update(tbl);
//    }

//    #endregion Private Methods
//  }

//  #endregion Public Classes
//}
