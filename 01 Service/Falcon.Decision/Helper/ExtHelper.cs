using Diamond.Common.Objects;
using Diamond.Common.Objects.Policy;

namespace Falcon.Decision.Helper;

public static class ExtHelper
{
  // private static readonly DateTime ZeroTime = new DateTime(1,1,1);

  #region Private Fields
  private static readonly char[] _SeparatorsCleanName = { ' ',',','-','.','‘','&','`','\'' };
  #endregion Private Fields

  #region Public Methods

  public static int AgeMonths(this DateTime @this)
  {
    var today = DateTime.Today;
    return ((today.Year - @this.Year) * 12) + (today.Month - @this.Month) - (today.Day >= @this.Day ? 0 : 1);
  }

  public static int AgeYear(this DateTime @this)
  => @this.AgeMonths() / 12;

  public static string CleanName(string str)
  {
    if(str is null)
      return "";
    str = str.Trim();
    if(str.Any(c => _SeparatorsCleanName.Contains(c)))
    {
      var temp = str.Split(_SeparatorsCleanName);
      return string.Concat(temp);
    }
    return str;
  }

  public static bool Contains(this string source,string value,StringComparison comparisonType)
        => source?.IndexOf(value,comparisonType) >= 0;

  public static void ForEach<T>(this IEnumerable<T> @this,Action<T> action)
  {
    if(@this?.Any() == true && action != null)
    {
      foreach(var item in @this)
      {
        action(item);
      }
    }
  }

  public static bool IsEqual(this InsDateTime @this,DateTime @that)
  => @this.Day == @that.Day && @this.Month == @that.Month && @this.Year == @that.Year;

  public static bool IsEqual(this DateTime @this,DateTime @that)
  => @this.Day == @that.Day && @this.Month == @that.Month && @this.Year == @that.Year;

  public static bool IsEqual(this string @this,string @that)
  => CleanName(@this).Equals(CleanName(@that),StringComparison.OrdinalIgnoreCase);

  public static void IsLocked(this Driver driver,bool isLocked = true) => driver.NonDriverRemark = isLocked ? "LOCKED" : null;

  public static void NotNull<T>(this T @this,Action<T> action)
  {
    if(@this != null && action != null)
    {
      action(@this);
    }
  }

  public static InsDateTime ToInsDateTime(this DateTime? @this)
  => @this.HasValue ? new InsDateTime(@this.Value) : new InsDateTime("1900-01-01");

  public static InsDateTime ToInsDateTime(this DateTime @this)
  => new(@this);

  public static string ToTitleCase(this string @this)
  => @this.IsNullOrWhiteSpace() ? null : UppercaseFirst(@this.ToLower().Trim());

  public static string ToUpperCase(this string @this)
  => @this.IsNullOrWhiteSpace() ? null : @this.Trim().ToUpper();

  public static string TrimNull(string strInput)
  => strInput.IsNullOrWhiteSpace() ? null : strInput.Trim();

  #endregion Public Methods

  #region Private Methods

  private static string UppercaseFirst(string strInput)
  {
    var charArray = strInput.ToCharArray();
    charArray[0] = char.ToUpper(charArray[0]);
    return new string(charArray);
  }

  #endregion Private Methods
}
