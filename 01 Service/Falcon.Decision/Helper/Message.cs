using Decisioning.Enums;

namespace Falcon.Decision.Helper;

public static class Message
{
  #region Public Classes

  public static class DiamondNote
  {
    #region Public Fields
    public const string FailHighScore = "Fail Score is High.";
    public const string FailPolicyCode = "Policy Type or Transaction is not allowed";
    public const string FailPolicyTerm = "Policy Term is not allowed";
    public const string HugoEvasion = "Fail Hugo Evation.";
    public const string PhotoFollowUp = "Review Photo is Required. [Follow-up]";
    public const string PhotoWarning = "Review Photo. [Warning]";
    #endregion Public Fields
  }

  public static class PortalError
  {
    #region Public Fields
    public const string TermError = "The selected policy term is not available. Please select a new term and rerate.";
    #endregion Public Fields

    #region Public Methods

    public static string DecisionError(ParamDecision param,EnRiskService riskService = EnRiskService.Null)
    {
      return param.RiskCheck switch
      {
        EnRiskCheck.AgencyAppXmlQuotePro => GetRaterMessage("Risk is not eligible for agency point of sale binding without further documentation. DO NOT bind or FAX application to Falcon."),
        EnRiskCheck.AgencyAppXmlITC or EnRiskCheck.AgencyAppXmlQuickQuote or EnRiskCheck.AgencyAppXmlQuomation => GetRaterMessage("Risk is not eligible for agency point of sale binding without further documentation."),
        _ => GetDefaultMessage(),
      };

      #region Methods

      string GetRaterMessage(string defaultMessage)
      {
        if(param.State.In(En_State.IN) && riskService == EnRiskService.RedMountain)
        {
          return "Risk is ineligible for online quoting and binding.";
        }
        return defaultMessage;
      }
      string GetDefaultMessage()
      {
        try
        {
          return AppGlobal.Me.ReadAllText(@"Helper\Template\DecisionError.html");
        }
        catch
        {
          return "Contact Falcon Underwriting";
        }

        #endregion Methods
      }
    }

    #endregion Public Methods
  }

  public static class PortalWarning
  {
    #region Public Fields
    public const string PhotoRequired = "Photo is Required.";
    public const string Udr = "Additional drivers were found.</br>Please go to the Drivers tab and click continue.";
    #endregion Public Fields
  }


  public static class WorkflowRemark
  {
    #region Public Fields
    public const string AccidentPrevention = "Policy Issued With Accident Prevention Discount.";
    public const string AccidentPreventionDescription = "Please provide proof of completed accident prevention course.";
    public const string ClaimAccepted = "Review for 2 or more accepted AFA claims from APlus.";
    public const string ClaimDisputed = "Proof of claim dispute is required.";
    public const string ClaimDisputedDescription = "Please provide proof of claim(s) disputed.";
    public const string DefensiveDriver = "Policy Issued With Defensive Driver Discount.";
    public const string DefensiveDriverDescription = "Please provide proof of completed defensive driver course.";
    public const string DriverExcluded = "Named Driver Exclusion form requires signature.";
    public const string DriverExcludedDescription = "Driver added as excluded requires signature of the Named Driver Exclusion form.";
    public const string DriverExclusionAndHealthStatement = "Driver Exclusion and Signed Health Statement Needed.";
    public const string ESignFollowUp = "Follow up for wet signature of application with automatic cancellation for no response.";
    public const string ESignFollowUpDescription = "Signature of application required, please click here to complete.";
    public const string ESignFollowUpDescriptionExcluded = "Signature of application required.";
    public const string HealthStatement = "Completed Health Statement not received.";
    public const string HealthStatementDescription = "Please provide a completed and signed Physician Health Statement for each required driver on policy.";
    public const string HomeownersDiscount = "Policy Issued With Homeowners Discount.";
    public const string HomeownersDiscountDescription = "Please provide proof of Homeowners Discount.";
    public const string MatureDriver = "Policy Issued With Mature Driver Discount.";
    public const string MatureDriverDescription = "Please provide proof of completed mature driver course.";
    public const string PhotosDescription = "Please submit photos of vehicles with full coverage.";
    public const string PhotosReceived = "Photo Model: Please verify that vehicle photos have been received.";
    public const string PhotosReceivedDescription = "Please submit photos of vehicle.";
    public const string PhotosRequired = "Photos are required for all vehicles that have COMP/COLL coverage. Policy may be canceled if photos are not received within 72 hours after binding.";
    public const string PhotosRequiredDescription = "Please provide photos. Photos are required for all vehicles that have COMP/COLL coverage. Policy may be canceled if photos are not received within 72 hours after binding.";
    public const string PhotosRequiredUMPD = "Photos are required for all vehicles that have COMP/COLL/UMPD coverage. Policy may be canceled if photos are not received within 72 hours after binding.";
    public const string PhotosRequiredUMPDDescription = "Please provide photos. Photos are required for all vehicles that have COMP/COLL/UMPD coverage. Policy may be canceled if photos are not received within 72 hours after binding.";
    public const string RewriteToStandardMaterialDifference = "Insured drivers from Standard policy do not match any insured drivers from previous Limited policy.";
    public const string TransferDiscount = "Policy Issued With Transfer Discount.";
    public const string TransferDiscountDescription = "Please provide proof of prior insurance.";
    public const string UMPDRejection = "Signature of UMPD Rejection Form is requried.";
    public const string UMPDRejectionDescription = "Please sign UMPD Rejection Form for each vehicle that has rejected the coverage.";
    #endregion Public Fields
  }
  #endregion Public Classes
}
