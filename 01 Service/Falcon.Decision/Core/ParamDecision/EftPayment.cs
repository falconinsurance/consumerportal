using Diamond.Common.Objects.EFT;

namespace Falcon.Decision
{
  public class EftPayment:PaymentMethod
  {
    #region Public Constructors

    public EftPayment(Eft eft)
    {
      AccountHolder = new(eft);
      EftAccount = new(eft);
    }

    #endregion Public Constructors

    #region Public Properties
    public NestAccountHolder AccountHolder { get; }

    public NestEftAccount EftAccount { get; }
    #endregion Public Properties

    #region Public Classes

    public class NestAccountHolder
    {
      #region Public Constructors

      public NestAccountHolder(Eft eft)
      {
        FullName = eft.AccountHolder;
      }

      #endregion Public Constructors

      #region Public Properties
      public string FullName { get; }
      #endregion Public Properties
    }

    public class NestEftAccount
    {
      #region Public Constructors

      public NestEftAccount(Eft eft)
      {
        AccountNumber = eft.AccountNumber;
        AccountType = BankAccountType.FromDiaId(eft.BankAccountTypeId);
        RoutingNumber = eft.RoutingNumber;
      }

      #endregion Public Constructors

      #region Public Properties
      public string AccountNumber { get; }

      public En_BankAccount AccountType { get; }

      public string RoutingNumber { get; }
      #endregion Public Properties

      #region Public Methods

      public override bool Equals(object obj) => obj is NestEftAccount account
        && account.ToTuple().Equals(ToTuple());

      public override int GetHashCode() => ToTuple().GetHashCode();

      #endregion Public Methods

      #region Private Methods

      private (En_BankAccount AccountType, string RoutingNumber, string AccountNumber) ToTuple() => (AccountType, RoutingNumber, AccountNumber);

      #endregion Private Methods
    }
    #endregion Public Classes
  }
}
