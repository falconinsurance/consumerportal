using Diamond.Common.Objects.Policy;
using Falcon.Tools.VehicleInfoCache;
using FE = Falcon.Enums;

namespace Falcon.Decision;

public partial class ParamDecision
{
  #region Public Classes

  public class ClsVehicle
  {
    #region Private Fields
    private readonly static IVehicleInfoCache _VehicleInfoCache = AppResolver.Get<IVehicleInfoCache>();
    #endregion Private Fields

    #region Public Constructors

    public ClsVehicle(Vehicle vehicle)
    {
      Vehicle = vehicle;
      Vin = vehicle.Vin;
      Vin10 = Vin?.Substring(0,10);
      IsFullCoverage = vehicle.Coverages.Any(c => c.CoverageCodeId == 5 /*COLL*/);

      ComColDeductible = FE.ComColDeductible.FromDiaId(vehicle.Coverages.FirstOrDefault(c => c.CoverageCodeId == 5)?.CoverageLimitId ?? 0);
      ComColDeductibleAmount = ComColDeductible switch
      {
        En_ComColDeductible.OneHundred => 100,
        En_ComColDeductible.TwoHundredFifty => 250,
        En_ComColDeductible.FiveHundred => 500,
        En_ComColDeductible.SevenHundredFifty => 750,
        En_ComColDeductible.OneThousand => 1000,
        _ => 0,
      };
      var vehicleVins = _VehicleInfoCache.GetVehiclesForVin(vehicle.Vin).ToArray();
      if(vehicleVins.Length == 1)
      {
        var vehicleVinMatch = vehicleVins[0];
        BodyType = vehicleVinMatch.BodyType;
        Make = vehicleVinMatch.Make;
        Model = vehicleVinMatch.Model;
        Year = vehicleVinMatch.Year;
        Symbol = vehicleVinMatch.Symbol;
      }
      Id = vehicle.VehicleNum;
      // Update VIN with VIN10
      if(Vin10 is null && Symbol?.Description is not null)
      {
        Vin = Symbol.Description.Substring(0,10);
      }
    }

    #endregion Public Constructors

    #region Public Properties
    public IdDescEntity BodyType { get; }
    public En_ComColDeductible ComColDeductible { get; }
    public int ComColDeductibleAmount { get; }
    public int Id { get; }
    public bool IsFullCoverage { get; }
    public IdDescEntity Make { get; }
    public IdDescEntity Model { get; }
    public List<FalconModifier> Modifiers { get; internal set; } = [];
    public bool SafetyEquipment { get; }
    public IdDescEntity Symbol { get; }
    public En_UMPDLimit UMPDLimit { get; }
    public Vehicle Vehicle { get; }
    public string Vin { get; }
    public string Vin10 { get; }
    public int Year { get; }
    #endregion Public Properties
  }
  #endregion Public Classes
}
