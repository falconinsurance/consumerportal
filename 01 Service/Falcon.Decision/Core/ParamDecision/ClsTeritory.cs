using Falcon.Tools.DBHandler;

namespace Falcon.Decision;

public partial class ParamDecision
{
  #region Private Classes

  private class ClsTeritory
  {
    #region Public Constructors

    public ClsTeritory(Record record)
    {
      Territory = record.To<int>("Territory");
      ZipCode = record.To<int>("ZipCode");
      County = record.To<string>("County");
    }

    #endregion Public Constructors

    #region Public Properties
    public string County { get; }
    public int Territory { get; }
    public int ZipCode { get; }
    #endregion Public Properties
  }
  #endregion Private Classes
}
