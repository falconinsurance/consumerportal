using System.Text.RegularExpressions;
using DecCommon.DB;
using DecCommon.Model;
using Decisioning.Config;
using Decisioning.Enums;
using Diamond.C0057.Common.Library.Enumerations;
using Diamond.Common.Objects.Policy;
using DiamondServices.Apis;
using Falcon.FalconDB;
using FCM = Falcon.Core.Models;
using FE = Falcon.Enums;

namespace Falcon.Decision;

public partial class ParamDecision
{
  #region Public Fields

  public static readonly En_Driver _Driver_Excluded = En_Driver.Excluded;
  public static readonly ICommonHandler _ICommonHandler = AppResolver.Get<ICommonHandler>();
  public static readonly IDecHandler _IDecHandler = AppResolver.Get<IDecHandler>();
  public static readonly IDiamondApi _IDiamondApi = AppResolver.Get<IDiamondApi>();
  public static readonly IFalconHandler _IFalconHandler = AppResolver.Get<IFalconHandler>();

  #endregion Public Fields

  #region Private Fields

  private readonly ClassCache _Cache = new();
  private readonly EnRiskCheck _RiskCheck;
  private readonly List<Action<int>> _UpdatePolicyIdList = [];

  #endregion Private Fields

  #region Public Properties

  //Done
  public string AgencyCode => _Cache.SetGet(() => DiamondImage.Agency.Code);

  //Done
  public int AgencyId => _Cache.SetGet(() => DiamondImage.AgencyId);

  //Done
  public int CutOffScore => _Cache.SetGet(() => _IDecHandler.CutOff(RiskLevel,(EnState)State,IsFullCoverage));

  //Done
  public Image DiamondImage { get; }

  //Done
  public ClsDriver[] Drivers => _Cache.SetGet(GetAllDrivers);

  //Done
  public ClsDriver[] DriversInsured => _Cache.SetGet(() => Drivers.Where(c => c.DriverType == En_Driver.Insured).ToArray());

  public FCM.DrvsViolations DriversViolations { get; set; }

  //Done
  public EftPayment EftRecurring => _Cache.SetGet(GetEftRecurring);

  //Done
  public ParamDecision ExistingParamDecision { get; }

  //Done
  public ClsAddress GarageAddress => _Cache.SetGet(GetGarageAddress);

  //Done
  public bool HasAccidentPrevention => _Cache.SetGet(() => Drivers.Any(c => c.HasAccidentPrevention));

  //Done
  public bool HasAddedVehicle => _Cache.SetGet(() => Vehicles.Any(c => c.Id < 1));

  //Done
  public bool HasDefensiveDriver => _Cache.SetGet(() => Drivers.Any(c => c.HasDefensiveDriver));

  //Done
  public bool HasExcludedDriver => _Cache.SetGet(() => Drivers.Any(c => c.DriverType == En_Driver.Excluded));

  //Done
  public bool HasHealthStatement => _Cache.SetGet(() => UnderwritingQuestionResponses.PhysicalOrMentalImpairment || DriversInsured.Any(c => c.DateOfBirth.GetAge() > 74));

  //Done
  public bool HasHomeownersDiscount => _Cache.SetGet(() => DiamondImage.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == (int)ModifierTypeId.HomeownersDiscount)?.CheckboxSelected == true);

  //Done
  public bool HasMatureDriver => _Cache.SetGet(() => Drivers.Any(c => c.MarriedDriver));

  //Done
  public bool HasPreexistingDamage => _Cache.SetGet(() => UnderwritingQuestionResponses.VehiclePreexistingDamage);

  //Done
  public bool HasSafetyEquipmentCoverage => _Cache.SetGet(() => Vehicles?.Any(v => v.SafetyEquipment) ?? false);

  public bool HasTransferDiscount => _Cache.SetGet(() => DiamondImage.LOB.PolicyLevel.PriorCarrier?.Name?.CommercialName1 is not null);

  //Done
  public ClsPhone HomePhone => _Cache.SetGet(GetHomePhone);

  //Done
  public DateTime ImageEffectiveDate => _Cache.SetGet(() => DiamondImage.EffectiveDate);

  //Done
  public int ImageNumber => _Cache.SetGet(() => DiamondImage.PolicyImageNum < 1 ? 1 : DiamondImage.PolicyImageNum);

  //Done
  public bool Is_CP_Page_1 => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl0);

  //Done
  public bool Is_CP_Page_2 => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl1);

  //Done
  public bool Is_CP_Page_3 => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl2);

  //Done
  public bool Is_CP_Page_4 => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl3);

  public bool Is_CP_Page_5 => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl4);

  //Done
  public bool Is_CP_Payment => _Cache.SetGet(() => RiskCheck == EnRiskCheck.ConsumerAppLvl5);

  //Done
  public bool IsEndorsement => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Endorsement) > 0);

  //Done
  public bool IsEndorsementQuoteOnly => TransactionReasons?.Contains(EnTransReason.QuoteCompColl) ?? false;

  //Done
  public bool IsFullCoverage => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.FullCoverage) > 0);

  //Done
  public bool IsLiability => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Liability) > 0);

  //Done
  public bool IsLimited => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Limited) > 0);

  //Done
  public bool IsNewBusiness => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Endorsement) == 0);

  public bool IsNonOwner => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.NonOwner) > 0);

  //Done
  public bool IsOwner => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Owner) > 0);

  //Done
  public bool IsRewriteToStandard => _Cache.SetGet(() => DiamondImage.LOB.PolicyLevel.Modifiers.FirstOrDefault(m => m.ModifierTypeId == 33)?.CheckboxSelected == true && !IsEndorsement && !IsEndorsementQuoteOnly);

  //Done
  public bool IsSR22 => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.SR22) > 0);

  //Done
  public bool IsStanderd => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Owner) > 0);

  //Done
  public bool IsUnlicensed => _Cache.SetGet(() => (PolicyCode & EnPolicyCode.Unlicensed) > 0);

  //Done
  public ClsAddress MailingAddress => _Cache.SetGet(GetMailingAddress);

  //Done
  public bool Pass { get; set; } = true;

  //Done
  public EnPolicyCode PolicyCode => _Cache.SetGet(GetPolicyCode);

  //Done
  public DateTime PolicyEffectiveDate => _Cache.SetGet(() => DiamondImage.Policy.FirstEffectiveDate);

  //Done
  public int PolicyId => _Cache.SetGet(() => DiamondImage.PolicyId);

  public En_Policy PolicyType => _Cache.SetGet(GetPolicyType);
  public string QuoteSource { get; internal set; }
  public En_RaterCreditScore RaterCreditScore { get; internal set; }
  public DecisionResult Result { get; set; }
  public EnRiskCheck RiskCheck => _Cache.SetGet(() => (IsRewriteToStandard) ? EnRiskCheck.AgencyAppRewriteToStandard : _RiskCheck);
  public EnRiskLevel RiskLevel => _Cache.SetGet(() => _IDecHandler.GetRiskLevel((EnState)State,IsFullCoverage,AgencyId,Territory));
  public DiamondServices.Service.ISession Session => _IDiamondApi.Login.Session_ConsumerService;
  public En_State State => _Cache.SetGet(() => _IDiamondApi.Static.GetState(VersionId));
  public En_TermLength TermLength => _Cache.SetGet(() => FE.TermLength.FromDiaId(DiamondImage.PolicyTermId));
  public DateTime SystemDate => _Cache.SetGet(() => _IDiamondApi.SystemDate.Date);
  public int Territory => _Cache.SetGet(GetTerritory);
  public EnPolicyTransaction Transaction => _Cache.SetGet(GetPolicyTransaction);
  public List<EnTransReason> TransactionReasons { get; set; }
  public UnderwritingQuestionResponses UnderwritingQuestionResponses => _Cache.SetGet(GetUnderwritingQuestionResponses);
  public int UserId => _Cache.SetGet(() => DiamondImage.TransactionUsersId);
  public bool ValidUser => UserId != 2882;
  public ClsVehicle[] Vehicles => _Cache.SetGet(GetAllVehicles);
  public int VersionId => _Cache.SetGet(() => DiamondImage.VersionId);
  public List<WorkFlowItem> WorkFlowItems { get; } = [];

  #endregion Public Properties

  #region Public Constructors

  public ParamDecision(EnRiskCheck riskCheck,FCM.AttachedTypes attachedTypes,Image currentImage,Image previousImage = null) : this(riskCheck,attachedTypes,currentImage)
  {
    if(currentImage is null)
    {
      throw new ArgumentNullException(nameof(currentImage));
    }

    if(previousImage is not null)
    {
      ExistingParamDecision = new(riskCheck,attachedTypes,previousImage);
    }
  }

  #endregion Public Constructors

  #region Private Constructors

  private ParamDecision(EnRiskCheck riskCheck,FCM.AttachedTypes attachedTypes,Image currentImage)
  {
    DiamondImage = currentImage ?? throw new ArgumentNullException(nameof(currentImage));
    DriversViolations = attachedTypes.DriversViolations;
    _RiskCheck = riskCheck;
  }

  #endregion Private Constructors

  #region Public Methods

  public static void AddManualTransactionItem(ManualTransactionItem manualTransactionItem)
  {
    if(manualTransactionItem.PolicyId > 0)
    {
      Task.Run(() => _ICommonHandler.Add(manualTransactionItem));
    }
  }

  public static void AddWorkFlowItem(WorkFlowItem workFlowItem)
  {
    if(workFlowItem.PolicyId > 0)
    {
      Task.Run(() => _ICommonHandler.Add(workFlowItem));
    }
  }

  public static void PhotoModelAddWorkFlowItem(EnPolicyCode policyCode,int policyId,int policyImgNum,EnRiskService enRiskService)
  {
    if(policyId > 0)
    {
      var workFlowItem = new WorkFlowItem
      {
        DueDateDays = 3,
        PolicyCode = policyCode,
        PolicyId = policyId,
        PolicyImgNum = policyImgNum,
        Remarks = Helper.Message.WorkflowRemark.PhotosReceived,
        WorkflowQueue = EnWorkflowQueue.RequestedInformation,
        Source = enRiskService,
      };
      Task.Run(() => _ICommonHandler.Add(workFlowItem));
    }
  }

  public static void PhotoModelRemoveWorkFlowItem(int policyId,EnRiskService enRiskService)
  {
    if(policyId > 0)
    {
      _ICommonHandler.Exec("DELETE FROM tblWorkFlowItem WHERE PolicyId=@1 AND Source=@2;",c => c.AddValues(policyId,(int)enRiskService));
    }
  }

  public static void RemoveManualTransactionItem(int policyId,EnRiskService enRiskService)
  {
    if(policyId > 0)
    {
      _ICommonHandler.Exec("DELETE FROM tblManualTransactionItem WHERE PolicyId=@1 AND Source=@2;",c => c.AddValues(policyId,(int)enRiskService));
    }
  }

  public static void RemoveWorkFlowItem(int policyId,EnRiskService enRiskService,EnPolicyCode policyCode)
  {
    if(policyId > 0)
    {
      if((policyCode & EnPolicyCode.Endorsement) > 0)
        _ICommonHandler.Exec("DELETE FROM tblWorkFlowItem WHERE PolicyId=@1 AND Source=@2 AND (PolicyCode & 32 > 0);",c => c.AddValues(policyId,(int)enRiskService));
      else
        _ICommonHandler.Exec("DELETE FROM tblWorkFlowItem WHERE PolicyId=@1 AND Source=@2 AND (PolicyCode & 16 > 0);",c => c.AddValues(policyId,(int)enRiskService));
    }
  }

  public Modifier CreateModifier(int modifierTypeId) => _IDiamondApi.Static.CreateModifier(modifierTypeId,VersionId);

  public bool IsPolicyCode(EnPolicyCode enPolicyCode) => (PolicyCode & enPolicyCode) > 0;

  public bool IsPolicyCodeAll(params EnPolicyCode[] enPolicyCodes) => enPolicyCodes.All(c => (PolicyCode & c) > 0);

  public bool IsPolicyCodeAny(params EnPolicyCode[] enPolicyCodes) => enPolicyCodes.Any(c => (PolicyCode & c) > 0);

  public bool TestCheckRisk()
  {
    var flag = (Logic)Pass;
    flag &= RiskCheck > EnRiskCheck.None;
    flag &= DiamondImage != null;
    return flag;
  }

  public void UpdatePolicyIdAdd(Action<int> item) => _UpdatePolicyIdList.Add(item);

  #endregion Public Methods

  #region Internal Methods

  internal string[] GetVins()
    => Vehicles
  .Select(c => c.Vin10?.Trim().ToUpper() ?? "")
  .Where(c => c.Length == 17 && !Regex.Match(c,"(.)\\1{3,}").Success)
  .ToArray();

  internal void UpdatePolicyId(int policyId)
  {
    if(PolicyId >= 1 || policyId <= 0)
    {
      return;
    }

    foreach(var item in _UpdatePolicyIdList)
    {
      Task.Run(() => item(policyId));
    }
  }

  #endregion Internal Methods

  #region Private Methods

  private ClsDriver[] GetAllDrivers()
  {
    if(DiamondImage.LOB.RiskLevel.Drivers is null)
      return Array.Empty<ClsDriver>();
    return DiamondImage.LOB.RiskLevel.Drivers
        //.Where(c => !string.IsNullOrEmpty(c.FirstName) && !string.IsNullOrEmpty(c.LastName))
        .Select(GetDriver).ToArray();

    ClsDriver GetDriver(Driver c) => new(Session,PolicyId,c,State);
  }

  private ClsVehicle[] GetAllVehicles()
  {
    if(DiamondImage.LOB.RiskLevel?.Vehicles?.Any() != true)
      return Array.Empty<ClsVehicle>();
    return DiamondImage.LOB.RiskLevel.Vehicles
        .Select(GetVehicle).ToArray();

    static ClsVehicle GetVehicle(Vehicle c) => new ClsVehicle(c);
  }

  private EftPayment GetEftRecurring()
  {
    var paymentType = PaymentType.FromDiaId(DiamondImage.CurrentPayplanId);
    if(paymentType == En_Payment.ACH)
    {
      var eft = DiamondImage.Policy.EFT;
      if(eft?.EftAccountId > 0)
      {
        return new(eft);
      }
    }
    return null;
  }

  private ClsAddress GetGarageAddress()
  {
    foreach(var item in Vehicles)
    {
      if(!string.IsNullOrEmpty(item.Vehicle?.GaragingAddress?.Address?.Zip))
      {
        return new(item.Vehicle.GaragingAddress.Address);
      }
    }
    return null;
  }

  private ClsPhone GetHomePhone()
  {
    var phones = DiamondImage.PolicyHolder.Phones;
    if(phones is null)
      return null;
    if(phones.FirstOrDefault(c => c.DetailStatusCode == (int)Diamond.Common.Enums.StatusCode.Active) is Diamond.Common.Objects.Phone phone)
    {
      return GetPhone(phone);
    }
    return null;
    static ClsPhone GetPhone(Diamond.Common.Objects.Phone phone)
    {
      var clsPhone = new ClsPhone(phone);
      return clsPhone.Success ? clsPhone : null;
    }
  }

  private ClsAddress GetMailingAddress() => string.IsNullOrWhiteSpace(DiamondImage.PolicyHolder?.Address?.Zip) ? null : (new(DiamondImage.PolicyHolder.Address));

  private EnPolicyCode GetPolicyCode()
  {
    var result = EnPolicyCode.Default;
    //Check Unlicensed
    result |= (DriversInsured?.Any(c => _LstUnLicensedGroup.Any(d => d == c.LicenseType))) switch
    {
      true => EnPolicyCode.Unlicensed,
      _ => EnPolicyCode.Licensed,
    };

    //Check FullCoverage
    result |= State switch
    {
      //EnState.TX or EnState.IL => AppInstance?.Vehicles?.Any(c => !(c.ComColDeductible == En_ComColDeductible.NoInsurance && c.UMPDLimit == En_UMPDLimit.None)) == true ? EnPolicyCode.FullCoverage : EnPolicyCode.Liability,
      //EnState.AZ or EnState.IN or EnState.OK or EnState.UT or EnState.CO => AppInstance?.Vehicles?.Any(c => c.ComColDeductible != En_ComColDeductible.NoInsurance) == true ? EnPolicyCode.FullCoverage : EnPolicyCode.Liability,
      //_ => EnPolicyCode.Default,
      _ => Vehicles?.Any(c => c.ComColDeductibleAmount != 0) == true ? EnPolicyCode.FullCoverage : EnPolicyCode.Liability
    };

    result |= (PolicyType) switch
    {
      En_Policy.NonOwner => EnPolicyCode.NonOwner,//Check NonOwner
      En_Policy.Owner => EnPolicyCode.Owner,//Check NonOwner
      En_Policy.Limited => EnPolicyCode.Limited,//Check Limited
      _ => EnPolicyCode.Default,
    };

    //Check SR22
    result |= (Drivers.Any(c => c.IsSR22)) switch
    {
      true => EnPolicyCode.SR22,
      _ => EnPolicyCode.Default,
    };

    //Check Transaction
    result |= Transaction switch
    {
      EnPolicyTransaction.Endorsement => EnPolicyCode.Endorsement,
      EnPolicyTransaction.NewPolicy => EnPolicyCode.NewPolicy,
      EnPolicyTransaction.RenewPolicy => EnPolicyCode.Renewal,
      _ => EnPolicyCode.Default,
    };
    return result;
  }

  private EnPolicyTransaction GetPolicyTransaction() => DiamondImage.TransactionTypeId switch
  {
    2 => EnPolicyTransaction.NewPolicy,
    3 => EnPolicyTransaction.Endorsement,
    4 => EnPolicyTransaction.RenewPolicy,
    _ => EnPolicyTransaction.NewPolicy
  };

  private En_Policy GetPolicyType()
  {
    var policyType = En_Policy.NoVal;
    var limited = DiamondImage.LOB.PolicyLevel.Modifiers.LastOrDefault(m => m.ModifierTypeId == (int)ModifierOptionId.Limited)?.CheckboxSelected == true;
    policyType = limited ? En_Policy.Limited : En_Policy.Owner;
    if(DiamondImage.LOB.RiskLevel.Vehicles.Any(v => v.NonOwned))
    {
      policyType = En_Policy.NonOwner;
    }
    return policyType;
  }

  private int GetTerritory()
  {
    var ret = DiamondImage?.LOB?.RiskLevel?.Vehicles?.FirstOrDefault()?.TerritoryNum ?? 0;

    if(ret == 0)
    {
      var address = MailingAddress ?? GarageAddress;
      var county = address.County.ToUpper();
      var state = address.State;
      var zipCode = int.Parse(address.ZipCode ?? "0");

      ret = LocalTerritory(state,zipCode,county);
    }

    return ret;

    static int LocalTerritory(string state,int zipCode,string county)
    {
      var lookup = StaticCache.SetGet("ParamDecision.AllTerritories",LocalAllTerritories);

      if(lookup.TryGetValue(state,out var territories))
      {
        ClsTeritory item;
        if(state.In("TX"))
        {
          item = territories.Find(c => c.County == county && c.ZipCode == zipCode);
          if(item is not null)
          {
            return item.Territory;
          }
        }
        item = territories.Find(c => c.ZipCode == zipCode);
        if(item is not null)
        {
          return item.Territory;
        }
      }
      return state.ToUpper() switch
      {
        "AZ" => 99,
        "CO" => 999,
        "IL" => 992000,
        "IN" => 999,
        "OK" => 99,
        "TX" => 215,
        "UT" => 999,
        _ => 0,
      };
    }
    static Dictionary<string,List<ClsTeritory>> LocalAllTerritories()
    {
      var set = _IFalconHandler.SqlSet("Exec spGetTerritories");
      return set.GroupBy(c => c.To<string>("State")).ToDictionary(g => g.Key,g => g.Select(r => new ClsTeritory(r)).ToList());
    }
  }

  private UnderwritingQuestionResponses GetUnderwritingQuestionResponses()
  {
    var res = new UnderwritingQuestionResponses();

    var underwritingQuestionResponses = DiamondImage.LOB.PolicyLevel.PolicyUnderwritings;
    if(underwritingQuestionResponses is not null)
    {
      var val = underwritingQuestionResponses
        .FirstOrDefault(c => c.PolicyUnderwritingCodeId.In(32,37,44,8));
      if(val is not null)
      {
        res.PhysicalOrMentalImpairment = val.PolicyUnderwritingAnswer == 1;
      }

      val = underwritingQuestionResponses
        .FirstOrDefault(c => c.PolicyUnderwritingCodeId.In(31,7,47,52));
      if(val is not null)
      {
        res.VehiclePreexistingDamage = val.PolicyUnderwritingAnswer == 1;
      }
    }

    return res;
  }

  #endregion Private Methods
}
