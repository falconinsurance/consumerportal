using Diamond.Common.Objects;

namespace Falcon.Decision;

public partial class ParamDecision
{
  #region Public Classes

  public class ClsAddress
  {
    #region Public Constructors

    public ClsAddress(Address address)
    {
      City = address.City;
      County = (address.County ?? "").Trim();
      State = address.StateAbbreviation;
      StateEn = StateType.FromDiaId(address.StateId);
      Street1 = GetStreet1(address);
      Street2 = address.ApartmentNumber;
      ZipCode = address.Zip.Substring(0,5);

      static string GetStreet1(Diamond.Common.Objects.Address address) => address.POBox.IsNullOrWhiteSpace()
        ? address.HouseNumber.IsNullOrWhiteSpace() ? address.StreetName : address.HouseNumber + " " + address.StreetName
        : "P.O. Box " + address.POBox;
    }

    #endregion Public Constructors

    #region Public Properties
    public string City { get; }
    public string County { get; }
    public string State { get; }
    public En_State StateEn { get; }
    public string Street1 { get; }
    public string Street2 { get; }
    public string ZipCode { get; }
    #endregion Public Properties
  }
  #endregion Public Classes
}
