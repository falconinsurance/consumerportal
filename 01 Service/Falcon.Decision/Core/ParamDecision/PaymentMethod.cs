namespace Falcon.Decision;

public class PaymentMethod
{
  #region Protected Constructors

  protected PaymentMethod()
  {
    PaymentType = GetType().Name;
  }

  #endregion Protected Constructors

  #region Public Properties
  public string PaymentType { get; }
  public string Source { get; set; }
  #endregion Public Properties
}
