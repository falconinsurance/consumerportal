using System.Text.RegularExpressions;
using Diamond.Common.Objects;

namespace Falcon.Decision;

public class ClsPhone
{
  #region Private Fields
  private static readonly Regex _PhoneNumberRegex = new(@"1??[^0-9]*(\d\d\d)[^0-9]*(\d\d\d)[^0-9]*(\d\d\d\d)(.)*",RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline);
  #endregion Private Fields

  #region Public Constructors

  public ClsPhone(Phone phone)
  {
    var number = phone.Number;
    if(string.IsNullOrEmpty(number))
    {
      Success = false;
      return;
    }
    var m = _PhoneNumberRegex.Match(number.Trim());
    if(!m.Success)
    {
      Success = false;
      return;
    }

    AreaCode = m.Groups[1].Value;
    Exchange = m.Groups[2].Value;
    Suffix = m.Groups[3].Value;
    Extra = m.Groups[4].Value;
    Success = true;
  }

  #endregion Public Constructors

  #region Public Properties
  public string AreaCode { get; }
  public string Exchange { get; }
  public string Extra { get; }
  public bool Success { get; }
  public string Suffix { get; }
  #endregion Public Properties
}
