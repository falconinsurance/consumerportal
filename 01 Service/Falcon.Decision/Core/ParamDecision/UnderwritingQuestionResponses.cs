namespace Falcon.Decision
{
  public class UnderwritingQuestionResponses//:List<UnderwritingQuestionRespons>
  {
    #region Public Properties
    public bool PhysicalOrMentalImpairment { get; internal set; }
    public bool VehiclePreexistingDamage { get; internal set; }
    #endregion Public Properties
  }
}
