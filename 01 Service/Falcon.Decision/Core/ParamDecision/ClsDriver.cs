using Diamond.Common.Objects.Policy;
using Falcon.Decision.Helper;
using Falcon.Decision.Risks;
using DCE = Diamond.Common.Enums;
using FDEnums = Diamond.C0057.Common.Library.Enumerations;
using FE = Falcon.Enums;

namespace Falcon.Decision;

public partial class ParamDecision
{
  #region Private Fields

  private static readonly En_License[] _LstUnLicensedGroup = { En_License.NotLicensed,En_License.International,En_License.Matricula,En_License.ForeignPassport };

  #endregion Private Fields

  #region Public Classes

  public class ClsDriver
  {
    #region Public Properties

    public DateTime DateOfBirth { get; }

    public En_Driver DriverType { get; }

    public string FirstName { get; }

    public bool HasAccidentPrevention { get; }

    public bool HasDefensiveDriver { get; }

    public int Id { get; }

    public bool IsExcluded { get; }

    public bool IsLicense { get; }

    public bool IsSR22 { get; }

    public bool IsSR50 { get; }

    public string LastName { get; }

    public string LicenseNumber { get; }

    public En_State LicenseState { get; }

    public En_License LicenseType { get; private set; }

    public En_MaritalStatus MaritalStatus { get; }

    public bool MarriedDriver { get; }

    public string MiddleName { get; }

    public En_DriverRelationship Relationship { get; }

    public En_State State { get; }

    public List<ViolationInstance> Violations { get; } = [];

    #endregion Public Properties

    #region Public Constructors

    public ClsDriver(DiamondServices.Service.ISession session,int policyId,Driver driver,En_State state)
    {
      DateOfBirth = driver.Name.BirthDate;
      DriverType = FE.DriverType.FromDiaId(driver.DriverExcludeTypeId);
      FirstName = driver.Name.FirstName?.ToUpperCase();
      MiddleName = driver.Name.MiddleName?.ToUpperCase();
      HasAccidentPrevention = driver.AccPrevCourse;
      HasDefensiveDriver = driver.DefDriver;
      IsSR22 = driver.FilingInformation?.SR22 ?? false;
      IsSR50 = driver.FilingInformation?.SR50 ?? false;
      LastName = driver.Name.LastName?.ToUpperCase();
      LicenseType = FE.LicenseType.FromDiaId(driver.LicenseStatusId);
      MaritalStatus = FE.MaritalStatus.FromDiaId(driver.Name.MaritalStatusId);
      MarriedDriver = driver.MarriedDriver;
      Relationship = DriverRelationship.FromDiaId(driver.RelationshipTypeId);

      /****** Setting License state and License type here ******/
      var diaState = StateType.From(driver.Name.DLStateId);
      LicenseState = diaState.IsValidUSState() ? diaState : En_State.Other;

      switch(driver.LicenseStatusId)
      {
        case (int)FDEnums.LicenseStatus.NotLicensed:
        case (int)FDEnums.LicenseStatus.None:
        case (int)FDEnums.LicenseStatus.Unknown:
          LicenseType = En_License.NotLicensed;
          LicenseState = En_State.Other;
          break;

        case (int)FDEnums.LicenseStatus.Expired:
          LicenseType = En_License.Expired;
          break;

        case (int)FDEnums.LicenseStatus.TemporaryLearnerPermit:
          LicenseType = En_License.Permit;
          LicenseState = LicenseState == En_State.Other ? state : LicenseState;
          break;

        case 10: // TVDL
          LicenseType = En_License.TVDL;
          LicenseState = LicenseState == En_State.Other ? state : LicenseState;
          break;

        case 11: // StateID
          LicenseType = En_License.StateID;
          LicenseState = LicenseState == En_State.Other ? state : LicenseState;
          break;

        case (int)FDEnums.LicenseStatus.Suspended:
        case (int)FDEnums.LicenseStatus.Revoked:
          LicenseType = En_License.Suspended;
          LicenseState = LicenseState == En_State.Other ? state : LicenseState;
          break;

        case (int)FDEnums.LicenseStatus.Valid:
          if(state == diaState)
          {
            LicenseType = FE.LicenseType.From(diaState,En_License.NotLicensed);
            LicenseState = state;
          }
          else
          {
            LicenseType = driver.Name.DLStateId switch
            {
              DCE.State.NA_Other => En_License.International,
              _ => En_License.OutOfState,
            };
          }

          break;

        default:
          break;
      }
      /*********************************************************/

      State = state;
      IsExcluded = DriverType == En_Driver.Excluded;
      IsLicense = !_LstUnLicensedGroup.Contains(LicenseType);
      var ln = "";
      try
      {
        if(driver.Name?.DLN?.Length > 0)
        {
          ln = _IDiamondApi.Security.DecryptLicenseNumber(session,driver.Name.DLN,policyId);
        }
      }
      catch { }
      LicenseNumber = ln.Trim().Replace("-","").Replace(" ","") ?? "";
    }

    #endregion Public Constructors

    #region Public Methods

    public void SetLicenseType(En_License license) => LicenseType = license;

    #endregion Public Methods
  }

  #endregion Public Classes
}
