namespace Falcon.Decision;

public interface IDecisionRisk:IDisposable
{
  #region Public Properties

  ParamDecision Param { get; }
  DecisionResult Result { get; }

  #endregion Public Properties

  #region Public Methods

  IDecisionRisk Check(ParamDecision param);

  void UpdatePolicyId(int policyId);

  #endregion Public Methods
}
