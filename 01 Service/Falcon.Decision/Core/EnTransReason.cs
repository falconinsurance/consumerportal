namespace Falcon.Decision;

public enum EnTransReason
{
  AddDriver = 1,
  AddSR22 = 2,
  AddVehicle = 3,
  ModifyContactInfo = 4,
  ModifyCoverage = 5,
  ModifyVehicle = 6,
  ModifyDriver = 7,
  DeleteVehicle = 8,
  DeleteSR22 = 9,
  ModifyAddress = 10,
  AddEftDiscount = 11,
  RemoveEftDiscount = 12,
  ModifyRecurringPayment = 13,
  AddViolation = 14,
  DeleteDriver = 15,
  AddSR50 = 16,
  DeleteSR50 = 17,
  AddCoverage = 18,
  DeleteCoverage = 19,
  QuoteCompColl = 20
}
