namespace Falcon.Decision;

public class DecisionResult
{
  #region Public Properties

  public int CutOffScore { get; set; }
  public HashSet<FalconNote> DiamondNotes { get; } = [];
  public bool Pass { set; get; } = true;

  public List<string> PortalError { get; } = [];

  public List<string> PortalWarning { get; } = [];

  public int Score { get; set; }

  #endregion Public Properties

  #region Public Methods

  public void AddMessage(FalconNote diamondNote = null,string portalError = null,string portalWarning = null)
  {
    if(!(diamondNote?._Value).IsNullOrWhiteSpace())
    {
      DiamondNotes.Add(diamondNote.Clean());
    }
    if(!portalError.IsNullOrWhiteSpace())
    {
      PortalError.AddIfNot(portalError,c => c == portalError);
    }
    if(!portalWarning.IsNullOrWhiteSpace())
    {
      PortalWarning.AddIfNot(portalWarning,c => c == portalWarning);
    }
  }

  public void AddResult(DecisionResult result)
  {
    if(result is null)
      return;

    PortalWarning.AddRange(result.PortalWarning);
    PortalError.AddRange(result.PortalError);
    DiamondNotes.AddRange(result.DiamondNotes);

    PortalWarning.RemoveDuplicates();

    Score += result.Score;
    Score = Score >= result.CutOffScore ? 100 : Score;
    Pass &= result.Pass && Score < 100;
  }

  #endregion Public Methods

  #region Internal Methods

  internal void CombineErrorAndWarning()
  {
    if(PortalWarning.Count > 0 && PortalError.Count > 0)
    {
      PortalError.Add("<h2>Validation Warnings</h2>");
      PortalError.AddRange(PortalWarning);
    }
  }

  #endregion Internal Methods
}

internal static class DecisionResultExt
{
  #region Internal Methods

  internal static bool AddRange<T>(this HashSet<T> source,IEnumerable<T> items)
  {
    if(items?.Any() != true)
    {
      return false;
    }
    var allAdded = true;
    foreach(T item in items)
    {
      allAdded &= source.Add(item);
    }
    return allAdded;
  }

  internal static bool AddString(this HashSet<string> source,string item)
  {
    if(item.IsNullOrWhiteSpace())
    {
      return false;
    }
    return source.Add(item.Trim());
  }

  internal static void RemoveDuplicates<T>(this List<T> source)
  {
    if(source.Count > 1)
    {
      //HashSet removes duplicates
      var newList = new HashSet<T>(source);

      //clear the source
      source.Clear();

      //populate the source without duplicates
      source.AddRange(newList);
    }
  }

  #endregion Internal Methods
}
