using DecAPlus.DB;
using DecCommon.DB;
using DecCommon.Model;
using Decisioning.Config;
using Decisioning.Enums;
using Falcon.Decision.Risks;
using FCM = Falcon.Core.Models;

namespace Falcon.Decision;

public class DecisionRisk:IDecisionRisk
{
  #region Private Fields

  private static readonly ICommonHandler _ICommonHandler = AppResolver.Get<ICommonHandler>();
  private static readonly IDecHandler _IDecHandler = AppResolver.Get<IDecHandler>();
  private readonly IAPlusHandler _APlusHandler = AppResolver.Get<IAPlusHandler>();
  private bool _DisposedValue;
  private ParamDecision _Param;

  #endregion Private Fields

  #region Public Properties

  public ParamDecision Param => _Param;

  public DecisionResult Result { get; set; }

  #endregion Public Properties

  #region Private Destructors

  ~DecisionRisk()
  {
    Dispose(false);
  }

  #endregion Private Destructors

  #region Public Methods

  public IDecisionRisk Check(ParamDecision param)
  {
    _Param = param;
    Result = _Param.Result = new DecisionResult { CutOffScore = _Param.CutOffScore };
    Result = CheckRisk() ?? new DecisionResult();
    UpdateViolations(param);
    return this;
  }

  public void Dispose()
  {
    Dispose(disposing: true);
    GC.SuppressFinalize(this);
  }

  public void UpdatePolicyId(int policyId) => _Param.UpdatePolicyId(policyId);

  #endregion Public Methods

  #region Protected Methods

  protected virtual void Dispose(bool disposing)
  {
    if(!_DisposedValue)
    {
    }
    _DisposedValue = true;
  }

  #endregion Protected Methods

  #region Private Methods

  private DecisionResult CheckRisk()
  {
    if(!_Param.TestCheckRisk())
      return _Param.Result;
    RunRisks(GetRisks());
    SaveCommon();
    return _Param.Result;
  }

  private IEnumerable<EnRiskService> GetRisks()
  {
    var result = _IDecHandler.GetWorkflowServicesCs(_Param.RiskCheck,(EnState)_Param.State)?.ToList() ?? [];
    return result
      .With(c =>
      {
        c.Insert(0,EnRiskService.AgencyBlock);//To Check deny risks
        c.Insert(1,EnRiskService.FalconPreValidation);//To Check Pre Validation
        c.Add(EnRiskService.FalconPostValidation);//To Check Post Validation)
      });
  }

  private DecisionResult RunRisk(EnRiskService risk)
  {
    using(var @this = AppResolver.Get<IRiskCommon>(risk.ToString()))
    {
      @this.Start(_Param,Result);
    }
    return Result;
  }

  private void RunRisks(IEnumerable<EnRiskService> risks)
  {
    if(risks is null)
      return;
    foreach(var risk in risks)
    {
      var result = RunRisk(risk);
      if(!result.Pass)
      {
        result.CombineErrorAndWarning();
        break;
      }
    }
  }

  private void SaveCommon()
  {
    if(!_Param.ValidUser)
      return;
    var tblCommon = new Common
    {
      AgencyId = _Param.AgencyId,
      CutOff = _Param.CutOffScore,
      FalconScore = _Param.Result.Score,
      Pass = _Param.Result.Pass,
      PolicyCode = _Param.PolicyCode,
      PolicyId = _Param.PolicyId,
      RiskLevel = _Param.RiskLevel,
      Transaction = _Param.Transaction,
      UserId = _Param.UserId
    };
    if(tblCommon.PolicyId > 0)
    {
      _ICommonHandler.Add(tblCommon);
    }
    else
    {
      _Param.UpdatePolicyIdAdd(policyId =>
      {
        tblCommon.PolicyId = policyId;
        _ICommonHandler.Add(tblCommon);
      });
    }
  }

  private void UpdateViolations(ParamDecision param)
  {
    if(_Param.Is_CP_Page_5)
    {
      var result = _APlusHandler.GetDrvsViolations(_Param.PolicyId);
      {
        param.DriversViolations = FormatConvert.DeserializeJson<FCM.DrvsViolations>(result);
      }
    }
  }

  #endregion Private Methods
}
