using DecAgencyBlock.DB;
using DecAPlus.DB;
using DecCarFax.DB;
using DecCommon.DB;
using DecCV.DB;
using DecEquifax.DB;
using DecHistory.DB;
using DecHugo;
using Decisioning.Config;
using Decisioning.Enums;
using DecMvr.DB;
using DecRedMountain.DB;
using DecUdr.DB;
using Falcon.Core.Settings;
using Falcon.Decision;
using Falcon.Decision.Risks;
using Falcon.Tools.VehicleInfoCache;

namespace DiamondServices.Apis;

public class DecisioningModule:IServiceModule
{
  #region Private Fields

  private readonly ConfigSettings _ConfigSettings;
  private IServiceBuilder _Builder;

  #endregion Private Fields

  #region Public Constructors

  public DecisioningModule(ConfigSettings configSettings)
  {
    _ConfigSettings = configSettings;
  }

  #endregion Public Constructors

  #region Public Methods

  public void Load(IServiceBuilder builder)
  {
    _Builder = builder;
    RigisterHandlers();
    RigisterRisks();
    RigisterDecisioning();
    RegisterVehicleInfoCache();
  }

  #endregion Public Methods

  #region Private Methods

  private void RegisterVehicleInfoCache()
  {
    _Builder.AopSetSingle(_ => PopulateISODatabase());

    IVehicleInfoCache PopulateISODatabase()
    {
      var ret = new VehicleInfoCache(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Falcon));
      return ret.Load();
    }
  }

  private void RigisterDecisioning() => _Builder.AopSet<IDecisionRisk>(_ => new DecisionRisk());

  private void RigisterHandlers()
  {
    _Builder.AopSetSingle<IDecHandler>(_ => new DecHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.AppConfig)));
    _Builder.AopSetSingle<IAgencyBlockHandler>(_ => new AgencyBlockHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.AppConfig)));
    _Builder.AopSetSingle<IAPlusHandler>(_ => new APlusHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecHugo),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<ICarFaxHandler>(_ => new CarFaxHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecCarFax),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<ICVHandler>(_ => new CVHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Verisk),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<ICommonHandler>(_ => new CommonHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecCommon)));
    _Builder.AopSetSingle<IEquifaxHandler>(_ => new EquifaxHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecEquifax),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<IHistoryHandler>(_ => new HistoryHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecHistory)));
    _Builder.AopSetSingle<IHugoHandler>(_ => new HugoHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecHugo),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<IMvrHandler>(_ => new MvrHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecMVR),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<IRedMountainHandler>(_ => new RedMountainHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.DecRedMountain),_ConfigSettings.IsProduction));
    _Builder.AopSetSingle<IUdrHandler>(_ => new UdrHandler(o => o.NameOrConnection = _ConfigSettings.GetDBConnection(EnLocalDatabase.Services),_ConfigSettings.IsProduction));
  }

  private void RigisterRisks()
  {
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.AgencyBlock),_ => new RiskAgencyBlock());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.APlus),_ => new RiskAPlus());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.CarFax),_ => new RiskCarFax());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.CV),_ => new RiskCV());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.Equifax),_ => new RiskEquifax());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.EquifaxQuote),_ => new RiskEquifaxQuote());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.FalconPostValidation),_ => new RiskFalconPostValidaton());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.FalconPreValidation),_ => new RiskFalconPreValidaton());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.History),_ => new RiskHistory());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.Hugo),_ => new RiskHugo());
    //_Builder.AopSet<IRiskCommon>(nameof(EnRiskService.IsoRisk),_=>new RiskIso());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.MVR),_ => new RiskMVR());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.Null),_ => new RiskNull());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.RedMountain),_ => new RiskRedMountain());
    _Builder.AopSet<IRiskCommon>(nameof(EnRiskService.UDR),_ => new RiskUDR());
  }

  #endregion Private Methods
}
