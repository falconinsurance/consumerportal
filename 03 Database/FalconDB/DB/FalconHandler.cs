using Falcon.Tools.DBHandler;

namespace Falcon.FalconDB;

public class FalconHandler:DBHandler<FalconHandler>, IFalconHandler
{
  #region Public Constructors

  public FalconHandler(Action<DBSetting> option = null) : base(option)
  {
  }

  #endregion Public Constructors

  #region Protected Methods

  protected override void ModelBuilder(Builder builder)
  {
  }

  #endregion Protected Methods
}
