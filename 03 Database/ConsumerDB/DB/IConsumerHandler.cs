using Falcon.Tools.DBHandler;

namespace Falcon.ConsumerDB;

public interface IConsumerHandler:IDBHandler
{
  #region Public Methods

  List<Agency> GetAgencies(Guid agencyGroupId);

  Dictionary<Guid,AgencyGroup> GetAgenciesGroups();

  #endregion Public Methods
}
