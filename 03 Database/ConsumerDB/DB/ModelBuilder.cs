using Falcon.Tools.DBHandler;

namespace Falcon.ConsumerDB;

internal class ModelBuilder
{
  #region Private Fields

  private readonly Builder _Builder;

  #endregion Private Fields

  #region Public Constructors

  public ModelBuilder(Builder builder)
  {
    _Builder = builder;
    BuildAgencyGroup();
    BuildAgency();
    BuildLabelLookup();
  }

  #endregion Public Constructors

  #region Private Methods

  private Agency AgencyCast(Record rec) => new()
  {
    Id = rec.To<int>("Id"),
    ProducerId = rec.To<int>("ProducerId"),
    AgencyGroupId = rec.To<Guid>("AgencyGroupId"),
    Name = rec.To<string>("Name"),
    Addresses = rec.To<Address[]>("Addresses"),
    Phone = rec.To<string>("Phone")
  };

  private AgencyGroup AgencyGroupCast(Record rec) => new()
  {
    Id = rec.To<Guid>("Id"),
    Theme = rec.To<string>("Theme"),
    Name = rec.To<string>("Name"),
    State = rec.To<int>("State"),
    Setting = rec.To<object>("Setting")
  };

  private void AgencyGroupInsertUpdate(AgencyGroup obj,string action) => _ = _Builder.Handler.Exec("EXEC tblAgencyGroup_InsertUpdate @Json=@1,@Action=@2",c => c.AddJson(obj).Add(action));

  private void AgencyGroupInsertUpdate(IEnumerable<AgencyGroup> obj,string action) => _ = _Builder.Handler.Exec("EXEC tblAgencyGroup_InsertUpdate @Json=@1,@Action=@2",c => c.AddJson(obj).Add(action));

  private void BuildAgency()
              => _Builder.Of<Agency>()
  .AssignCast(AgencyCast);

  private void BuildAgencyGroup()
                  => _Builder.Of<AgencyGroup>()
    .AssignCast(AgencyGroupCast)
    .AssignInsert(c => AgencyGroupInsertUpdate(c,"INSERT"))
    .AssignUpdate(c => AgencyGroupInsertUpdate(c,"UPDATE"))
    .AssignInsertBulk(c => AgencyGroupInsertUpdate(c,"INSERT"))
    .AssignUpdateBulk(c => AgencyGroupInsertUpdate(c,"UPDATE"));

  private void BuildLabelLookup()
                => _Builder.Of<LabelLookup>()
  .AssignCast(LabelLookupCast)
  .AssignInsert(c => LabelLookupInsertUpdate(c,"INSERT"))
  .AssignUpdate(c => LabelLookupInsertUpdate(c,"UPDATE"))
  .AssignInsertBulk(c => LabelLookupInsertUpdate(c,"INSERT"))
  .AssignUpdateBulk(c => LabelLookupInsertUpdate(c,"UPDATE"));

  private LabelLookup LabelLookupCast(Record rec) => new()
  {
    Key = rec.To<string>("Key"),
    Type = rec.To("Type",EnTranslateKey.NA),
    En = rec.To<string>("En"),
    Es = rec.To<string>("Es"),
  };

  private void LabelLookupInsertUpdate(LabelLookup obj,string action) => _ = _Builder.Handler.Exec("EXEC tblLabelLookup_InsertUpdate @Json=@1,@Action=@2",c => c.AddJson(obj).Add(action));

  private void LabelLookupInsertUpdate(IEnumerable<LabelLookup> obj,string action) => _ = _Builder.Handler.Exec("EXEC tblLabelLookup_InsertUpdate @Json=@1,@Action=@2",c => c.AddJson(obj).Add(action));

  #endregion Private Methods
}
