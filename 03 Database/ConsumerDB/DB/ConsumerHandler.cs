using Falcon.Tools.DBHandler;

namespace Falcon.ConsumerDB;

public class ConsumerHandler:DBHandler<ConsumerHandler>, IConsumerHandler
{
  #region Public Constructors

  public ConsumerHandler(Action<DBSetting> option = null) : base(option)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public List<Agency> GetAgencies(Guid agencyGroupId) => Sql<Agency>("select * from tblAgency (NOLOCK) where AgencyGroupId=@1;",c => c.AddValues(agencyGroupId)).ToList();

  public Dictionary<Guid,AgencyGroup> GetAgenciesGroups() => Sql<AgencyGroup>("select * from tblAgencyGroup (NOLOCK)").ToDictionary(c => c.Id,c => c);

  #endregion Public Methods

  #region Protected Methods

  protected override void ModelBuilder(Builder builder) => _ = new ModelBuilder(builder);

  #endregion Protected Methods
}
