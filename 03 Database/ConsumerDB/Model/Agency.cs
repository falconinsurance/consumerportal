using Falcon.Tools.DBHandler;

namespace Falcon.ConsumerDB;

public class Agency
{
  #region Public Properties

  public Address[] Addresses { get; set; }
  public Guid AgencyGroupId { get; set; }
  public string Code { get; set; }
  public int Id { get; set; }
  public string Name { get; set; }
  public string Phone { get; set; }
  public int ProducerId { get; set; }
  public string Signer { get; set; }
  public int State { get; set; }

  #endregion Public Properties

  #region Public Methods

  public static Agency Cast(Record record) => new()
  {
    Addresses = record.To<Address[]>("Addresses"),
    AgencyGroupId = record.To<Guid>("AgencyGroupId"),
    Code = record.To<string>("Code"),
    Id = record.To<int>("Id"),
    Name = record.To<string>("Name"),
    Phone = record.To<string>("Phone"),
    ProducerId = record.To<int>("ProducerId"),
    Signer = record.To<string>("Signer"),
    State = record.To<int>("State"),
  };

  #endregion Public Methods
}
