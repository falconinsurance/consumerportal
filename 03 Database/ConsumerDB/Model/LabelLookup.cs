using Falcon.Tools.DBHandler;

namespace Falcon.ConsumerDB;

public class LabelLookup
{
  #region Public Properties

  public string En { get; set; }
  public string Es { get; set; }
  public string Key { get; set; }
  public EnTranslateKey Type { get; set; }

  #endregion Public Properties

  #region Public Methods

  public static LabelLookup Cast(Record record) => new()
  {
    En = record.To<string>("En"),
    Es = record.To<string>("Es"),
    Key = record.To<string>("Key"),
    Type = record.To("Type",EnTranslateKey.NA),
  };

  #endregion Public Methods
}
