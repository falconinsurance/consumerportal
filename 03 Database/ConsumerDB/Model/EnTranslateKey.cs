namespace Falcon.ConsumerDB;

//[Serializable]
//[JsonConverter(typeof(StringEnumConverter))]
public enum EnTranslateKey
{
  NA = 0,
  ErrorCCPaymentReq = 1,
  ErrorInvalidRoutingNumber = 2,
  DocuSignDefault = 3,
  DocuSignComplete = 4,
  DocuSignDeclined = 5,
  DocuSignExpired = 6,
  ErrorRegistration = 7,
  ErrorAccExists = 8
}
