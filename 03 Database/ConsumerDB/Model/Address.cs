namespace Falcon.ConsumerDB
{
  public class Address
  {
    #region Public Properties

    public string City { get; set; }
    public string State { get; set; }
    public string Street { get; set; }
    public string Zip { get; set; }

    #endregion Public Properties
  }
}
