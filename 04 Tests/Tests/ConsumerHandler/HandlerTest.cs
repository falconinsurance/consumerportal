using Falcon.ConsumerDB;
using Falcon.Tools.DBHandler;

namespace Tests.ConsumerHandlerTest;

[TestFixture]
public class HandlerTest
{
  #region Private Fields

  private static readonly IConsumerHandler _Handler = new ConsumerHandler(o => { o.NameOrConnection = GetConnection(EnServer.Test02,Helper.EnDatabase.Consumer); });

  #endregion Private Fields

  #region Public Methods

  [Test]
  public void AgencyGroupsStoredProcedure()
  {
    var output = _Handler.Sql<AgencyGroup>("EXEC spGetAgencyTheme @Id=@1;",c => c.AddValues(Guid.Parse("D7CAC8F3-841D-4CB6-8216-2B40D35D05C4")));
    WriteLineOutput(output);
  }

  [Test]
  public void GetAllAgenciesBasedOnAgencyGroupId()
  {
    var output = _Handler.Sql<Agency>("select * from tblAgency (NOLOCK) where AgencyGroupId=@1;",c => c.AddValues(Guid.Parse("00000000-0000-0000-0000-000000000000")));
    var output1 = _Handler.GetAgencies(Guid.Parse("00000000-0000-0000-0000-000000000000"));
    var output2 = _Handler.GetAgencies(Guid.Parse("0a155574-448e-4cf0-955f-d7fc260ece4f"));
    WriteLineOutput(output);
    WriteLineOutput(output1);
    WriteLineOutput(output2);
  }

  [Test]
  public void GetConsumerAgencyInfo()
  {
    var output = GetAgencyInfoList();
    var singleAgency = output.GroupBy(c => c.Id).Select(y => y.FirstOrDefault()).Distinct().ToList();
    var multipleAgencies = output.GroupBy(c => c.Id).Where(g => g.Count() > 6)?.SelectMany(x => x)?.ToList();
  }

  [Test]
  public void InsertAgencyGroup()
  {
    _ = File.ReadAllBytes(@"\\CORP\F01Filestore\Users\pkondapuram\Desktop\falcon-logo.png");
    var val = new AgencyGroup
    {
      Id = Guid.NewGuid(),
      Theme = ":root { --color-main-theme: red; }",
      Name = "Agency Group 4",
    };
    _Handler.Add(val);
  }

  [Test]
  public void InsertAgencyGroupArray()
  {
    _ = File.ReadAllBytes(@"\\CORP\F01Filestore\Users\pkondapuram\Desktop\jabra_logo.jpg");
    _ = File.ReadAllBytes(@"\\CORP\F01Filestore\Users\pkondapuram\Desktop\Polaroid_logo.jpeg");
    _ = File.ReadAllBytes(@"\\CORP\F01Filestore\Users\pkondapuram\Desktop\gipsi-gif-logo.gif");
    var val = new List<AgencyGroup>
    {
      new ()
      {
        Id = GuidTools.GetGuid(),
      Theme = ":root { --color-main-theme: green; }",
      Name = "Agency Group 3",
      },
      new ()
      {
        Id = GuidTools.GetGuid(),
      Theme = ":root { --color-main-theme: yellow; }",
      Name = "Agency Group 4",
      },
      new ()
      {
        Id = GuidTools.GetGuid(),
      Theme = ":root { --color-main-theme: orange; }",
      Name = "Agency Group 5",
      }
    };
    _Handler.AddBulk(val);
  }

  [Test]
  public void GenerateGuids()
  {
    var guids = new List<Guid>();
    for(var i=0; i<16; i++)
    {
      guids.Add(Guid.NewGuid());
    }
    WriteLine(guids);
  }

  [Test]
  public void InsertAgencyGroupArray1()
  {
    var greenTheme = ReadFileAsString("Theme\\green.txt");
    var redTheme = ReadFileAsString("Theme\\red.txt");
    var mainList = GetAgencyInfoList();
    var agencies = GetSingleAgencyInfoList(mainList);
    var GroupedAgency6 = GetMultipleAgencyInfoListFor6(mainList);
    var GroupedAgency2 = GetMultipleAgencyInfoListFor2(mainList);

    var val = new List<AgencyGroup>();

    foreach(var item in agencies)
    {
      var addFlag = val.Where(c => c.State == item.StateId)?.Count() < 2;
      var addRed = val.Any(c => c.Theme == greenTheme);
      if(addFlag)
      {
        val.Add(new()
        {
          Name = item.GroupName,
          State = item.StateId,
          Theme = greenTheme
        });
      }
    }
    if(GroupedAgency6 is null || GroupedAgency6.Count == 0)
    {
      foreach(var item in GroupedAgency2)
      {
        var addFlag = val.Where(c => c.State == item.StateId)?.Count() < 2;
        if(addFlag)
        {
          val.Add(new()
          {
            Name = item.GroupName,
            State = item.StateId,
            Theme = greenTheme
          });
        }
      }
    }
    if(GroupedAgency6.Count > 1)
    {
      foreach(var item in GroupedAgency6)
      {
        var addFlag = val.Where(c => c.State == item.StateId)?.Count() < 4;

        if(addFlag)
        {
          val.Add(new()
          {
            Name = item.GroupName,
            State = item.StateId,
            Theme = greenTheme
          });
        }
      }
    }
    else if(GroupedAgency6.Count == 1)
    {
      var item = GroupedAgency6[0];
      var addFlag = val.Where(c => c.State == item.StateId)?.Count() < 3;
      addFlag = addFlag && !val.Any(c => c.Name == item.GroupName);
      addFlag = addFlag && val.Count(c => c.Name == item.GroupName) <= 1;

      if(addFlag)
      {
        val.Add(new()
        {
          Name = item.GroupName,
          State = item.StateId,
          Theme = greenTheme
        });
      }
    }

    WriteLine(val);
    //_Handler.AddBulk(val);
  }

  [Test]
  public void InsertTranslationLabel()
  {
    var val = new LabelLookup
    {
      Key = "L_Privay_Pol",
      En = "Privacy Policy",
      Es = "Política de privacidad",
    };
    _Handler.Add(val);
  }

  [Test]
  public void InsertTranslationLabelsArray()
  {
    var keys = FileDeserialize<List<string>>("Translation/Keys.json");
    var en = FileDeserialize<List<string>>("Translation/En.json");
    var es = FileDeserialize<List<string>>("Translation/Es.json");

    var val = new List<LabelLookup>();

    for(var i = 0;i < keys.Count();i++)
    {
      var l = new LabelLookup()
      {
        Key = keys[i],
        En = en[i],
      };

      if(i < es.Count)
      {
        l.Es = es[i];
      }
      switch(keys[i])
      {
        case "L_Error_CC_payment_req":
          l.Type = EnTranslateKey.ErrorCCPaymentReq;
          break;

        case "L_Error_Invalid_routing":
          l.Type = EnTranslateKey.ErrorInvalidRoutingNumber;
          break;

        default:
          break;
      }
      val.Add(l);
      var ess = l.Es != null ? $"'{l.Es}'" : "null";
      WriteLine($"('{l.Key}',{l.Type.ToInt()},'{l.En}',{ess}),");
    }
    var p = JsonConvert.SerializeObject(val);
    //_Handler.AddBulk(val);
  }

  [Test]
  public void SelectAllDictionaryFromAgencyGroupTbl()
  {
    var output = _Handler.GetAgenciesGroups();
    WriteLineOutput(output);
  }

  [Test]
  public void SelectAllFromAgencyGroupTbl()
  {
    var output = _Handler.Sql<AgencyGroup>("select * from tblAgencyGroup");
    WriteLineOutput(output);
  }

  [Test]
  public void UpdateAgencyGroup()
  {
    _ = File.ReadAllBytes(@"\\CORP\F01Filestore\Users\pkondapuram\Desktop\gipsi-gif-logo.gif");
    var val = new AgencyGroup
    {
      Id = Guid.Parse("8F4D463F-0300-0000-3508-202201211031"),
      //Logo = imageArray2,
    };
    _Handler.Update(val);
  }

  [Test]
  public void UpdateTranslationLabel()
  {
    var val = new LabelLookup
    {
      Key = "L_Privay_Pol",
      En = "Privacy Policy",
      Es = "Política de privacidad",
    };
    _Handler.Update(val);
  }

  #endregion Public Methods

  #region Private Methods

  private List<ConsumerAgencyInfo> GetAgencyInfoList()
  {
    var query = """
      WITH T1
      as
      (
      	select *
      	from [UserManagement].[Users].[tblAgency] A (NOLOCK)
      	WHERE A.StateId > 0
      --ORDER BY A.StateId, A.Name
      )
      ,    T2
      as
      (
      	Select *
      	from [UserManagement].[Users].[tblAgencyLocation] A(NOLOCK)
      	where A.AgencyId in (
      		SELECT [AgencyId]
      		FROM [UserManagement].[Users].[tblAgencyLocation]
      		Group By AgencyId
      		Having Count(*) > 1
      		)
      --Order by A.AgencyId, A.Name
      )
      ,    T3
      as
      (
      	select T1.StateId
      	,      T1.Id
      	,      T1.Name                                                            as GroupName
      	,      IIF(T2.Name is not null, T2.Description, T1.Name)                  as Name
      	,      IIF(T2.DiaAgencyLocationId is not null, T2.DiaAgencyLocationId, 0) as ProducerId
      	,      T1.Code
      	from      T1
      	left join T2 ON T1.Id = T2.AgencyId
      --ORDER BY T1.StateId, T1.Name
      )
      ,    T4
      as
      (
      	SELECT AAL.agency_id
      	,      A.street_name
      	,      A.city
      	,      A.state_id
      	,      A.zip
      	,      A.display_address
      	,      A.pobox
      	FROM      [Diamond].[dbo].[Address]  A

      	left join Diamond..AgencyAddressLink AAL ON (A.address_id = AAL.address_id)
      )
      ,    T5
      as
      (
      	SELECT APL.agency_id
      	,      P.phone_num
      	FROM      [Diamond].[dbo].[Phone]  P

      	left join Diamond..AgencyPhoneLink APL ON (P.phone_id = APL.phone_id)
      )
      ,    T6
      as
      (
      	select T3.*
      	,      Address = CONCAT('{"Street": "',IIF(T4.street_name = '','PO Box'+T4.pobox,T4.street_name),'", "City": "',T4.city,'", "State": "',T4.state_id,'", "Zip": "',CAST(T4.zip AS VARCHAR(5)),'"}')
      	from      T3
      	left JOIN T4 ON T3.Id = T4.agency_id
      	WHERE T4.state_id > 1
      )

      select T6.*
      ,      Phone = T5.phone_num
      from      T6
      left JOIN T5 ON T6.Id = T5.agency_id
      ORDER BY T6.StateId
      ,        T6.GroupName
      ,        T6.Name
      """;

    return _Handler.SqlList(query,null,ConsumerAgencyInfo.Cast).ToList();
  }

  private List<ConsumerAgencyInfo> GetMultipleAgencyInfoListFor2(List<ConsumerAgencyInfo> mainList)
  {
    var all = mainList.GroupBy(c => c.Id);
    var res = all.Where(g => g.Count() > 1).ToList();
    return res?.SelectMany(x => x)?.ToList();
  }

  private List<ConsumerAgencyInfo> GetMultipleAgencyInfoListFor6(List<ConsumerAgencyInfo> mainList)
  {
    var all = mainList.GroupBy(c => c.Id);
    var res = all.Where(g => g.Count() > 6).ToList();
    return res?.SelectMany(x => x)?.ToList();
  }

  private List<ConsumerAgencyInfo> GetSingleAgencyInfoList(List<ConsumerAgencyInfo> mainList)
  {
    return mainList.GroupBy(c => c.StateId).Select(y => y.FirstOrDefault()).Distinct().ToList();
  }

  #endregion Private Methods
}
