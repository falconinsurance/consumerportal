//using ConsumerServiceHost.Core;

//namespace Tests.ConsumerHandlerTest;

//[TestFixture]
//public class MethodCacheTest
//{
//  #region Private Fields
//  private readonly MethodsCache _MethodsCache = new();
//  #endregion Private Fields

//  #region Public Methods

//  [Test]
//  public void P1()
//  {
//    Param1();
//    Param1();
//    Param1();
//  }

//  #endregion Public Methods

//  #region Private Methods

//  private int Param0()
//  {
//    return _MethodsCache.SetGet(X1);
//    static int X1() => 1;
//  }

//  private int Param1()
//  {
//    return _MethodsCache.SetGet(() => X2(5,5),TimeSpan.FromSeconds(5),new { x = 5,y = 5 });
//    static int X1(int x) => x;
//    static int X2(int x,int y) => x + y;
//  }

//  #endregion Private Methods
//}
