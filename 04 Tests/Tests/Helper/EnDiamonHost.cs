namespace Tests.Helper;

public enum EnDiamonHost
{
  Demo,
  Production_1,
  Production_2,
  Production_3,
  SandBox,
  Selenum,
  Test01_1,
  Test01_2,
  Test02,
  Test03,
}
