namespace Tests.Helper;

public enum EnServer
{
  ITC,
  Production,
  SandBox,
  Test01,
  Test02,
  Test03,
  Selenium,
  Demo,
}
