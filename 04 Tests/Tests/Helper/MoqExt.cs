using Moq;
using Tools.Logger;

namespace Tests.Helper;

public static class MoqExt
{
  #region Public Methods

  public static T Mocking<T>(this T input,Action<Mock<T>> action) where T : class
  {
    var mock = Mock.Get(input);
    action(mock);
    return input;
  }

  public static T Mocking<T>(Action<Mock<T>> action) where T : class => Mock.Of<T>().Mocking(action);

  public static T Mocking<T>() where T : class => Mock.Of<T>();

  public static void SetLogger(this Mock<IResolver> input)
  {
    _ = input.Setup(s => s.Resolve<ILogHandler>()).Returns(Mock.Of<ILogHandler>().Mocking(c1 => c1.Setup(s1 => s1.GetLogger(It.IsAny<string>())).Returns(Mock.Of<ILog>())));
  }

  #endregion Public Methods
}
