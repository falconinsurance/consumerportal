using Falcon.Tools.DBHandler;

namespace Tests.Helper;

public class ConsumerAgencyInfo
{
  public int StateId { get; set; }
  public int Id { get; set; }
  public string GroupName { get; set; }
  public string Code { get; set; }
  public string Name { get; set; }
  public string Phone { get; set; }
  public int ProducerId { get; set; }
  public ClsAddress Address { get; set; }

  public static ConsumerAgencyInfo Cast(Record record) => new ConsumerAgencyInfo()
  {
    StateId = record.To<int>("StateId"),
    Id = record.To<int>("Id"),
    GroupName = record.To<string>("GroupName"),
    Code = record.To<string>("Code"),
    Name = record.To<string>("Name"),
    Phone = record.To<string>("Phone"),
    ProducerId = record.To<int>("ProducerId")
  }.With(c =>
  {
    var addr = record.To<ClsAddress1>("Address");
    c.Address = new()
    {
      Street = addr.Street,
      City = addr.City,
      State = addr.State.ToString(),
      Zip = addr.Zip,
    };
  });
}

public class ClsAddress
{
  public string Street { get; set; }
  public string City { get; set; }
  public string State { get; set; }
  public string Zip { get; set; }
}

public class ClsAddress1
{
  public string Street { get; set; }
  public string City { get; set; }
  public EnState State { get; set; }
  public string Zip { get; set; }
}
