using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Tests.Helper;

public static class TestExt
{
  #region Private Properties

  private static JsonSerializerSettings _JsonSerializerSettings { get; } =
            new JsonSerializerSettings
            {
              NullValueHandling = NullValueHandling.Ignore,
              //DefaultValueHandling = DefaultValueHandling.Ignore,
              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

  #endregion Private Properties

  #region Public Methods

  public static T FileDeserialize<T>(string fileName)
  {
    var path = Path.Combine(TestContext.CurrentContext.TestDirectory,"Files",fileName);
    var str = File.ReadAllText(path);
    return JsonConvert.DeserializeObject<T>(str);
  }

  public static string ReadFileAsString(string fileName)
  {
    var path = Path.Combine(TestContext.CurrentContext.TestDirectory,"Files",fileName);
    return File.ReadAllText(path);
  }

  public static string GetConnection(EnServer server,EnDatabase database) => GetConnection(server,database.ToString());

  public static string GetConnection(EnServer server,string database)
  {
    var serverStr = server switch
    {
      EnServer.Production => "P01-LSR-0001",
      EnServer.SandBox => "S01-MLT-0001",
      EnServer.Demo => "D02-MDB-0001",
      EnServer.Test01 => "T01-LSR-0001",
      EnServer.Test02 => "T02-MDB-0001",
      EnServer.Test03 => "T03-LSR-0001",
      EnServer.Selenium => "T01-SEL-0001",
      EnServer.ITC => "I01-BKT-0001",
      _ => "S01-MLT-0001",
    };
    return $"Server={serverStr};Database={database};Trusted_Connection=True;";
  }

  public static string GetDiamondHost(EnServer server) => server switch
  {
    EnServer.Production => GetDiamondHost(EnDiamonHost.Production_1),
    EnServer.Test01 => GetDiamondHost(EnDiamonHost.Test01_1),
    EnServer.Test02 => GetDiamondHost(EnDiamonHost.Test02),
    EnServer.Test03 => GetDiamondHost(EnDiamonHost.Test03),
    EnServer.Selenium => GetDiamondHost(EnDiamonHost.Selenum),
    EnServer.Demo => GetDiamondHost(EnDiamonHost.Demo),
    _ => GetDiamondHost(EnDiamonHost.SandBox),
  };

  public static string GetDiamondHost(EnDiamonHost host) => host switch
  {
    EnDiamonHost.Demo => "http://192.168.81.101",
    EnDiamonHost.Production_1 => "http://192.168.41.61",
    EnDiamonHost.Production_2 => "http://192.168.41.71",
    EnDiamonHost.Production_3 => "http://192.168.41.81",
    EnDiamonHost.SandBox => "http://192.168.50.154",
    EnDiamonHost.Selenum => "http://192.168.61.22",
    EnDiamonHost.Test01_1 => "http://192.168.61.61",
    EnDiamonHost.Test01_2 => "http://192.168.61.71",
    EnDiamonHost.Test02 => "http://192.168.61.120",
    EnDiamonHost.Test03 => "http://192.168.61.130",
    _ => "http://192.168.50.154", //SandBox
  };

  public static void SerializeAndWrite<T>(this T obj,string fileName)
  {
    var path = Path.Combine(TestContext.CurrentContext.TestDirectory,"Files",fileName);
    File.WriteAllText(path,SerializeString(obj));
  }

  public static string SerializeString<T>(T anObject)
  => JsonConvert.SerializeObject(anObject,Newtonsoft.Json.Formatting.Indented,_JsonSerializerSettings);

  public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source,int N) => source.Skip(Math.Max(0,source.Count() - N));

  public static void WriteLine<T>(T input)
  {
    if(input is string)
    {
      Console.WriteLine(input);
    }
    else
    {
      Serialize(input);
    }
  }

  // private static JsonSerializerSettings _JsonSerializerSettings = new JsonSerializerSettings();
  public static void WriteLineInput<T>(T input)
  {
    WriteLine($"Input :--------------------------{DateTime.Now:hh:mm:ss.fff}");
    WriteLine(input);
  }

  public static void WriteLineJson<T>(T input) => Console.WriteLine(JsonConvert.SerializeObject(input,Newtonsoft.Json.Formatting.Indented,_JsonSerializerSettings));

  public static void WriteLineOutput<T>(T input)
  {
    WriteLine($"Output:--------------------------{DateTime.Now:hh:mm:ss.fff}");
    WriteLine(input);
    WriteLine("*********************************");
  }

  public static void WriteLineXML<T>(T input)
  {
    if(input is string)
    {
      Console.WriteLine(input);
    }
    else
    {
      SerializeXML(input);
    }
  }

  public static void WriteLog()
  {
    var dec = new Dictionary<string,object>();
    var method = GetMethod();
    var X = new
    {
      Class = method.DeclaringType.Name,
      Method = method.Name,
      Params = method.GetParameters()
        .Select(p => new { p.Name,Value = p.ParameterType.GetField(p.Name),Type = p.ParameterType.Name }),
    };

    WriteLine(X);
  }

  #endregion Public Methods

  #region Private Methods

  private static MethodBase GetMethod() => new StackTrace().GetFrame(2).GetMethod();

  private static void Serialize<T>(T anObject)
  {
    Console.WriteLine($"Type : {typeof(T).GetFriendlyName()}");
    if(anObject is IEnumerable<object> ienumerable)
    {
      Console.WriteLine("Count : " + ienumerable.Count());
    }
    Console.WriteLine(JsonConvert.SerializeObject(anObject,Newtonsoft.Json.Formatting.Indented,_JsonSerializerSettings));
    //Console.WriteLine(FormatConvert.SerializeXml(anObject));
  }

  private static void SerializeXML<T>(T input)
  {
    Console.WriteLine($"Type : {typeof(T).GetFriendlyName()}");
    if(input is IEnumerable<object> ienumerable)
    {
      Console.WriteLine("Count : " + ienumerable.Count());
    }

    var xsSubmit = new XmlSerializer(typeof(T));

    var xml = "";

    using(var sww = new StringWriter())
    using(var writer = XmlWriter.Create(sww,new XmlWriterSettings { Indent = true,OmitXmlDeclaration = true,}))
    {
      xsSubmit.Serialize(writer,input,new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty }));
      xml = sww.ToString();
    }

    Console.WriteLine(xml);
  }

  #endregion Private Methods
}
