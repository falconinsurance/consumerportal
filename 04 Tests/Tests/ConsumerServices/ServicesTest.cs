using System.Net.Http;
using System.Net.Http.Formatting;
using ConsumerServiceHost.Apis;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.DependencyInjection;
using ConsumerServiceHost.Models;
using ConsumerServiceHost.Models.Dropdowns;
using DecCommon.DB;
using DiamondServices.Apis;
using DiamondServices.Service;
using Falcon.ConsumerDB;
using Falcon.Core.Models;
using Falcon.DependencyInjection;
using Falcon.Enums;
using Falcon.FalconDB;
using Moq;
using Tools.Logger;

namespace Tests.ConsumerHandlerTest;

[TestFixture]
public class ServicesTest
{
  #region Public Fields

  public static ConsumerSession _Session;

  #endregion Public Fields

  #region Private Fields

  private const EnServer _Environment = EnServer.Test02;
  private readonly IDiamondApi _ApiDiamond;
  private readonly HttpClient _Client;
  private IConsumerApi _Api;

  #endregion Private Fields

  #region Private Properties

  private static JsonMediaTypeFormatter JsonFormatter => new JsonMediaTypeFormatter { SerializerSettings = { NullValueHandling = NullValueHandling.Ignore,DefaultValueHandling = DefaultValueHandling.Ignore } };

  #endregion Private Properties

  #region Public Constructors

  public ServicesTest()
  {
    var falcon = new FalconHandler(o => o.NameOrConnection = GetConnection(_Environment,"Falcon"));
    var consumer = new ConsumerHandler(o => o.NameOrConnection = GetConnection(_Environment,"Consumer"));
    var service = new DiamondService(GetDiamondHost(_Environment));
    var common = new CommonHandler(o => o.NameOrConnection = GetConnection(_Environment,"DecCommon"));
    var oneInc = new OneIncApi(false);
    var enTypeLoader = new EnTypeDbLoader();
    //var consumerApi = new ConsumerApi();
    _ApiDiamond = new DiamondApi();

    var serviceBuilder = new ServiceContainer()
      .SetSingle<ILogHandler>(_ => Mock.Of<ILogHandler>().Mocking(c1 => c1.Setup(s1 => s1.GetLogger(It.IsAny<string>())).Returns(Mock.Of<ILog>())))
    .SetSingle<IFalconHandler>(_ => falcon)
    .SetSingle<IDiamondService>(_ => service)
    .SetSingle<IConsumerHandler>(_ => consumer)
    .SetSingle<ICommonHandler>(_ => common)
    .SetSingle<IOneIncApi>(_ => oneInc)
    .SetSingle<IEnTypeLoader>(_ => enTypeLoader)
    .SetSingle<IDiamondApi>(_ => _ApiDiamond)
    .Build();

    //Mocking<IResolver>(c =>
    //{
    //  c.SetLogger();
    //  c.Setup(s => s.Resolve<IFalconHandler>()).Returns(falcon);
    //  c.Setup(s => s.Resolve<IDiamondService>()).Returns(service);
    //  c.Setup(s => s.Resolve<IConsumerHandler>()).Returns(consumer);
    //  c.Setup(s => s.Resolve<ICommonHandler>()).Returns(common);
    //  c.Setup(s => s.Resolve<IOneIncApi>()).Returns(oneInc);
    //  c.Setup(s => s.Resolve<IEnTypeLoader>()).Returns(new EnTypeDbLoader());
    //  c.Setup(s => s.Resolve<IDiamondApi>()).Returns(_ApiDiamond);
    //  //c.Setup(s => s.Resolve<IConsumerApi>()).Returns(consumerApi);
    //}).SetResolver();

    EnTypeHelper.SetLoader(AppResolver.Get<IEnTypeLoader>);

    const string baseUri = "stgportalone.processonepayments.com/api/api";
    const string portalKey = "58e5024f-ecc0-40a5-bac8-4ffd5e9f8cd8";
    var oneIncSessionUri = new Uri($"https://{baseUri}/Session/Create?PortalOneAuthenticationKey={portalKey}");
    _Client = new() { BaseAddress = oneIncSessionUri };
  }

  #endregion Public Constructors

  #region Public Methods

  [Test]
  public void AgencyGroupsStoredProcedure()
  {
    var output = GetAgencyGroupId("/theme/D7CAC8F3-841D-4CB6-8216-2B40D35D05C4/root.css");
    Assert.That(output == Guid.Parse("D7CAC8F3-841D-4CB6-8216-2B40D35D05C4"));
    WriteLineOutput(output);
  }

  [Test]
  public void BindPolicyWithCC()
  {
    var sess = GetSession();
    var instance = FileDeserialize<Instance>("tt.json");
    var image = instance.ToImage(sess,_ApiDiamond.SystemDate).New;

    var (Image, ValidationItems) = _ApiDiamond.Policy.Submit(sess,image,instance.Agency.State);

    var instanceFromImage = Image.ToInstance(sess);
    var uwRs = FileDeserialize<List<UnderwritingResponse>>("UnderwritingResponsesFromCP.json");

    instanceFromImage.Policy.UnderwritingResponses = uwRs;
    instanceFromImage.Drivers.ForEach(d => d.License.Number = "123123123");
    instanceFromImage.InsVehicles.Vehicles[0].Vin = "2B3CJ7DJ6B1234567";
    instanceFromImage.PolicyHolder.Phones =
    [
      new()
      {
        Number = "8765676545",
        Type = En_PhoneNumber.Cellular
      }
    ];

    var loadImageWithUWQs = instanceFromImage.ToImage(sess,_ApiDiamond.SystemDate).New;

    var ratedImage = _ApiDiamond.Policy.Submit(sess,loadImageWithUWQs,instance.Agency.State);

    var instanceAfterRate = ratedImage.Image.ToInstance(sess);

    var finalImage = instanceAfterRate.ToImage(sess,_ApiDiamond.SystemDate).New;

    var saveCC = SaveCardOnOneInc();

    _Api.Policy.Bind(sess,finalImage,new()
    {
      Type = En_Payment.CreditCard,
      Source = "ConsumerPortal",
      Amount = 88.1m,
      CreditCard = new()
      {
        Expiration = new()
        {
          Month = 2,
          Year = 2024,
        },
        HolderName = "Pri test",
        Last4 = "1111",
        Type = En_PaymentCard.Visa,
        Token = saveCC.Token
      }
    },instance.Agency.State);

    WriteLine("");
  }

  [Test]
  public void DiaIds()
  {
    var NonOwner = En_Policy.NonOwner.GetDiaId();
    var Owner = En_Policy.Owner.GetDiaId();
    var Limited = En_Policy.Limited.GetDiaId();
    var NoVal = En_Policy.NoVal.GetDiaId();
    WriteLine(new { NonOwner,Owner,Limited,NoVal });
  }

  [Test]
  public void GetCCToken()
  {
    using var client = new HttpClient();
  }

  [Test]
  public void GetLabelLookup()
  {
    _Api = new ConsumerApi();

    var res = _Api.Translator.GetLabelLookupsDictionary(En_Language.EN);
    var res1 = _Api.Translator.GetLabelKey(EnTranslateKey.ErrorCCPaymentReq);
    //WriteLine(res);
    WriteLine(res);
    WriteLine(res1);
  }

  [Test]
  public void GetRoutingNumbers()
  {
    _Api = new ConsumerApi();
    var res = _ApiDiamond.DbFalcon.Sql("EXEC [dbo].[spGetValidRoutingNumbers];").Select(c => c["RoutingNumber"].ToString());
    WriteLine(res);
  }

  [Test]
  public void GetStateSpecificsTest()
  {
    _Api = new ConsumerApi();
    //var res = _Api.StateSpec.GetStateSpecifics(En_State.OK,En_Language.EN,_ApiDiamond.SystemDate);
    var resList = new List<StateSpecifics>()
    {
    _Api.StateSpec.GetStateSpecifics(En_State.OK,En_Language.EN,_ApiDiamond.SystemDate),
    _Api.StateSpec.GetStateSpecifics(En_State.AZ,En_Language.EN,_ApiDiamond.SystemDate),
    _Api.StateSpec.GetStateSpecifics(En_State.CO,En_Language.EN,_ApiDiamond.SystemDate),
    _Api.StateSpec.GetStateSpecifics(En_State.UT,En_Language.EN,_ApiDiamond.SystemDate),
    _Api.StateSpec.GetStateSpecifics(En_State.TX,En_Language.EN,_ApiDiamond.SystemDate),
    };

    var ressListOK = new List<string>();

    foreach(var res in resList)
    {
      foreach(var prop in res.Dropdowns.DriverOptions.GetType().GetProperties())
      {
        var p = prop.GetValue(res.Dropdowns.DriverOptions,null);
        var pp = p as StateDropdown;
        if(pp.Label != null)
        {
          ressListOK.Add(pp.Label);
        }
      }

      foreach(var prop in res.Dropdowns.VehicleOptions.GetType().GetProperties())
      {
        var p = prop.GetValue(res.Dropdowns.VehicleOptions,null);
        var pp = p as StateDropdown;
        if(pp.Label != null)
        {
          ressListOK.Add(pp.Label);
        }
      }

      foreach(var prop in res.Dropdowns.PolicyOptions.GetType().GetProperties())
      {
        var p = prop.GetValue(res.Dropdowns.PolicyOptions,null);
        if(p is StateDropdown pp && pp.Label != null)
        {
          ressListOK.Add(pp.Label);
        }
      }
    }
    var g = ressListOK.Distinct().Where(c => c != "SR-22"
    && c != "Driver Type"
    && c != "Gender"
    && c != "State"
    && c != "Relationship"
    && c != "Violations"
    && c != "Rental"
    && c != "Com/Col Deductible"
    && c != "UMPD Deductible"
    && c != "Bodily Injury (BI)"
    && c != "Medical Payment (MP)"
    && c != "Property Damage (PD)"
    && c != "Uninsured Bodily Injury (UMBI)"
    && c != "Comp/Col Deductible"
    && c != "Medical Payments (MP)"
    && c != "Underinsured Bodily Injury (UIMBI)"
    && c != "Personal Insurance Protection (PIP)"
    && c != "UMPD"
    && c != "Uninsured/Underinsured Bodily Injury (UMBI/UIMBI)").ToList();

    for(var i = 0;i < g.Count;i++)
    {
      WriteLine($"({i + 1}) {g[i]}");
    }

    //WriteLine(res);
    //var m = _ApiDiamond.DbFalcon.SqlFirstOrDefault("EXEC [Enum].[SpGetStateDropDown] @State=@1;",c => c.AddValues(En_State.OK.ToInt()),SqlDropdowns.Cast);
    //WriteLine(m);
  }

  [Test]
  public void LoadBillingData()
  {
    var sess = GetSession();
    var billingData = _ApiDiamond.Billing.LoadBillingData(sess,49169806);
    WriteLineOutput(billingData);
  }

  [Test]
  public void PaymentProfile()
  {
    var result = _ApiDiamond.DbFalcon.Sql("SELECT TOP(1) default_payment_profile_identifier FROM Diamond..CreditCardTokenClientInfo(NOLOCK) WHERE client_id = @1;",c => c.AddValues(45827864));
    var m = result.FirstOrDefault(c => c["default_payment_profile_identifier"] != null);
    var result1 = _ApiDiamond.DbFalcon.Sql("SELECT TOP(1) default_payment_profile_identifier FROM Diamond..CreditCardTokenClientInfo(NOLOCK) WHERE client_id = @1;",c => c.AddValues(45827867));
    var m1 = result1.FirstOrDefault(c => c["default_payment_profile_identifier"] != null);
    var m2 = result1.FirstOrDefault()?["default_payment_profile_identifier"];
    var m3 = result.FirstOrDefault()?["default_payment_profile_identifier"];
  }

  [Test]
  public void PayPlans()
  {
    var sess = GetSession();
    var image = _ApiDiamond.Policy.LoadImage(sess,49169806,1);
    var request = new Diamond.Common.Services.Messages.BillingService.CreateMultipleShortPreviewInvoices.Request()
    {
      RequestData = new()
      {
        PolicyImage = image
      }
    }.ApplySession(sess);

    var output = _ApiDiamond.DiamondService.Billing.CreateMultipleShortPreviewInvoices(request);
    WriteLineOutput(output);
    WriteLineOutput(image);
  }

  [Test]
  public void SaveCreditCardOnOneInc()
  {
    var res = SaveCardOnOneInc();
    WriteLine(res);
  }

  [Test]
  public void Translation()
  {
    var e = FileDeserialize<List<string>>("Translation/Enums.json");
    var enumLabels = e.Distinct().ToList();
    var uWQLabels = FileDeserialize<List<string>>("Translation/UnderwritingQs.json");
    var otherLabels = FileDeserialize<List<string>>("Translation/En.json").TakeLast(12).ToList();

    enumLabels.AddRange(uWQLabels);
    enumLabels.AddRange(otherLabels);
    var list = enumLabels.Select((c,i) => $"({i + 1}) {c}");
    list.ForEach(c => Console.WriteLine(c));
    //WriteLine(enumLabels);
  }

  [Test]
  public void GetPayplanTypeId()
  {
    const string sqlQuery = """
        SELECT [paymenttype_id]
        FROM [Diamond].[dbo].[BillingPayPlan]
        where [billingpayplan_id] = @1
        """;
    var res = _ApiDiamond.DbFalcon.SqlFirstOrDefault<int>(sqlQuery,c => c.AddValues(11));
  }

  [Test]
  public void UpdateQuote()
  {
    var sess = GetSession();
    var instance = FileDeserialize<Instance>("ttest1.json");
    var image1 = instance.ToImage(sess,_ApiDiamond.SystemDate).New;

    var submitedImage1 = _ApiDiamond.Policy.Submit(sess,image1,instance.Agency.State);

    var instanceFromImage1 = submitedImage1.Image.ToInstance(sess);

    var uwRs = FileDeserialize<List<UnderwritingResponse>>("UnderwritingResponsesFromCP.json");

    instanceFromImage1.Policy.UnderwritingResponses = uwRs;
    instanceFromImage1.Drivers.ForEach(d =>
    {
      d.License.Number = "123123123";
      d.Employment.Employer = "Falcon";
      d.Employment.Occupation = "hmm";
    });
    instanceFromImage1.InsVehicles.Vehicles[0].Vin = "ZFB4FACH1L1234567";
    //instanceFromImage1.Vehicles[0].Vin = "1C3CDFEB2G1234567";
    //instanceFromImage.Vehicles[0].Vin = "2HGFG3A50E2132132";
    //instanceFromImage.Vehicles[1].Vin = "1GTH5PFE0D4556765";
    instanceFromImage1.PolicyHolder.Phones =
    [
      new()
      {
        Number = "8765676545",
        Type = En_PhoneNumber.Cellular
      }
    ];

    WriteLine("1st");
    WriteLine(instanceFromImage1.InsVehicles.Vehicles[0]);

    var image2 = instanceFromImage1.ToImage(sess,_ApiDiamond.SystemDate).New;

    var submitedImage2 = _ApiDiamond.Policy.Submit(sess,image2,instance.Agency.State);

    var instanceFromImage2 = submitedImage2.Image.ToInstance(sess);

    WriteLine("2nd");
    WriteLine(instanceFromImage2.InsVehicles.Vehicles[0]);

    ChangeDriver(instanceFromImage2);
    ChangeVehicle(instanceFromImage2);

    var image3 = instanceFromImage2.ToImage(sess,_ApiDiamond.SystemDate).New;

    var submitedImage3 = _ApiDiamond.Policy.Submit(sess,image3,instance.Agency.State);
    //var submitedImage3 = _Api.Policy.Submit(sess,image3,instance.Agency.State);

    var instanceFromImage3 = submitedImage3.Image.ToInstance(sess);

    WriteLine("3rd");
    WriteLine(instanceFromImage3.InsVehicles.Vehicles[0]);
  }

  #endregion Public Methods

  #region Private Methods

  private static Guid GetAgencyGroupId(string str)
  {
    if(str == null)
    {
      return default;
    }
    //var guid = str.Substring(7,36);
    var guid = str.Split(new char[] { '/' },StringSplitOptions.RemoveEmptyEntries);
    return Guid.Parse(guid[1]);
  }

  private void ChangeDriver(Instance instance)
  {
    instance.PolicyHolder.Name.First = "Helen";
    instance.PolicyHolder.Name.Last = "Gone";
    instance.PolicyHolder.Name.Gender = En_Gender.Female;
    instance.Drivers[0].Name.First = "Helen";
    instance.Drivers[0].Name.Last = "Gone";
    instance.Drivers[0].Name.Gender = En_Gender.Female;
  }

  private void ChangeVehicle(Instance instance)
  {
    instance.InsVehicles.Vehicles[0].Year = 2013;
    instance.InsVehicles.Vehicles[0].Make = new("gmc","GMC");
    instance.InsVehicles.Vehicles[0].Model = new("canyon crew cab sle2","CANYON CREW CAB SLE2");
    instance.InsVehicles.Vehicles[0].BodyType = new("17","Pickup - Four Wheel Drive 4 door");
    instance.InsVehicles.Vehicles[0].Symbol = new("1gth5pfe&d (18) (19) (00)","1GTH5PFE&D (18) (19) (00)");
    instance.InsVehicles.Vehicles[0].Vin = null;
  }

  private ConsumerSession GetSession() => _Session ??= _ApiDiamond.Login.ForDomainUsername<ConsumerSession>("ConsumerService");

  private SaveCardObj SaveCardOnOneInc()
  {
    _Api = new ConsumerApi();
    var paymentToken = _Api.OneInc.GetSessionId();
    var url = _Api.OneInc.GetSaveUrl();
    var cc = new
    {
      Card = new
      {
        Number = "4111111111111111",
        ExpirationMonth = 2,
        ExpirationYear = 2024,
        Holder = new
        {
          Name = "Pri test",
          Zip = "75001"
        },
      },
      PortalOneSessionKey = paymentToken,
    };

    return SaveCardOnOneIncAsync(cc,url).Result;
  }

  private async Task<SaveCardObj> SaveCardOnOneIncAsync(object obj,string url)
  {
    try
    {
      var res = await _Client.With(c => c.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type","application/json"))
      .PostAsync(url,obj,JsonFormatter).ConfigureAwait(false);
      if(res.IsSuccessStatusCode)
      {
        return await res.Content.ReadAsAsync<SaveCardObj>().ConfigureAwait(false);
      }
    }
    catch
    {
      return null;
    }

    return null;
  }

  #endregion Private Methods
}

public class SaveCardObj
{
  #region Public Properties

  public string BaseCardType { get; set; }
  public string SaveCardError { get; set; }
  public string Token { get; set; }

  #endregion Public Properties
}
