using ConsumerServiceHost.Core;
using Diamond.Common.Enums;
using Diamond.Common.Enums.Security;
using Diamond.Common.Objects;
using Diamond.Common.Objects.Policy;
using Diamond.Common.Objects.Transactions;
using DiamondServices.Service;
using DCS = Diamond.Common.Services;

namespace Tests.ConsumerHandlerTest;

[TestFixture]
public class DiamondTest
{
  #region Private Fields

  private const string _FileName = "SubmitApplication-220209 17023826.json";
  private readonly DiamondService _Service = new("http://T02-MLT-0001");
  private ConsumerSession _DiamondSession;

  #endregion Private Fields

  #region Public Methods

  [SetUp]
  public void _SetUp() => Mocking<IResolver>(c => c.SetLogger()).SetResolver();

  [Test]
  public void DiamonDate()
  {
    DateTime d = _Service.SystemDate;
    WriteLine(d);
    var x = _Service.Utility.GetSystemDate(new());

    WriteLine(x);
    var y = _Service.Utility.GetSystemTime(new());

    WriteLine(y);
  }

  [Test]
  public void DiamondSessionWithDiamondId()
  {
    try
    {
      DCS.Messages.LoginService.GetDiamTokenForUsersId.Request req = new() { RequestData = new() { UsersId = 14516 } };
      var response = _Service.Login.GetDiamTokenForUsersId(req);
      WriteLine(response.ResponseData);
      if(response?.ResponseData?.DiamondSecurityToken == null)
      {
        throw new Exception("Login Failure");
      }
    }
    catch(Exception e)
    {
      throw new DiamondException("Failed establishing diamond token for methods.",e);
    }
  }

  [Test]
  public void DiamondSessionWithUsernamePassword()
  {
    try
    {
      var res = GetSession();
      WriteLine(res);
    }
    catch(Exception e)
    {
      throw new DiamondException("Failed establishing diamond token for methods.",e);
    }
  }

  [Test]
  public void DiamondSessionWithUsernamePassword1sda()
  {
    var response = _Service.Login.GetDiamTokenForUsernamePassword(new DCS.Messages.LoginService.GetDiamTokenForUsernamePassword.Request
    {
      RequestData =
          {
            LoginName = "mrevilla",
            Password = "apples.123"
          }
    });

    var diamondSession = new ConsumerSession(response.ResponseData.DiamondSecurityToken,response.ResponseData.DiamondTrustedSecurityToken);

    var diamondImage = CreateDiamondImage();
    SetOtherDiamondImageObjects(diamondImage,diamondSession,83,749);
    //WriteLine(diamondImage);

    var req1 = new DCS.Messages.PolicyService.Rate.Request()
    {
      RequestData = new()
      {
        PolicyImage = diamondImage,
      }
    }.ApplySession(diamondSession);
    req1.RequestData.PolicyImage.LOB.PolicyLevel.RateNoReports = true;

    var res = _Service.Policy.Rate(req1);
    //WriteLine(res);
    WriteLine(res.ResponseData);
  }

  [Test]
  public void DiamondSessionWithUsernamePassword2()
  {
    var diamondSession = GetSession();
    //We need to set DiamondAgencyId=>AgencyId & DiamondAgencyGroupId=>AgencyGroupId
    var agencyResponse = _Service.Security.GetUserAgency(new() { RequestData = new() { UsersId = diamondSession.DiamondSecurityToken.DiamUserId } }).ResponseData.Agency;

    var diamondImage = CreateDiamondImage();
    SetOtherDiamondImageObjects(diamondImage,diamondSession,83,749);

    DCS.Messages.PolicyService.SubmitApplication.Request request = new()
    {
      RequestData =
          {
            IsQuote = true,
            SubmitVersion = new SubmitVersion(1, 44, 1, diamondImage.TransactionEffectiveDate, (Diamond.Common.Enums.TransType) diamondImage.TransactionTypeId, diamondImage.TransactionEffectiveDate),
            PolicyImage = diamondImage,
          }
    };
    File.WriteAllText(@$"C:\Temp\Test-{DateTime.Now:yyMMdd HHmmssff}.json",request.SerializeJson());
    var res = _Service.Policy.SubmitApplication(request);

    WriteLine(res.ResponseData);
    WriteLine(res.DiamondValidation.Children);
  }

  [Test]
  public void DiamondSessionxyDomainUsername()
  {
    DCS.Messages.LoginService.GetDiamTokenForDomainUsername.Request req = new() { RequestData = new() { LoginName = "mrevilla",LoginDomain = "384" } };
    var response = _Service.Login.GetDiamTokenForDomainUsername(req);
    WriteLine(response.ResponseData);
  }

  [Test]
  public void RateFromFile()
  {
    var request = FileDeserialize<DCS.Messages.PolicyService.SubmitApplication.Request>(_FileName);
    request.ApplySession(GetSession());
    var response = _Service.Policy.SubmitApplication(request);
    var reqLoadImage = new DCS.Messages.PolicyService.LoadImage.Request()
    {
      RequestData =
          {
            PolicyId=response.ResponseData.PolicyId,
            ImageNumber=response.ResponseData.PolicyImageNum,
          }
    }.ApplySession(GetSession());

    var loadImageResponse = _Service.Policy.LoadImage(reqLoadImage);

    var requestRate = new DCS.Messages.PolicyService.Rate.Request
    {
      RequestData =
          {
             PolicyImage = loadImageResponse.ResponseData.Image,
          }
    }.ApplySession(GetSession());

    var saveRateResponse = _Service.Policy.Rate(requestRate);

    WriteLine(saveRateResponse.ResponseData.PolicyImage.FullTermPremium);
  }

  [Test]
  public void RateOnlyFromFile()
  {
    var request = FileDeserialize<DCS.Messages.PolicyService.SubmitApplication.Request>(_FileName);
    request.ApplySession(GetSession(1));

    //  File.WriteAllText(@$"C:\Temp\TestRequestBeforeSubmit-{DateTime.Now:yyMMdd HHmmssff}.json",request.SerializeJson());

    var response = _Service.Policy.SubmitApplication(request);
    var reqLoadImage = new DCS.Messages.PolicyService.LoadImage.Request()
    {
      RequestData =
          {
            PolicyId=response.ResponseData.PolicyId,
            ImageNumber=response.ResponseData.PolicyImageNum,
          }
    }.ApplySession(GetSession());

    var loadImageResponse = _Service.Policy.LoadImage(reqLoadImage);

    var requestRateOnly = new DCS.Messages.PolicyService.RateOnly.Request
    {
      RequestData =
          {
             PolicyImage = loadImageResponse.ResponseData.Image,
          }
    }.ApplySession(GetSession());

    var saveRateOnlyResponse = _Service.Policy.RateOnly(requestRateOnly);

    WriteLine(saveRateOnlyResponse.ResponseData.PolicyImage.FullTermPremium);
  }

  [Test]
  public void SubmitApplicationFromFile()
  {
    var request = FileDeserialize<DCS.Messages.PolicyService.SubmitApplication.Request>(_FileName);
    request.ApplySession(GetSession());
    var response = _Service.Policy.SubmitApplication(request);
    var reqLoadImage = new DCS.Messages.PolicyService.LoadImage.Request()
    {
      RequestData =
          {
            PolicyId=response.ResponseData.PolicyId,
            ImageNumber=response.ResponseData.PolicyImageNum,
          }
    }.ApplySession(GetSession());

    var loadImageResponse = _Service.Policy.LoadImage(reqLoadImage);

    var requestSaveRate = new DCS.Messages.PolicyService.SaveRate.Request
    {
      RequestData =
          {
            Image = loadImageResponse.ResponseData.Image,
          }
    }.ApplySession(GetSession());

    var saveRateResponse = _Service.Policy.SaveRate(requestSaveRate);
    WriteLine(saveRateResponse.ResponseData);
  }

  [Test]
  public void PromotePolicy()
  {
    var sess = GetSession();

    var reqLoadImage = new DCS.Messages.PolicyService.LoadImage.Request()
    {
      RequestData =
          {
            PolicyId=49170327,
            ImageNumber=1,
          }
    }.ApplySession(sess);

    var loadImageResponse = _Service.Policy.LoadImage(reqLoadImage);

    var req = new DCS.Messages.PolicyService.PromoteQuoteToPending.Request()
    {
      RequestData = new()
      {
        PolicyId = 49170327,
        PolicyImageNum = 1,
      }
    }.ApplySession(sess);

    var promote = _Service.Policy.PromoteQuoteToPending(req);
    WriteLine(promote);
  }

  #endregion Public Methods

  #region Private Methods

  private Image CreateDiamondImage()
  {
    var effectiveDate = DateTime.Now.ToInsDateTime();
    var expirationDate = effectiveDate.AddMonths(6);
    var image = new Image
    {
      TransactionTypeId = (int)Diamond.Common.Enums.TransType.NewPolicy,
      QuoteTypeId = (int)QuoteType.QuickQuote,
      ReceivedDate = (InsDateTime)_Service.SystemDate,
      TransactionDate = (InsDateTime)_Service.SystemDate,
      EffectiveDate = effectiveDate,
      GuaranteedRatePeriodEffectiveDate = effectiveDate,
      TransactionEffectiveDate = effectiveDate,
      PolicyStatusCodeId = (int)PolicyStatusCode.Quote,
      PolicyTermId = 1, //DiaId for 6 months
      ExpirationDate = expirationDate,
      TransactionExpirationDate = expirationDate,
      GuaranteedRatePeriodExpirationDate = expirationDate,
      PolicyHolder = new() { DetailStatusCode = (int)StatusCode.Active },
      AgencyId = 41,
      AgencyProducerId = 14,
    };
    image.PolicyHolder.Address = new() { DetailStatusCode = (int)StatusCode.Active,StreetName = "" };

    return image;
  }

  private Image CreateImageDriverVehicle(Image image)
  {
    var lob = image.LOB ??= new();
    var riskLevel = lob.RiskLevel ??= new();
    var drivers = riskLevel.Drivers ??= [];
    drivers.Add(GetDriver());
    var vehicles = riskLevel.Vehicles ??= [];
    vehicles.Add(GetVehicle());
    return image;
  }

  private Driver GetDriver() => new()
  {
    Name = new()
    {
      FirstName = "Priyanka",
      LastName = "K",
      BirthDate = DateTime.Parse("Mar 14, 1994").ToInsDateTime()
    }
  };

  private ConsumerSession GetSession(int i = 0)
  {
    if(_DiamondSession is null)
    {
      _DiamondSession = i switch
      {
        1 => GetSessionForDomainUsername(),
        2 => GetSessionForUsersId(),
        _ => GetSessionForUsernamePassword(),
      };
    }
    return _DiamondSession;

    ConsumerSession GetSessionForUsernamePassword()
    {
      var response = _Service.Login.GetDiamTokenForUsernamePassword(new DCS.Messages.LoginService.GetDiamTokenForUsernamePassword.Request
      {
        RequestData =
        {
            LoginName = "FalconService",
            Password = "sr7OCOFIyLkPhYjtDJIJ"
          }
      });
      return new ConsumerSession(response.ResponseData.DiamondSecurityToken,response.ResponseData.DiamondTrustedSecurityToken);
    }

    ConsumerSession GetSessionForDomainUsername()
    {
      DCS.Messages.LoginService.GetDiamTokenForDomainUsername.Request req = new()
      {
        RequestData = new()
        {
          BusinessInterfaceSourceId = BusinessInterfaceSourceType.DiamondAgencyPortal,
          LoginName = "ConsumerService",
          LoginDomain = "CORP"
        }
      };
      var response = _Service.Login.GetDiamTokenForDomainUsername(req);
      var token = response.ResponseData.diamondSecurityToken;
      return new ConsumerSession(token);
    }

    ConsumerSession GetSessionForUsersId()
    {
      DCS.Messages.LoginService.GetDiamTokenForUsersId.Request req = new() { RequestData = new() { UsersId = 14516 } };

      var response = _Service.Login.GetDiamTokenForUsersId(req);
      var token = response.ResponseData.DiamondSecurityToken;
      return new ConsumerSession(token);
    }
  }

  private Vehicle GetVehicle() => new()
  {
    Year = 2021,
    Make = "Mazda",
    Model = "CX - 5 GRAND TOURING",
    BodyTypeId = 19,
  };

  private void PopulateClientName(Name clientName,Name driverName)
  {
    clientName.FirstName = driverName.FirstName;
    clientName.MiddleName = driverName.MiddleName;
    clientName.LastName = driverName.LastName;
    clientName.BirthDate = driverName.BirthDate;
    clientName.DLStateId = driverName.DLStateId;
    clientName.NameId = driverName.NameId;
    clientName.TypeId = driverName.TypeId;
    clientName.SexId = driverName.SexId;
    clientName.MaritalStatusId = driverName.MaritalStatusId;
    return;
  }

  private void SetAddress(Address address)
  {
    address.County = "DALLAS";
    address.Zip = "75001-0000";
  }

  private void SetOtherDiamondImageObjects(Image diamondImage,ConsumerSession diamondSession,int agencyId,int agencyProducerId)
  {
    CreateImageDriverVehicle(diamondImage);
    //diamondImage.RatingVersionId = 28;
    diamondImage.RatingVersionId = 0;
    diamondImage.TransactionUsersId = diamondSession.DiamondSecurityToken.DiamUserId;

    diamondImage.Policy.QuoteSourceId = 5;
    diamondImage.Policy.Account ??= new();

    //diamondImage.AgencyId = 83;
    diamondImage.AgencyId = agencyId;
    diamondImage.CurrentPayplanId = 43;

    //policy holder
    diamondImage.PolicyHolder.Name.Fill(diamondImage.LOB.RiskLevel.Drivers[0].Name);
    SetAddress(diamondImage.PolicyHolder.Address);

    //policy client
    diamondImage.Policy.Client ??= new Client();
    PopulateClientName(diamondImage.Policy.Client.Name,diamondImage.LOB.RiskLevel.Drivers[0].Name);
    SetAddress(diamondImage.Policy.Client.Address);

    //diamondImage.AgencyProducerId = diamondSession.DiamondAgencyProducerId;
    diamondImage.AgencyProducerId = agencyProducerId;
  }

  #endregion Private Methods
}
