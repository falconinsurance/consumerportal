using ConsumerServiceHost;
using ConsumerServiceHost.Core;
using ConsumerServiceHost.DependencyInjection;
using ConsumerServiceHost.Models;
using ConsumerServiceHost.Models.Dropdowns;
using Diamond.UI.StaticDataManager;
using DiamondServices.Apis;
using DiamondServices.Service;
using Falcon.Core.Models;
using Falcon.DependencyInjection;
using Falcon.Enums;
using Falcon.FalconDB;
using Falcon.Tools.VehicleInfoCache;
using DB = Falcon.Tools.DBHandler;
using DCE = Diamond.Common.Enums;

namespace Tests.ConsumerHandlerTest;

[TestFixture]
public class ConsumerApiTest
{
  #region Public Fields

  public static ConsumerSession _Session;

  #endregion Public Fields

  #region Private Fields

  private const EnServer _Environment = EnServer.Test02;
  private const string _FileName = "GetRate-TX.json";
  private readonly IDiamondApi _Api;

  #endregion Private Fields

  #region Public Properties

  public ServicesHost ServicesHost { get; }

  #endregion Public Properties

  #region Public Constructors

  public ConsumerApiTest()
  {
    var falcon = new FalconHandler(o => o.NameOrConnection = GetConnection(_Environment,"Falcon"));
    var service = new DiamondService(GetDiamondHost(EnDiamonHost.Test02));
    Mocking<IResolver>(c =>
    {
      c.SetLogger();
      c.Setup(s => s.Resolve<IFalconHandler>()).Returns(falcon);
      c.Setup(s => s.Resolve<IDiamondService>()).Returns(service);
      //c.Setup(s => s.Resolve<IDiamondApi>()).Returns(_Api);
      c.Setup(s => s.Resolve<IEnTypeLoader>()).Returns(new EnTypeDbLoader());
    }).SetResolver();
    ServicesHost = new ServicesHost();
    _Api = AppResolver.Get<IDiamondApi>();
    //Mocking<IResolver>(c =>
    //{
    //  c.Setup(s => s.Resolve<DiamondClient>()).Returns(new DiamondClient());
    //});

    EnTypeHelper.SetLoader(FResolver.Resolve<IEnTypeLoader>);
  }

  #endregion Public Constructors

  #region Public Methods

  [Test]
  public void Phone()
  {
    var p = PhoneNumberType.FromDiaId((int)DCE.PhoneType.Business);
    var p1 = PhoneNumberType.From(DCE.PhoneType.Business);
  }

  [Test]
  public void BindPolicy()
  {
    var sess = GetSession();
    var instance = FileDeserialize<Instance>("tt.json");
    var images = instance.ToImage(sess,_Api.SystemDate);

    var submitedImage = _Api.Policy.SubmitApplication(sess,images.New,instance.Agency.State);

    var instanceFromImage = submitedImage.Image.ToInstance(sess);
    var uwRs = FileDeserialize<List<UnderwritingResponse>>("UnderwritingResponsesFromCP.json");

    instanceFromImage.Policy.UnderwritingResponses = uwRs;
    instanceFromImage.Drivers.ForEach(d => d.License.Number = "123123123");
    instanceFromImage.InsVehicles.Vehicles[0].Vin = "2HGFG3A50E2132132";
    instanceFromImage.InsVehicles.Vehicles[1].Vin = "1GTH5PFE0D4556765";
    instanceFromImage.PolicyHolder.Phones =
    [
      new()
      {
        Number = "8765676545",
        Type = En_PhoneNumber.Cellular
      },
    ];

    var loadImageWithUWQs = instanceFromImage.ToImage(sess,_Api.SystemDate).New;

    var ratedImage = _Api.Policy.SaveRate(sess,loadImageWithUWQs);

    var instanceAfterRate = ratedImage.Image.ToInstance(sess);

    var finalImage = instanceAfterRate.ToImage(sess,_Api.SystemDate);

    var ratedImage1 = _Api.Policy.SaveRate(sess,loadImageWithUWQs);

    //_Api.Policy.BindPolicy(sess,finalImage,new()
    //{
    //  Amount = 1,
    //  Eft = new()
    //  {
    //    AccountNumber = "123123",
    //    RoutingNumber = "071000013",
    //    Type = En_BankAccount.Checking,
    //    FullName = "RRR"
    //  }
    //});

    WriteLine("");
  }

  [Test]
  public void PromotePolicy()
  {
    var sess = GetSession();
    var loadedImage = _Api.Policy.LoadImage(sess,49170363,1);
    var promote = _Api.Policy.Promote(sess,49170363,1);
    var demote = _Api.Policy.Demote(sess,49170363,1);
  }

  [Test]
  public void BindPolicy2()
  {
    var sess = GetSession();

    var loadedImage = _Api.Policy.LoadImage(sess,45864409,1);

    //_Api.Policy.BindPolicy(sess,loadedImage,new()
    //{
    //  Amount = 1,
    //  Eft = new()
    //  {
    //    AccountNumber = "123123",
    //    RoutingNumber = "071000013",
    //    Type = En_BankAccount.Checking,
    //    FullName = "RRR"
    //  }
    //});

    WriteLine("");
  }

  [Test]
  public void BindPolicy3()
  {
    var sess = GetSession();
    var instance = FileDeserialize<Instance>("tt.json");
    var image = instance.ToImage(sess,_Api.SystemDate).New;

    var submitedImage = _Api.Policy.Submit(sess,image,instance.Agency.State);

    var instanceFromImage = submitedImage.Image.ToInstance(sess);

    var uwRs = FileDeserialize<List<UnderwritingResponse>>("UnderwritingResponsesFromCP.json");

    instanceFromImage.Policy.UnderwritingResponses = uwRs;
    instanceFromImage.Drivers.ForEach(d =>
    {
      d.License.Number = "123123123";
      d.Employment.Employer = "Falcon";
      d.Employment.Occupation = "hmm";
    });
    instanceFromImage.InsVehicles.Vehicles[0].Vin = "2B3CJ7DJ1B1234567";
    //instanceFromImage.Vehicles[0].Vin = "2HGFG3A50E2132132";
    //instanceFromImage.Vehicles[1].Vin = "1GTH5PFE0D4556765";
    instanceFromImage.PolicyHolder.Phones =
    [
      new()
      {
        Number = "8765676545",
        Type = En_PhoneNumber.Cellular
      }
    ];

    var loadImageWithUWQs = instanceFromImage.ToImage(sess,_Api.SystemDate).New;

    var ratedImage = _Api.Policy.Submit(sess,loadImageWithUWQs,instance.Agency.State);

    var instance1 = ratedImage.Image.ToInstance(sess);

    var image1 = instance1.ToImage(sess,_Api.SystemDate);

    WriteLine("");
  }

  [Test]
  public void ConvertImageToInstance()
  {
    var ret = _Api.Static.GetState(12);
    ret = _Api.Static.GetState(13);
    ret = _Api.Static.GetState(14);
    WriteLine(ret);
  }

  [Test]
  public void Dem()
  {
    var d = $"Hello {_Environment}";
    WriteLine(d);
  }

  [Test]
  public void DropdownsTest()
  {
    //var obj = _Api.DbFalcon.Sql<SqlDropdowns>("Exec [Enum].[SpGetStateDropDown] @State=@1",c => c.AddValues(En_State.TX.ToInt()));
    //var q = _Api.DbFalcon.Sql("SELECT State, Value FROM Enum.tblStateDropDown (NOLOCK)",null,SqlDropdowns.Cast);
    //WriteLine(q);

    var m = FileDeserialize<SqlStateDropdowns>("tt.json");
    //var ret = q.GroupBy(g => g.State);
    //var ret2 = ret.ToDictionary(c => c.Key,c => c.Select(i => i.Value).FirstOrDefault());
    //var x = StateDropdowns.Convert(ret2[En_State.TX],En_Language.EN);
    //WriteLine(ret);
    //WriteLine(ret);
  }

  [Test]
  public void GetCounties()
  {
    var p = new DB.Record
    {
      ["State"] = En_State.OK,
      ["Zip"] = 75019
    };

    p = new DB.Record
    {
      ["State"] = En_State.TX,
      ["Zip"] = 76524
    };
    //var date = p.ToDateTime("EffectiveDate",default);
    var d = _Api.Static.GetCounties(p.To<En_State>("State"),p.To<string>("Zip"));
    WriteLine(d);
  }

  [Test]
  public void Login()
  {
    var forDomainUsername = _Api.Login.ForDomainUsername<ConsumerSession>("ConsumerService");
    WriteLine(forDomainUsername);

    var forUsersId = _Api.Login.ForUsersId<ConsumerSession>(forDomainUsername.UserId);
    WriteLine(forUsersId);

    var forUsernamePassword = _Api.Login.ForUsernamePassword<ConsumerSession>("mrevilla","apples.123");
    WriteLine(forUsernamePassword);
    WriteLine(_Api.Login.Session_ConsumerService);
  }

  [Test]
  public void Modifiers()
  {
    var ret = _Api.Static.CreateModifier(1,7);
    WriteLine(ret);
  }

  [Test]
  public void Payplan()
  {
    //var ret = _Api.Static.GetPayPlans(En_State.TX,1);
    //WriteLine(ret);
    var ret1 = _Api.Static.GetPayPlans(En_State.OK,1);
    var temp = ret1.Select(c => (c, PayPlanType.FromDiaId(c).ToString()));
    WriteLine(ret1);
    WriteLine(temp);
  }

  [Test]
  public void Payplan2()
  {
    _ = GetSession();
    var payPlans = SystemDataManager.SystemData.get_BillingPayPlans(1,2,1);
    var ret = payPlans?.Count > 0 ? payPlans[0].BillingPayPlanId : 1;
    WriteLine(ret);
  }

  [Test]
  public void SaveRate()
  {
    var sess = GetSession();

    var instance = FileDeserialize<Instance>("tt.json");
    var image = instance.ToImage(sess,_Api.SystemDate).New;

    var submitedImage = _Api.Policy.SubmitApplication(sess,image,instance.Agency.State);

    var instanceFromImage = submitedImage.Image.ToInstance(sess);

    var uwRs = FileDeserialize<List<UnderwritingResponse>>("UnderwritingResponsesFromCP.json");
    instanceFromImage.Policy.UnderwritingResponses = uwRs;

    var loadImageWithUWQs = instanceFromImage.ToImage(sess,_Api.SystemDate).New;

    var ratedImage = _Api.Policy.SaveRate(sess,loadImageWithUWQs);
    var instanceAfterRate = ratedImage.Image.ToInstance(sess);

    WriteLine(instanceAfterRate);
  }

  [Test]
  public void SubmitApplication()
  {
    var instance = FileDeserialize<Instance>($"GetRate-{_Environment}.json");
    var sess = GetSession();
    var image = instance.ToImage(sess,_Api.SystemDate).WriteToTempAsJson("ImageFromCP").New;
    var responseImage = _Api.Policy.SubmitApplication(sess,image,instance.Agency.State).WriteToTempAsJson("ResponseImageFromCP");
    var ret = responseImage.Image.ToInstance(sess).WriteToTempAsJson("RetFromCP");
    WriteLine(ret);
  }

  [Test]
  public void SubmitApplication2()
  {
    var instance = FileDeserialize<Instance>($"GetRate-{_Environment}.json");
    var sess = GetSession();
    var image = instance.ToImage(sess,_Api.SystemDate).New;
    image.WriteToTempAsJson("ImageFromCP");
    var responseImage = _Api.Policy.SubmitApplication(sess,image,instance.Agency.State);
    responseImage.WriteToTempAsJson("ResponseImageFromCP");
    var ret = responseImage.Image.ToInstance(sess);
    ret.WriteToTempAsJson("RetFromCP");

    var image2 = _Api.Policy.LoadImage(sess,responseImage.Image.PolicyId,responseImage.Image.PolicyImageNum);
    image2.WriteToTempAsJson("LoadedImage");
    var ratedImage = _Api.Policy.Rate(sess,image2);
    ratedImage.WriteToTempAsJson("ratedImage");
    var ret2 = ratedImage.ToInstance(sess);
    ret2.WriteToTempAsJson("Ret2FromCP");

    //instance.WriteToTempAsJson("InstanceFromImage-Test");
    WriteLine(responseImage);
    WriteLine(ret);
  }

  [Test]
  public void SystemDate()
  {
    var d = _Api.SystemDate;
    WriteLine(d);
    var x = _Api.SystemTime;
    WriteLine(x);
  }

  [Test]
  public void SystemVersionIdFromState()
  {
    var date = _Api.SystemDate;
    var version = _Api.Static.GetVersionId(En_State.TX,date);
    WriteLine("versionId: " + version);
    var state = _Api.Static.GetState(28);
    WriteLine("state: " + state);
  }

  [Test]
  public void Vehicle()
  {
    const int year = 2021;
    var make = new IdDescEntity("mazda","Mazda");
    var model = new IdDescEntity("cx-5 grand touring awd","CX-5 GRAND TOURING AWD");
    var bodyType = new IdDescEntity("19","Utility Vehicle");
    var a = _Api.Vehicle.GetYears();
    WriteLine(a);
    var b = _Api.Vehicle.GetMakes(year);
    WriteLine(b);
    var c = _Api.Vehicle.GetModels(year,make);
    WriteLine(c);
    var d = _Api.Vehicle.GetBodyTypes(year,make,model);
    WriteLine(d);
    var e = _Api.Vehicle.GetSymbols(year,make,model,bodyType);
    WriteLine(e);
    var y = _Api.Vehicle.GetVehicles("JM3KFBDM&M");
    WriteLine(y);
  }

  [Test]
  public void ZQuote()
  {
    var sess = GetSession();
    var instance = FileDeserialize<Instance>("tt.json");
    var tempV = instance.InsVehicles.Vehicles[1];
    instance.InsVehicles.Vehicles.RemoveAt(1);

    var image = instance.ToImage(sess,_Api.SystemDate).New;

    var submitedImage = _Api.Policy.SubmitApplication(sess,image,instance.Agency.State);

    //var ppLoadImage = _Api.Policy.LoadImage(sess,submitedImage.PolicyId,submitedImage.PolicyImageNum);
    //var ppRatedImage = _Api.Policy.SaveRate(sess,ppLoadImage);

    var instanceFromImage = submitedImage.Image.ToInstance(sess);

    instanceFromImage.InsVehicles.Vehicles[0].Make = tempV.Make;
    instanceFromImage.InsVehicles.Vehicles[0].Model = tempV.Model;
    instanceFromImage.InsVehicles.Vehicles[0].Year = tempV.Year;
    instanceFromImage.InsVehicles.Vehicles[0].BodyType = tempV.BodyType;
    instanceFromImage.InsVehicles.Vehicles[0].Symbol = tempV.Symbol;

    var loadImage = instanceFromImage.ToImage(sess,_Api.SystemDate).New;

    WriteLine(loadImage.LOB.RiskLevel.Vehicles);

    var ratedImage = _Api.Policy.SaveRate(sess,loadImage);

    WriteLine("AFTER UDPATE");

    WriteLine(ratedImage.Image.LOB.RiskLevel.Vehicles);
  }

  #endregion Public Methods

  #region Private Methods

  private ConsumerSession GetSession() => _Session ??= _Api.Login.ForDomainUsername<ConsumerSession>("ConsumerService");

  #endregion Private Methods
}
