using ConsumerPortal.Middleware;
using Newtonsoft.Json;

namespace ConsumerPortal.DependencyInjenction;

internal static class AutofacHelper
{
  #region Private Fields

  private static readonly Dictionary<string,object> _Registry = new();

  #endregion Private Fields

  #region Public Methods

  public static T GetSection<T>(this IConfiguration config)
  {
    var name = typeof(T).Name;
    return (T)(_Registry.TryGetValue(name,out var val) ? val : _Registry[name] = config.GetSection(name).Get<T>());
  }

  #endregion Public Methods

  #region Internal Methods

  internal static IConfiguration RegisterAutofac(this IConfiguration config)
  {
    var falcon = config.GetSection<Falcon>();
    // falcon.ToTempFile();
    Loader.Load(falcon.Uri,falcon.UriPolicyPrivacy);

    return config;
  }

  internal static void ToTempFile<T>(this T me)
  {
    var tempPath = @"C:\Temp\ConsumerPortal-" + typeof(T).Name + $" {DateTime.Now:MM-dd HHmmssff}.json";
    File.WriteAllText(tempPath,JsonConvert.SerializeObject(me));
  }

  #endregion Internal Methods
}

internal class Falcon
{
  #region Private Fields

  private string _Uri;
  private string _UriPolicyPrivacy;

  #endregion Private Fields

  #region Public Properties

  public string Server { set; get; }
  public string Uri => _Uri ??= GetUri();
  public string UriPolicyPrivacy => _UriPolicyPrivacy ??= GetUriPolicyPrivacy();

  #endregion Public Properties

  #region Private Methods

  private string GetUri()
  {
    var ser = Server?.ToLower()?.Trim() switch
    {
      "demo" => "192.168.81.105",
      "production" => "192.168.41.86",
      "sandbox" => "192.168.50.170",
      "selenium" => "192.168.61.26",
      "test-01" => "192.168.61.86",
      "test-02" => "192.168.61.125",
      "test-03" => "192.168.61.135",
      _ => "localhost",
    };
    return "http://" + ser + ":23246";
  }

  private string GetUriPolicyPrivacy()
  {
    var ser = Server?.ToLower()?.Trim() switch
    {
      "production" => "insured",
      "sandbox" => "insured-sandbox",
      "selenium" => "insured-selenium",
      "test-01" => "insured-test",
      "test-02" => "insured-test-0002",
      "test-03" => "insured-test-0003",
      _ => "insured-test",
    };
    return $"https://{ser}" + ".falconinsgroup.com/{0}/Privacy/Policy";
  }

  #endregion Private Methods
}
