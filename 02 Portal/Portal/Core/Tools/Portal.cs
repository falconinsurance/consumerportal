﻿using Falcon.Tools;
using Tools.Logger;

namespace ConsumerPortal;

public static class Portal
{
  #region Private Fields
  private static readonly IRelax<ILogHandler> _LogHandler = RelaxFactory.Create(() => FResolver.Resolve<ILogHandler>());
  #endregion Private Fields

  #region Public Properties
  public static ILogHandler LogHandler => _LogHandler.Value;
  #endregion Public Properties

  #region Public Methods

  public static ILog GetLogger(string className) => _LogHandler?.Value?.GetLogger(className) ?? new DumyLog();

  #endregion Public Methods
}
