using System.Net;
using System.Net.Http.Formatting;
using Falcon.Tools;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Tools.Logger;

namespace ConsumerPortal;

public class FalconHttpClient:IFalconHttpClient
{
  #region Private Fields

  private static readonly JsonMediaTypeFormatter _JsonFormatter = new()
  {
    SerializerSettings = {
        NullValueHandling = NullValueHandling.Ignore,
        DefaultValueHandling = DefaultValueHandling.Ignore,
         ContractResolver = new DefaultContractResolver {
           IgnoreSerializableAttribute = true
         }
      }
  };

  private static readonly JsonMediaTypeFormatter[] _JsonFormatterArray = { _JsonFormatter };
  private static readonly IRelax<ILog> _LogRelaxed = FResolver.Relaxed(() => Portal.GetLogger(nameof(FalconHttpClient)));
  private readonly HttpClient _Client;
  #endregion Private Fields

  #region Public Constructors

  public FalconHttpClient(string baseAddress)
  {
    _Client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip })
    {
      BaseAddress = new Uri(baseAddress),
      Timeout = TimeSpan.FromMinutes(30)
    };
  }

  #endregion Public Constructors

  #region Private Properties
  private ILog _Log => _LogRelaxed.Value;
  #endregion Private Properties

  #region Public Methods

  public Res GetJson<Res>(string uri) => GetJsonAsync<Res>(uri).GetAwaiter().GetResult();

  public async Task<Res> GetJsonAsync<Res>(string uri)
  {
    try
    {
      using var response = await _Client.GetAsync(uri).ConfigureAwait(false);
      if(response.IsSuccessStatusCode)
      {
        return await response.Content.ReadAsAsync<Res>(_JsonFormatterArray).ConfigureAwait(false);
      }
      await Task.Delay(100).ConfigureAwait(false);
      using var response2 = await _Client.GetAsync(uri).ConfigureAwait(false);
      if(response2.IsSuccessStatusCode)
      {
        return await response2.Content.ReadAsAsync<Res>(_JsonFormatterArray).ConfigureAwait(false);
      }
      _Log.Error(note: $"{_Client.BaseAddress}{uri}",obj: null,ex: null,msg: $"GetJsonAsync:  \n{response2.StatusCode}\n{response2.ReasonPhrase}");
      throw new($"GetJsonAsync:{uri}  \n{response2.StatusCode}\n{response2.ReasonPhrase}");
    }
    catch(Exception)
    {
      throw;
    }
  }

  public Res PostJson<Res>(string uri,object reqObject)
  {
    try
    {
      return PostJsonAsync<Res>(uri,reqObject).GetAwaiter().GetResult();
    }
    catch
    {
      throw;
    }
  }

  public async Task<Res> PostJsonAsync<Res>(string uri,object reqObject)
  {
    try
    {
      using var response = await _Client.PostAsync(uri,reqObject,_JsonFormatter).ConfigureAwait(false);
      if(response.IsSuccessStatusCode)
      {
        return await response.Content.ReadAsAsync<Res>(_JsonFormatterArray).ConfigureAwait(false);
      }
      if(uri.In("Login","GetAgencyCodes"))
      {
        throw new();
      }
      await Task.Delay(100).ConfigureAwait(false);
      using var response2 = await _Client.PostAsync(uri,reqObject,_JsonFormatter).ConfigureAwait(false);
      if(response2.IsSuccessStatusCode)
      {
        return await response2.Content.ReadAsAsync<Res>(_JsonFormatterArray).ConfigureAwait(false);
      }

      _Log.Error(note: $"{_Client.BaseAddress}{uri}",obj: reqObject,ex: null,msg: $"PostJsonAsync:  \n{response2.StatusCode}\n{response2.ReasonPhrase}");
      throw new($"PostJsonAsync:{uri}  \n{response2.StatusCode}\n{response2.ReasonPhrase}");
      //var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
    }
    catch(Exception)
    {
      throw;
    }
  }

  #endregion Public Methods
}
