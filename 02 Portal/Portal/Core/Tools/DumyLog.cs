using System.Runtime.CompilerServices;
using Tools.Logger;

namespace ConsumerPortal;

internal class DumyLog:ILog
{
  #region Public Methods

  public Guid Debug(string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null)
  => Guid.Empty;

  public Guid Error(string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null)
   => Guid.Empty;

  public LogException Exception(string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null) => new LogException(Guid.Empty);

  public Guid Info(string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null)
  => Guid.Empty;

  public Guid Other(string action,string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null)
  => Guid.Empty;

  public Guid Warn(string key = null,object val = null,string note = null,object obj = null,string msg = null,Exception ex = null,[CallerMemberName] string methodName = null)
  => Guid.Empty;

  #endregion Public Methods
}
