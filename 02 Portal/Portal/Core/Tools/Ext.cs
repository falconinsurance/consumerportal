namespace ConsumerPortal;

public static class PortalExt
{
  #region Private Fields
  private static readonly Dictionary<string,object> _Registry = new();
  #endregion Private Fields

  #region Public Methods

  public static Task<T> GetAsync<T>(this T me) => Task.FromResult(me);

  public static bool In<T>(this T me,params T[] args)
  {
    foreach(var item in args)
    {
      if(me.Equals(item))
        return true;
    }

    return false;
  }

  public static bool In<T>(this T me,IEnumerable<T> args)
  {
    foreach(var item in args)
    {
      if(me.Equals(item))
        return true;
    }

    return false;
  }

  public static T With<T>(this T me,Action<T> action)
  {
    if(me is not null)
    {
      action(me);
    }
    return me;
  }

  public static M With<T, M>(this T me,Func<T,M> func) => (me is not null) ? func(me) : default;

  public static Task<T> WithAsync<T>(this T me,Action<T> action)
  {
    if(me is not null)
    {
      action?.Invoke(me);
    }
    return Task.FromResult(me);
  }

  public static Task<M> WithAsync<T, M>(this T me,Func<T,M> func) => Task.FromResult((me is not null) ? func(me) : default);

  #endregion Public Methods

  #region Internal Methods

  internal static T GetSection<T>(this IConfiguration config)
  {
    var name = typeof(T).Name;
    return (T)(_Registry.TryGetValue(name,out var val) ? val : _Registry[name] = config.GetSection(name).Get<T>());
  }

  internal static DependencyInjenction.Falcon GetSettings(this IConfiguration me) => me.GetSection<DependencyInjenction.Falcon>();

  #endregion Internal Methods
}
