using CompressedStaticFiles.AspNet;
using ConsumerPortal.DependencyInjenction;
using ConsumerPortal.Middleware;

namespace ConsumerPortal;

public class Startup
{
  #region Public Constructors

  public Startup(IConfiguration configuration)
  {
    configuration.RegisterAutofac();
  }

  #endregion Public Constructors

  #region Public Methods

  // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
  public void Configure(IApplicationBuilder app,IWebHostEnvironment env)
  {
    if(env.IsDevelopment())
    {
      app.UseDeveloperExceptionPage();
    }
    else
    {
      app.UseExceptionHandler("/Home/Error");
      // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
      app.UseHsts();
    }
    //app.UseHttpsRedirection();
    //app.UseStaticFiles();

    app.UseCompressedStaticFiles();

    app.UseRouting();

    app.UseAuthorization();

    app.UseAppMiddleware();

    app.UseEndpoints(endpoints =>
    {
      endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
    });
  }

  // This method gets called by the runtime. Use this method to add services to the container.
  public void ConfigureServices(IServiceCollection services)
  {
    services.AddControllersWithViews();
    services.AddCompressedStaticFiles();
  }

  #endregion Public Methods
}
