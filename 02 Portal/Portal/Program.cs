namespace ConsumerPortal;

public static class Program
{
  #region Public Methods

  public static void Main()
  {
    if(Environment.UserInteractive)
    {
      var tempFile = new FileInfo("c:\\Temp\\Consumer\\Consumer-Check.txt");
      do
      {
        Thread.Sleep(500);
        tempFile.Refresh();
      }
      while(!tempFile.Exists);
    }
    CreateHostBuilder().Build().Run();
  }

  #endregion Public Methods

  #region Private Methods

  private static IHostBuilder CreateHostBuilder() => Host.CreateDefaultBuilder()
    .ConfigureWebHostDefaults(builder => builder.UseStartup<Startup>());

  #endregion Private Methods
}
