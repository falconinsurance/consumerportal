namespace ConsumerPortal;

public class Test
{
  #region Private Fields
  private readonly IConfigurationRoot _ConfigRoot;
  #endregion Private Fields
  #region Public Constructors

  public Test(IConfiguration configRoot)
  {
    _ConfigRoot = (IConfigurationRoot)configRoot;
  }

  #endregion Public Constructors
}
