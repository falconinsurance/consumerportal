namespace ConsumerPortal.Middleware;

public class AppMiddleware
{
  #region Protected Fields
  protected readonly IConfiguration _Config;
  #endregion Protected Fields

  #region Private Fields
  private readonly RequestDelegate _Next;
  #endregion Private Fields

  #region Public Constructors

  public AppMiddleware(RequestDelegate next,IConfiguration config)
  {
    _Next = next;
    _Config = config;
  }

  #endregion Public Constructors

  #region Public Methods

  public virtual async Task InvokeAsync(HttpContext httpContext) => await _Next(httpContext);

  #endregion Public Methods
}
