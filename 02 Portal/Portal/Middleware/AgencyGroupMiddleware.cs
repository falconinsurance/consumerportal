using System.Net.Http.Headers;
using ConsumerPortal.Models;

namespace ConsumerPortal.Middleware;

internal static class Loader
{
  #region Private Fields

  private static Dictionary<Guid,AgencyGroup> _AgencyGroups;
  private static Uri _BaseAddress;
  private static string _PrivacyPolicy;

  #endregion Private Fields

  #region Internal Properties

  internal static Dictionary<Guid,AgencyGroup> AgencyGroupsLookup => _AgencyGroups;
  internal static string PrivacyPolicy => _PrivacyPolicy;

  #endregion Internal Properties

  #region Internal Methods

  internal static void Load(string uri,string privacyPolicyUri)
  {
    _BaseAddress = new(uri);
    _AgencyGroups ??= GetAgencyGroupsDict().Result;
    _PrivacyPolicy ??= privacyPolicyUri;
  }

  #endregion Internal Methods

  #region Private Methods

  private static async Task<Dictionary<Guid,AgencyGroup>> GetAgencyGroupsDict()
  {
    using var http = NewClient();
    var res = await http.PostAsync("Consumer/GetAgencyGroups",new StringContent("")).ConfigureAwait(false);
    if(res.IsSuccessStatusCode)
    {
      return await res.Content.ReadAsAsync<Dictionary<Guid,AgencyGroup>>().ConfigureAwait(false);
    }
    return null;
  }

  private static HttpClient NewClient()
  {
    var result = new HttpClient { BaseAddress = _BaseAddress,Timeout = TimeSpan.FromMinutes(30) };
    result
        .DefaultRequestHeaders
        .Accept
        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
    return result;
  }

  #endregion Private Methods
}

public class AgencyGroupMiddleware:AppMiddleware
{
  #region Private Fields

  private static readonly Dictionary<Guid,AgencyGroup> _Lookup = Loader.AgencyGroupsLookup;

  #endregion Private Fields

  #region Public Constructors

  public AgencyGroupMiddleware(RequestDelegate next,IConfiguration config) : base(next,config)
  {
  }

  #endregion Public Constructors

  #region Public Methods

  public override async Task InvokeAsync(HttpContext httpContext)
  {
    var path = httpContext.Request.Path.Value;
    if(CheckCss(path))
    {
      if(TryGetAgencyGroupId(path,out var agencyId))
      {
        var agencyGroup = _Lookup[agencyId];
        try
        {
          if(path.EndsWith("root.css"))
          {
            var css = agencyGroup.Theme;
            await httpContext.Response.WriteAsync(css);
          }

          //if(path.EndsWith("logo"))
          //{
          //  var bytes = agencyGroup.Logo;
          //  httpContext.Response.ContentType = GetContentType(agencyGroup.LogoName);
          //  await httpContext.Response.Body.WriteAsync(bytes);
          //}
        }
        catch { }
      }
    }
    else
    {
      await base.InvokeAsync(httpContext);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static bool CheckCss(string path) => path.StartsWith("/theme/",System.StringComparison.OrdinalIgnoreCase);

  //private static string GetContentType(string logoName) => logoName switch
  //{
  //  "svg" => "image/svg+xml",
  //  _ => logoName,
  //};

  private static bool TryGetAgencyGroupId(string str,out Guid result)
  {
    result = default;
    if(str == null)
    {
      return true;
    }
    var guid = str.Split(new char[] { '/' },StringSplitOptions.RemoveEmptyEntries);
    if(guid.Length > 1)
    {
      result = Guid.TryParse(guid[1],out var value) ? value : default;
    }
    return true;
  }

  #endregion Private Methods
}
