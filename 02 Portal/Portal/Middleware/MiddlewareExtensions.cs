namespace ConsumerPortal.Middleware;

public static class MiddlewareExtensions
{
  #region Public Methods

  public static IApplicationBuilder UseAppMiddleware(
        this IApplicationBuilder builder)
  {
    builder.UseMiddleware<AgencyGroupMiddleware>();
    builder.UseMiddleware<AppRedirect>();
    builder.UseMiddleware<AppExtLinksMiddleware>();
    return builder;
  }

  #endregion Public Methods
}
