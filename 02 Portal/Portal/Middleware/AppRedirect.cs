using System.Net.Http.Headers;

namespace ConsumerPortal.Middleware;

public class AppRedirect:AppMiddleware
{
  #region Private Fields

  private const StringComparison _ORDINAL_IGNORE_CASE = StringComparison.OrdinalIgnoreCase;
  private static HttpClient _Client;

  #endregion Private Fields

  #region Public Constructors

  public AppRedirect(RequestDelegate next,IConfiguration config) : base(next,config)
  {
    _Client ??= GetClient(_Config.GetSection<DependencyInjenction.Falcon>().Uri);
  }

  #endregion Public Constructors

  #region Public Methods

  public override async Task InvokeAsync(HttpContext httpContext)
  {
    var path = httpContext.Request.Path.Value;
    if(CheckServicePath(path))
    {
      try
      {
        var res = await _Client.SendAsync(new RedirectedMsg(httpContext)).ConfigureAwait(false);
        await SendResponseMessage(res,httpContext.Response).ConfigureAwait(false);
      }
      catch(Exception ex)
      {
        var m = ex;
      }
    }
    else
    {
      await base.InvokeAsync(httpContext);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static bool CheckServicePath(string path) => path.Contains("/CP/",_ORDINAL_IGNORE_CASE);

  private static HttpClient GetClient(string uri)
  {
    var result = new HttpClient { BaseAddress = new(uri),Timeout = TimeSpan.FromMinutes(30) };
    result
        .DefaultRequestHeaders
        .Accept
        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
    return result;
  }

  private static async Task SendResponseMessage(HttpResponseMessage sourceRes,HttpResponse targetRes)
  {
    targetRes.StatusCode = (int)sourceRes.StatusCode;
    // Copy non-content headers
    var targetResponseHeaders = targetRes.Headers;
    foreach(var header in sourceRes.Headers)
    {
      targetResponseHeaders[header.Key] = header.Value.ToArray();
    }
    var responseContent = sourceRes.Content;
    if(responseContent == null)
    {
      // Set the content-length to 0 to prevent the server from sending back the response chunked
      targetResponseHeaders["Content-Length"] = new string[] { "0" };
    }
    else
    {
      // Copy content headers
      foreach(var contentHeader in responseContent.Headers)
      {
        targetResponseHeaders[contentHeader.Key] = contentHeader.Value.ToArray();
      }
      // Copy body
      await responseContent.CopyToAsync(targetRes.Body).ConfigureAwait(false);
    }
  }

  #endregion Private Methods
}

public class RedirectedMsg:HttpRequestMessage
{
  #region Public Constructors

  public RedirectedMsg(HttpContext httpContext,string path = null) : base(new HttpMethod(httpContext.Request.Method),requestUri: path ?? httpContext.Request.Path.Value + httpContext.Request.QueryString)
  {
    var request = httpContext.Request;
    var rHearders = request.Headers;
    Content = new StreamContent(content: request.Body);
    Content.Headers.TryAddWithoutValidation("Content-Type","application/json");
    foreach(var item in rHearders)
    {
      Headers.TryAddWithoutValidation(item.Key,item.Value.ToString());
    }
  }

  #endregion Public Constructors
}
