namespace ConsumerPortal.Middleware;

public class AppExtLinksMiddleware:AppMiddleware
{
  #region Private Fields

  private const StringComparison _ORDINAL_IGNORE_CASE = StringComparison.OrdinalIgnoreCase;
  private static string _PrivacyPolicy;

  #endregion Private Fields

  #region Public Constructors

  public AppExtLinksMiddleware(RequestDelegate next,IConfiguration config) : base(next,config)
  {
    var falcon = config.GetSection<DependencyInjenction.Falcon>();
    _PrivacyPolicy = falcon.UriPolicyPrivacy;
  }

  #endregion Public Constructors

  #region Public Methods

  public override async Task InvokeAsync(HttpContext httpContext)
  {
    var path = httpContext.Request.Path.Value;
    if(CheckPath(path,"PrivacyPolicy"))
    {
      var lang = CheckPath(path,"ES") ? "es" : "en";
      httpContext.Response.Redirect(string.Format(_PrivacyPolicy,lang));
      return;
    }
    else
    {
      await base.InvokeAsync(httpContext);
    }
  }

  #endregion Public Methods

  #region Private Methods

  private static bool CheckPath(string path,string key) => path.Contains($"/{key}",_ORDINAL_IGNORE_CASE);

  #endregion Private Methods
}
