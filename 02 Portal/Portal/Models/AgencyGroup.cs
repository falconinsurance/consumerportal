namespace ConsumerPortal.Models;

public class AgencyGroup
{
  #region Public Properties

  public Guid Id { get; set; }
  public string Name { get; set; }
  public object Setting { get; set; }
  public int State { get; set; }
  public string Theme { get; set; }

  #endregion Public Properties
}
