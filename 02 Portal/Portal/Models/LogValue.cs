namespace ConsumerPortal.Models;

public class LogValue
{
  #region Public Properties

  public string Message { get; set; }

  #endregion Public Properties
}
