namespace ConsumerPortal.Models;

public static class Helper
{
  #region Public Properties
  public static string VersionNumber { get; } = $"V{typeof(Program).Assembly.GetName().Version}";
  #endregion Public Properties
}
