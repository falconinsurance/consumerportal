# Vue Consumer Portal

## Project setup

```PS1
npm install
```

### Compiles and hot-reloads for development

```PS1
npm run serve
```

### Compiles for development

```PS1
npm run dev
```

### Compiles and minifies for production

```PS1
npm run prod
```

### Compiles and minifies for development and production

```PS1
npm run build
```

### Lints and fixes files

```PS1
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
