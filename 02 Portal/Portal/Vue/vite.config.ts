import { defineConfig, BuildOptions, ConfigEnv, UserConfig, PluginOption, splitVendorChunkPlugin } from 'vite'
import viteCompression from 'vite-plugin-compression';
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path';
import fs from 'fs';


const rootDir = resolve(__dirname, 'src');
const outDir = resolve(__dirname, '../wwwroot');

const currentBuild = (command: string, mode: string): BuildOptions => {

  console.log(mode, command)
  const format = 'esm';
  const minify = mode == 'production';

  const hash = true ? "-[hash]" : "";

  const GetFileNames = (assetInfo: { name?: string; }) => {
    let name = assetInfo.name || "";
    if (/CPIcoMoon/i.test(name)) {
      return `font/[name]${hash}[extname]`;
    }
    if (/.css/i.test(name)) {
      return `css/[name]${hash}[extname]`;
    }
    if (/.png|.jpe?g|.svg|.gif|.tiff|.bmp|.ico/i.test(name)) {
      return `img/[name]${hash}[extname]`;
    }
    if (/.html/i.test(name)) {
      return `htm/[name][extname]`
    }
    return `js/[name]${hash}.js`;
  };
  const home = resolve(rootDir, 'pages/home/home.html');

  return {
    emptyOutDir: true,
    outDir,
    minify,
    cssCodeSplit: true,
    assetsDir: resolve(rootDir, 'Assets'),
    assetsInlineLimit: 0,
    rollupOptions: {
      input: { home, },
      output: [{
        dir: outDir,
        format,
        assetFileNames: GetFileNames,
        chunkFileNames: GetFileNames,
        entryFileNames: GetFileNames,
      },
      ]
    }
  }
}

// https://vitejs.dev/config/
export default defineConfig((env: ConfigEnv): UserConfig => {
  return {
    plugins: [
      vue(),
      viteCompression({ filter: /\.(js|mjs|json|css|svg)$/i }),
      viteCopy(),
      splitVendorChunkPlugin()
    ],
    build: currentBuild(env.command, env.mode),
    server: {
      port: 44302,
      cors: true,
      proxy: {
        '/theme': {
          target: 'https://localhost:44300',// Proxies to ASP.NET
          changeOrigin: true,
          secure: false,
        },
        '/CP': {
          target: 'https://localhost:44300',// Proxies to ASP.NET
          changeOrigin: true,
          secure: false,
        },
        '/Other': {
          target: 'https://localhost:44300',// Proxies to ASP.NET
          changeOrigin: true,
          secure: false,
        }
      }
    },
    resolve: {
      alias: {
        "@": resolve(__dirname, 'src'),
        "@form": resolve(__dirname, 'src/Components/Form'),
        "@quoteSections": resolve(__dirname, 'src/Components/QuoteSections'),
        //   "@assets":  resolve(__dirname, 'src/Assets'),
        //   "@components": "./src/components",
        //   "@scripts": "./src/scripts",
        //   "@styles": "./src/styles",
        //   "@home": "./src/pages/home",
        //   "@shims": "./src/shims",
      }
    }
  }
});


const viteCopy = (options?: Record<string, string>): PluginOption => {
  const closeBundle = () => {
    const src = resolve(__dirname, '../');
    const fileName = 'wwwroot/src/pages/home/home.html';
    const fileIn = resolve(src, fileName);
    const fileOut = resolve(src, 'views/Home/home.cshtml');
    if (fs.existsSync(fileIn)) {
      fs.rename(fileIn, fileOut, (error) => {
        if (error) {
          throw error
        } else {

          console.log(`\n\x1b[33m[Moved]\x1b[0m\n${fileName}`)
        }
      })
    }
  }
  return {
    name: "my-plugin",
    apply: 'build',
    enforce: 'post',
    closeBundle,
  }
}
