export interface ILogValue {
	Message: string,
	Note: string,
	Action: string,
	Method: string,
	Class: string,
	Obj: object,
	Exception: any
}

export interface IResponse {
	Status: number,
	Error: TResError,
	Result: any
}

export interface TResError {
	Message: string,
	Exception: string
}