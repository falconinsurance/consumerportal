function Set(key: string, value: string) {
	sessionStorage.setItem(key, value);
}
function Get(key: string): string | null {
	return sessionStorage.getItem(key);
}
function GetConsumerId(): string | null {
	return sessionStorage.getItem("ConsumerId");
}
function Remove(key: string) {
	sessionStorage.removeItem(key);
}
function Clear() {
	sessionStorage.clear()
}

export default {
	Set,
	Get,
	GetConsumerId,
	Remove,
	Clear,
}
