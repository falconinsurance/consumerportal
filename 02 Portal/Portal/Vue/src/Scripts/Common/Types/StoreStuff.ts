export type TCommon = {
    AppVersion: string | null,
    CurrentYear: number,
    Language: string,
    IsBusy: boolean,
    Labels: Record<string, string>,
    Popup: any
}


