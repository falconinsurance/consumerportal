import { HTMLAttributes, InputHTMLAttributes, OptionHTMLAttributes, SelectHTMLAttributes } from "vue";

export type TFormGroup = HTMLAttributes & {
    header: string,
    required?: boolean,
    mask: string | undefined,
    error?: string,
    errParams: string[],
    validate: string,
    toolTip: string | undefined,
    disabled?: boolean,
    headers: string[]
}

export type TTextbox = InputHTMLAttributes & TFormGroup & {
    validate: () => void
}

export type TDropdown = {
    default: any,
    allow: boolean,
    item: SelectHTMLAttributes & TFormGroup,
    type: "IDBEntity" | "string" | "number",
    options: OptionHTMLAttributes[],
}

export type TRadio = {
    value: any,
    alignClass: "vertical" | "horizontal",
}

export type TCheckbox = {
    checked: boolean,
    type: "radio" | undefined,
    wrapped: boolean
}

export type TButton = TTextbox & {
    spanBefore: string,
    spanAfter: string,
    icon: string
}

export type TAddress = {
    street: TTextbox,
    city: TTextbox,
    state: TDropdown,
    zip: TTextbox,
    county: TDropdown
}

export type TTransistionTable = {
    header: string,
    showCount: boolean,
    hide: number[],
    displayName?: (item: any) => string | null
    removeBtn: TButton
}

export type TTickAnim = {
    id?: number | string,
    animationAction(): void,
    svgCls: string,
    pathCls: string,
    circleCls: string,
    checkCls: string
}

export type TGsap = {
    className: string,
    duration: number,
    cord: number | string,
    scale: number,
    staggerFrom: "start" | "center" | "end" | "edges" | "random",
    ease: "none" | "power1" | "power2" | "power2.in" | "power3.inOut"
}