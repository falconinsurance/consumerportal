const getCookie = (name: string): string | null => {
    const match = `; ${document.cookie}`.match(`;\\s*${name}=([^;]+)`);
    return match ? match[1].replaceAll('+', ' ') : null;
}

const deleteCookie = (name: string): void => {
    document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}

const getValidDate = (value: string): Date | undefined => {
    if (value && value.length == 10) {
        const tempDate = new Date(value);
        if (
            tempDate &&
            Object.prototype.toString.call(tempDate) === "[object Date]" &&
            !isNaN(tempDate.valueOf())
        ) {
            return tempDate;
        }
    }
    return undefined;
}

const hasValue = (obj: any) => {
    return obj != null && obj != undefined && obj != "";
}

const emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/;

const isValidEmail = (text: string) => {
    return emailReg.test(text);
}

const DATE_FULL_FORMAT = { month: 'long', day: 'numeric', year: 'numeric' } as Intl.DateTimeFormatOptions;

const getDateToday = (): string => {
    return new Date().toLocaleDateString("en-US", DATE_FULL_FORMAT);
}

const DATE_FORMAT = { day: "2-digit", month: "2-digit", year: "numeric" } as Intl.DateTimeFormatOptions;

const getDate = (date: Date): string => {
    return date && new Date(date).toLocaleDateString("en-US", DATE_FORMAT);
}

const getDateFromString = (date: Date): Date | undefined => {
    const tempDate = getDate(date);
    if (tempDate) {
        return getValidDate(tempDate);
    }
}

const getStringDateFromDate = (date: Date): string => {
    return date.toLocaleDateString("en-US", DATE_FORMAT);
}

export default {
    getCookie,
    deleteCookie,
    getValidDate,
    getDate,
    getDateFromString,
    isValidEmail,
    getStringDateFromDate,
    getDateToday,
    hasValue
};