import vHeader from '@/Components/Common/Layouts/Header.vue'
import vFooter from '@/Components/Common/Layouts/Footer.vue'
import vFormGroup from '@/Components/Common/Form/FormGroup.vue'
import vCheckbox from '@/Components/Common/Form/Checkbox.vue'
import vTextbox from '@/Components/Common/Form/Textbox/Textbox.vue'
import vDatepicker from '@/Components/Common/Form/Textbox/Datepicker.vue'
import vueDatepicker from '@vuepic/vue-datepicker'
import vDropdown from '@/Components/Common/Form/Dropdown.vue'
import vButton from '@/Components/Common/Form/Button.vue'
import vRadioButton from '@/Components/Common/Form/RadioButton.vue'
import vTransitionTable from '@/Components/Common/Layouts/TransitionTable.vue'
import vModalWrapper from '@/Components/Common/Layouts/ModalWrapper.vue'
import vActivityIndicator from '@/Components/Common/ActivityIndicator.vue'
import vAlert from '@/Components/Common/Alert.vue'
import vTickAnim from '@/Components/Common/TickAnim.vue'

export default [
	{
		Key: 'vHeader',
		Value: vHeader
	},
	{
		Key: 'vFooter',
		Value: vFooter
	},
	{
		Key: 'vFormGroup',
		Value: vFormGroup
	},
	{
		Key: 'vCheckbox',
		Value: vCheckbox
	},
	{
		Key: 'vTextbox',
		Value: vTextbox
	},
	{
		Key: 'vDatepicker',
		Value: vDatepicker
	},
	{
		Key: 'vueDatepicker',
		Value: vueDatepicker
	},
	{
		Key: 'vDropdown',
		Value: vDropdown
	},
	{
		Key: 'vButton',
		Value: vButton
	},
	{
		Key: 'vRadioButton',
		Value: vRadioButton
	},
	{
		Key: 'vTransitionTable',
		Value: vTransitionTable
	},
	{
		Key: 'vModalWrapper',
		Value: vModalWrapper
	},
	{
		Key: 'vActivityIndicator',
		Value: vActivityIndicator
	},
	{
		Key: 'vAlert',
		Value: vAlert
	},
	{
		Key: 'vTickAnim',
		Value: vTickAnim
	},
]
