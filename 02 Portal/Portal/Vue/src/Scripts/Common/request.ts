import axios, { AxiosRequestConfig } from "axios";
import { ILogValue, IResponse } from "./Interfaces/Misc";
import { IInstance, IOneIncRequest, IOneIncResponse } from "../Home/Interfaces/Insurance";
import { TCommon } from "./Types/StoreStuff";
import tools from "../Common/tools";
import { useMaster } from "@/Store/Home/Master";

let commonStoreData = {} as TCommon;
let masterStore = null;
// let commonStoreData = useCommon();

const tryInstance = (obj: unknown): IInstance | undefined => {
	const curr = obj as IInstance;
	if (curr && curr.Agency && curr.Drivers) {
		return curr;
	}
	return undefined;
}

const headerDict = {
	"Content-Type": "application/json"
}

const getRequestOptions = { headers: headerDict } as AxiosRequestConfig;

const logAsync = async (data: ILogValue): Promise<any> => await axios.post('/CP/Logger/LogWeb', data, getRequestOptions);

const processFalconResponse = (resp: IResponse, onSuccess?: (data?: any) => void, onFail?: () => void, error?: string, errors?: string[]) => {
	if (resp === null) {
		if (error) {
			commonStoreData.Popup = { Component: "vAlert", Message: error };
		}
		else if (errors) {
			commonStoreData.Popup = { Component: "vAlert", Messages: errors };
		}
		onFail && onFail();
	}
	else if (resp.Status != 0 || resp.Error != null || resp.Result == null || resp.Result == undefined) {
		if (resp.Error) {
			let msg = error;
			if (resp.Error.Message) {
				msg = resp.Error.Message;
			}
			if (msg) {
				commonStoreData.Popup = { Component: "vAlert", Message: msg };
			}
			else if (errors) {
				commonStoreData.Popup = { Component: "vAlert", Messages: errors };
			}
		}
		onFail && onFail();
	}
	else {
		//if FalconResponse has no errors, then check if the Result is an Instance with validation errors/warnings
		const instance = tryInstance(resp.Result);
		if (instance) {
			const validation = instance.Validation;
			masterStore ??= useMaster();
			if (validation) {
				//if there are validation errors, display the errors and stop user from proceeding further
				if (validation.Errors.length > 0) {
					commonStoreData.Popup = { Component: "vAlert", Error: validation.Errors, OnClose: () => { validation.Errors = [] } };
					return;
				}
				//if there are validation warnings, change page and then display the warnings
				if (validation.Warnings.length > 0) {
					commonStoreData.Popup = { Component: "vAlert", Warning: validation.Warnings, OnClose: () => { validation.Warnings = [] } };
				}
			}
			masterStore.UpdateInstance(instance);
		}
		onSuccess && onSuccess(resp.Result);
	}
}


const consumerService = async (uri: string, data?: object, onSuccess?: (data?: any) => void, onFail?: () => void, error?: string, loading?: string, errors?: string[], fireForget?: boolean) => {
	if (!fireForget)
		commonStoreData.Popup = {
			Component: "vActivityIndicator",
			Message: loading ?? "L_Please_wait",
		}
	const resp = await callService(uri, data);
	if (!fireForget)
		commonStoreData.Popup = undefined;
	processFalconResponse(resp, onSuccess, onFail, error, errors);
}

const captcha = async (uri: string, data?: object): Promise<any> => {
	return await callService('Captcha/' + uri, data);
}

const oneInc = async (uri: string, data: IOneIncRequest, onSuccess?: (data: IOneIncResponse) => void) => {
	try {
		commonStoreData.Popup = {
			Component: "vActivityIndicator",
			Message: "L_Please_wait",
		}
		const response = await axios.post(uri, data, getRequestOptions);
		commonStoreData.Popup = undefined;
		if (response.status === 200) {
			onSuccess && onSuccess(response.data);
			return;
		}
		commonStoreData.Popup = { Component: "vAlert", Messages: ['L_Error_CC_payment', 'L_Error_Please_try_later'] };
		logAsync({ Method: 'oneInc', Class: 'request.ts', Action: 'Warn', Note: 'statusText: ' + response.statusText + '; status: ' + response.status } as ILogValue);
	} catch (e: any) {
		logAsync({ Exception: JSON.stringify(e), Method: 'oneInc', Class: 'request.ts', Action: 'Warn' } as ILogValue);
		if (e.response && e.response.data && e.response.data.ResponseMessage) {
			logAsync({ Exception: JSON.stringify(e.response.data), Method: 'oneInc', Class: 'request.ts', Action: 'Warn', Note: 'Wrong details' } as ILogValue);
			commonStoreData.Popup = { Component: "vAlert", Message: e.response.data.ResponseMessage };
		}
	}
}

const InitCookie = (attr: string, error: string, timeout?: number) => {
	commonStoreData.Popup = {
		Component: "vActivityIndicator",
		Message: "L_Please_wait",
	}
	//First delete cookie.
	tools.deleteCookie(attr);
	//Set timer.	
	if (!timeout) {
		timeout = 60000;
	}
	const startTime = new Date().getTime();
	let showDefError = false;

	const timer = setInterval(function () {
		//Clearing everything if timeout reaches.
		if (timeout && new Date().getTime() - startTime > timeout) {
			clearInterval(timer);
			if (showDefError) {
				commonStoreData.Popup = { Component: "vAlert", Message: error };
			}
			commonStoreData.Popup = undefined;
			return;
		}
		//Else try reading the cookie (this part gets called in a loop until the timeout is hit)
		showDefError = true;
		const cookie = tools.getCookie(attr);
		//If the cookie is found, use it and then delete the cookie.
		if (cookie) {
			showDefError = false;
			tools.deleteCookie(attr);
			clearInterval(timer);
			commonStoreData.Popup = undefined;
		}
	}, 1000);
}

const privacyPolicy = async (): Promise<string> => {
	return await callOther('PrivacyPolicyLink');
}

const callService = async (uri: string, data?: object): Promise<any> => {
	try {
		const response = await axios.post('/CP/' + uri, data, getRequestOptions);
		if (response.status === 200) {
			return response.data;
		}
		console.log('FAILED response status for callService.');
		return null;
	} catch (e) {
		logAsync({ Exception: JSON.stringify(e), Method: `callService; parent: ${uri.toLowerCase}`, Class: 'request.js', Action: 'Error' } as ILogValue);
		console.log('REQUEST FAILED!');
		return null;
	}
}

const callOther = async (uri: string, data?: object): Promise<any> => {
	try {
		const response = await axios.post('/Other/' + uri + '/', data);
		if (response.status === 200) {
			return response.data;
		}
		console.log('FAILED response status for callOther.');
		return null;
	} catch (e) {
		logAsync({ Exception: JSON.stringify(e), Method: `callOther; parent: ${uri.toLowerCase}`, Class: 'request.js', Action: 'Error' } as ILogValue);
		console.log('REQUEST FAILED!');
		return null;
	}
}

const initialize = (obj: TCommon) => {
	commonStoreData = obj;
}

export default {
	initialize,
	logAsync,
	consumerService,
	oneInc,
	captcha,
	privacyPolicy,
	InitCookie,
}
