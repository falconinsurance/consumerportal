import gsap from 'gsap';
import { TGsap } from './Types/UIStuff';

const fromBottom = (obj: TGsap) => {
    let temp = '100';
    if (obj.cord) {
        temp = obj.cord.toString();
    }
    gsap.from(`.${obj.className}`, {
        duration: obj.duration,
        opacity: 0,
        scale: obj.scale,
        y: temp,
        ease: obj.ease,
        stagger: {
            each: 0.1,
            from: obj.staggerFrom,
            ease: obj.ease,
        }
    });
}

const fromRight = (obj: TGsap) => {
    let temp = '100';
    if (obj.cord) {
        temp = obj.cord.toString();
    }
    gsap.from(`.${obj.className}`, {
        duration: obj.duration,
        opacity: 0,
        scale: obj.scale,
        x: temp,
        ease: obj.ease,
        stagger: 0.1
    });
}

const fromTop = (obj: TGsap) => {
    obj.cord = `-${obj.cord}`;
    return fromBottom(obj);
}

const fromLeft = (obj: TGsap) => {
    obj.cord = `-${obj.cord}`;
    return fromRight(obj);
}

export default {
    fromLeft,
    fromRight,
    fromTop,
    fromBottom
};