import vNavigation from '@/Components/Home/Navigation.vue'
import vSectionWrapper from '@/Components/Home/SectionWrapper.vue'
import vInfo from '@/Components/Home/QuickQuote/Info.vue'
import vAddress from '@/Components/Home/QuickQuote/Address.vue'
import vAddDriver from '@/Components/Home/Drivers/AddDriver.vue'
import vAddVehicle from '@/Components/Home/Vehicles/AddVehicle.vue'
import vPopupAction from '@/Components/Home/PopupAction.vue'
import vAgentLocations from '@/Components/Home/QuickQuote/AgentLocations.vue'
import vAddressError from '@/Components/Home/QuickQuote/AddressError.vue'
import vRate from '@/Components/Home/Rate.vue'
import vFinalRate from '@/Components/Home/FinalRate.vue'
import vDriver from '@/Components/Home/Drivers/Driver.vue'
import vViolationsTbl from '@/Components/Home/Drivers/Violations/ViolationsTbl.vue'
import vAddViolation from '@/Components/Home/Drivers/Violations/AddViolation.vue'
import vOtherDrivers from '@/Components/Home/Drivers/OtherDrivers.vue'
import vVehicles from '@/Components/Home/Vehicles/Vehicles.vue'
import vCoverages from '@/Components/Home/Coverages/Coverages.vue'
import vCoveragePackages from '@/Components/Home/Coverages/CoveragePackages.vue'
import vCoverageDropdown from '@/Components/Home/Coverages/CoverageDropdown.vue'
import vUWQ from '@/Components/Home/Uwq/UnderwritingQuestions.vue'
import vAplusMvrViolations from '@/Components/Home/Uwq/AplusMvrViolations.vue'
import vPayplans from '@/Components/Home/Payplans.vue'
import vMakePayment from '@/Components/Home/Payment/MakePayment.vue'
import vPaymentType from '@/Components/Home/Payment/PaymentType.vue'
import vEft from '@/Components/Home/Payment/Eft.vue'
import vCreditCard from '@/Components/Home/Payment/CreditCard.vue'
import vEsign from '@/Components/Home/Esign/Esign.vue'
import vSigning from '@/Components/Home/Esign/Signing.vue'
import vAppLinkIcons from '@/Components/Home/AppLinkIcons.vue'
import vConfirmation from '@/Components/Home/Confirmation.vue'
import vAccount from '@/Components/Home/Account.vue'
import vDocuments from '@/Components/Home/Documents.vue'

export default [
    {
        Key: 'vNavigation',
        Value: vNavigation
    },
    {
        Key: 'vSectionWrapper',
        Value: vSectionWrapper
    },
    {
        Key: 'vInfo',
        Value: vInfo
    },
    {
        Key: 'vAddress',
        Value: vAddress
    },
    {
        Key: 'vAddDriver',
        Value: vAddDriver
    },
    {
        Key: 'vAddVehicle',
        Value: vAddVehicle
    },
    {
        Key: 'vPopupAction',
        Value: vPopupAction
    },
    {
        Key: 'vAgentLocations',
        Value: vAgentLocations
    },
    {
        Key: 'vAddressError',
        Value: vAddressError
    },
    {
        Key: 'vRate',
        Value: vRate
    },
    {
        Key: 'vDriver',
        Value: vDriver
    },
    {
        Key: 'vViolationsTbl',
        Value: vViolationsTbl
    },
    {
        Key: 'vAddViolation',
        Value: vAddViolation
    },
    {
        Key: 'vOtherDrivers',
        Value: vOtherDrivers
    },
    {
        Key: 'vVehicles',
        Value: vVehicles
    },
    {
        Key: 'vCoverages',
        Value: vCoverages
    },
    {
        Key: 'vCoverageDropdown',
        Value: vCoverageDropdown
    },
    {
        Key: 'vCoveragePackages',
        Value: vCoveragePackages
    },
    {
        Key: 'vFinalRate',
        Value: vFinalRate
    },
    {
        Key: 'vUWQ',
        Value: vUWQ
    },
    {
        Key: 'vAplusMvrViolations',
        Value: vAplusMvrViolations
    },
    {
        Key: 'vPayplans',
        Value: vPayplans
    },
    {
        Key: 'vEsign',
        Value: vEsign
    },
    {
        Key: 'vSigning',
        Value: vSigning
    },
    {
        Key: 'vMakePayment',
        Value: vMakePayment
    },
    {
        Key: 'vPaymentType',
        Value: vPaymentType
    },
    {
        Key: 'vEft',
        Value: vEft
    },
    {
        Key: 'vCreditCard',
        Value: vCreditCard
    },
    {
        Key: 'vAppLinkIcons',
        Value: vAppLinkIcons
    },
    {
        Key: 'vConfirmation',
        Value: vConfirmation
    },
    {
        Key: 'vAccount',
        Value: vAccount
    },
    {
        Key: 'vDocuments',
        Value: vDocuments
    }

]
