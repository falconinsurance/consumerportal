import { TGsap } from "@/Scripts/Common/Types/UIStuff";
import gsapAnim from "@/Scripts/Common/gsapAnim";
import { useUI } from "@/Store/Home/UI";
import { onActivated } from "vue";

export function useAnim(flag?: boolean) {
    const ui = useUI();
    const AnimateActivation = () => {
        gsapAnim.fromTop({
            className: "animateWrapperHeader",
            duration: 0.9,
            cord: 100,
            scale: 0.8,
            staggerFrom: "random",
            ease: "power1",
        });
        if (!flag) {
            const obj = {
                className: "animateQQ",
                duration: 0.8,
                cord: '50%',
                scale: 0.8,
                staggerFrom: 'random',
                ease: 'power1'
            } as TGsap;
            if (ui.Data.Forward) {
                gsapAnim.fromRight(obj);
            }
            else {
                gsapAnim.fromLeft(obj);
            }
        }
    }
    onActivated(() => {
        AnimateActivation();
    });
}