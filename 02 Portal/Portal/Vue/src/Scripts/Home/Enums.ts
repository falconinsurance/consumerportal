export enum En_Sections {
	vInfo = 0,
	vRate = 1,
	vDriver = 2,
	vOtherDrivers = 3,
	vVehicles = 4,
	vCoveragePackages = 5,
	vFinalRate = 6,
	vUWQ = 7,
	vPayplans = 8,
	vEsign = 9,
	vMakePayment = 10,
	vAccount = 11,
	vDocuments = 12,
}

export enum En_Nav {
	Start = 0,
	Drivers = 1,
	Vehicles = 2,
	CoveragePackages = 3,
	CurrentQuote = 4,
	Finish = 5,
}

export enum En_Bind {
	PolicyHolder = 0,
	Driver = 1,
	Vehicle = 2,
	Mailing = 3,
	Garage = 4
}

export enum En_Payplan {
	None = 0,
	PayInFull = 1,
	Recurring = 2,
	Installment = 3,
}

export enum En_Address {
	None = 0,
	Ignore = 1,
	Manual = 2,
	Suggested = 3,
}
