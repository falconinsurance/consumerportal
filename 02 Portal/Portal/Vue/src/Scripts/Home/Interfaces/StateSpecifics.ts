import { IAgency } from "./Insurance"

export interface IStateDropdown {
	DefaultValue: number,
	Items: IStateDropdownItem[],
	Label: string,
	Locked: boolean
}

export interface IStateDropdownItem {
	Label: string,
	Name: string,
	Value: number
}

export interface IStateDriverOptions {
	LicenseState: IStateDropdown,
	LicenseType: IStateDropdown,
	DriverType: IStateDropdown,
	Gender: IStateDropdown,
	MatureDriver: IStateDropdown,
	DefensiveDriver: IStateDropdown,
	AccidentPrevention: IStateDropdown,
	License36Months: IStateDropdown,
	MaritalStatus: IStateDropdown,
	Relationship: IStateDropdown,
	UDRResolution: IStateDropdown,
	Violations: IStateDropdown
}

export interface IStatePolicyOptions {
	State: IStateDropdown,
	BILimit: IStateDropdown,
	EffectiveDate: Date,
	EFTAutopay: IStateDropdown,
	HomeownersDiscount: IStateDropdown,
	IsSR22: IStateDropdown,
	MPLimit: IStateDropdown,
	PayPlanType: IStateDropdown,
	PDLimit: IStateDropdown,
	PolicyType: IStateDropdown,
	PriorCoverage: IStateDropdown,
	TermLength: IStateDropdown,
	PrimaryPhone: IStateDropdown,
	SecondaryPhone: IStateDropdown,
	UMBILimit: IStateDropdown,
	UIMBILimit: IStateDropdown,
	PIPLimit: IStateDropdown,
	WorkLossBenefitsWaiver: IStateDropdown
}

export interface IStateVehicleOptions {
	ComColDeductible: IStateDropdown,
	Rental: IStateDropdown,
	Towing: IStateDropdown,
	UMPDLimit: IStateDropdown,
	UMPDDeductible: IStateDropdown,
	VehicleUse: IStateDropdown,
	Lienholder: IStateDropdown,
	Ownership: IStateDropdown,
	SafetyEquipment: IStateDropdown
}

export interface IStatePaymentOptions {
	AccountType: IStateDropdown,
	PaymentType: IStateDropdown
}

export interface IStateDropdowns {
	DriverOptions: IStateDriverOptions,
	PolicyOptions: IStatePolicyOptions,
	VehicleOptions: IStateVehicleOptions,
	PaymentOptions: IStatePaymentOptions,
}

export interface IUnderwritingQuestion {
	DiamondId: number,
	HasDescription: number,
	Question: string,
}

export interface IStateSpec {
	State: number,
	StateStr: string,
	Agencies: IAgency[],
	Dropdowns: IStateDropdowns,
	UnderwritingQuestions: IUnderwritingQuestion[],
	EffectiveDate: string,
	UpdateQuote: boolean,
	DefaultsUpdated: boolean
}
