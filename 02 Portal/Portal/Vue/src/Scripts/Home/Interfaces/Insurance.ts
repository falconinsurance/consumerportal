import { En_Address } from "../Enums"

export interface IESign {
    DocuSignItems: IDocuSignItem[]
}

export interface IInstance {
    AttachedTypes: IAttachedTypes,
    PolicyHolder: IPolicyHolder,
    Policy: IPolicy,
    Drivers: IDriver[],
    UDRDrivers: IDriver[],
    InsVehicles: IInsVehicles,
    Agency: IAgency,
    Validation: IValidation,
    RiskCheck: string,
}

export interface IAttachedTypes {
    DriversViolations: IDrvsViolations,
}

export interface IDrvsViolations {
    AnyDriver: boolean,
    Drivers: IDrvViolationItem[],
    Key: string,
    Status: string,
}

export interface IDrvViolationItem {
    First: string,
    Last: string,
    DOB: string,
    AnyViolation: boolean,
    Violations: IDrvViolation[]
}

export interface IDrvViolation {
    Amount: number,
    CarrierName: string,
    CarrierNameShort: string,
    ClaimNumber: string,
    ClaimNumberShort: string,
    Date: string,
    Status: string,
    Type: string,
}

export interface IFormattedDriverViolation {
    Driver: string,
    Violation: string,
    CarrierName: string,
    ClaimNumber: string,
    Date: string,
}

export interface IInsVehicles {
    Address: IAddress,
    Vehicles: IVehicle[]
}

export interface IPayplans {
    Current: IPayPlanItem,
    Collection: IPayPlanItem[]
}

export interface IValidation {
    Errors: string[],
    Warnings: string[],
}

export interface IPayment {
    Amount: number,
    CreditCard: ICreditCard,
    Eft: IEft,
    Type: number,
}

export interface ICreditCard {
    Expiration: IExpiration,
    HolderName: string,
    Zip: string,
    Last4: string,
    Number: string,
    Token: string,
    Type: string,
}

export interface IExpiration {
    Month: number,
    Year: number,
}

export interface IEft {
    AccountNumber: string,
    FullName: string,
    RoutingNumber: string,
    Type: number,
}

export interface IAgency {
    Id: number,
    State: number,
    ProducerId: number,
    Name: string,
    Addresses: IAddress[],
    Phone: string,
}

export interface IPolicy {
    Id: number,
    ImageNum: number,
    Number: string,
    Type: number,
    Term: number,
    PaymentInfo: IPaymentInfo,
    Modifiers: IModifiers,
    Coverages: IPolicyCoverages
    EffectiveDate: string,
    ExpirationDate: string,
    UnderwritingResponses: IUnderwritingResponse[],
    Documents: IDocuments
}

export interface IModifiers {
    HomeownersDiscount: number,
    ReceiveElectronicDocuments: number,
    TransferDiscount: ITransferDiscount,
}

export interface ITransferDiscount {
    CompanyName: string,
    PolicyNumber: string,
    ExpirationDate: string,
    Flag: number
}

export interface IDocument {
    FormCategoryTypeId: number,
    FormId: number,
    PolicyId: number,
    PolicyImageNumber: number,
    Description: string,
    FormNumber: string,
    Recipient: string,
    Version: string,
    KeyValue: string,
}

export interface IDocuments {
    ESign: IDocument[],
    Regular: IDocument[],
    WetSign: IDocument[],
}

export interface IPaymentInfo {
    Payplans: IPayplans,
    RecurringPayment: IPayment | null,
}

export interface IPayPlanItem {
    Amount: IBillingItem,
    Installments: number,
    PaymentType: number,
    Type: number,
    Id: number,
    Tag: string
}

export interface IBillingItem {
    Down: number,
    Installment: number,
    Total: number,
}

export interface IPolicyCoverages extends ICoverages {
    BI: number,
    MP: number,
    PD: number,
    PIP: number,
    UIMBI: number,
    UMBI: number,
    WorkLossBenefitsWaiver: number
}

export interface IAddress extends IUpdater {
    Street: string,
    City: string,
    State: number,
    Zip: string,
    County: string,
}

export interface IDocuSignItem {
    Description: string,
    Status: string,
    ClientUserId: string,
    Url: string,
    Signer: string,
    Name: string
}

export interface IVehicleDropdowns extends IUpdater {
    Vehicle: IVehicle,
    Makes: IDBEntity[],
    Models: IDBEntity[],
    BodyTypes: IDBEntity[],
    Symbols: IDBEntity[]
}

export interface IVehicle extends IUpdater, IListObj {
    Year: number,
    Make: IDBEntity,
    Model: IDBEntity,
    BodyType: IDBEntity,
    PrimaryUse: number,
    Coverages: IVehicleCoverages
    Symbol: IDBEntity,
    Vin: string
}

export interface ICoverages {

}

export interface IVehicleCoverages extends ICoverages {
    Rental: number,
    SafetyEquipment: number,
    Towing: number,
    UMPDDeductible: number,
    ComColDeductible: number,
    UMPD: number,
}

export interface IListObj {
    Id: number,
}

export interface IPolicyHolder {
    Name: IPerson,
    Address: IAddress,
    Email: string,
    Phones: IPhone[],
    Insurance: IInsurance[],
}

export interface IInsurance {
    Number: string,
    Company: string,
}

export interface IPhone {
    Number: string,
    Type: number,
}

export interface IDriver extends IListObj {
    Name: IPerson,
    Relationship: number
    Filing: IFiling,
    License: ILicense,
    Employment: IEmployment;
    Type: number,
    IsUDR: boolean,
    CourseDiscount: ICourseDiscount,
    Violations: IViolation[]
}

export interface IFiling {
    IsSR22: boolean,
    IsSR50: boolean,
}

export interface IViolation extends IListObj {
    Type: number,
    Date: string
}

export interface IEmployment {
    Occupation: string,
    Employer: string,
}

export interface ICourseDiscount {
    AccidentPrevention: ICourse,
    DefensiveDriver: ICourse,
    MatureDriver: ICourse,
}

export interface ICourse {
    Date: string,
    Value: number,
}

export interface ILicense {
    Number: string,
    Type: number,
    State: number,
    Months36: number
}

export interface IPerson extends IUpdater {
    First: string,
    Middle: string,
    Last: string,
    Gender: number,
    DateOfBirth: string,
    MaritalStatus: number
}

export interface IDBEntity {
    Description: string,
    Id: string
}

export interface IRate {
    MonthlyPayment: number,
    DownPayment: number
}

export interface IUnderwritingResponse {
    Answer: string | null,
    DiamondId: number,
    Value: number,
}

export interface IUpdater {
}

export interface IOneIncResponse {
    BaseCardType: string,
    Token: string,
    SaveCardError: string
}

export interface IOneIncRequest {
    PortalOneSessionKey: string,
    Card: {
        Number: string,
        ExpirationMonth: number,
        ExpirationYear: number,
        Holder:
        {
            Name: string,
            Zip: string
        }
    }
}

export interface IAccount {
    Flag: boolean,
    Password: string,
    Created: boolean,
    SentEmail: boolean
}

export interface ISuggestedAddress {
    Address: IAddress,
    IsResidentialAddress: boolean,
    MissingOrWrongSecondaryInfo: boolean,
    ValidAddressWithUnknownApt: boolean,
    WasSuccess: boolean,
}

export interface ISmartyStreet {
    Addresses: IAddress[],
    SuggestedAddress: ISuggestedAddress,
    CurrentSelection: En_Address
}


export interface IMisc {
    DisableVehicleUpdate: boolean,
    PolicyBoundWithEmail: boolean,
    PaymentComplete: boolean,
    SignComplete: boolean,
    SmartyStreet: ISmartyStreet
}
