import { TButton, TDropdown, TFormGroup, TTextbox } from "@/Scripts/Common/Types/UIStuff"
import { En_Nav, En_Sections } from "../Enums"
import { Ref, ref } from "vue"

export type TNavigation = {
    Agent: boolean,
    Items: TNavItem[]
}

export type TNavItem = {
    label: string,
    icon: string,
    list: boolean,
    type: En_Nav,
    enable: boolean,
    complete: boolean
}

export type TSection = {
    Heading: string,
    Nav: En_Nav,
    BtnWrapClass: string,
    Continue: TSectionBtnAction
    Back: TSectionBtnAction,
    Validate: {
        Action: () => boolean,
        Items: TValidateTB[]
    }
}

export type TSectionBtnAction = {
    Btn: TButton,
    Validate: [],
    Action: () => void
}

export type TValidateTB = {
    Type?: "IDBEntity" | "string" | "number",
    FormItem: TTextbox,
    Value: Ref,
}

export type TUI = {
    Window: Window & typeof globalThis,
    ContainerPadding: string,
    Outernav: HTMLDivElement;
    OuterNavClass: string,
    CurrentSection: En_Sections,
    Forward: boolean
}

export type TPaymentInfo = {
    Amount: string,
    Cents: string,
    Down: string
}


export type TVehicleControls = {
    Year: TDropdown,
    Make: TDropdown,
    Model: TDropdown,
    BodyType: TDropdown,
    Symbol: TDropdown,
    PrimaryUse: TDropdown,
    ComColl: TDropdown,
    UMPD: TDropdown,
    UMPDDeDuct: TDropdown,
    Rental: TDropdown,
    Towing: TDropdown,
    SafetyEquip: TDropdown,
    Vin: TTextbox
}

export type TDriverControls = {
    FirstName: TTextbox,
    MiddleName: TTextbox,
    LastName: TTextbox,
    Gender: TDropdown,
    DOB: TTextbox,
    MaritalStatus: TDropdown,
    Relationship: TDropdown,
    DriverType: TDropdown,
    LicType: TDropdown,
    LicState: TDropdown,
    LicNum: TTextbox,
    LicMon36: TDropdown,
    MatureDr: TDropdown,
    DefensiveDr: TDropdown,
    AccPrev: TDropdown,
    AccPrevDate: TTextbox,
    DriverOccupation: TTextbox,
    DriverEmployer: TTextbox
}
