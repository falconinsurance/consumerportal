import { createApp } from 'vue'
import App from './App.vue'
import commonComponents from '@/Scripts/Common/Components'
import components from '@/Scripts/Home/Components'
import vueMask from 'maska';
import { createPinia } from 'pinia'

getApp()
	.use(vueMask)
	.mount('#app');

function getApp() {
	const app = createApp(App);
	const pinia = createPinia()
	app.use(pinia)
	commonComponents.forEach(c => app.component(c.Key, c.Value));
	components.forEach(c => app.component(c.Key, c.Value));
	return app;
}

