import { TCommon } from '@/Scripts/Common/Types/StoreStuff';
import session from '@/Scripts/Common/session';
import { defineStore } from 'pinia'
import { reactive, watch } from 'vue'
import request from "@/Scripts/Common/request";

export const useCommon = defineStore("Common", () => {
    const Data = reactive<TCommon>({
        AppVersion: session.Get("VersionNumber"),
        CurrentYear: new Date().getFullYear(),
        Language: "EN",
        IsBusy: false,
        Labels: {
            ["L_Please_wait"]: "Please wait..."
        } as Record<string, string>,
    } as TCommon);

    let OnLanguageChange = (lang: string) => {
        //assigned later 
    }

    const HasAgencyLogo = () => {
        return false;
    }

    const GetLabel = (type: string): string => {
        const w = window.innerWidth;
        const exp = HasAgencyLogo() ? w > 394 && w < 490 : w < 314;
        return exp ? type == 'en' ? 'en' : 'es' : type == 'en' ? 'english' : 'español';
    }

    const ChangeLang = async (lang: string) => {
        Data.Language = lang;
        GetTranslationLabels();
        OnLanguageChange && OnLanguageChange(lang);
    }

    const GetTranslationLabels = () => {
        request.consumerService('Init/GetLabels', { Language: Data.Language },
            (data: Record<string, string>) => {
                Data.Labels = data;
            }
        );
    }

    const T = (label: string | undefined, params?: string[]) => {
        let res = "";
        if (label) {
            res = label
            if (Data.Labels.hasOwnProperty(label)) {
                res = Data.Labels[label];
            }
        }
        if (params) {
            for (let i = 0; i < params.length; i++) {
                res = res.replace(`{${i}}`, params[i]);
            }
        }
        return res;
    }

    const TMulti = (labels: string[]) => {
        return labels.map((l) => T(l)).join(' ');
    }

    const UpdateOnLanguageChangeAction = (data: (lang: string) => void) => {
        OnLanguageChange = data;
    }

    return {
        Data,
        HasAgencyLogo,
        GetLabel,
        ChangeLang,
        T,
        TMulti,
        GetTranslationLabels,
        UpdateOnLanguageChangeAction,
    }
});

// async proccessResponse(_: TContext, param: IStoreGetActionParam): Promise<void> {
//     _.commit('SetBusy', true);
//     //First delete cookie.
//     tools.DeleteCookie(param.attribute);
//     //Set timer.
//     if (!param.timeout) {
//         param.timeout = 60000;
//     }
//     const startTime = new Date().getTime();
//     let showDefError = false;

//     const timer = setInterval(function () {
//         //Clearing everything if timeout reaches.
//         if (new Date().getTime() - startTime > param.timeout) {
//             clearInterval(timer);
//             if (showDefError) {
//                 _.commit('ShowError', param.errorMessage);
//             }
//             _.commit('SetBusy', false);
//             return;
//         }
//         //Else try reading the cookie (this part gets called in a loop until the timeout is hit)
//         showDefError = true;
//         const cookie = tools.getCookie(param.attribute);
//         //If the cookie is found, use it and then delete the cookie.
//         if (cookie) {
//             showDefError = false;
//             tools.DeleteCookie(param.attribute);
//             clearInterval(timer);
//             _.commit('SetBusy', false);
//         }
//     }, 1000);
// },