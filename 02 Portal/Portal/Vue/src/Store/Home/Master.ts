import { IAccount, ICreditCard, IDocuSignItem, IDocument, IDriver, IESign, IEft, IInstance, IPayPlanItem, IPayment, IPhone, IUnderwritingResponse, IVehicle, IVehicleCoverages, IViolation, IOneIncResponse, IMisc, ISmartyAddress, IAddress } from '@/Scripts/Home/Interfaces/Insurance';
import { defineStore } from 'pinia'
import { reactive, ref, watch } from 'vue'
import { useControls } from './Controls';
import { TDropdown, TFormGroup } from '@/Scripts/Common/Types/UIStuff';
import { TPaymentInfo } from '@/Scripts/Home/Types/StoreStuff';
import request from '@/Scripts/Common/request';
import { useCommon } from '../Common';
import { useStateSpec } from './StateSpec';
import { useUI } from './UI';
import tools from '@/Scripts/Common/tools';
import { En_Address } from '@/Scripts/Home/Enums';
import { useValidate } from './Validate';

export const useMaster = defineStore("Master", () => {

    const st = useStateSpec();
    const ui = useUI();
    const c = useControls();
    const common = useCommon();
    const validate = useValidate();

    const InitDriverDefaults = (driver: IDriver) => {
        const options = st.Data.Dropdowns.DriverOptions;
        if (options.MaritalStatus.Label) {
            driver.Name.MaritalStatus = options.MaritalStatus.DefaultValue;
        }
        if (options.LicenseType.Label) {
            driver.License.Type = options.LicenseType.DefaultValue;
        }
        if (options.License36Months.Label) {
            driver.License.Months36 = options.License36Months.DefaultValue;
        }
        if (options.LicenseState.Label) {
            driver.License.State = options.LicenseState.DefaultValue;
        }
        if (options.AccidentPrevention.Label) {
            driver.CourseDiscount.AccidentPrevention.Value = options.AccidentPrevention.DefaultValue;
        }
        if (options.DefensiveDriver.Label) {
            driver.CourseDiscount.DefensiveDriver.Value = options.DefensiveDriver.DefaultValue;
        }
        if (options.MatureDriver.Label) {
            driver.CourseDiscount.MatureDriver.Value = options.MatureDriver.DefaultValue;
        }
    }

    const InitVehicleCoverageDefaults = (coverages: IVehicleCoverages) => {
        const options = st.Data.Dropdowns.VehicleOptions;
        if (options.ComColDeductible.Label) {
            coverages.ComColDeductible = options.ComColDeductible.DefaultValue
        }
        if (options.UMPDLimit.Label) {
            coverages.UMPD = options.UMPDLimit.DefaultValue
        }
        if (options.UMPDDeductible.Label) {
            coverages.UMPDDeductible = options.UMPDDeductible.DefaultValue
        }
        if (options.Rental.Label) {
            coverages.Rental = options.Rental.DefaultValue
        }
        if (options.SafetyEquipment.Label) {
            coverages.SafetyEquipment = options.SafetyEquipment.DefaultValue
        }
        if (options.Towing.Label) {
            coverages.Towing = options.Towing.DefaultValue
        }
    }

    const GetNewDriver = () => {
        const d = {
            Name: {},
            License: {},
            Employment: {},
            CourseDiscount: {
                AccidentPrevention: {},
                MatureDriver: {},
                DefensiveDriver: {}
            }, Violations: [] as IViolation[]
        } as IDriver;
        st.Data.Dropdowns && InitDriverDefaults(d);
        return d
    }

    const GetNewVehicle = () => {
        const v = {
            Make: {},
            Model: {},
            BodyType: {},
            Symbol: {},
            Coverages: {}
        } as IVehicle;
        st.Data.Dropdowns && InitVehicleCoverageDefaults(v.Coverages);
        return v;
    }

    const firstDriver = GetNewDriver();

    const ESign = reactive({
        DocuSignItems: []
    } as IESign);

    const Payment = reactive({
        CreditCard: {
            Expiration: {}
        },
        Eft: {
            Type: 1
        }
    } as IPayment);

    const Misc = reactive({
        SmartyStreet: {
            CurrentSelection: En_Address.Manual
        }
    } as IMisc);

    const Instance = reactive<IInstance>({
        RiskCheck: "None",
        AttachedTypes: {},
        PolicyHolder: {
            Name: firstDriver.Name,
            Address: {},
            Phones: [{}] as IPhone[]
        },
        Drivers: [firstDriver] as IDriver[],
        UDRDrivers: [] as IDriver[],
        InsVehicles: {
            Vehicles: [{
                Make: {},
                Model: {},
                BodyType: {},
                Symbol: {},
                Coverages: {}
            }] as IVehicle[]
        },
        Policy: {
            PaymentInfo: {
                Payplans: {
                    Collection: [] as IPayPlanItem[]
                }
            },
            Modifiers: {
                TransferDiscount: {}
            },
            UnderwritingResponses: [] as IUnderwritingResponse[]
        }
    } as IInstance);

    const PaymentInfo = reactive<TPaymentInfo>({
    } as TPaymentInfo);

    const Account = reactive({} as IAccount)

    watch(
        () => Instance.Policy.PaymentInfo.Payplans.Current, (newValue) => {
            if (newValue) {
                let amt = 0;
                if (newValue.Tag == "PayInFull") {
                    amt = newValue.Amount.Total;
                }
                else {
                    //installments
                    amt = newValue.Amount.Installment;
                }
                const [whole, fraction] = amt.toFixed(2).split(".");
                PaymentInfo.Amount = whole;
                PaymentInfo.Cents = fraction;
                PaymentInfo.Down = newValue.Amount.Down.toFixed(2);
            }
        },
    );

    watch(() => Instance.Agency, (o, n) => {
        if (n) {
            const c = useCommon();
            if (c.Data.Popup?.SubComponent === "vAgentLocations") {
                c.Data.Popup.AgencyError = "";
            }
        }
    });

    watch(() => Instance.Policy.PaymentInfo.Payplans.Current, (newValue) => {
        if (newValue) {
            Payment.Amount = newValue.Amount.Down;
            Payment.Type = newValue.PaymentType
        }
    });

    const AddDriver = () => {
        Instance.Drivers.push(GetNewDriver());
        c.AddDriverToDrivers();
        ui.RemoveNavComplete();
        // ui.Sections[ui.Data.CurrentSection].Validate.Items.forEach((item) => {
        //     item.FormItem.disabled = true;
        // })
        // setTimeout(() => {
        // }, 5000);
    };
    const AddUDRDriver = (driver: IDriver) => {
        //Set DriverType to Insured and then add the UDR driver.
        const ins = st.GetDriverType("Insured");
        if (ins) {
            driver.Type = ins;
        }
        Instance.Drivers.push(driver);
        c.AddDriverToDrivers(true);
        ui.RemoveNavComplete();
    };
    const RemoveDriver = (index: number) => {
        const dr = Instance.Drivers[index];
        //When removing a UDR driver, set DriverType to Excluded and then remove it.
        if (dr.IsUDR) {
            const t = st.GetDriverType("Excluded");
            if (t) {
                dr.Type = t;
            }
        }
        Instance.Drivers.splice(index, 1);
        c.RemoveDriverFromDrivers(index - 1)
        ui.RemoveNavComplete();
    };

    const AddVehicle = () => {
        const v = GetNewVehicle();
        Instance.InsVehicles.Vehicles.push(v);
        //making sure umpd is the same for all the vehicles (TX).
        if (st.Data.Dropdowns.DriverOptions.LicenseState.Items.find(c => c.Value == st.Data.State)?.Name == "TX") {
            v.Coverages.UMPD = Instance.InsVehicles.Vehicles[0].Coverages.UMPD;
        }
        c.AddVehicleToVehicles();
        ui.RemoveNavComplete();
    };

    const RemoveVehicle = (index: number) => {
        if (Instance.InsVehicles.Vehicles.length === 1) {
            common.Data.Popup = { Component: "vAlert", Message: "L_Error_Min_Vehicle_Req" };
            return;
        }
        Instance.InsVehicles.Vehicles.splice(index, 1);
        c.RemoveVehicleFromVehicles(index);
        ui.RemoveNavComplete();
    };

    const InfoValid = [];

    const SetInfoValid = (form: TFormGroup) => {
        InfoValid.push(form);
    }

    const GetPaymentInfo = () => {
        const val = Instance.Policy.PaymentInfo.Payplans.Current;
        let p = "";
        if (val.Tag !== "PayInFull") {
            p = `<b>$${val.Amount.Installment.toFixed(2)}</b> ${common.T("L_per_month")}`;
        } else {
            p = `<b>$${val.Amount.Total.toFixed(2)}</b> ${common.T("L_Fullpay")}`;
        }
        return p;
    }

    const UpdateInstance = (instance: IInstance) => {
        const temp = Instance.Agency;

        Object.assign(Instance, instance);
        Instance.Agency = temp;
        if (instance.PolicyHolder.Phones.length == 0) {
            instance.PolicyHolder.Phones.push({} as IPhone);
        }
        //Adding only non UDR drivers and UDR driver with Type Insured to to main Drivers list.
        Instance.Drivers = instance.Drivers.filter(d => (d.IsUDR && d.Type == st.GetDriverType("Insured")) || !d.IsUDR);
        Instance.UDRDrivers = instance.Drivers.filter(d => d.IsUDR);
        Instance.Drivers[0].Name = instance.PolicyHolder.Name;
    }

    const GetInstanceReq = () => {
        //creating request by adding back udr drivers.
        //This creates instance obj without UDRDrivers
        const { UDRDrivers, Drivers, ...mainReq } = Instance;

        //Make sure to update drivres in 'DriversTemp', since Instance is reactive, 
        //the view gets updated when you are adding drivers directly to 'Drivers'
        const DriversTemp = [] as IDriver[];
        Instance.Drivers.forEach((d) => {
            DriversTemp.push(d);
        })
        Instance.UDRDrivers.forEach((d) => {
            const udr = Drivers.find(x => x == d);
            if (!udr) {
                DriversTemp.push(d);
            }
        });
        return { Drivers: DriversTemp, ...mainReq } as IInstance;
    }

    let initFlag = false;
    const Init = () => {
        if (!initFlag) {
            InitUnderwritingResponses();
            InitDefaults();
            initFlag = true;
        }
    }

    const InitDefaults = () => {
        InitDriverDefaults(Instance.Drivers[0]);
        InitVehicleCoverageDefaults(Instance.InsVehicles.Vehicles[0].Coverages);
        InitModifierDefaults();
    }

    const InitModifierDefaults = () => {
        const options = st.Data.Dropdowns.PolicyOptions;
        if (options.PriorCoverage.Label) {
            Instance.Policy.Modifiers.TransferDiscount.Flag = options.PriorCoverage.DefaultValue
        }
        if (options.PolicyType.Label) {
            Instance.Policy.Type = options.PolicyType.DefaultValue;
        }
    }

    const InitUnderwritingResponses = () => {
        Instance.Policy.UnderwritingResponses = [];
        st.Data.UnderwritingQuestions.forEach((q) => {
            Instance.Policy.UnderwritingResponses.push({ DiamondId: q.DiamondId } as IUnderwritingResponse);
        });
    }

    const ClearAnswer = (index: number) => {
        Instance.Policy.UnderwritingResponses[index].Answer = null;
    }

    const LaunchEsignPop = (doc: IDocuSignItem) => {
        common.Data.Popup = {
            Component: "vPopupAction",
            ClosePopup: () => GetDocuSignItems(undefined, { SignerId: doc.ClientUserId }, "L_Error_Loading_Esign_status"),
            RootCls: "esign",
            HideButtons: true,
            SubComponent: "vSigning",
            Url: doc.Url
        }
    }

    const ClearCCPayment = (payment: IPayment) => {
        payment.CreditCard = {
            Expiration: {}
        } as ICreditCard;
    }

    const ClearEFTPayment = (payment: IPayment) => {
        payment.Eft = {
            Type: 1
        } as IEft
    }

    const GetQuote = async (onSuccess?: (inst: IInstance) => void, onFail?: () => void, errorMessage?: string, loading?: string) => {
        request.consumerService("Quoting/GetRate", GetInstanceReq(), onSuccess, onFail, errorMessage ?? "L_Error_Quote_failed", loading)
    }

    const GetDocuSignItems = (onSuccess?: () => void, obj?: { SignerId: string }, error?: string) => {
        request.consumerService("Quoting/GetDocuSignItems", obj ?? { Instance: GetInstanceReq() }, (data: IDocuSignItem[]) => {
            ESign.DocuSignItems = data;
            onSuccess && onSuccess();
        }, undefined, error ?? "L_Error_Loading_Esign_failed", "L_Loading_esign");
    }

    const DisplayPaymentConfirmation = (paymentMethod: string, onSuccess: () => void) => {
        common.Data.Popup = {
            Component: "vAlert",
            Title: "L_Thank_you",
            SubComponent: "vConfirmation",
            OnClose: onSuccess,
            Details: {
                Message: "L_Your_payment_processed",
                Content: [
                    { Label: "L_Payment_Amount", Value: `$${Payment.Amount.toFixed(2)}` },
                    { Label: "L_Payment_Method", Value: paymentMethod },
                    { Label: "L_Payment_Date", Value: tools.getDateToday() },
                ]
            }
        };
        Misc.PaymentComplete = true;
        Misc.PolicyBoundWithEmail = Instance.PolicyHolder.Email ? true : false;
    }

    const SaveCardOnOneInc = (token: string, url: string, onSuccess: () => void) => {
        request.oneInc(url, {
            Card: {
                Number: Payment.CreditCard.Number,
                ExpirationMonth: Payment.CreditCard.Expiration.Month,
                ExpirationYear: Payment.CreditCard.Expiration.Year,
                Holder: { Name: Payment.CreditCard.HolderName, Zip: Payment.CreditCard.Zip }
            },
            PortalOneSessionKey: token
        }, (oneIncResponse: IOneIncResponse) => BindPolicy(oneIncResponse, onSuccess));
    }

    const BindPolicy = (oneIncResponse: IOneIncResponse, onSuccess: () => void) => {
        if (oneIncResponse.Token && oneIncResponse.BaseCardType) {
            const tempPayment = {
                Amount: Payment.Amount,
                CreditCard: {
                    Last4: oneIncResponse.Token.substring(oneIncResponse.Token.length - 4),
                    Token: oneIncResponse.Token,
                    Expiration: Payment.CreditCard.Expiration,
                    HolderName: Payment.CreditCard.HolderName,
                    Type: oneIncResponse.BaseCardType
                } as ICreditCard
            } as IPayment;
            request.consumerService("Quoting/BindPolicy", { Instance: GetInstanceReq(), Payment: tempPayment }, () => {
                DisplayPaymentConfirmation(`${common.T('L_CC')}, ${tempPayment.CreditCard.Type} (${tempPayment.CreditCard.Last4})`, onSuccess);
            }, undefined, "L_Error_Payment_not_made")
        }
    }

    const MakePayment = async (onSuccess: () => void) => {
        //CC Payment
        if (Payment.CreditCard.Number) {
            request.consumerService("Quoting/GetOneIncToken", undefined, (token: string) => {
                request.consumerService("Quoting/GetOneIncSaveUrl", undefined, (url: string) => SaveCardOnOneInc(token, url, onSuccess), undefined, undefined, undefined, ['L_Error_CC_payment', 'L_Error_Please_try_later']);
            }, undefined, undefined, undefined, ['L_Error_CC_payment', 'L_Error_Please_try_later']);
        }
        //EFT Payment
        else if (Payment.Eft.AccountNumber) {
            request.consumerService("Quoting/BindPolicy", { Instance: GetInstanceReq(), Payment: Payment }, () => {
                DisplayPaymentConfirmation(`${common.T(Payment.Eft.Type == 1 ? 'L_Checking_Acc' : 'L_Savings_Acc')}`, onSuccess);
            }, undefined, "L_Error_Payment_not_made");
        }
    }

    const Register = (onSuccess: () => void) => {
        request.consumerService("Quoting/RegisterUser", {
            PolicyNumber: Instance.Policy.Number,
            ZipCode: Instance.PolicyHolder.Address.Zip,
            Email: Instance.PolicyHolder.Email,
            Password: Account.Password
        }, () => {
            common.Data.Popup = {
                Component: "vAlert",
                Title: "L_Done",
                SubComponent: "vConfirmation",
                OnClose: onSuccess,
                Details: {
                    Message: "L_Your_account_registered",
                    SubMessage: "L_You_can_now_login",
                    Content: [{ Label: "L_Email", Value: Instance.PolicyHolder.Email }],
                }
            };
            Account.Created = true;
        });
    }

    const SendWelcomeEmail = () => {
        if (!Account.SentEmail && !Misc.PolicyBoundWithEmail && Instance.PolicyHolder.Email) {
            request.consumerService("Quoting/SendWelcomeEmail", {
                Email: Instance.PolicyHolder.Email,
                Name: Instance.PolicyHolder.Name,
                PolicyNumber: Instance.Policy.Number
            }, () => Account.SentEmail = true);
        }
    }

    const SetupAccount = (onSuccess: () => void) => {
        if (Account.Flag) {
            Register(onSuccess);
        }
        else {
            SendWelcomeEmail();
            onSuccess();
        }
    }

    const OpenDocument = (doc: IDocument) => {
        request.InitCookie("file_download", "L_Error_Load_document");
        window.open(`/CP/Quoting/GetDocumentAsHttpResp?id=${btoa(JSON.stringify(doc))}`);
    }

    const OpenDocuments = () => {
        request.InitCookie("file_download", "L_Error_Load_document");
        window.open(`/CP/Quoting/GetDocumentsAsHttpResp?id=${btoa(JSON.stringify(Instance.Policy.Documents.Regular))}`);
    }



    return {
        Init,
        Instance,
        ESign,
        Payment,
        AddDriver,
        RemoveDriver,
        AddVehicle,
        PaymentInfo,
        RemoveVehicle,
        UpdateInstance,
        SetInfoValid,
        AddUDRDriver,
        GetPaymentInfo,
        InitUnderwritingResponses,
        ClearAnswer,
        GetQuote,
        GetDocuSignItems,
        LaunchEsignPop,
        ClearCCPayment,
        ClearEFTPayment,
        MakePayment,
        Account,
        Misc,
        SetupAccount,
        OpenDocument,
        OpenDocuments
    }
});
