import { defineStore } from 'pinia';
import { useMaster } from './Master';
import { TDropdown, TTextbox } from '@/Scripts/Common/Types/UIStuff';
import request from '@/Scripts/Common/request';
import { OptionHTMLAttributes, Ref, reactive } from 'vue';
import { useStateSpec } from './StateSpec';
import { IAddress, IDriver, IPhone, ISuggestedAddress, IVehicle, IVehicleCoverages } from '@/Scripts/Home/Interfaces/Insurance';
import tools from '@/Scripts/Common/tools';
import { useUI } from './UI';
import { useCommon } from '../common';
import { useControls } from './Controls';
import { En_Address } from '@/Scripts/Home/Enums';
import { ICourse } from '@/Scripts/Home/Interfaces/Insurance';

export const useValidate = defineStore("Validate", () => {
    const Data = reactive({
        OnUMPDChange: (Coverages: IVehicleCoverages, covControls: any) => { },
        OnPIPChange: (mpObj: any) => { },
        OnMPChange: (pipObj: any) => { },
        OnComCollChange: (coverages: IVehicleCoverages, covControls: any) => { }
    });
    let _State: string | undefined;
    const stateSpec = useStateSpec();
    const master = useMaster();
    const ui = useUI();
    const common = useCommon();
    const controls = useControls();

    const OnLicTypeChange = (driver: IDriver, driverObj: any) => {
        const stateSpecOptions = stateSpec.Data.Dropdowns.DriverOptions;
        const v = stateSpecOptions.LicenseType.Items.find((item) => item.Value == driver.License.Type)?.Name;
        let licState = stateSpecOptions.LicenseState.Items.find(c => c.Name == "Other")?.Value;
        let lic36Yes = stateSpecOptions.License36Months.Items.find(c => c.Name == "Yes")?.Value;
        if (driver.License.Number == "N/A") {
            driver.License.Number = "";
        }
        switch (v) {
            case "AZ":
            case "CO":
            case "IL":
            case "IN":
            case "OK":
            case "TX":
            case "UT":
                licState = stateSpecOptions.LicenseState.Items.find(c => c.Name == v)?.Value;
                if (licState) {
                    driver.License.State = licState;
                }
                driverObj.LicState.item.disabled = true;
                driverObj.LicMon36.item.disabled = false;
                driverObj.LicNum.disabled = false;
                driverObj.LicNum.error = undefined;
                break;
            case "Expired":
                if (lic36Yes) {
                    driver.License.Months36 = lic36Yes;
                }
                driverObj.LicState.item.disabled = false;
                driverObj.LicMon36.item.disabled = true;
                driverObj.LicNum.disabled = false;
                driverObj.LicNum.error = undefined;
                break;
            case "OutOfState":
            case "Permit":
            case "Suspended":
                driverObj.LicState.item.disabled = false;
                driverObj.LicMon36.item.disabled = false;
                driverObj.LicNum.disabled = false;
                driverObj.LicNum.error = undefined;
                break;
            case "ForeignPassport":
            case "International":
            case "Matricula":
            case "StateID":
                if (licState) {
                    driver.License.State = licState;
                }
                if (lic36Yes) {
                    driver.License.Months36 = lic36Yes;
                }
                driverObj.LicState.item.disabled = true;
                driverObj.LicMon36.item.disabled = true;
                driverObj.LicNum.disabled = false;
                driverObj.LicNum.error = undefined;
                break;
            case "NotLicensed":
                if (licState) {
                    driver.License.State = licState;
                }
                if (lic36Yes) {
                    driver.License.Months36 = lic36Yes;
                }
                driverObj.LicState.item.disabled = true;
                driverObj.LicNum.disabled = true;
                driverObj.LicNum.error = undefined;
                driver.License.Number = "N/A";
                break;
            default:
                break;
        }
    }
    const OnPriorCovChange = (driverObj: any) => {
        const stateSpecOptions = stateSpec.Data.Dropdowns.PolicyOptions.PriorCoverage;
        const trans = master.Instance.Policy.Modifiers.TransferDiscount;
        const v = stateSpecOptions.Items.find((item) => item.Value == trans.Flag)?.Name;
        if (v) {
            (driverObj.PriorCovCompany as TTextbox).error = undefined;
            (driverObj.PriorCovNumber as TTextbox).error = undefined;
            if (v === "Yes") {
                driverObj.PriorCovCompany.disabled = false;
                driverObj.PriorCovNumber.disabled = false;
            }
            else {
                driverObj.PriorCovCompany.disabled = true;
                driverObj.PriorCovNumber.disabled = true;
                trans.CompanyName = "";
                trans.PolicyNumber = "";
            }
        }
    }

    const Vin = (vehicle: IVehicle, item: any) => {
        if (vehicle.Vin && vehicle.Year && vehicle.Make.Description
            && vehicle.Model.Description
            && vehicle.BodyType.Description
            && vehicle.Symbol.Description
            && vehicle.Vin.length === 17) {
            request.consumerService("VehicleInfo/ValidateVin",
                vehicle,
                (obj: boolean) => {
                    if (obj) {
                        item.error = "";
                    }
                    else {
                        item.error = "L_Error_Invalid_vin";
                    }
                });
        }
        else if (vehicle.Vin && vehicle.Vin.length > 0 && vehicle.Vin.length < 17) {
            item.error = "L_Error_Enter_valid_vin";
        }
    }
    const Email = (value: string, item: TTextbox, updateModal: (value: any) => void) => {
        if (value === "" || value === undefined) {
            item.required = false;
            item.error = undefined;
        }
        else if (!tools.isValidEmail(value)) {
            item.error = "L_Error_Invalid_email";
            item.required = true;
        }
    }
    const Account = (value: boolean, emailItem: { error?: string, required?: boolean }, passwordItem: { error?: string }, passwordConfirmItem: { error?: string }) => {
        (master.Account as { Password?: string }).Password = undefined;
        master.Account.Flag = value;
        emailItem.required = value;
        emailItem.error = undefined;

        passwordItem.error = undefined
        passwordConfirmItem.error = undefined

        const emailValue = master.Instance.PolicyHolder.Email;
        if (emailValue && !tools.isValidEmail(emailValue)) {
            emailItem.error = "L_Error_Invalid_email";
        }
    }

    const DobStr = (value: string, item: TTextbox, updateModal: (value: any) => void) => {
        const msg = "L_Error_Enter_valid_date";
        if (DateStr(value, item, updateModal)) {
            const arr = value.split('/');
            const year = Number(arr[2]);
            const today = new Date();
            const yearNow = today.getFullYear();
            if (year < 0 || year < yearNow - 109) {
                item.error = msg;
                return;
            }
            else {
                item.error = "";
            }
        }
    }

    const DateStr = (value: string, item: TTextbox, updateModal: (value: any) => void) => {
        const date = tools.getValidDate(value);
        const msg = "L_Error_Enter_valid_date";
        if (date) {
            const today = new Date();
            const year = date.getFullYear();
            const month = date.getMonth();
            const day = date.getDate();
            const yearNow = today.getFullYear();
            if (year < yearNow - 109) {
                item.error = msg;
                return false;
            }
            if (year == today.getFullYear()) {
                const monthNow = today.getMonth();
                if (month == monthNow) {
                    if (day > today.getDate()) {
                        item.error = msg;
                        return false;
                    }
                } else if (month > monthNow) {
                    item.error = msg;
                    return false;
                }
            }
            if (year > yearNow) {
                item.error = msg;
                return false;
            }
            //valid dob
            updateModal(value);
            item.error = "";
            return true;
        }
        else {
            item.error = msg;
        }
        return false;
    }

    const EffectiveDateStr = (value: string, item: TTextbox, updateModal: (value: any) => void) => {
        const date = tools.getValidDate(value);
        const msg = "L_Error_Enter_valid_date";
        if (date) {
            updateModal(value);
            const today = new Date(stateSpec.Data.EffectiveDate);
            const newTime = date.getTime();
            const startTime = today.getTime();
            const limit = new Date(today.getTime());
            limit.setDate(today.getDate() + 7);
            const endTime = limit.getTime();
            if (newTime >= startTime && newTime <= endTime) {
                item.error = "";
                return;
            }
        }
        item.error = msg;
    }

    const Zip = async (value: string | undefined, item: { error?: string }) => {
        if (value && value.length === 5) {
            item.error = "";
        }
        else {
            item.error = "L_Error_Enter_valid_zip";
        }
    }

    const UpdateAddress = async (addr: IAddress, smartAddr: IAddress, addrType: string, zip: any, city: any) => {
        master.Misc.SmartyStreet.Addresses = [];
        addr.Street = smartAddr.Street;
        addr.City = smartAddr.City;
        addr.Zip = smartAddr.Zip;
        city.error = "";
        zip.error = "";
        await ZipAndGetCounties(addr.Zip, zip, addr, addrType, true);
    }

    const GetSmartyCounty = (type: string, addr: IAddress) => {
        if (tools.hasValue(addr.Street) && tools.hasValue(addr.City)) {
            const req = {
                Instance: master.Instance,
                Type: type,
                Ignore: false
            }
            request.consumerService("SmartyStreets/GetAddress", req, (obj: ISuggestedAddress) => {
                if (obj.Address?.County) {
                    master.Instance.PolicyHolder.Address.County = obj.Address.County;
                }
            });
        }
    }

    const ZipAndGetCounties = async (value: string | undefined, item: { error?: string }, addr: IAddress, addrType: string, autoSlelect?: boolean) => {
        item.error = "";
        await Zip(value, item);
        if (!item.error) {
            if (addr.State) {
                request.consumerService("Quoting/GetCounties", { State: addr.State, Zip: value }, (counties: string[]) => {
                    (addr as { County?: string }).County = undefined;
                    if (!counties || counties.length == 0) {
                        item.error = "L_Error_Enter_valid_zip";
                        return;
                    }
                    if (counties.length > 0) {
                        GetSmartyCounty(addrType, addr);
                    }
                }, undefined, "L_Loading_counties_failed");
            }
        }
        else {
            (addr as { County?: string }).County = undefined;
        }
    }

    let _timeoutId: number | undefined = undefined;

    const SmartyAddressAutoComplete = async (addr: IAddress) => {
        clearTimeout(_timeoutId);
        _timeoutId = setTimeout(() => {
            if (addr.Street.length >= 3) {
                request.consumerService("SmartyStreets/GetAddressAutocomplete", master.Instance.PolicyHolder.Address,
                    (obj: IAddress[]) => master.Misc.SmartyStreet.Addresses = obj,
                    () => master.Misc.SmartyStreet.Addresses = [], undefined, undefined, undefined, true);
            }
        }, 300); // Set the debounce delay to 300 milliseconds (adjust as needed)
    }

    const SmartyAddress = (onSuccess: () => void) => {
        const req = {
            Instance: master.Instance,
            Type: 1,
            Ignore: false
        }
        request.consumerService("SmartyStreets/GetAddress", req, (obj: ISuggestedAddress) => {
            master.Misc.SmartyStreet.CurrentSelection = En_Address.Manual;
            master.Misc.SmartyStreet.SuggestedAddress = obj;
            if (!obj.Address && obj.MissingOrWrongSecondaryInfo && !obj.ValidAddressWithUnknownApt) {
                common.Data.Popup = { Component: "vAlert", Message: "L_Error_Missing_Apt" };
            }
            else if (!obj.Address && obj.WasSuccess) {
                request.consumerService("SmartyStreets/LogValidatedAddress", req, undefined, undefined, undefined, undefined, undefined, true);
                onSuccess();
            }
            else if (!obj.Address) {
                common.Data.Popup = { Component: "vAlert", Message: "L_Error_Provide_Valid_Addr" };
            }
            else {
                controls.ContinueS.value = 'L_Continue';
                controls.CancelS.value = 'L_Cancel';
                common.Data.Popup = {
                    Component: "vPopupAction",
                    RootCls: "addr-error",
                    Heading: "L_Attention",
                    SubComponent: "vAddressError",
                    ContinueAction: async () => {
                        if (master.Misc.SmartyStreet.CurrentSelection == En_Address.Manual) {
                            ui.ClosePopup();
                        }
                        else {
                            if (master.Misc.SmartyStreet.CurrentSelection == En_Address.Suggested) {
                                const addr = master.Instance.PolicyHolder.Address;
                                const suggAddr = master.Misc.SmartyStreet.SuggestedAddress.Address;
                                addr.Street = suggAddr.Street;
                                addr.City = suggAddr.City;
                                addr.Zip = suggAddr.Zip;
                                if (suggAddr.County) {
                                    addr.County = suggAddr.County;
                                }
                            }
                            else {
                                req.Ignore = true;
                            }
                            request.consumerService("SmartyStreets/LogValidatedAddress", req, undefined, undefined, undefined, undefined, undefined, true);
                            onSuccess();
                        }
                    }
                };
            }
        });
    }

    const Phone = (value: string | undefined, item: { error?: string }) => {
        if (value && value.length === 14) {
            item.error = "";
        }
        else {
            item.error = "L_Error_Enter_valid_phone_num";
        }
    }

    const PolicyHolderPhone = (value: string | undefined, item: { error?: string }) => {
        Phone(value, item);
        CheckHomePhone();
    }

    const CardNumber = async (value: string | undefined, item: { error?: string }) => item.error = value && value.length >= 14 ? "" : "L_Error_Enter_valid_card";


    const CheckConfirm = async (mainValue: string | undefined, confirmValue: string | undefined, confirmItem: { error?: string }, error: string) => {
        if (confirmValue) {
            if (confirmValue != mainValue) {
                confirmItem.error = error;
            }
            else {
                confirmItem.error = "";
            }
        }
    }

    const Confirm = async (confirmValue: string | undefined, mainValue: string | undefined, confirmItem: { error?: string }, error: string) => {
        if (confirmValue && mainValue) {
            if (confirmValue != mainValue) {
                confirmItem.error = error;
            }
            else {
                confirmItem.error = "";
            }
        }
    }

    const Password = async (value: string | undefined, item: { error?: string, errParams: string[] }, confirmValue: string | undefined, confirmItem: { error?: string }, error: string) => {
        if (value && value.length < 7) {
            item.error = "L_Error_Password_length";
            item.errParams = ["7"];
            confirmItem.error = undefined;
        }
        CheckConfirm(value, confirmValue, confirmItem, error);
    };

    const Run = (action?: () => void) => {
        const validate = ui.Sections[ui.Data.CurrentSection].Validate;
        const items = validate.Items;
        let hasErrors = false;
        if (items && items.length > 0) {
            items.forEach(item => {
                if (item.FormItem.required && !item.FormItem.disabled) {
                    if (item.Type === "IDBEntity") {
                        if (typeof item.Value === "string" && item.Value != '{}') {
                            item.FormItem.error = "";
                        }
                        else {
                            item.FormItem.error = "L_Required";
                        }
                    }
                    else if (item.Value === null || item.Value === undefined || item.Value === "") {
                        item.FormItem.error = "L_Required";
                    }
                    else if (item.FormItem.id === 'dd_violation' && item.Value == -1) {
                        item.FormItem.error = "L_Required";
                    }
                }
                if (item.FormItem.error) {
                    hasErrors = true;
                }
            });
        }
        if (hasErrors) {
            setTimeout(() => {
                MoveToError();
            }, 1);
            return false;
        }
        if (validate.Action && !validate.Action()) {
            return false;
        }
        if (action) {
            action();
        }
        return true;
    }

    const MoveToError = async () => {
        const elements = document.getElementsByClassName("field-error");
        if (elements.length > 0) {
            const firstElement = elements[0] as HTMLElement;
            // firstElement.scrollIntoView({ behavior: 'smooth' });
            const elementRect = firstElement.getBoundingClientRect();
            const targetScrollPosition = window.scrollY + elementRect.top - 200;
            window.scrollTo({
                top: targetScrollPosition,
                behavior: 'smooth',
            });
        }


    }

    const MarriedOrCivilDrivers = (): boolean => {
        const items = stateSpec.Data.Dropdowns.DriverOptions.MaritalStatus.Items;
        const marriedValue = stateSpec.GetMaritalStatus("Married");
        const relationSpouseValue = stateSpec.GetRelationship("Spouse")
        const civilUnionValue = items.find((item) => item.Name === "CivilUnion")?.Value;
        const mainDriver = master.Instance.Drivers[0];

        //First, checking for other even number of married/civilUnion drivers
        const marriedDrivers = master.Instance.Drivers.filter(
            (d): boolean => d.Name.MaritalStatus == marriedValue
        );

        const civilUnionDrivers = master.Instance.Drivers.filter(
            (d) => d.Name.MaritalStatus == civilUnionValue
        );

        if (
            marriedDrivers.length % 2 != 0 ||
            (civilUnionValue && civilUnionDrivers.length % 2 != 0)
        ) {
            common.Data.Popup = { Component: "vAlert", Message: "L_Error_Add_even_married_civil_drivers" };
            return false;
        }

        //Second, checking for conditions if policy holder / driver[0] is married  || checking if civil union exists and then validate conditions if policy holder / driver[0] is civil union
        if (marriedValue && relationSpouseValue && mainDriver.Name.MaritalStatus === marriedValue
            && (!master.Instance.Drivers.find((d, i) => i > 0 && d.Name.MaritalStatus === marriedValue)
                || !master.Instance.Drivers.find((d, i) => i > 0 && d.Relationship === relationSpouseValue))) {
            common.Data.Popup = { Component: "vAlert", Message: "L_Error_Add_spouse_civil" };
            return false;
        }
        if (civilUnionValue && relationSpouseValue && mainDriver.Name.MaritalStatus === civilUnionValue
            && (!master.Instance.Drivers.find((d, i) => i > 0 && d.Name.MaritalStatus === civilUnionValue)
                || !master.Instance.Drivers.find((d, i) => i > 0 && d.Relationship === relationSpouseValue))) {
            common.Data.Popup = { Component: "vAlert", Message: "L_Error_Add_spouse_civil" };
            return false;
        }

        return true;
    }

    const Init = () => {
        _State = stateSpec.GetStateStr(stateSpec.Data.State);
        const drops = stateSpec.Data.Dropdowns;
        switch (_State) {
            case "AZ":
                Data.OnComCollChange = (coverages: IVehicleCoverages, covControls: any) => {
                    const dis = coverages.ComColDeductible != drops.VehicleOptions.ComColDeductible.DefaultValue;
                    covControls.SafetyEquip.item.disabled = dis;
                    coverages.SafetyEquipment = drops.VehicleOptions.SafetyEquipment.DefaultValue;
                }
                break;
            case "CO":
                Data.OnComCollChange = (coverages: IVehicleCoverages, covControls: any) => {
                    const dis = coverages.ComColDeductible != drops.VehicleOptions.ComColDeductible.DefaultValue;
                    covControls.UMPD.item.disabled = dis;
                    coverages.UMPD = drops.VehicleOptions.UMPDLimit.DefaultValue;
                }
                break;
            case "IL":
                Data.OnUMPDChange = (coverages: IVehicleCoverages, covControls: any) => {
                    const dis = coverages.UMPD != drops.VehicleOptions.UMPDLimit.DefaultValue;
                    covControls.ComColl.item.disabled = dis;
                    coverages.ComColDeductible = drops.VehicleOptions.ComColDeductible.DefaultValue;
                };
                Data.OnComCollChange = (coverages: IVehicleCoverages, covControls: any) => {
                    const dis = coverages.ComColDeductible != drops.VehicleOptions.ComColDeductible.DefaultValue;
                    covControls.UMPD.item.disabled = dis;
                    coverages.UMPD = drops.VehicleOptions.UMPDLimit.DefaultValue;
                }
                break;
            case "IN":
                break;
            case "OK":
                break;
            case "TX":
                Data.OnPIPChange = (mpObj: any) => {
                    master.Instance.Policy.Coverages.MP = stateSpec.Data.Dropdowns.PolicyOptions.MPLimit.DefaultValue;
                    (mpObj as TDropdown).item.disabled = master.Instance.Policy.Coverages.PIP != stateSpec.Data.Dropdowns.PolicyOptions.PIPLimit.DefaultValue;
                }
                Data.OnMPChange = (pipObj: any) => {
                    master.Instance.Policy.Coverages.PIP = stateSpec.Data.Dropdowns.PolicyOptions.PIPLimit.DefaultValue;
                    (pipObj as TDropdown).item.disabled = master.Instance.Policy.Coverages.MP != stateSpec.Data.Dropdowns.PolicyOptions.MPLimit.DefaultValue;
                }
                Data.OnUMPDChange = (Coverages: IVehicleCoverages, covControls: any) => {
                    master.Instance.InsVehicles.Vehicles.forEach((v) => {
                        v.Coverages.UMPD = Coverages.UMPD;
                    });
                }
                break;
            case "UT":
                Data.OnComCollChange = (coverages: IVehicleCoverages, covControls: any) => {
                    const dis = coverages.ComColDeductible != drops.VehicleOptions.ComColDeductible.DefaultValue;
                    covControls.UMPD.item.disabled = dis;
                    coverages.UMPD = drops.VehicleOptions.UMPDLimit.DefaultValue;
                }
                break;
            default:
                break;
        }
    }

    const UWQ = () => {
        //checking if all questions are answered.
        const v = master.Instance.Policy.UnderwritingResponses.every(q => q.Value == -1 || q.Value == 1);
        if (!v) {
            common.Data.Popup = { Component: "vAlert", Message: "L_Error_Ans_UWQs" };
        }
        return v;
    }

    const RestrictVehicleUpdate = () => {
        controls.ContinueS.value = 'L_Continue';
        controls.CancelS.value = 'L_Cancel';
        common.Data.Popup = {
            Component: "vPopupAction",
            RootCls: "vehicle-dis",
            Heading: "L_Attention",
            Message: "L_Vehicle_update_popup",
            ContinueAction: async () => {
                window.location.reload();
            }
        };
    }

    const CheckHomePhone = () => {
        //CDAP-2793
        const primaryPhone = master.Instance.PolicyHolder.Phones[0];
        if (master.Instance.PolicyHolder.Phones.length == 1 && (primaryPhone.Type == stateSpec.GetPhoneTypeVal("Cellular") || primaryPhone.Type == stateSpec.GetPhoneTypeVal("Work")))
            master.Instance.PolicyHolder.Phones.push({ Type: stateSpec.GetPhoneTypeVal("Home") } as IPhone);
        if (master.Instance.PolicyHolder.Phones.length > 1)
            master.Instance.PolicyHolder.Phones[1].Number = primaryPhone.Number;
        if (primaryPhone.Type == stateSpec.GetPhoneTypeVal("Home") && master.Instance.PolicyHolder.Phones.length > 1)
            master.Instance.PolicyHolder.Phones.splice(1, 1);
    }

    const EnableCourseDiscountDate = (driverIndex: number, control: any) => {
        const accPrev = master.Instance.Drivers[driverIndex].CourseDiscount.AccidentPrevention;
        (accPrev as { Date?: string }).Date = undefined;
        control.error = undefined;
        control.disabled = true;
        if (accPrev.Value == stateSpec.GetAccidentPreventionVal("Yes")) {
            control.disabled = false;
        }
    }

    return {
        Init,
        Data,
        Run,
        DateStr,
        DobStr,
        EffectiveDateStr,
        OnLicTypeChange,
        Zip,
        ZipAndGetCounties,
        Phone,
        PolicyHolderPhone,
        Email,
        OnPriorCovChange,
        MarriedOrCivilDrivers,
        Vin,
        UWQ,
        CardNumber,
        CheckConfirm,
        Confirm,
        Password,
        Account,
        RestrictVehicleUpdate,
        SmartyAddressAutoComplete,
        SmartyAddress,
        CheckHomePhone,
        EnableCourseDiscountDate,
        UpdateAddress
    }
});
