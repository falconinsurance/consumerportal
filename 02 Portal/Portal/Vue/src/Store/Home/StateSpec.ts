import { IStateSpec } from '@/Scripts/Home/Interfaces/StateSpecifics';
import { defineStore } from 'pinia'
import { reactive } from 'vue'
import { useCommon } from '../Common';
import request from '@/Scripts/Common/request';
import session from '@/Scripts/Common/session';
import { IAgency } from '@/Scripts/Home/Interfaces/Insurance';
import { useMaster } from './Master';
import { useUI } from './UI';
import { useValidate } from './Validate';

export const useStateSpec = defineStore("StateSpec", () => {
    const Data = reactive({} as IStateSpec & { VehicleYears: number[] });
    const GetDriverType = (name: "Insured" | "Excluded") => {
        return Data.Dropdowns.DriverOptions.DriverType.Items.find((item) => item.Name === name)?.Value;
    }
    const GetRelationship = (name: "Spouse") => {
        return Data.Dropdowns.DriverOptions.Relationship.Items.find((item) => item.Name === name)?.Value;
    }
    const GetMaritalStatus = (name: "Married" | "CivilUnion") => {
        return Data.Dropdowns.DriverOptions.MaritalStatus.Items.find((item) => item.Name === name)?.Value;
    }
    const GetPayPlanTypeLabel = (value: number) => {
        return Data.Dropdowns.PolicyOptions.PayPlanType.Items.find((item) => item.Value === value)?.Label;
    }
    const UpdateStateSpecifics = (data: IStateSpec) => {
        Data.EffectiveDate = data.EffectiveDate;
        useMaster().Instance.Policy.EffectiveDate = data.EffectiveDate;
        Data.Dropdowns = data.Dropdowns;
        Data.UnderwritingQuestions = data.UnderwritingQuestions;
        Data.State = Data.Agencies[0].State;
        useValidate().Init();
        useMaster().Init(); //inits only 1st time
    }

    const GetStateStr = (state: number) => {
        return Data.Dropdowns.DriverOptions.LicenseState.Items.find(c => c.Value == state)?.Name;
    }

    const GetPhoneTypeVal = (name: string) => {
        return Data.Dropdowns.PolicyOptions.PrimaryPhone.Items.find(c => c.Name == name)?.Value;
    }

    const GetAccidentPreventionVal = (name: string) => {
        return Data.Dropdowns.DriverOptions.AccidentPrevention.Items.find(c => c.Name == name)?.Value;
    }

    const GetMaxEffectiveDate = () => {
        const today = Data.EffectiveDate;
        if (today) {
            const todayTemp = new Date(today)
            return new Date(todayTemp.setDate(todayTemp.getDate() + 7));
        }
        return undefined;
    }

    const GetAgencies = async () => {
        await request.consumerService('Init/GetAgencies', { AgencyGroupId: session.GetConsumerId() },
            (data: IAgency[]) => {
                Data.Agencies = data;
                if (data.length > 0) {
                    Data.State = data[0].State;
                }
                if (data.length === 1) {
                    useMaster().Instance.Agency = data[0];
                }
                useUI().Init();
            }
        );
    }

    const GetStateSpecifics = (lang?: string, error?: string, action?: () => void) => {
        request.consumerService('Init/GetStateSpecifics', {
            State: Data.State,
            Language: lang ?? useCommon().Data.Language
        }, (data: IStateSpec) => { UpdateStateSpecifics(data); action && action(); }, undefined, error);
    }

    const GetVehicleYears = () => {
        request.consumerService('VehicleInfo/GetYears', undefined, (years: number[]) => {
            return Data.VehicleYears = years;
        });
    }

    const Init = async () => {
        const common = useCommon();
        request.initialize(common.Data);
        common.UpdateOnLanguageChangeAction((lang: string) => GetStateSpecifics(lang, 'L_Error_Lang_change'));
        // common.OnLanguageChange = (lang: string) => GetStateSpecifics(lang);
        common.GetTranslationLabels();
        await GetAgencies();
        GetStateSpecifics(undefined, undefined, useMaster().Init);
        GetVehicleYears();
    }
    return {
        Data,
        Init,
        GetMaxEffectiveDate,
        GetDriverType,
        GetRelationship,
        GetMaritalStatus,
        GetPayPlanTypeLabel,
        GetStateStr,
        GetPhoneTypeVal,
        GetAccidentPreventionVal
    }
});