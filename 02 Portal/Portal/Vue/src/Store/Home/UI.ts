import { TNavItem, TNavigation, TSection, TUI, TValidateTB } from '@/Scripts/Home/Types/StoreStuff';
import { En_Nav, En_Sections } from "@/Scripts/Home/Enums";
import { defineStore } from 'pinia'
import { Ref, computed, reactive, ref, watch } from 'vue'
import { IAddress, IDriver, IInstance, IVehicle } from '@/Scripts/Home/Interfaces/Insurance';
import { useMaster } from './Master';
import { useControls } from './Controls';
import { useStateSpec } from './StateSpec';
import { TButton } from '@/Scripts/Common/Types/UIStuff';
import { useCommon } from '../Common';
import { useValidate } from './Validate';
import gsapAnim from '@/Scripts/Common/gsapAnim';
import tools from '@/Scripts/Common/tools';

export const useUI = defineStore("UI", () => {
  const Data = reactive<TUI>({
    Window: window,
    ContainerPadding: "0",
    Outernav: null as HTMLDivElement | null,
    OuterNavClass: "",
    CurrentSection: En_Sections.vInfo,
    Forward: true
  } as TUI);

  const Resize = () => {
    Data.ContainerPadding = Data.Window?.innerWidth < 992 ? Data.Outernav.clientHeight + 10 + "px" : "0";
    if (Data.Window?.innerWidth >= 992) {
      Nav.Agent = false;
    }
    Data.OuterNavClass = Data.Outernav.clientHeight >= Data.Window.innerHeight ? "outer-nav-full" : "";
  }

  // Listen to the resize event on the Window object
  window.addEventListener('resize', Resize);

  const master = useMaster();
  const stateSpec = useStateSpec();
  const common = useCommon();
  const validate = useValidate();
  const controls = useControls();

  watch(() => Data.CurrentSection, (n, o) => {
    Data.Forward = n > o;
    return OnSectionChange();
  });

  watch(() => master.Account.Flag, (value) => {
    if (!master.Account.Created && value) {
      SetCreateAccountBtn();
    }
    else {
      UpdateSectionInfo();
    }
  });

  const SetCreateAccountBtn = () => {
    Section.Continue.Btn.id = "createAccount";
    Section.Continue.Btn.value = "L_Create_account";
    Section.Continue.Btn.spanAfter = "";
  }

  const Nav = reactive<TNavigation>({
    Agent: true,
    Items: [
      {
        type: En_Nav.Start,
        label: "L_Start",
        icon: "icon-start",
        list: false,
        enable: true,
      },
      {
        type: En_Nav.Drivers,
        label: "L_Drivers",
        icon: "icon-users",
        list: false,
        enable: false,
      },
      {
        type: En_Nav.Vehicles,
        label: "L_Vehicles",
        icon: "icon-cars",
        list: false,
        enable: false,
      },
      {
        type: En_Nav.CoveragePackages,
        label: "L_Coverages",
        icon: "icon-shield-tick",
        list: false,
        enable: false,
      },
      {
        type: En_Nav.CurrentQuote,
        label: "L_CurrentQuote",
        icon: "icon-doc-bill",
        list: false,
        enable: false,
      },
      {
        type: En_Nav.Finish,
        label: "L_Finish",
        icon: "icon-clipboard",
        list: false,
        enable: false,
      },
    ],
  } as TNavigation);

  const Sections = reactive<Record<En_Sections, TSection>>({
    [En_Sections.vInfo]: {
      Heading: "L_Tell_about_self_QQ",
      Nav: En_Nav.Start,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vRate]: {
      Heading: "L_Based_on_info_startring_rate",
      Nav: En_Nav.Start,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vDriver]: {
      Heading: "L_Tell_more_abt_self",
      Nav: En_Nav.Drivers,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vOtherDrivers]: {
      Heading: "L_Like_to_add_drivers",
      Nav: En_Nav.Drivers,
      Validate: {
        Action: validate.MarriedOrCivilDrivers,
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vVehicles]: {
      Heading: "L_Tell_more_about_vehicle",
      Nav: En_Nav.Vehicles,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vCoveragePackages]: {
      Heading: "L_Select_coverage",
      Nav: En_Nav.CoveragePackages,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vFinalRate]: {
      Heading: "L_As_result_new_rate",
      Nav: En_Nav.CurrentQuote,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vUWQ]: {
      Heading: "L_Underwriting_Qs",
      Nav: En_Nav.CurrentQuote,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vPayplans]: {
      Heading: "L_Choose_pay_plan",
      Nav: En_Nav.Finish,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vEsign]: {
      Heading: "L_Esign_process",
      Nav: En_Nav.Finish,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vMakePayment]: {
      Heading: "L_Complete_payment",
      Nav: En_Nav.Finish,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vAccount]: {
      Heading: "L_Complete_account_creation",
      Nav: En_Nav.Finish,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
    [En_Sections.vDocuments]: {
      Heading: "L_Documents",
      Nav: En_Nav.Finish,
      Validate: {
        Items: [] as TValidateTB[]
      }
    } as TSection,
  });


  const AssignOuterNav = (value: HTMLDivElement) => {
    Data.Outernav = value;
    Data.ContainerPadding = Data.Window?.innerWidth < 992 ? Data.Outernav.clientHeight + 10 + "px" : "0";
    Data.OuterNavClass = Data.Outernav.clientHeight >= Data.Window.innerHeight ? "outer-nav-full" : "";
  }


  const ToggleAgent = () => {
    Nav.Agent = !Nav.Agent
  }

  const OnNavClicked = (nav: En_Nav) => {
    switch (nav) {
      case En_Nav.Start:
        Data.CurrentSection = En_Sections.vInfo;
        break;
      case En_Nav.Drivers:
        Data.CurrentSection = En_Sections.vDriver;
        break;
      case En_Nav.Vehicles:
        Data.CurrentSection = En_Sections.vVehicles;
        break;
      case En_Nav.CoveragePackages:
        Data.CurrentSection = En_Sections.vCoveragePackages;
        break;
      case En_Nav.CurrentQuote:
        Data.CurrentSection = En_Sections.vFinalRate;
        break;
      case En_Nav.Finish:
        Data.CurrentSection = En_Sections.vPayplans;
        break;
      default:
        break;

    }
  }

  const GetNavListItems = (nav: En_Nav, instance: IInstance) => {
    switch (nav) {
      case En_Nav.Drivers:
        return instance.Drivers.filter(d => {
          return tools.hasValue(d.Name.First) && tools.hasValue(d.Name.Last);
        }).map((driver: IDriver) => `${driver.Name.First} ${driver.Name.Last}`);
      case En_Nav.Vehicles:
        return instance.InsVehicles.Vehicles.filter(v => {
          return tools.hasValue(v.Year) && v.Make && tools.hasValue(v.Make.Description) && v.Model && tools.hasValue(v.Model.Description);
        }).map((vehicle: IVehicle) => `${vehicle.Year} ${vehicle.Make.Description} ${vehicle.Model.Description}`);
      default:
        break;
    }
  }

  const GetLocationForMaps = (addr: IAddress): string => {
    return `http://maps.apple.com/?address=${addr.Street}+${addr.State}+${addr.Zip}&t=m`;
  };

  const GetOuterNavClass = (navHeight?: number) => {
    return navHeight && navHeight >= Data.Window?.innerHeight ? "outer-nav-full" : "";
  };

  const GetPageHeading = () => {
    return Sections[Data.CurrentSection].Heading;
  }

  const GetDisableCls = () => {
    return "";
  }

  const GetComponentName = computed(() => {
    const m = Object.keys(En_Sections).find((key: string) => En_Sections[key as keyof typeof En_Sections] === Data.CurrentSection) as keyof typeof En_Sections;
    return m;
  });

  const AddValidateItems = (FormItem: any, Value: Ref, Type?: "IDBEntity" | "string" | "number") => {
    const section = Data.CurrentSection;
    Sections[section].Validate.Items.push({ FormItem, Type, Value });
  }

  const RemoveValidateItems = (FormItem: any) => {
    const section = Data.CurrentSection;
    const i = Sections[section].Validate.Items.findIndex((tb) => tb.FormItem == FormItem);
    if (i >= 0) {
      Sections[section].Validate.Items.splice(i, 1);
    }
  }

  const UpdateNavComplete = () => {
    const navItem = Nav.Items.find((n) => n.type === Section.Nav);
    if (navItem) {
      navItem.complete = true;
    }
  }

  const RemoveNavComplete = (self?: boolean) => {
    let navItems = [] as TNavItem[];
    if (self) {
      navItems = Nav.Items.filter((n) => n.type >= Section.Nav);
    }
    else {
      navItems = Nav.Items.filter((n) => n.type > Section.Nav);
    }
    if (navItems) {
      navItems.forEach(n => {
        n.complete = false;
        if (n.type != Section.Nav) {
          n.enable = false;
          n.list = false;
        }
      });
    }

    //removing driver/vehicle from nav
    if (!Nav.Items[0].complete) {
      Nav.Items[1].list = false;
    }
    if (!Nav.Items[2].complete) {
      Nav.Items[2].list = false;
    }
  }

  const SetInfoAction = () => {
    master.Instance.RiskCheck = "ConsumerAppLvl0";
    if (master.Misc.SignComplete) {
      return () => { Data.CurrentSection++; }
    }
    if (stateSpec.Data.Agencies.length > 0 && !master.Instance.Agency) {
      return () => {
        if (validate.Run()) {
          return validate.SmartyAddress(() => {
            common.Data.Popup = {
              Component: "vPopupAction",
              HeadCls: "icon-modal-head",
              RootCls: "agency-locat",
              SubComponent: "vAgentLocations", Icon: "icon-user-location", AgencyError: "", ContinueAction: async () => {
                if (master.Instance.Agency) {
                  common.Data.Popup.AgencyError = "";
                  ClosePopup();
                  master.Instance.InsVehicles.Vehicles.forEach((v) => {
                    (v as { Vin?: string }).Vin = undefined;
                  });
                  return master.GetQuote(() => {
                    Data.CurrentSection++;
                  });
                }
                else {
                  common.Data.Popup.AgencyError = "L_Error_Select_agency_location";
                }
              }
            };
          });
        };
      }
    }
    return () => {
      RemoveNavComplete(true);
      if (validate.Run()) {
        validate.SmartyAddress(() => {
          master.Instance.InsVehicles.Vehicles.forEach((v) => {
            (v as { Vin?: string }).Vin = undefined;
          });
          return master.GetQuote(() => {
            Data.CurrentSection++;
          });
        })
      }
    };
  }

  const Section = reactive<TSection>({
    BtnWrapClass: "single-right",
    Nav: En_Nav.Start,
    Continue: {
      Btn: { id: "getQQ", value: "L_Get_QQ" } as TButton,
      Validate: []
    },
    Back: {
      Btn: { id: "back", value: "L_Back", spanBefore: "← ", hidden: true } as TButton,
      Action: () => { Data.CurrentSection--; }
    }
  } as TSection);

  let _NavLimit = Section.Nav;

  const UpdateSectionInfo = () => {
    Section.Heading = Sections[Data.CurrentSection].Heading;
    Section.Nav = Sections[Data.CurrentSection].Nav;
    Section.Continue.Btn.id = "continue";
    Section.Continue.Btn.value = "L_Continue";
    Section.Continue.Btn.spanAfter = " →";
    Section.Continue.Btn.hidden = false;
    Section.Back.Action = () => { Data.CurrentSection--; };
    Section.Back.Btn.hidden = false;
    Section.BtnWrapClass = "";
  }

  const UpdateNavEnable = () => {
    const navItem = Nav.Items.find((n) => n.type === Section.Nav);
    if (navItem) {
      navItem.enable = true;
    }
  }

  const CheckDisable = async () => {
    if (master.Misc.SignComplete || master.Misc.PaymentComplete) {
      Sections[Data.CurrentSection].Validate.Items.forEach((item) => {
        item.FormItem.disabled = true;
      })
    }
  }

  const OnSectionChange = async () => {
    UpdateSectionInfo();
    const paymentComplete = master.Misc.PaymentComplete;
    const signComplete = master.Misc.SignComplete;
    switch (Data.CurrentSection) {
      case En_Sections.vInfo:
        CheckDisable();
        Section.BtnWrapClass = "single-right";
        Section.Continue.Btn.id = "getQQ"
        Section.Continue.Btn.value = "L_Get_QQ"
        Section.Continue.Btn.spanAfter = ""
        Section.Back.Btn.hidden = true;
        Section.Continue.Action = SetInfoAction();
        return;
      case En_Sections.vRate:
        Section.Continue.Action = () => {
          UpdateNavComplete();
          Data.CurrentSection++;
          Nav.Items[1].list = true;
        }
        return;
      case En_Sections.vDriver:
        UpdateNavEnable();
        CheckDisable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "ConsumerAppLvl1";
          RemoveNavComplete(true);
          validate.Run(() => master.GetQuote(() => Data.CurrentSection++))
        };
        return;
      case En_Sections.vOtherDrivers:
        CheckDisable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "ConsumerAppLvl2";
          return validate.Run(() => master.GetQuote(() => {
            UpdateNavComplete();
            RemoveNavComplete();
            Data.CurrentSection++;
          }, undefined, "L_Error_update_driver"));
        };
        return;
      case En_Sections.vVehicles:
        CheckDisable();
        UpdateNavEnable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "ConsumerAppLvl3";
          return validate.Run(() => master.GetQuote(() => {
            UpdateNavComplete();
            RemoveNavComplete();
            Data.CurrentSection++;
            Nav.Items[2].list = true;
          }));
        };
        return;
      case En_Sections.vCoveragePackages:
        CheckDisable();
        UpdateNavEnable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          if (Section.Continue.Btn.value == "L_Update_Rate") {
            master.Instance.RiskCheck = "None";
            RemoveNavComplete(true);
            master.GetQuote(() => {
              Section.Continue.Btn.id = "continue";
              Section.Continue.Btn.value = "L_Continue";
              Section.Continue.Btn.spanAfter = " →";
            });
            return;
          }
          UpdateNavComplete();
          Data.CurrentSection++;
        };
        return;
      case En_Sections.vFinalRate:
        UpdateNavEnable();
        Section.Continue.Action = () => { Data.CurrentSection++; }
        return;
      case En_Sections.vUWQ:
        CheckDisable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "ConsumerAppLvl4";
          return validate.UWQ() && validate.Run(() => master.GetQuote((inst: IInstance) => {
            master.Misc.DisableVehicleUpdate = true;
            if (inst.AttachedTypes.DriversViolations?.AnyDriver) {
              controls.ContinueS.value = 'L_Accept_rerate';
              controls.CancelS.value = 'L_Cancel';
              common.Data.Popup = {
                Component: "vPopupAction",
                RootCls: "aplus",
                Heading: "L_Driver_viol_found",
                SubComponent: "vAplusMvrViolations",
                ContinueAction: async () => {
                  ClosePopup();
                  RemoveNavComplete();
                  return master.GetQuote(() => {
                    Data.CurrentSection++;
                    UpdateNavComplete();
                  });
                }
              };
            }
            else {
              RemoveNavComplete();
              Data.CurrentSection++;
              UpdateNavComplete();
            }
          }));
        };
        return;
      case En_Sections.vPayplans:
        CheckDisable();
        UpdateNavEnable();
        Section.Continue.Action = () => {
          if (signComplete || paymentComplete) {
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "None";
          RemoveNavComplete(true);
          master.GetQuote(
            () => master.GetDocuSignItems(() => {
              master.Instance.RiskCheck = "None";
              Data.CurrentSection++;
            })
            ,
            undefined,
            "L_Error_Updating_payplan",
            "L_Updating_payplan");
        };
        return;
      case En_Sections.vEsign:
        Section.Continue.Action = () => {
          if (paymentComplete) {
            Data.CurrentSection++;
            Data.CurrentSection++;
            return;
          }
          master.Instance.RiskCheck = "None";
          if (master.ESign.DocuSignItems.every((d) => d.Status == "signing_complete")) {
            Data.CurrentSection++;
            return;
          }
          common.Data.Popup = { Component: "vAlert", Message: "L_Error_Sign_all_documents" };
        }
        return;
      case En_Sections.vMakePayment:
        Section.Continue.Btn.id = "makePayment";
        Section.Continue.Btn.value = "L_Make_Payment";
        Section.Continue.Btn.spanAfter = "";
        Section.Continue.Action = () => validate.Run(() => master.MakePayment(() => { UpdateNavComplete(); Data.CurrentSection++; }))
        return;
      case En_Sections.vAccount:
        if (!master.Account.Created && master.Account.Flag) {
          SetCreateAccountBtn();
        }
        if (master.Account.Created) {
          CheckDisable();
        }
        if (paymentComplete) {
          Section.Back.Action = () => {
            Data.CurrentSection--;
            Data.CurrentSection--;
          };
        }
        Section.Continue.Action = () => validate.Run(() => {
          if (master.Account.Created) {
            Data.CurrentSection++;
            return;
          }
          return master.SetupAccount(() => { Data.CurrentSection++; });
        })
        return;
      case En_Sections.vDocuments:
        Section.Continue.Btn.hidden = true;
        return;

    }
  }

  const NavTickAnimation = (str: string) => {
    Data.Window.innerWidth < 992
      ? gsapAnim.fromBottom({
        className: str,
        duration: 0.5,
        cord: 100,
        scale: 1,
        staggerFrom: "start",
        ease: "power1",
      })
      : gsapAnim.fromLeft({
        className: str,
        duration: 0.5,
        cord: 200,
        scale: 1,
        staggerFrom: "start",
        ease: "power1",
      });
  }

  const ConfirmTickAnimation = (str: string) => {
    gsapAnim.fromTop({
      className: str,
      duration: 1,
      cord: "50%",
      scale: 0,
      staggerFrom: "start",
      ease: "power1",
    })
  }

  const ClosePopup = async () => {
    common.Data.Popup = undefined;
  }

  const Tooltips = ref({} as Record<string, boolean>);

  const AddTooltip = (key?: string) => {
    if (key) {
      Tooltips.value[key] = false;
    }
  }

  const ClickListener = async () => {
    CloseTooltips();
    master.Misc.SmartyStreet.Addresses = [];
  }

  const CloseTooltips = async (k?: string) => {
    if (k) {
      Object.keys(Tooltips.value).forEach((key) => {
        if (key !== k) {
          Tooltips.value[key] = false;
        }
      });
      return;
    }
    Object.keys(Tooltips.value).forEach((key) => {
      Tooltips.value[key] = false;
    });
  }

  const ToggleTooltip = (key?: string) => {
    if (key) {
      CloseTooltips(key);
      Tooltips.value[key] = !Tooltips.value[key];
    }
  }

  let InfoMainVehicle = ref<HTMLDivElement>();

  const Init = () => {
    Section.Continue.Action = SetInfoAction();
  }

  return {
    Nav,
    InfoMainVehicle,
    AssignOuterNav,
    GetOuterNavClass,
    ToggleAgent,
    OnNavClicked,
    GetNavListItems,
    GetLocationForMaps,
    Sections,
    GetPageHeading,
    GetDisableCls,
    GetComponentName,
    Section,
    Data,
    ClosePopup,
    Init,
    SetInfoAction,
    AddValidateItems,
    RemoveValidateItems,
    RemoveNavComplete,
    NavTickAnimation,
    ConfirmTickAnimation,
    Tooltips,
    ClickListener,
    AddTooltip,
    CloseTooltips,
    ToggleTooltip,
    CheckDisable
  }
});
