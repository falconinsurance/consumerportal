import { TButton, TDropdown, TTextbox, TTransistionTable } from '@/Scripts/Common/Types/UIStuff';
import { IDBEntity, IDriver, IVehicle, IVehicleDropdowns } from '@/Scripts/Home/Interfaces/Insurance';
import { defineStore } from 'pinia';
import { reactive, SelectHTMLAttributes, OptionHTMLAttributes, ref, computed, Ref } from 'vue';
import { useStateSpec } from './StateSpec';
import { IStateDropdown, IStateDropdownItem } from '@/Scripts/Home/Interfaces/StateSpecifics';
import request from "@/Scripts/Common/request";
import { TDriverControls, TVehicleControls } from '@/Scripts/Home/Types/StoreStuff';
import { useUI } from './UI';
import { useMaster } from './Master';
import { En_Sections } from '@/Scripts/Home/Enums';
import { useValidate } from './Validate';

export const useControls = defineStore("Controls", () => {

    let _StateSpec = useStateSpec();
    let _Ui = useUI();
    let _Master = useMaster();
    let _validate = useValidate();

    const GetInput = (id: string, header: string, value?: string | number, type?: string, required?: boolean, placeholder?: string, maxLen?: number, mask?: string, cls?: string) => {
        const p = {} as TTextbox;
        p.id = "tb_" + id;
        p.name = "L_" + id;
        p.header = header;
        p.required = required;
        p.placeholder = placeholder;
        p.mask = mask;
        p.type = type;
        p.class = cls;
        p.maxlength = maxLen;
        if (value) {
            p.value = value;
        }
        return p;
    }

    const GetSelect = (id: string, type: string, header: string, options?: OptionHTMLAttributes[], disabled?: boolean, required?: boolean) => {
        const p = {} as SelectHTMLAttributes;
        p.required = required;
        p.id = "dd_" + id;
        p.disabled = disabled;
        const res = { item: p, type: type } as TDropdown;
        res.options = [];
        if (options) {
            res.options = options;
        }
        res.item.header = header;
        res.allow = true;
        return res;
    }

    const GetFalconSelect = (id: string, type: string, stateDropdown?: IStateDropdown, header?: string, cls?: string, toolTip?: string, required?: boolean) => {
        const res = GetSelect(id, type, header ?? "");
        res.item.toolTip = toolTip;
        res.item.class = cls;
        if (stateDropdown) {
            res.options = stateDropdown.Items.map((item: IStateDropdownItem) => ({
                label: item.Label,
                value: item.Value
            } as OptionHTMLAttributes));
            res.item.header = stateDropdown.Label ?? header;
            res.default = stateDropdown.DefaultValue;
            res.item.disabled = stateDropdown.Items.length == 1;
            res.item.required = required;
            if (!res.item.header) {
                res.allow = false;
            }
        }
        else {
            res.allow = false;
        }
        return res;
    }

    const Pad = (d: number): string => {
        return (d < 10) ? '0' + d.toString() : d.toString();
    }

    const GetNumbers = (start: number, end: number) => {
        const arr = [];
        for (start; start <= end; start++) {
            arr.push({ value: start, label: Pad(start) });
        }
        return arr;
    }

    const GetCardYears = () => {
        const year = (new Date()).getFullYear();
        return GetNumbers(year, year + 10);
    }

    const GetInputWithHeaders = (header: string, params: string[], type?: string, required?: boolean) => {
        const tb = GetInput(header, "", undefined, undefined, required);
        tb.headers = params;
        return tb;
    }

    const FirstName = () => reactive<TTextbox>(GetInput("firstName", "L_FirstName", undefined, undefined, true));
    const MiddleName = () => reactive<TTextbox>(GetInput("middleName", "L_MiddleName"));
    const LastName = () => reactive<TTextbox>(GetInput("lastName", "L_LastName", undefined, undefined, true));
    const DOB = () => reactive<TTextbox>(GetInput("dob", "L_DoB", undefined, 'date', true, "MM/DD/YYYY", 10, '##/##/####'));
    const Gender = () => reactive<TDropdown>(GetFalconSelect("gender", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.Gender));
    const Street = () => reactive<TTextbox>(GetInput("street", "L_Streets", undefined, undefined, true));
    const City = () => reactive<TTextbox>(GetInput("city", "L_City", undefined, undefined, true));
    const State = () => reactive<TDropdown>(GetFalconSelect("state", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.State, "L_State", "horizonalign"));
    const Zip = () => reactive<TTextbox>(GetInput("zip", "L_Zip", undefined, 'number', true, "eg: 12345", 5, '#'.repeat(5), "horizonalign zip"));
    const County = () => reactive<TDropdown>(GetSelect("county", "number", "L_County", undefined, true));
    const OwnHome = () => reactive<TDropdown>(GetFalconSelect("ownHome", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.HomeownersDiscount));

    const MaritalStatus = () => reactive<TDropdown>(GetFalconSelect("marital", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.MaritalStatus));
    const Relationship = () => reactive<TDropdown>(GetFalconSelect("relation", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.Relationship));
    const DriverType = () => reactive<TDropdown>(GetFalconSelect("driverType", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.DriverType));
    const LicType = () => reactive<TDropdown>(GetFalconSelect("licType", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.LicenseType));
    const LicState = () => reactive<TDropdown>(GetFalconSelect("licState", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.LicenseState));
    const LicNum = () => reactive<TTextbox>(GetInput("licNum", "L_License_num", undefined, undefined, true));
    const LicMon36 = () => reactive<TDropdown>(GetFalconSelect("licMon36", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.License36Months, undefined, undefined, "L_Lic36"));
    const MatureDr = () => reactive<TDropdown>(GetFalconSelect("matureDr", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.MatureDriver));
    const DefensiveDr = () => reactive<TDropdown>(GetFalconSelect("DefensiveDr", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.DefensiveDriver));
    const AccPrev = () => reactive<TDropdown>(GetFalconSelect("accPrev", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.AccidentPrevention));
    const AccPrevDate = () => reactive<TTextbox>(GetInput("accPrevDate", "L_AccPrevDate", undefined, "date", true, "MM/DD/YYYY", 10, '##/##/####'));
    const DriverOccupation = () => reactive<TTextbox>(GetInput("occupation", "L_Occupation", undefined, undefined, true));
    const DriverEmployer = () => reactive<TTextbox>(GetInput("employer", "L_Employer"));
    const HasViolations = () => reactive<TDropdown>(GetSelect("hasViolations", "number", "L_Have_violations", [{ value: 1, label: 'L_Yes' }, { value: 2, label: 'L_No' }]));
    const Violation = () => reactive<TDropdown>(GetFalconSelect("violation", "number", _StateSpec.Data.Dropdowns?.DriverOptions?.Violations, undefined, undefined, undefined, true));
    const ViolationDate = () => reactive<TTextbox>(GetInput("date", "", undefined, 'date', true, "MM/DD/YYYY", 10, '##/##/####'));

    const Phone = () => reactive<TTextbox>(GetInput("phone", "L_Phone", undefined, "phone", true, "(555) 555-5555", 14, "(###) ###-####"));
    const PhoneType = () => reactive<TDropdown>(GetFalconSelect("phoneType", "number", _StateSpec.Data.Dropdowns?.PolicyOptions.PrimaryPhone, "L_Type"));
    const Email = () => reactive<TTextbox>(GetInput("email", "L_Email", undefined, "email", undefined, "email@example.com"));
    const PriorCoverage = () => reactive<TDropdown>(GetFalconSelect("priorCoverage", "number", _StateSpec.Data.Dropdowns?.PolicyOptions.PriorCoverage));
    const PriorCovCompany = () => reactive<TTextbox>(GetInput("priorCovCompany", "L_Current_Comp", undefined, undefined, true));
    const PriorCovNumber = () => reactive<TTextbox>(GetInput("priorCovNumber", "L_Insurance_num", undefined, undefined, true));


    const Year = () => reactive<TDropdown>(GetSelect("year", "number", "L_Year", undefined, undefined, true));
    const Make = () => reactive<TDropdown>(GetSelect("make", "IDBEntity", "L_Make", undefined, undefined, true));
    const Model = () => reactive<TDropdown>(GetSelect("model", "IDBEntity", "L_Model", undefined, undefined, true));
    const BodyType = () => reactive<TDropdown>(GetSelect("bodyType", "IDBEntity", "L_Body_Type", undefined, undefined, true));
    const Symbol = () => reactive<TDropdown>(GetSelect("symbol", "IDBEntity", "L_Vin10", undefined, undefined, true));
    const PrimaryUse = () => reactive<TDropdown>(GetFalconSelect("primaryUse", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.VehicleUse));
    const ComColl = () => reactive<TDropdown>(GetFalconSelect("comcoll", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.ComColDeductible, undefined, undefined, "L_COMP_COLL_desc"));
    const UMPD = () => reactive<TDropdown>(GetFalconSelect("umpd", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.UMPDLimit, undefined, undefined, "L_UMPD_desc"));
    const UMPDDeDuct = () => reactive<TDropdown>(GetFalconSelect("umpdDeduct", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.UMPDDeductible, undefined, undefined, "L_UMPD_desc"));
    const Rental = () => reactive<TDropdown>(GetFalconSelect("rental", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.Rental, undefined, undefined, "L_Rental_desc"));
    const Towing = () => reactive<TDropdown>(GetFalconSelect("towing", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.Towing, undefined, undefined, "L_Towing_desc"));
    const SafetyEquip = () => reactive<TDropdown>(GetFalconSelect("safetyEqup", "number", _StateSpec.Data.Dropdowns?.VehicleOptions?.SafetyEquipment, undefined, undefined, "******* Coverage description *******"));
    const Vin = () => reactive<TTextbox>(GetInput("fullVin", "L_Vin", undefined, undefined, true, undefined, 17, GetMaskPattern(/[0-9a-zA-Z]/, true)));

    const BI = () => reactive<TDropdown>(GetFalconSelect("bi", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.BILimit, undefined, "group-horizontal", "L_BI_desc"));
    const PD = () => reactive<TDropdown>(GetFalconSelect("pd", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.PDLimit, undefined, "group-horizontal", "L_PD_desc"));
    const UMBI = () => reactive<TDropdown>(GetFalconSelect("umbi", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.UMBILimit, undefined, "group-horizontal", "L_UMBI_desc"));
    const UIMBI = () => reactive<TDropdown>(GetFalconSelect("uimbi", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.UIMBILimit, undefined, "group-horizontal", "L_UMBI_desc"));
    const PIP = () => reactive<TDropdown>(GetFalconSelect("pip", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.PIPLimit, undefined, "group-horizontal", "L_PIP_desc"));
    const MP = () => reactive<TDropdown>(GetFalconSelect("mp", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.MPLimit, undefined, "group-horizontal", "L_MP_desc"));
    const WorkLoss = () => reactive<TDropdown>(GetFalconSelect("workLoss", "number", _StateSpec.Data.Dropdowns?.PolicyOptions?.WorkLossBenefitsWaiver, undefined, "group-horizontal", "******* Coverage description *******"));

    const Uwq = () => reactive<TTextbox>(GetInput("uwq", "", undefined, undefined, true, 'L_Explain'));

    const Password = () => reactive<TTextbox>(GetInput("password", "L_Password", undefined, "password", true));
    const ConfirmPassword = () => reactive<TTextbox>(GetInput("confirmPassword", "L_ConfirmPassword", undefined, "password", true));

    const CardHolderName = () => reactive<TTextbox>(GetInput("cardHolderName", "L_Card_holder_name", undefined, undefined, true));
    const CardNumber = () => reactive<TTextbox>(GetInput("cardNumber", "L_Card_num", undefined, 'number', true, undefined, 16, '#'.repeat(16)));
    const CardMonth = () => reactive<TDropdown>(GetSelect("cardmonth", "number", "L_Exp_month", GetNumbers(1, 12), undefined, true));
    const CardYear = () => reactive<TDropdown>(GetSelect("year", "number", "L_Exp_year", GetCardYears(), undefined, true));

    const AccHolderName = () => reactive<TTextbox>(GetInput("accountHolderName", "L_Acc_Holder_Name", undefined, undefined, true));
    const EftType = () => reactive<TDropdown>(GetSelect("eftType", "number", "L_Account_Type", [{ value: 1, label: 'L_Checking' }, { value: 2, label: 'L_Savings' }]));
    const AccNumber = () => reactive<TTextbox>(GetInput("accountNumber", "L_Acc_Num", undefined, 'number', true));
    const AccNumberConfirm = () => reactive<TTextbox>(GetInputWithHeaders("accountNumberConfirm", ["L_Re_enter", "L_Acc_Num"], 'number', true));
    const RoutingNumber = () => reactive<TTextbox>(GetInput("routingNumber", "L_Rounting_Num", undefined, 'number', true));
    const RoutingNumberConfirm = () => reactive<TTextbox>(GetInputWithHeaders("routingNumberConfirm", ["L_Re_enter", "L_Rounting_Num"], 'number', true));

    const AddDriver = computed(() => reactive<TButton>(GetButton("addOtherDriver", "L_Add_another_driver", "secondary")));
    const AddViolation = computed(() => reactive<TButton>(GetButton("addViolation", "L_Add_violation", "secondary")));
    const AddVehicle = computed(() => {
        const obj = reactive<TButton>(GetButton("addOtherVehicle", "L_Add_another_vehicle", "secondary"));
        obj.disabled = _Master.Instance.InsVehicles.Vehicles.length >= 6;
        return obj;
    });
    const Continue = computed(() => reactive<TButton>(GetButton("continue", "L_Continue")));
    const ContinueS = computed(() => reactive<TButton>(GetButton("continue", "L_Continue", "secondary")));
    const Back = computed(() => reactive<TButton>(GetButton("back", "L_Back")));
    const CancelS = computed(() => reactive<TButton>(GetButton("cancel", "L_Cancel", "secondary")));
    const LaunchEsign = computed(() => reactive<TButton>(GetButton("launchEsign", "L_Launch_Esign", "secondary")));
    const Print = computed(() => reactive<TButton>(GetButton("print", "L_Print_all", "secondary mar")));

    const DriverTable = computed(() => reactive<TTransistionTable>(GetTransTable("L_Additional_driver", true, "removeDriver", (driver: IDriver) => {
        if (driver.Name.First && driver.Name.Last) {
            return driver.Name.First + " " + driver.Name.Last;
        }
        return "";
    })));
    const ViolationTable = () => reactive<TTransistionTable>(GetTransTable("", true, "removeViolation"));
    const VehicleTable = computed(() => {
        const obj = reactive<TTransistionTable>(GetTransTable("L_Additional_vehicle", true, "removeVehicle", (vehicle: IVehicle) => {
            if (vehicle.Year && vehicle.Make.Description && vehicle.Model.Description) {
                return vehicle.Year + " " + vehicle.Make.Description + " " + vehicle.Model.Description;
            }
            return "";
        }));
        obj.header = _Ui.Data.CurrentSection === En_Sections.vVehicles ? "L_Vehicle" : "L_Additional_vehicle";
        return obj;
    });

    const GetTransTable = (header: string, showCount: boolean, btnId: string, displayNameAction?: (item: any) => string) => {
        return {
            header: header,
            showCount: showCount,
            hide: [],
            removeBtn: GetButton(btnId),
            displayName: displayNameAction
        } as TTransistionTable;
    }

    const EffectiveDate = computed(() => reactive<TTextbox>(GetInput("effectiveDate", "L_EffDate", undefined, 'date', true, "MM/DD/YYYY", 10, '##/##/####')));

    // const GetYears = () => {
    //     return [
    //         { label: "2024", value: 2024 } as OptionHTMLAttributes,
    //         { label: "2023", value: 2023 } as OptionHTMLAttributes,
    //         { label: "2022", value: 2022 } as OptionHTMLAttributes,
    //         { label: "2021", value: 2021 } as OptionHTMLAttributes,
    //         { label: "2020", value: 2020 } as OptionHTMLAttributes
    //     ];
    // }

    const GetMaskPattern = (v: RegExp, uppercase = false): any => {
        return {
            mask: "H*",
            tokens: { H: { pattern: v, uppercase: uppercase } },
        }
    }

    const GetInputMode = (val: string | undefined) => {
        switch (val) {
            case "date":
            case "number":
            case "phone":
                return "numeric";
            default:
                return "none";
        }
    }

    const GetInputType = (val: string | undefined) => {
        switch (val) {
            case "date":
            case "phone":
                return "text";
            default:
                return val;
        }
    }

    const GetInputValidationCls = (error: string | undefined) => {
        if (error === undefined) {
            return "";
        }
        if (error && error.trim()) {
            return "vue-validation-error";
        }
        return "vue-valid";
    }

    const GetButton = (id: string, value?: string, className?: string) => {
        const p = {} as TButton;
        p.id = "btn_" + id;
        p.value = value;
        p.class = className;
        return p;
    }


    const InputHandler = (event: Event, props: { modelValue: any, item: TTextbox }, emits: (event: "update:modelValue" | "CHANGE", ...args: any[]) => void) => {
        const value = (event.target as HTMLInputElement | HTMLSelectElement).value;
        if (props.item.disabled) {
            return;
        }
        // Check the type of modelValue and convert it if necessary
        if (props.item.required) {
            if (!value) {
                props.item.error = 'L_Required';
            }
            else {
                props.item.error = "";
            }
        }

        //remove navigation complete icon (check mark) when a field is updated.
        _Ui.RemoveNavComplete();

        const up = (value: any) => emits('update:modelValue', (value));

        up(value);
        // emits('update:modelValue', (value));
        emits('CHANGE', value, props.item, up);
    };

    const SelectHandler = (event: Event, props: { itemOptions: TDropdown, disableSelection: boolean }, emits: Function) => {
        const value = (event.target as HTMLInputElement | HTMLSelectElement).value;
        props.itemOptions.item.error = "";
        if (props.itemOptions) {
            if (props.itemOptions.item.disabled || props.disableSelection) {
                emits('DISABLE_SELECT_ACTION');
                return;
            }
            if (props.itemOptions.type === "IDBEntity") {
                emits('update:modelValue', (JSON.parse(value)));
            }
            else {
                emits('update:modelValue', (value));
            }
        }
        _Ui.RemoveNavComplete(true);
        if (_Ui.Data.CurrentSection === En_Sections.vCoveragePackages) {
            _Ui.Section.Continue.Btn.id = "updateRate";
            _Ui.Section.Continue.Btn.value = "L_Update_Rate";
            _Ui.Section.Continue.Btn.spanAfter = "";
        }
        emits('CHANGE');
    };

    const RadioHandler = (event: Event, emits: Function) => {
        const value = (event.target as HTMLInputElement).value;
        emits('update:modelValue', (value));
        emits('CHANGE');
    }

    const UpdateMakes = (vehicle: IVehicle, obj: any): void => {
        vehicle.Make = {} as IDBEntity;
        vehicle.Model = {} as IDBEntity;
        vehicle.BodyType = {} as IDBEntity;
        vehicle.Symbol = {} as IDBEntity;
        obj.Make.options = [];
        obj.Model.options = [];
        obj.BodyType.options = [];
        obj.Symbol.options = [];
        request.consumerService('VehicleInfo/GetMakes', { Year: vehicle.Year }, (data: IDBEntity[]) => {
            UpdateVehicleInfo(obj.Make, data);
        });
    }

    const UpdateModels = (vehicle: IVehicle, obj: any) => {
        vehicle.Model = {} as IDBEntity;
        vehicle.BodyType = {} as IDBEntity;
        vehicle.Symbol = {} as IDBEntity;
        obj.Model.options = [];
        obj.BodyType.options = [];
        obj.Symbol.options = [];
        request.consumerService('VehicleInfo/GetModels', { Year: vehicle.Year, Make: vehicle.Make }, (data: IDBEntity[]) => {
            const drop = (obj.Model as TDropdown);
            UpdateVehicleInfo(drop, data);
            if (data.length == 1) {
                vehicle.Model = data[0];
                drop.item.error = "";
                UpdateBodyTypes(vehicle, obj);
            }
        });
    }

    const UpdateBodyTypes = (vehicle: IVehicle, obj: any) => {
        vehicle.BodyType = {} as IDBEntity;
        vehicle.Symbol = {} as IDBEntity;
        obj.BodyType.options = [];
        obj.Symbol.options = [];
        request.consumerService('VehicleInfo/GetBodyTypes', { Year: vehicle.Year, Make: vehicle.Make, Model: vehicle.Model }, (data: IDBEntity[]) => {
            const drop = (obj.BodyType as TDropdown);
            UpdateVehicleInfo(drop, data);
            if (data.length == 1) {
                vehicle.BodyType = data[0];
                drop.item.error = "";
                UpdateSymbols(vehicle, obj);
            }
        });
    }

    const UpdateSymbols = (vehicle: IVehicle, obj: any) => {
        vehicle.Symbol = {} as IDBEntity;
        obj.Symbol.options = [];
        request.consumerService('VehicleInfo/GetSymbols', { Year: vehicle.Year, Make: vehicle.Make, Model: vehicle.Model, BodyType: vehicle.BodyType }, (data: IDBEntity[]) => {
            const drop = (obj.Symbol as TDropdown);
            UpdateVehicleInfo(drop, data);
            if (data.length == 1) {
                vehicle.Symbol = data[0];
                drop.item.error = "";
                _validate.Vin(vehicle, obj.Vin);
            }
        });
    }

    const UpdateAllDropdowns = (vehicle: IVehicle, obj: { Make: TDropdown, Model: TDropdown, BodyType: TDropdown, Symbol: TDropdown }) => {
        if (vehicle.Year) {
            request.consumerService('VehicleInfo/GetDropdowns', { Vehicles: [vehicle] }, (data: IVehicleDropdowns[]) => {
                UpdateVehicleInfo(obj.Make, data[0].Makes); vehicle.Make = data[0].Vehicle.Make;
                UpdateVehicleInfo(obj.Model, data[0].Models); vehicle.Model = data[0].Vehicle.Model;
                UpdateVehicleInfo(obj.BodyType, data[0].BodyTypes); vehicle.BodyType = data[0].Vehicle.BodyType;
                UpdateVehicleInfo(obj.Symbol, data[0].Symbols); vehicle.Symbol = data[0].Vehicle.Symbol;
            });
        }
    }

    const UpdateVehicleInfo = (obj: TDropdown, coll: IDBEntity[]) => {
        if (coll) {
            obj.options = coll.map((item: IDBEntity) => ({
                label: item.Description,
                value: JSON.stringify(item)
            } as OptionHTMLAttributes))
        }
    }

    const GetInfo = () => {
        return {
            FirstName: FirstName(),
            MiddleName: MiddleName(),
            LastName: LastName(),
            Gender: Gender(),
            DOB: DOB(),
            OwnHome: OwnHome(),
        }
    }

    const GetAddress = () => {
        return {
            Street: Street(),
            City: City(),
            State: State(),
            Zip: Zip(),
        }
    }

    const GetMainDriver = () => {
        return {
            ...GetDriver(),
            Phone: Phone(),
            PhoneType: PhoneType(),
            Email: Email(),
            PriorCoverage: PriorCoverage(),
            PriorCovCompany: PriorCovCompany(),
            PriorCovNumber: PriorCovNumber()
        }
    }

    const GetDriver = () => {
        return {
            FirstName: FirstName(),
            MiddleName: MiddleName(),
            LastName: LastName(),
            Gender: Gender(),
            DOB: DOB(),
            MaritalStatus: MaritalStatus(),
            Relationship: Relationship(),
            DriverType: DriverType(),
            LicType: LicType(),
            LicState: LicState(),
            LicNum: LicNum(),
            LicMon36: LicMon36(),
            MatureDr: MatureDr(),
            DefensiveDr: DefensiveDr(),
            AccPrev: AccPrev(),
            AccPrevDate: AccPrevDate(),
            DriverOccupation: DriverOccupation(),
            DriverEmployer: DriverEmployer()
        }
    }

    const GetVehicle = (): any => {
        const y = Year();
        const options = _StateSpec.Data.VehicleYears?.map(item => ({ label: `${item}`, value: item }));
        if (options) {
            y.options = options;
        }
        return {
            Year: y,
            Make: Make(),
            Model: Model(),
            BodyType: BodyType(),
            Symbol: Symbol(),
            PrimaryUse: PrimaryUse(),
            ComColl: ComColl(),
            UMPD: UMPD(),
            UMPDDeDuct: UMPDDeDuct(),
            Rental: Rental(),
            Towing: Towing(),
            SafetyEquip: SafetyEquip(),
            Vin: Vin()
        }
    }

    const GetPolicyCoverages = () => {
        return {
            BI: BI(),
            PD: PD(),
            UMBI: UMBI(),
            UIMBI: UIMBI(),
            PIP: PIP(),
            MP: MP(),
            WorkLoss: WorkLoss(),
        }
    }

    const GetViolation = () => {
        return {
            Violation: Violation(),
            Date: ViolationDate()
        }
    }

    const GetUWQs = () => {
        return _StateSpec.Data.UnderwritingQuestions.map(item => (Uwq()));
    }

    const GetCC = () => {
        return {
            CardHolderName: CardHolderName(),
            CardNumber: CardNumber(),
            CardMonth: CardMonth(),
            CardYear: CardYear(),
            Zip: Zip(),
        }
    }

    const GetEFT = () => {
        return {
            EftType: EftType(),
            AccHolderName: AccHolderName(),
            AccNumber: AccNumber(),
            AccNumberConfirm: AccNumberConfirm(),
            RoutingNumber: RoutingNumber(),
            RoutingNumberConfirm: RoutingNumberConfirm(),
        }

    }

    const GetAccount = () => {
        return {
            Email: Email(),
            Phone: Phone(),
            Password: Password(),
            ConfirmPassword: ConfirmPassword()
        }
    }

    const Vehicles = reactive<TVehicleControls[]>([GetVehicle()]);
    const Drivers = reactive<TDriverControls[]>([]);

    const AddVehicleToVehicles = () => {
        Vehicles.push(GetVehicle());
    }

    const RemoveVehicleFromVehicles = (index: number) => {
        Vehicles.splice(index, 1);
    }

    const AddDriverToDrivers = (udr?: boolean) => {
        const d = GetDriver();
        if (udr) {
            d.FirstName.disabled = true;
            d.MiddleName.disabled = true;
            d.LastName.disabled = true;
            d.DriverType.item.disabled = true;
            d.DOB.disabled = true;
        }
        Drivers.push(d);
    }

    const RemoveDriverFromDrivers = (index: number) => {
        Drivers.splice(index, 1);
    }

    const SetVehicleCoveragesCls = (cls?: 'group-horizontal') => {
        Vehicles.forEach((v) => {
            v.ComColl.item.class = cls;
            v.UMPD.item.class = cls;
            v.UMPDDeDuct.item.class = cls;
            v.Towing.item.class = cls;
            v.Rental.item.class = cls;
            v.SafetyEquip.item.class = cls;
        })
    }

    return {
        GetInfo,
        GetAddress,
        GetMainDriver,
        GetDriver,
        GetVehicle,
        AddDriver,
        AddViolation,
        HasViolations,
        GetViolation,
        ViolationTable,
        AddVehicle,
        EffectiveDate,
        InputHandler,
        GetInputMode,
        GetInputType,
        SelectHandler,
        UpdateMakes,
        UpdateModels,
        DriverTable,
        VehicleTable,
        Continue,
        ContinueS,
        Back,
        CancelS,
        LaunchEsign,
        Print,
        Vehicles,
        Drivers,
        UpdateBodyTypes,
        UpdateSymbols,
        GetButton,
        GetInputValidationCls,
        RadioHandler,
        UpdateAllDropdowns,
        AddVehicleToVehicles,
        AddDriverToDrivers,
        RemoveDriverFromDrivers,
        RemoveVehicleFromVehicles,
        GetPolicyCoverages,
        SetVehicleCoveragesCls,
        GetUWQs,
        Uwq,
        GetCC,
        GetEFT,
        GetAccount
    }
});
