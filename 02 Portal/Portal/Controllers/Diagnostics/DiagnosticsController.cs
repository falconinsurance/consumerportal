using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace ConsumerPortal.Controllers.Diagnostics;

[Route("Diagnostics")]
public class DiagnosticsController:Controller
{
  #region Private Fields
  private const string _HealthCheckFilePath = @"c:\consumer-portal-status.txt";
  #endregion Private Fields

  #region Public Methods

  [HttpGet, Route("HealthCheck")]
  public IActionResult Check() => Ok(ServiceHealthSummary.Up());

  [HttpGet, Route("HealthCheckFile")]
  public IActionResult CheckFile() => FileExists() ? Ok(ServiceHealthSummary.Up()) : StatusCode((int)HttpStatusCode.Gone,ServiceHealthSummary.Down());

  #endregion Public Methods

  #region Private Methods

  private static bool FileExists() => System.IO.File.Exists(_HealthCheckFilePath);

  #endregion Private Methods
}
