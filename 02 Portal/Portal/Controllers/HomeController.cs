using System.Diagnostics;
using ConsumerPortal.Models;
using ConsumerPortal.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ConsumerPortal.Controllers;

[Route("Home")]
public class HomeController:Controller
{
  #region Public Methods

  [ResponseCache(Duration = 0,Location = ResponseCacheLocation.None,NoStore = true)]
  [Route("Error")]
  [Route("~/Error")]
  public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });

  [HttpGet("GetOther")]
  public Task<string> GetOther() => Task.FromResult("Task Success!");

  [Route("{consumerId=}")]
  [Route("~/{consumerId=}")]
  //public IActionResult Index(string consumerId) => View(new HomeViewModel { ConsumerId = consumerId ?? "" });
  public IActionResult Index(string consumerId) => PartialView("home",new HomeViewModel { ConsumerId = consumerId ?? "" });

  public IActionResult Privacy() => View();

  #endregion Public Methods
}
